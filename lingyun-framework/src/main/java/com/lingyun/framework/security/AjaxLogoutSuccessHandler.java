package com.lingyun.framework.security;

import com.alibaba.fastjson.JSON;
import com.lingyun.common.utils.AjaxResult;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 注销成功
 *
 * @author 没事别学JAVA
 *
 */
@Component
public class AjaxLogoutSuccessHandler implements LogoutSuccessHandler {



    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletResponse.setHeader("Content-type", "application/json; charset=utf-8");
        httpServletResponse.getWriter().write(JSON.toJSONString(AjaxResult.success("注销成功")));
    }
}
