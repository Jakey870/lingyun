package com.lingyun.framework.quartz;


import com.lingyun.common.pojo.LyArticle;
import com.lingyun.system.service.LyArticleService;
import org.quartz.*;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author 没事别学JAVA
 * 2021/4/27 21:29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
public class HiJob extends QuartzJobBean {

    @Resource
    private LyArticleService lyArticleService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        JobDataMap data=jobExecutionContext.getTrigger().getJobDataMap();

        LyArticle lyArticle =new LyArticle();
        BeanUtils.copyProperties(data.get("lyArticle"), lyArticle);

        lyArticle.setCreateDate(new Date());  // 创建文章时间
        lyArticle.setUpdateDate(new Date());     // 文章最后更新时间
        lyArticle.setDelDelete("0");                       // 给文章设置删除表示
//        lyArticle.setUserId(24L);                       // 设置文章的用户id(也就事创建者)
        if(lyArticleService.save(lyArticle)){
            System.out.println("发布成功");
        }else{
            System.out.println("发布失败");
        }

    }
}
