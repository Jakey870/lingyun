package com.lingyun.framework.quartz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.JobAndTrigger;
import com.lingyun.common.pojo.JobFrom;
import org.quartz.SchedulerException;

/**
 * @author 没事别学JAVA
 * 2021/4/27 17:56
 */
public interface JobService{

    IPage<JobAndTrigger> quartzlist(Integer pagenum, Integer pagesize);

    void quartzadd (JobFrom job) throws SchedulerException, ClassNotFoundException, InstantiationException, IllegalAccessException;

    void deleteJob(JobFrom job) throws Exception;

}
