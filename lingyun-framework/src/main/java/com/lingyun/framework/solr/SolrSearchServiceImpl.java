package com.lingyun.framework.solr;


import com.lingyun.common.utils.Page;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;


/**
 * @author 没事别学JAVA
 */
@Service("ArticleSearchService")
public class SolrSearchServiceImpl implements SolrSearchService {

    @Resource
    private HttpSolrClient solrClient;

    @Override
    public Page searchArticlePage(Integer current, Integer size, String name) {

        SolrDocumentList solrlist = null;
        List<Object> solrVolist = new ArrayList<Object>();
        // 创建搜索对象
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");

        if (name != null) {
            //拼接Q查询条件
            StringBuffer query = new StringBuffer();
            //模糊查询
            query.append("articlename:").append("*" + name + "*");
            // 分页
            solrQuery.setStart((current - 1) * size);
            solrQuery.setRows(size);
            //启动高亮
            solrQuery.setHighlight(true);
            //设置域名称
            solrQuery.addHighlightField("articlename");
            solrQuery.setHighlightSimplePre("<font color='red'>");
            solrQuery.setHighlightSimplePost("</font>");
            //将查询条件设置到q查询中
            solrQuery.setQuery(query.toString());
            QueryResponse queryResponse = null;
            try {
                queryResponse = solrClient.query(solrQuery);
                solrlist = queryResponse.getResults();
                Map<String, Map<String, List<String>>> map = queryResponse.getHighlighting();

                for (SolrDocument entries : solrlist) {
                    entries.setField("articlename", map.get(entries.get("wid")).get("articlename").get(0));
                    solrVolist.add(entries);
                }
            } catch (SolrServerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // 分页
            solrQuery.setStart((current - 1) * size);
            solrQuery.setRows(size);
            //设置域名称
            solrQuery.addHighlightField("articlename");
            QueryResponse queryResponse = null;
            try {
                queryResponse = solrClient.query(solrQuery);
                solrlist = queryResponse.getResults();
                for (SolrDocument entries : solrlist) {
                    solrVolist.add(entries);
                }
            } catch (SolrServerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Page page = new Page(current, size, Math.toIntExact(solrlist.getNumFound()), solrVolist);
        return page;
    }


}
