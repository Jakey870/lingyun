package com.lingyun.framework.solr;

import com.lingyun.common.utils.Page;

import java.util.List;

/**
 * @author 24188
 */

public interface SolrSearchService {

    //文章分页查询
    Page searchArticlePage(Integer current, Integer size, String name);


}
