package com.lingyun.framework.elasticsearch;


import com.alibaba.fastjson.JSON;
import com.lingyun.common.exception.es.IndexException;
import com.lingyun.common.mapper.LyArticleMapper;
import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.utils.es.ElasticSearchUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 没事别学JAVA
 * 2021/6/2 18:03
 */
@Service("elasticSearchService")
public class ElasticSearchServiceImpl implements ElasticSearchService {

    @Resource
    RestHighLevelClient restHighLevelClient;

    @Resource
    ElasticSearchUtils elasticSearchUtils;

    @Resource
    LyArticleMapper lyArticleMapper;

    @Resource
    EsRepository esRepository;


    // 索引名称
    private static final String ARTICLE_INDEX = "article";


    /**
     * 轮播图
     *
     * @return 轮播图集合
     */
    @Override
    public List<Map<String, Object>> shufflingFigure() throws IOException {
        //指定索引库
        SearchRequest searchRequest = new SearchRequest(ARTICLE_INDEX);

        //构造查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //精准匹配
        QueryBuilder builder = QueryBuilders.matchQuery("isShuffling", 1);
        searchSourceBuilder.query(builder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);

        //发送请求
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            list.add(hit.getSourceAsMap());
        }
        return list;
    }

    /**
     *
     * 推广内容
     *
     * @return 推广集合
     */
    @Override
    public List<Map<String, Object>> queryPopularize() throws IOException {
        //指定索引库
        SearchRequest searchRequest = new SearchRequest(ARTICLE_INDEX);

        //构造查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //精准匹配
        QueryBuilder builder = QueryBuilders.matchQuery("isPopularize", 1);
        searchSourceBuilder.query(builder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);

        //发送请求
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            list.add(hit.getSourceAsMap());
        }
        return list;
    }

    @Override
    public Map<String,Object> queryArticle(Long articleId) throws IOException {
        GetRequest request = new GetRequest(ARTICLE_INDEX ,  Long.toString(articleId)) ;
        GetResponse response = restHighLevelClient.get(request , RequestOptions.DEFAULT);
        return response.getSource();
    }

    /**
     *
     * @param keyword
     * @param current  当前页
     * @param size 页大小
     * @return
     * @throws IOException
     */
    @Override
    public List<Map<String, Object>> search(String keyword, Integer current, Integer size) throws IOException, IndexException {
        //判断索引是否存在
        if(!elasticSearchUtils.isExistsIndex(ARTICLE_INDEX)){
            throw new IndexException();
        };

        //当前页不能小于1，如果小于1 ，就让他等于1
        if (current <= 1) {
            current = 1;
        }

        //指定索引库
        SearchRequest searchRequest = new SearchRequest(ARTICLE_INDEX);
        //构造查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();


        //分页 from = (当前页-1)*一页显示条数
        searchSourceBuilder.from((current - 1) * size); // 开始位置
        searchSourceBuilder.size(size);    // 页大小


        //精准匹配
        QueryBuilder builder = QueryBuilders.matchQuery("articleName", keyword);
        searchSourceBuilder.query(builder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));


        //高亮显示
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.requireFieldMatch(false)
                .field("articleName")
                .preTags("<a style=\"color:red\">").postTags("</a>");


        //执行搜索
        if(keyword!=null){
            searchSourceBuilder.highlighter(highlightBuilder);
            searchRequest.source(searchSourceBuilder);
        }else{
            searchRequest.source();
        }

        //发送请求
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {

            //获取高亮
            String articleName = hit.getHighlightFields().get("articleName").getFragments()[0].string();
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            sourceAsMap.put("articleName",articleName);

            list.add(sourceAsMap);
        }
        return list;
    }



    @Override
    public Integer reset(){

        //1.创建批量导入数据
        BulkRequest bulkRequest = new BulkRequest ();
        List<LyArticle> lyArticleList= lyArticleMapper.resetList();

        lyArticleList.forEach (lyArticle -> {
            bulkRequest.add(new IndexRequest(ARTICLE_INDEX)
                    .id(String.valueOf(lyArticle.getArticleId()))
                    .source(JSON.toJSONString(lyArticle), XContentType.JSON)
            );
        });

        //4.执行请求
        BulkResponse bulkResponse = null;
        try {
            bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //5.响应 判断是否执行成功
        RestStatus status = bulkResponse.status();
        return status.getStatus ();
    }







}
