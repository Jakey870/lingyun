package com.lingyun.framework.elasticsearch;

import com.lingyun.common.pojo.LyArticle;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;



@Component
public interface EsRepository extends ElasticsearchRepository<LyArticle,Long> {

}
