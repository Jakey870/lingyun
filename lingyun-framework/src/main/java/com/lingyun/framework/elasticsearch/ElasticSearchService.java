package com.lingyun.framework.elasticsearch;

import com.lingyun.common.exception.es.IndexException;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * @author 没事别学JAVA
 * 2021/6/1 17:25
 */
public interface ElasticSearchService<T> {

    /**
     * 轮播图
     *
     * @return 轮播图集合
     */
    public List<Map<String, Object>> shufflingFigure() throws IOException;

    /**
     * 推广内容
     *
     * @return 推广集合
     */
    public List<Map<String, Object>> queryPopularize() throws IOException;

    /**
     * 根据文章id 获取内容
     *
     * @return
     */
    public Map<String,Object> queryArticle(Long articleId) throws IOException;





    public List<Map<String, Object>> search(String keyword, Integer current, Integer size) throws IOException, IndexException;

    public Integer reset();

}
