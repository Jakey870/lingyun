/*
 Navicat Premium Data Transfer

 Source Server         : 8.0.16
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : ly

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 13/09/2022 01:06:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ly_article
-- ----------------------------
DROP TABLE IF EXISTS `ly_article`;
CREATE TABLE `ly_article`  (
  `article_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `article_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章名字',
  `article_keyword` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章关键字',
  `article_description` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章描述',
  `article_thumbnail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图(路径)',
  `img_color` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题色（缩略图主题色）',
  `article_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '文章内容',
  `del_delete` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否被删除  0 正常  1 删除（逻辑删除，再回收站里等待彻底删除）',
  `article_status` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态 0暂存 1正常 2隐藏 3违规',
  `is_shuffling` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否轮播 0 不轮播 1轮播',
  `is_popularize` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否推广 0不推广   1推广',
  `create_date` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1552672113284931588 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_article
-- ----------------------------
INSERT INTO `ly_article` VALUES (1536271301700804609, '“五一档”仅有五部新片上映 全国影院复工率回升至68%', '“五一档”仅有五部新片上映 全国影院复工率回升至68%', '“五一档”仅有五部新片上映 全国影院复工率回升至68%', '/ly/ly-images/article/b4.webp', 'rgba(117,86,71)', '<p style=\"text-indent: 2em;\">今年，电影市场“五一档”较为冷清。据灯塔专业版数据显示，截至5月3日16时许，“五一档”总票房（含预售）仅为2.45亿元。相比去年&nbsp;“五一档”总票房16.73亿元显得惨淡许多。不过值得欣慰的是在整个五一期间，全国影院复工率回升至68%，鼓舞了行业信心。</p><p style=\"text-indent: 36px; text-align: justify;\">据了解，原本计划在今年“五一档期”上映的新片有11部。但随着清明档期的惨淡收尾与疫情影响，包括《哥，你好》、《检察风云》、《您好，北京》、《保你平安》等8部影片相继宣布撤档。五一期间，仅有《出拳吧，妈妈》、《我是真的讨厌异地恋》等五部新片上映。</p><p style=\"text-indent: 36px; text-align: justify;\"><span style=\"color: rgb(38, 38, 38);\">作为一线城市票仓的北京和上海因为疫情原因均缺席今年的“五一档”。数据显示，2021年“五一档”，上海占全国票房市场的比例为5.37%，北京占比为4.74%。</span></p><p style=\"text-indent: 36px; text-align: center;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/4f3c7e56ee382a44ba6bad4fb42cc946.jpg\" alt=\"4f3c7e56ee382a44ba6bad4fb42cc946.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/4f3c7e56ee382a44ba6bad4fb42cc946.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: center;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/2da0ad091e40e498a33ba599a0e9284c.jpg\" alt=\"2da0ad091e40e498a33ba599a0e9284c.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/2da0ad091e40e498a33ba599a0e9284c.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/e68fe7c7b0f4c1c8dbe65238fed1467d.jpg\" alt=\"e68fe7c7b0f4c1c8dbe65238fed1467d.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/e68fe7c7b0f4c1c8dbe65238fed1467d.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/f193fb7978785ad300d23bd7ed7dbf25.jpg\" alt=\"f193fb7978785ad300d23bd7ed7dbf25.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/f193fb7978785ad300d23bd7ed7dbf25.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/f62c729445a16fdfe3b12687f60605ba.jpg\" alt=\"f62c729445a16fdfe3b12687f60605ba.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/f62c729445a16fdfe3b12687f60605ba.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/0bea0d484ed8b12f71f19ada18591b71.png\" alt=\"0bea0d484ed8b12f71f19ada18591b71.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/0bea0d484ed8b12f71f19ada18591b71.png\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/4bc005393258b88961a823c352884d70.jpg\" alt=\"4bc005393258b88961a823c352884d70.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/4bc005393258b88961a823c352884d70.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/a1717fa0309bcba1c2f88f108c7d00d9.jpg\" alt=\"a1717fa0309bcba1c2f88f108c7d00d9.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/a1717fa0309bcba1c2f88f108c7d00d9.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/894d6ff743d33f6c61fe1277d6c547fe.png\" alt=\"894d6ff743d33f6c61fe1277d6c547fe.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/894d6ff743d33f6c61fe1277d6c547fe.png\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/74885d12cbb9d1e556a8c5471b5bff00.jpg\" alt=\"74885d12cbb9d1e556a8c5471b5bff00.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/74885d12cbb9d1e556a8c5471b5bff00.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/8760a3099e12f7b0022553d4250974c4.png\" alt=\"8760a3099e12f7b0022553d4250974c4.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/8760a3099e12f7b0022553d4250974c4.png\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/4209114c0aa2ff8804bf66cdbd803b01.jpg\" alt=\"4209114c0aa2ff8804bf66cdbd803b01.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/4209114c0aa2ff8804bf66cdbd803b01.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/21c8fa4db1be002350340fdfd6d48113.jpg\" alt=\"21c8fa4db1be002350340fdfd6d48113.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/21c8fa4db1be002350340fdfd6d48113.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/c7edff14d455acde20ead6d93684e625.jpg\" alt=\"c7edff14d455acde20ead6d93684e625.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/c7edff14d455acde20ead6d93684e625.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/88ff8a2b47124bdaade0c908b5dbd35b.jpg\" alt=\"88ff8a2b47124bdaade0c908b5dbd35b.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/88ff8a2b47124bdaade0c908b5dbd35b.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/961e1e121fc5c94e8643cead51940b74.jpg\" alt=\"961e1e121fc5c94e8643cead51940b74.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/961e1e121fc5c94e8643cead51940b74.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/407b2eb54701053df3d69e6e9ab4ade3.png\" alt=\"407b2eb54701053df3d69e6e9ab4ade3.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/407b2eb54701053df3d69e6e9ab4ade3.png\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/26e877772babf9b3fc86fa145d1b6231.jpg\" alt=\"26e877772babf9b3fc86fa145d1b6231.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/26e877772babf9b3fc86fa145d1b6231.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/8914e6cc89d822440f8ad8d52fe39db4.jpg\" alt=\"8914e6cc89d822440f8ad8d52fe39db4.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/8914e6cc89d822440f8ad8d52fe39db4.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/c70de47c7cf402dfe274cd3e32d10c87.jpg\" alt=\"c70de47c7cf402dfe274cd3e32d10c87.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/c70de47c7cf402dfe274cd3e32d10c87.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/dbf637a1421ce2cae137afdf3c6a762c.jpg\" alt=\"dbf637a1421ce2cae137afdf3c6a762c.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/dbf637a1421ce2cae137afdf3c6a762c.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/87396c7d099ef913d2403cb1a2cb6414.jpg\" alt=\"87396c7d099ef913d2403cb1a2cb6414.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/87396c7d099ef913d2403cb1a2cb6414.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/3a0f61cd9c5db063837fe4a93c8a6df1.jpg\" alt=\"3a0f61cd9c5db063837fe4a93c8a6df1.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/3a0f61cd9c5db063837fe4a93c8a6df1.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/2758dbb007eeab81497b9ad1b135b456.jpg\" alt=\"2758dbb007eeab81497b9ad1b135b456.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/2758dbb007eeab81497b9ad1b135b456.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/99a7fa5168306c8e0a36dc2b10fa20c7.jpg\" alt=\"99a7fa5168306c8e0a36dc2b10fa20c7.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/99a7fa5168306c8e0a36dc2b10fa20c7.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/8d6021962cb7def73d737399590a8759.jpg\" alt=\"8d6021962cb7def73d737399590a8759.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/8d6021962cb7def73d737399590a8759.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/42813c8a71a400a84bce3110b98c5d34.jpg\" alt=\"42813c8a71a400a84bce3110b98c5d34.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/42813c8a71a400a84bce3110b98c5d34.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/5a97b310d2dc7281ed69d6bba076d97d.jpg\" alt=\"5a97b310d2dc7281ed69d6bba076d97d.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/5a97b310d2dc7281ed69d6bba076d97d.jpg\" style=\"\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/4df27e5c24038ae50d2e61f956ca929d.jpg\" alt=\"4df27e5c24038ae50d2e61f956ca929d.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/4df27e5c24038ae50d2e61f956ca929d.jpg\" style=\"\"></p>', '0', '1', '1', '0', '2022-07-09 20:15:51', '2022-06-13 16:56:36');
INSERT INTO `ly_article` VALUES (1536271379299622913, '微信上线出青少年模式支付限额 家长可设置消费限额', '微信上线出青少年模式支付限额 家长可设置消费限额', '微信上线出青少年模式支付限额 家长可设置消费限额', '/ly/ly-images/article/b5.webp', 'rgba(222,222,222)', '<p style=\"text-indent: 36px; text-align: justify;\">【环球网科技综合报道】6月28日消息，微信官方消息称，微信正式推出青少年模式支付限额功能。</p><p style=\"text-indent: 36px; text-align: justify;\">据悉，将手机更新到微信安卓版本8.0.23后，父母或监护人就可在青少年模式中设置微信支付的限额，包括“每日消费限额”和“单次消费限额”。完成设置后，青少年使用微信支付时，将会受到已设置的额度限制。</p><p style=\"text-indent: 36px; text-align: justify;\">具体方式为：进入微信后，通过“我-设置-青少年模式-微信支付”进行设置。</p><p style=\"text-indent: 36px; text-align: justify;\">微信方面称，开启支付限额功能完成设置后，青少年每日累计消费无法超过监护人所设置的每日消费限额，单次消费也无法超过监护人所设置的单次消费限额。家长可以根据青少年的消费情况，设置合理的消费限额，引导青少年健康消费。</p><p style=\"text-indent: 36px; text-align: justify;\">目前，iOS微信也在逐步覆盖中，更新到微信最新版本即可体验。</p>', '0', '1', '1', '0', '2022-07-09 20:15:49', '2022-06-13 16:56:54');
INSERT INTO `ly_article` VALUES (1536271527991894018, '奇安信汪列军：个人账号被攻击 首当其冲的漏洞是弱口令缺陷', '奇安信汪列军：个人账号被攻击 首当其冲的漏洞是弱口令缺陷', '奇安信汪列军：个人账号被攻击 首当其冲的漏洞是弱口令缺陷', '/ly/ly-images/article/b6.webp', 'rgba(124,90,64)', '<p style=\"text-indent: 36px; text-align: justify;\"><em>【环球网科技报道&nbsp;记者&nbsp;林迪】</em>针对大批用户反馈QQ号码被盗一事，腾讯QQ官方微博在6月27日午间发文回应并道歉，“系用户扫描过不法分子伪造的游戏登录二维码并授权登录，该登录行为被黑产团伙劫持并记录，随后被不法分子利用发送不良图片广告。”</p><p style=\"text-indent: 36px; text-align: justify;\">黑灰产在个人账号领域猖獗的原因何在？企业和用户又该如何做好防护？对此，记者采访了奇安信集团威胁情报中心负责人汪列军。</p><p style=\"text-indent: 36px; text-align: justify;\">在汪列军看来，上述“QQ号码被盗一事”涉及的攻击方式与若干年前流行的盗号木马如出一辙，只要恶意工具可以大范围传播，便可以批量盗窃大量用户的账号。</p><p style=\"text-indent: 36px; text-align: justify;\">他分析称，有关个人账号的盗窃、贩卖、滥用已经形成了完成的黑产经济链条，彻底根治的难度极大。同时，随着黑灰产团伙的迅速发展，黑客工具变得越来越廉价和易用，即便小白用户也可通过购买完整的黑客工具和服务，发起高质量的网络攻击，让人防不胜防。</p><p style=\"text-indent: 36px; text-align: justify;\">“在用户层面，由于个人安全意识的缺失，导致黑灰产团伙拥有大量可乘之机。”&nbsp;汪列军举例称，“例如，密码设置过于简单、对于潜在的威胁（如虚假二维码、钓鱼网站、钓鱼邮件等）认知不足，导致个人账户极易被窃取。同时为便于记忆，用户经常在多个平台设置同一套密码，一旦一个平台账户被窃，很容易导致多个账户出事。尤其是涉及电商、游戏等平台账户，由于旗下往往拥有大量虚拟财产或者绑定支付账户，容易成为黑灰产窃取的主要对象。”</p><p style=\"text-indent: 36px; text-align: justify;\">他进一步指出，尽管随着《网络安全法》、《数据安全法》等法律法规的出台，网络安全保护力度大大增强，但由于历史原因，很多平台依然存在着或多或少的安全盲区或者缺陷，容易遭到黑灰产团伙的利用，导致用户账户失窃。</p><p style=\"text-indent: 36px; text-align: justify;\">具体来看，企业方面也多年在个人账号方面做出网络安全方面的防护，但仍被黑灰产攻击到，可能存在的网络安全漏洞会包括哪些？</p><p style=\"text-indent: 36px; text-align: justify;\">汪列军表示，首当其冲的当属弱口令缺陷。“如某些办公系统（如OA）和数据库的管理员账户或者员工域账号使用弱口令遭到黑客利用，导致数据库被拖库的事件已屡见不鲜。在奇安信参与的多次实战攻防演习以及参与处置的网络安全事件中，针对弱口令的暴力破解已经被证实为攻击者成功入侵的最主要方法之一，这就相当于在关好的防盗门和防盗窗旁边，留下了一串钥匙，攻击者要做的无非就是试试在这串钥匙中，哪一把才能把门打开。”</p><p style=\"text-indent: 36px; text-align: justify;\">其次，缺乏相应的安全防护手段。网络安全建设是一个体系化工程，存在木桶效应，任何一块短板都可能导致整个系统的失陷。例如重视网络边界防御却轻视了内部安全威胁的监测，这就导致一旦攻击者突破网络边界，就可以在企业内网如入无人之境。</p><p style=\"text-indent: 36px; text-align: justify;\">第三是员工安全意识参差不齐，容易遭到攻击者的利用，如点击来历不明的邮件、网页链接等，导致企业遭遇钓鱼攻击。在此次qq账户失窃的事件中，如果用户擦亮眼睛，有可能就可以发现一些蛛丝马迹的。</p><p style=\"text-indent: 36px; text-align: justify;\">最后，他建议道：“因此站在企业层面，应当用体系化、工程化的思想，实现网络安全与信息化的深度融合与全面覆盖，部署相应的安全设备，同时制定账户安全规则，定期修改登录密码。同时，企业应当定期开展网络安全教育与实战攻防演习，提升员工整体安全意识基线。一旦发现弱口令、漏洞等安全隐患应及时解决，杜绝其成为历史遗留。而作为个人用户应当擦亮眼睛，不要轻易在来路不明的渠道输入账号、密码等敏感个人信息，如有必要应当在个人电脑或者手机上安装安全软件。”</p>', '0', '1', '1', '1', '2022-07-09 20:16:01', '2022-06-13 16:57:29');
INSERT INTO `ly_article` VALUES (1541591168259756034, '还记得《琅琊榜》的穆青吗？《梦华录》他简直“胖”若两人', '还记得《琅琊榜》的穆青吗？《梦华录》他简直“胖”若两人', '还记得《琅琊榜》的穆青吗？《梦华录》他简直“胖”若两人', '/ly/ly-images\\2022/06/28/e7ddbb4d79f8a8fd5d24baad4999745f.jpeg', 'rgba(225,206,190)', '<p>要说如今风头正劲的电视剧，当属刘亦菲和陈晓主演的《梦华录》。一开拍便备受期待，开播也未让观众失望，热度与口碑双丰收，可谓是“高开高走”。</p><p>除了主演之外剧中的配角也是《梦华录》的一大看点，例如出镜不多却成功引起了观众注意的杜长风。</p><p>张晓谦扮演的角色杜长风是欧阳旭的同窗，本是进士之身却因御前失仪而去做了书院夫子。听了同窗欧阳旭的挑唆之后，想要为他“伸张争议”，没想到被孙三娘暴打，性格单纯又自带喜感。在张晓谦的理解中，杜长风虽然有些古板，但是他的本性并不坏，就像是剧中有些可爱的“傻白甜”。</p><p>他人设新颖之处便在于他是全剧唯一的“近视眼”，若没有眼镜在手，便形同瞎子。杜长风也不是什么绝世大美男，有点胖胖的反而让这个角色生动了起来。</p><p>杜长风的扮演者张晓谦其实是童星出身，9岁就参演首部电视剧《快乐七八岁》进入演艺圈。之后他考入中戏，虽鲜少挑大梁担任男主，却参演过多部爆剧。</p><p>说出张晓谦这个名字，不少人必然会觉得陌生。但是，提起《琅琊榜》的穆青或是《知否知否应是绿肥红瘦》中的盛长枫，那么脑海中或许便会浮现熟悉的脸庞。</p><p>可是，张晓谦在《梦华录》中的状态简直与《琅琊榜》以及《知否知否应是绿肥红瘦》判若两人。</p><p>只因在《琅琊榜》中的穆青是这样的。少年气满满的同时，更会带有一丝顽皮。</p><p>而在《知否知否应是绿肥红瘦》中的盛长枫是这样的。纨绔气质尽显，更是演活了盛长枫这个角色。</p><p>为此，不管是穆青还是盛长枫都给观众留下了深刻的印象。演技不俗的同时，更是能够对角色进行有效的拿捏。</p><p>可是，到了《梦华录》中却恍如换了一个人。</p><p>面部看起来尤为浮肿不说，扮相也不如《琅琊榜》《知否知否应是绿肥红瘦》那样清爽。与穆青以及盛长枫时期相比，肉眼可见的胖了。</p><p>原来张晓谦拍摄上一部剧时受了伤，在拍梦华录的时候是带伤。</p><p>白天拍《梦华录》，晚上去做治疗。也是因为治疗的原因，所以身体浮肿发胖。</p><p>《梦华录》拍完后才做得手术，现在还在做康复治疗。</p>', '0', '2', '1', '1', '2022-07-09 20:15:59', '2022-06-28 09:15:51');
INSERT INTO `ly_article` VALUES (1541974057610674177, '新研究称木星可能“吞噬”了其他较小的行星', '新研究称木星可能“吞噬”了其他较小的行星', '新研究称木星可能“吞噬”了其他较小的行星', '/ly/ly-images\\2022/06/29/d9ab8ee145caa7470e08d3cb00ab9322.jpg', 'rgba(28,27,24)', '<p><br></p><p style=\"text-indent: 36px; text-align: justify;\">据BGR报道，多年来，木星对天文学家来说一直是个相当神秘的天体。尽管是我们太阳系内最大的行星，但天文学家对这颗气态巨行星的内部运作知之甚少。不过现在，美国宇航局6月的太空探测器可能对这颗行星的形成过程有了一些新的了解。<em>研究人员现在认为，木星可能已经“吞噬”了其他较小的行星，以促进其自身的成长。</em></p><p style=\"text-indent: 36px; text-align: justify;\"><br></p><p style=\"text-indent: 36px; text-align: justify;\">一颗行星吞噬另一颗行星的想法可能听起来像是科幻小说中的情节。但是，科学家们相信，距离太阳第五近的行星木星在其形成过程中吞噬了其他行星。</p><p style=\"text-indent: 36px; text-align: justify;\">几十年来，科学家们一直无法“看透”覆盖在该行星表面的气体云。现在美国宇航局的朱诺号探测器让人们得以一窥。研究人员利用引力数据来窥视木星的云层。这颗气态巨行星核心的岩石物质的构成表明，木星吞噬了小行星来促进其成长。</p><p style=\"text-indent: 36px; text-align: justify;\">这是一个大胆的说法，但这是一个已经存在了一段时间的说法。不过，有了这些新数据，科学家们终于有了一点证据，证明木星吞噬其他行星可能不仅仅是科幻小说。这特别有意义，因为这颗气态巨行星需要吸收大量的质量，才能变得像现在这样巨大。</p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/028eb07a40281132fb20ceeada1b176d.jpg\" alt=\"028eb07a40281132fb20ceeada1b176d.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/028eb07a40281132fb20ceeada1b176d.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\">关于木星是如何形成的，有两种主要理论。第一种说法是，木星积累了数十亿颗较小的太空岩石。这些岩石最终形成了构成气态巨行星的凝聚核心。另一个理论，也是这个新证据所支持的理论，就是木星可能吞噬了其他行星，才长成了现在这样的规模。</p><p style=\"text-indent: 36px; text-align: justify;\">这些行星会被这颗气态巨行星吸收，最终形成这个星球的致密核心。然后，科学家们认为，木星开始吸收和收集来自我们太阳诞生时的气体。这也是帮助形成我们现在看到的覆盖在该行星表面的云层的原因。</p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/08b525bda26f13d1eff280b6ab91d9a7.jpg\" alt=\"08b525bda26f13d1eff280b6ab91d9a7.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/08b525bda26f13d1eff280b6ab91d9a7.jpg\" style=\"\"></p><p><br></p><p><br></p>', '0', '1', '1', '1', '2022-07-09 20:14:57', '2022-06-29 10:37:18');
INSERT INTO `ly_article` VALUES (1542512386559197186, '吉利第4代帝豪醇电混动轿车助力“火热十一运吉利贵州行”正式启动', '吉利第4代帝豪醇电混动轿车助力“火热十一运吉利贵州行”正式启动', '吉利第4代帝豪醇电混动轿车助力“火热十一运吉利贵州行”正式启动', '/ly/ly-images\\2022/06/30/9e7416b86b3e12310da00fc0a9069ab1.jpg', 'rgba(97,138,176)', '<p style=\"text-indent: 36px; text-align: justify;\">【环球网汽车报道】能源和环境是当今世界各国经济发展所面临的两大突出问题，甲醇作为一种清洁能源，其燃料特性优秀、适用范围广、来源广泛、污染少、生产制造技术成熟，在我国发展甲醇替代燃料、推广甲醇汽车具有十分重要的战略意义，不仅推动我国能源多元化发展、保障构建能源安全，还是实现交通领域“双碳”目标的重要路径。</p><p style=\"text-indent: 36px; text-align: justify;\"><br></p><p style=\"text-indent: 36px; text-align: justify;\">2012年-2018年，工信部先后在山西、上海、陕西、甘肃、贵州5省市的晋中、长治、上海、西安、宝鸡、榆林、汉中、兰州、平凉、贵阳共10座城市，正式组织开展甲醇汽车试点工作，吉利甲醇汽车的安全性、节能性、环保性得到了充分验证。2020年，甲醇汽车全面市场化提上日程，甲醇汽车产业发展驶入快车道。</p><p style=\"text-indent: 36px; text-align: justify;\"><br></p><p style=\"text-indent: 36px; text-align: justify;\">贵州省立足自身资源禀赋，发展甲醇燃料和甲醇汽车，目前已建立完善的甲醇汽车生产、销售、服务体系和甲醇燃料输配送供应保障体系，出台了包括出租车在内的一系列鼓励措施，逐步形成了保障甲醇汽车运行的制度体系和管理机制，并通过市场化方式推广甲醇汽车超17000辆，总运行里程近100亿公里，全省投入运营甲醇燃料加注站超过60座，最大单车运行里程超过120万公里，年消耗甲醇约25万吨，相当于替代汽油15万吨。其中，贵阳成为全球甲醇燃料和甲醇汽车市场化推广最成功、规模最大、覆盖区域广的城市，并在全球范围内产生一定影响力。</p><p style=\"text-indent: 36px; text-align: justify;\"><br></p><p style=\"text-indent: 36px; text-align: justify;\">甲醇产业的发展，推动了贵州省产业结构低碳绿色转型升级，也为全球能源革命蓄势赋能。吉利是探索甲醇汽车产业的先行者，甲醇汽车技术引领全球，深耕甲醇汽车市场17年，成功开发多款甲醇动力及甲醇汽车，获得200余项技术专利。在贵州省甲醇产业发展历程中，吉利也发挥了举足轻重的作用，不仅助力贵州省建立起完善的甲醇汽车生产、销售、服务体系和甲醇燃料输配送供应保障体系，也在贵州投入超17000辆吉利甲醇汽车，总运行里程近100亿公里，最大单车运行里程超过120万公里。</p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/1b49692040abffa7582d4033b6ea0518.png\" alt=\"1b49692040abffa7582d4033b6ea0518.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/1b49692040abffa7582d4033b6ea0518.png\" style=\"width: 131.00px;height: 131.00px;\"></p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/320f7e94c565e6e52a1cd6d77fcff436.png\" alt=\"320f7e94c565e6e52a1cd6d77fcff436.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/320f7e94c565e6e52a1cd6d77fcff436.png\" style=\"width: 135.00px;height: 135.00px;\"></p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/9aa416b7bd82f8028ba8aff422bc4dbf.png\" alt=\"9aa416b7bd82f8028ba8aff422bc4dbf.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/9aa416b7bd82f8028ba8aff422bc4dbf.png\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\">在今年的第十一届贵州省运会中，全球首款甲醇混合动力轿车——吉利第4代帝豪醇电混动轿车成为赛事官方指定用车，全力助力省运会的成功举办。由第4代帝豪醇电混动轿车组成的省运会文化巡游车队也将从贵阳出发，途径安顺、兴义、黔南、黔东南、铜仁、遵义、毕节、六盘水共九市州，为第十一届省运会宣传助威，将绿色低碳的生活态度和拼搏向上的赛事精神传递给更多人，在传递竞技精神、冠军精神的同时，全面展示吉利先进的造车科技和领先的甲醇技术。</p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/6c1354e0119680b9e63aec9c2c2e7aff.png\" alt=\"6c1354e0119680b9e63aec9c2c2e7aff.png\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/6c1354e0119680b9e63aec9c2c2e7aff.png\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\"><img src=\"/zkc-dev/ly/ly-images\\2022/07/08/7199410ad9f79aff7fa12f8c99e815b7.jpg\" alt=\"7199410ad9f79aff7fa12f8c99e815b7.jpg\" data-href=\"/zkc-dev/ly/ly-images\\2022/07/08/7199410ad9f79aff7fa12f8c99e815b7.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\">第4代帝豪醇电混动轿车凭借40%的节能率、百公里醇耗9.2L的经济性（相当于汽油车3.4L的百公里油耗），每公里不到3毛钱的出行成本，比同级汽油车减少42%的碳排放，成为同级最节能、最低碳、最经济的混动轿车。</p><p style=\"text-indent: 36px; text-align: justify;\">第4代帝豪醇电混动轿车也作为一个样板呼吁更多的人加入到低碳、健康的生活方式中来。</p>', '0', '1', '1', '1', '2022-07-09 20:15:00', '2022-06-30 22:16:26');
INSERT INTO `ly_article` VALUES (1545336911646715905, '高铁跑出旅游加速度1', '高铁跑出旅游加速度', '高铁跑出旅游加速度', '/ly/ly-images\\2022/07/08/5107510c6f611534a5433bfa3d549e11.jpg', 'rgba(62,62,60)', '<p style=\"text-indent: 2em; text-align: justify;\">早上在郑州喝胡辣汤，途中有襄阳牛肉面，中午就可抵达重庆涮火锅，这样的出游体验已成为现实。近日，全国铁路实施新运行图，郑渝高铁等新线路开通，城市间旅行时间再缩短，“乘着高铁去旅游”在今夏再迎热潮。</p><p style=\"text-indent: 2em; text-align: justify;\"><em>快旅慢游微度假</em></p><p style=\"text-indent: 2em; text-align: justify;\">铁路的利好消息提升了旅游热度。途牛旅游网近15天的预订数据显示，以高铁为主要交通方式的出游人次环比涨幅达132%。马蜂窝旅游关于“高铁游”的搜索热度上涨310%。同程研究院首席研究员程超功认为，高铁的进一步提速，拉近了城市之间的距离，使高铁旅游迎来了新的发展机遇，在周边短途游的带动下，今年暑期高铁游将成为出行的热门选择。</p><p style=\"text-indent: 2em; text-align: justify;\">郑渝高铁贯通郑州和重庆，是联结华北、华中和西南等地的交通大动脉，穿越万水千山，以“千里江陵一日还”的速度，令出游变得更快捷。郑渝高铁牵手黄河与长江，一路串联起嵩山少林寺、南阳武侯祠、襄阳古隆中、神农架原始森林、奉节白帝城、巫山小三峡、磁器口古镇等众多旅游景点。乘坐郑渝高铁，从郑州到重庆只需4小时左右，不到原来时间的一半，“坐着高铁去三峡”从梦想照进了现实。</p><p style=\"text-indent: 2em; text-align: justify;\">同程旅行数据显示，6月以来，高铁游订单量比5月同期增长超80%。超过半数的用户认为，高铁出行已成为旅行的首选，高铁游将继续保持更快的增长之势。携程旅行网在6月推出“火车奇妙漫游地图”，通过图文、视频、内容专题等形式，围绕“火车游”玩法，向游客推荐全国风景优美、特色小众、新开高铁线、最美火车站、热门火车主题线路游等，提升游客的旅游兴趣和关注度，近期还将推出暑期高铁游系列活动，为游客推荐特别适合暑期游玩，观看大海、极光、草原、星辰，探寻自然奇观的火车专线。</p><p style=\"text-indent: 2em; text-align: justify;\"><em>高铁游丰富多彩</em></p><p style=\"text-indent: 2em; text-align: justify;\">如今，利用周末或小长假来一场说走就走的微度假，已成为许多人出游的主流选择。高铁极大地助力了快旅漫游。高铁游以其便捷、舒适的特点受到游客喜爱，人们乘坐高铁，用短暂的旅途时间，围绕一个目的地进行深度探索，并展开亲子、音乐、美食等主题的旅行。</p><p style=\"text-indent: 2em; text-align: justify;\">7月1日，湖北省今年开行的首趟旅游专列——“武铁旅游·神农架号”从汉口火车站出发，驶往“华中屋脊”湖北神农架，开启为期3天的清凉夏日之旅。鄂西地区旅游资源丰富，郑渝高铁填补了鄂西多地的铁路空白，将沿线诸多旅游资源连点成串。襄阳、宜昌、十堰、恩施、神农架等地纷纷推出持高铁票景区免门票或门票打折等优惠政策。据统计，郑渝高铁开通仅一周，就有近6万游客乘坐高铁前往鄂西地区观光旅游。同一天，在甘肃省环县车站，几百位老人乘坐银西高铁参加“坐高铁、游故城、访圣地、看变化”主题活动，游览环州故城、宋代砖塔、陇东民居、秦代长城等景区景点。</p><p style=\"text-indent: 2em; text-align: justify;\">日前，世界首个沙漠铁路环线新疆和田至若羌铁路（简称和若铁路）开通运营。和若铁路是中国西部地区重要的区域路网干线，是新疆“四纵四横”铁路主骨架的重要组成部分，也是南疆通往内地的便捷通道。如今，洛浦县、策勒县、于田县、民丰县和且末县结束了不通火车的历史，游客可以坐着火车穿越塔克拉玛干沙漠，游览尼雅遗址、安迪尔古城、达玛沟小佛寺等文化遗址，去往更多以前遥不可及的地方，感受无与伦比的奇观壮景。近日，途牛旅游网平台上的“新疆喀什帕米尔全景和田10日游”“新疆喀什和田深度11日游”等火车游线路的预订量也在持续猛增。</p><p style=\"text-indent: 2em; text-align: justify;\"><em>利好沿线旅游发展</em></p><p style=\"text-indent: 2em; text-align: justify;\">马蜂窝旅游研究院院长冯饶认为：“近两年中国高铁网络不断完善，城市间的距离不断缩短，游客旅行的半径也随之延长，这不仅重新定义了城市周边游的概念，而且对沿线旅游的带动作用也不可估量。”马蜂窝旅行玩乐大数据显示，郑渝高铁开通后，“神农架”搜索热度上升133%，“长江三峡”的热度更是增长357%。</p><p style=\"text-indent: 2em; text-align: justify;\">乘着高铁的“东风”，沿线旅游目的地纷纷推出精彩纷呈的旅游惠民活动，邀请游客乘高铁饱览壮美山河。湖北、重庆等地推出凭高铁票享受多个知名景点门票半价、免费等活动，为当地暑期旅游市场复苏再添一把火。湖北省襄阳市抓住郑渝高铁贯通机遇，推出夜经济系列主题旅游线路，以襄阳古城北街、唐城景区、鱼梁洲等夜间经济集聚区为重点，吸引游客到此感受古城魅力。</p><p style=\"text-indent: 2em; text-align: justify;\"><br></p><p style=\"text-indent: 2em; text-align: justify;\">随着高铁网络的扩大，旅行变得越来越便捷。激活跨省游、周边游市场，成为振兴旅游业的“刚需”。近日，江苏、上海、浙江、安徽四地的文化和旅游管理部门联合起来，依托高铁网络和站点，公布了“首批长三角高铁旅游小城”名单，上海市徐汇区、南京市江宁区、杭州市淳安县和黄山市徽州区等31个区县入选。</p>', '0', '0', '1', '1', '2022-07-09 20:15:01', '2022-07-08 17:20:05');
INSERT INTO `ly_article` VALUES (1545703699827941377, 'emoji表情符号大全', '表情与感情、人类和身体、动物和自然、食物和饮料、旅行和地点、活动、物品、符号、旗帜', 'emoji表情符号大全', '/ly/ly-images\\2022/07/09/101bdd17908e5f37af53db5f3126e9f6.png', 'rgba(27,37,32)', '<p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>表情与感情</strong></span></p><p style=\"line-height: 1;\">😀&nbsp;😃&nbsp;😄&nbsp;😁&nbsp;😆&nbsp;😅&nbsp;🤣&nbsp;😂&nbsp;🙂&nbsp;🙃&nbsp;😉&nbsp;😊&nbsp;😇&nbsp;🥰&nbsp;😍&nbsp;🤩&nbsp;😘&nbsp;😗&nbsp;☺&nbsp;😚&nbsp;😙&nbsp;🥲&nbsp;😋&nbsp;😛&nbsp;😜&nbsp;🤪&nbsp;😝&nbsp;🤑&nbsp;🤗&nbsp;🤭&nbsp;🤫&nbsp;🤔&nbsp;🤐&nbsp;🤨&nbsp;😐&nbsp;😑&nbsp;😶&nbsp;😏&nbsp;😒&nbsp;🙄&nbsp;😬&nbsp;🤥&nbsp;😌&nbsp;😔&nbsp;😪&nbsp;🤤&nbsp;😴&nbsp;😷&nbsp;🤒&nbsp;🤕&nbsp;🤢&nbsp;🤮&nbsp;🤧&nbsp;🥵&nbsp;🥶&nbsp;🥴&nbsp;😵&nbsp;🤯&nbsp;🤠&nbsp;🥳&nbsp;🥸&nbsp;😎&nbsp;🤓&nbsp;🧐&nbsp;😕&nbsp;😟&nbsp;🙁&nbsp;☹&nbsp;😮&nbsp;😯&nbsp;😲&nbsp;😳&nbsp;🥺&nbsp;😦&nbsp;😧&nbsp;😨&nbsp;😰&nbsp;😥&nbsp;😢&nbsp;😭&nbsp;😱&nbsp;😖&nbsp;😣&nbsp;😞&nbsp;😓&nbsp;😩&nbsp;😫&nbsp;🥱&nbsp;😤&nbsp;😡&nbsp;😠&nbsp;🤬&nbsp;😈&nbsp;👿&nbsp;💀&nbsp;☠&nbsp;*&nbsp;🤡&nbsp;👹&nbsp;👺&nbsp;👻&nbsp;👽&nbsp;👾&nbsp;🤖&nbsp;😺&nbsp;😸&nbsp;😹&nbsp;😻&nbsp;😼&nbsp;😽&nbsp;🙀&nbsp;😿&nbsp;😾&nbsp;🙈&nbsp;🙉&nbsp;🙊&nbsp;💋&nbsp;💌&nbsp;💘&nbsp;💝&nbsp;💖&nbsp;💗&nbsp;💓&nbsp;💞&nbsp;💕&nbsp;💟&nbsp;❣&nbsp;💔&nbsp;❤&nbsp;🧡&nbsp;💛&nbsp;💚&nbsp;💙&nbsp;💜&nbsp;🤎&nbsp;🖤&nbsp;🤍&nbsp;💯&nbsp;💢&nbsp;💥&nbsp;💫&nbsp;💦&nbsp;💨&nbsp;🕳&nbsp;💣&nbsp;💬&nbsp;👁‍🗨&nbsp;🗨&nbsp;🗯&nbsp;💭&nbsp;💤</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>人类和身体</strong></span></p><p style=\"line-height: 1;\">*&nbsp;🤚&nbsp;🖐&nbsp;✋&nbsp;🖖&nbsp;👌&nbsp;🤌&nbsp;🤏&nbsp;✌&nbsp;🤞&nbsp;🤟&nbsp;🤘&nbsp;🤙&nbsp;👈&nbsp;👉&nbsp;👆&nbsp;🖕&nbsp;👇&nbsp;☝&nbsp;👍&nbsp;👎&nbsp;✊&nbsp;👊&nbsp;🤛&nbsp;🤜&nbsp;👏&nbsp;🙌&nbsp;👐&nbsp;🤲&nbsp;🤝&nbsp;🙏&nbsp;✍&nbsp;💅&nbsp;🤳&nbsp;💪&nbsp;🦾&nbsp;🦿&nbsp;🦵&nbsp;🦶&nbsp;👂&nbsp;🦻&nbsp;*&nbsp;🧠&nbsp;🫀&nbsp;🫁&nbsp;🦷&nbsp;🦴&nbsp;👀&nbsp;👁&nbsp;👅&nbsp;👄&nbsp;👶&nbsp;🧒&nbsp;👦&nbsp;👧&nbsp;🧑&nbsp;👱&nbsp;👨&nbsp;🧔&nbsp;👩&nbsp;🧓&nbsp;👴&nbsp;👵&nbsp;🙍&nbsp;🙎&nbsp;🙅&nbsp;🙆&nbsp;💁&nbsp;🙋&nbsp;🧏&nbsp;🙇&nbsp;🤦&nbsp;🤷&nbsp;👮&nbsp;🕵&nbsp;💂&nbsp;🥷&nbsp;👷&nbsp;🤴&nbsp;👸&nbsp;👳&nbsp;👲&nbsp;🧕&nbsp;🤵&nbsp;👰&nbsp;🤰&nbsp;🤱&nbsp;👼&nbsp;🎅&nbsp;🤶&nbsp;🦸&nbsp;🦹&nbsp;🧙&nbsp;🧚&nbsp;🧛&nbsp;🧜&nbsp;🧝&nbsp;🧞&nbsp;🧟&nbsp;💆&nbsp;💇&nbsp;🚶&nbsp;🧍&nbsp;🧎&nbsp;🏃&nbsp;💃&nbsp;🕺&nbsp;🕴&nbsp;👯&nbsp;🧖&nbsp;🧗&nbsp;🤺&nbsp;🏇&nbsp;⛷&nbsp;🏂&nbsp;🏌&nbsp;🏄&nbsp;🚣&nbsp;🏊&nbsp;⛹&nbsp;🏋&nbsp;🚴&nbsp;🚵&nbsp;🤸&nbsp;🤼&nbsp;🤽&nbsp;🤾&nbsp;🤹&nbsp;🧘&nbsp;🛀&nbsp;🛌&nbsp;👭&nbsp;👫&nbsp;👬&nbsp;💏&nbsp;💑&nbsp;👪&nbsp;🗣&nbsp;👤&nbsp;👥&nbsp;🫂&nbsp;👣</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>动物和自然</strong></span></p><p style=\"line-height: 1;\">🐵&nbsp;🐒&nbsp;🦍&nbsp;🦧&nbsp;*&nbsp;🐕&nbsp;🦮&nbsp;🐕‍🦺&nbsp;🐩&nbsp;🐺&nbsp;🦊&nbsp;🦝&nbsp;🐱&nbsp;🐈&nbsp;🐈‍⬛&nbsp;🦁&nbsp;🐯&nbsp;🐅&nbsp;🐆&nbsp;*&nbsp;*&nbsp;🦄&nbsp;🦓&nbsp;🦌&nbsp;🦬&nbsp;*&nbsp;🐂&nbsp;🐃&nbsp;🐄&nbsp;🐷&nbsp;🐖&nbsp;🐗&nbsp;🐽&nbsp;🐏&nbsp;🐑&nbsp;🐐&nbsp;🐪&nbsp;🐫&nbsp;🦙&nbsp;🦒&nbsp;🐘&nbsp;🦣&nbsp;🦏&nbsp;🦛&nbsp;🐭&nbsp;🐁&nbsp;🐀&nbsp;🐹&nbsp;🐰&nbsp;🐇&nbsp;🐿&nbsp;🦫&nbsp;🦔&nbsp;🦇&nbsp;*&nbsp;*‍❄&nbsp;🐨&nbsp;🐼&nbsp;🦥&nbsp;🦦&nbsp;🦨&nbsp;🦘&nbsp;🦡&nbsp;🐾&nbsp;🦃&nbsp;🐔&nbsp;🐓&nbsp;🐣&nbsp;🐤&nbsp;🐥&nbsp;🐦&nbsp;*&nbsp;🕊&nbsp;🦅&nbsp;🦆&nbsp;🦢&nbsp;🦉&nbsp;🦤&nbsp;🪶&nbsp;🦩&nbsp;🦚&nbsp;🦜&nbsp;🐸&nbsp;🐊&nbsp;*&nbsp;🦎&nbsp;🐍&nbsp;🐲&nbsp;🐉&nbsp;🦕&nbsp;🦖&nbsp;🐳&nbsp;🐋&nbsp;🐬&nbsp;🦭&nbsp;🐟&nbsp;*&nbsp;🐡&nbsp;🦈&nbsp;🐙&nbsp;🐚&nbsp;🐌&nbsp;🦋&nbsp;*&nbsp;🐜&nbsp;🐝&nbsp;🪲&nbsp;🐞&nbsp;🦗&nbsp;🪳&nbsp;🕷&nbsp;🕸&nbsp;🦂&nbsp;🦟&nbsp;🪰&nbsp;🪱&nbsp;🦠&nbsp;💐&nbsp;🌸&nbsp;💮&nbsp;🏵&nbsp;🌹&nbsp;🥀&nbsp;🌺&nbsp;🌻&nbsp;🌼&nbsp;🌷&nbsp;🌱&nbsp;🪴&nbsp;🌲&nbsp;🌳&nbsp;🌴&nbsp;🌵&nbsp;🌾&nbsp;🌿&nbsp;☘&nbsp;🍀&nbsp;🍁&nbsp;🍂&nbsp;🍃</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>食物和饮料</strong></span></p><p style=\"line-height: 1;\">🍇&nbsp;🍈&nbsp;🍉&nbsp;🍊&nbsp;🍋&nbsp;🍌&nbsp;🍍&nbsp;🥭&nbsp;🍎&nbsp;🍏&nbsp;*&nbsp;🍑&nbsp;🍒&nbsp;🍓&nbsp;🫐&nbsp;🥝&nbsp;🍅&nbsp;🫒&nbsp;🥥&nbsp;🥑&nbsp;🍆&nbsp;🥔&nbsp;🥕&nbsp;🌽&nbsp;🌶&nbsp;🫑&nbsp;🥒&nbsp;🥬&nbsp;🥦&nbsp;🧄&nbsp;🧅&nbsp;🍄&nbsp;🥜&nbsp;🌰&nbsp;🍞&nbsp;🥐&nbsp;🥖&nbsp;🫓&nbsp;🥨&nbsp;🥯&nbsp;🥞&nbsp;🧇&nbsp;🧀&nbsp;🍖&nbsp;🍗&nbsp;🥩&nbsp;🥓&nbsp;🍔&nbsp;🍟&nbsp;🍕&nbsp;🌭&nbsp;🥪&nbsp;🌮&nbsp;🌯&nbsp;🫔&nbsp;🥙&nbsp;🧆&nbsp;🥚&nbsp;🍳&nbsp;🥘&nbsp;🍲&nbsp;🫕&nbsp;🥣&nbsp;🥗&nbsp;🍿&nbsp;🧈&nbsp;🧂&nbsp;🥫&nbsp;🍱&nbsp;🍘&nbsp;🍙&nbsp;🍚&nbsp;🍛&nbsp;🍜&nbsp;🍝&nbsp;🍠&nbsp;🍢&nbsp;🍣&nbsp;🍤&nbsp;🍥&nbsp;🥮&nbsp;🍡&nbsp;🥟&nbsp;🥠&nbsp;🥡&nbsp;🦀&nbsp;🦞&nbsp;🦐&nbsp;🦑&nbsp;🦪&nbsp;🍦&nbsp;🍧&nbsp;🍨&nbsp;🍩&nbsp;🍪&nbsp;🎂&nbsp;🍰&nbsp;🧁&nbsp;🥧&nbsp;🍫&nbsp;🍬&nbsp;🍭&nbsp;🍮&nbsp;🍯&nbsp;🍼&nbsp;🥛&nbsp;☕&nbsp;🫖&nbsp;🍵&nbsp;🍶&nbsp;🍾&nbsp;🍷&nbsp;🍸&nbsp;🍹&nbsp;*&nbsp;🍻&nbsp;🥂&nbsp;🥃&nbsp;🥤&nbsp;🧋&nbsp;🧃&nbsp;🧉&nbsp;🧊&nbsp;🥢&nbsp;🍽&nbsp;🍴&nbsp;🥄&nbsp;🔪&nbsp;🏺</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>旅行和地点</strong></span></p><p style=\"line-height: 1;\">🌍&nbsp;🌎&nbsp;🌏&nbsp;🌐&nbsp;🗺&nbsp;🗾&nbsp;🧭&nbsp;🏔&nbsp;⛰&nbsp;🌋&nbsp;🗻&nbsp;🏕&nbsp;🏖&nbsp;🏜&nbsp;🏝&nbsp;🏞&nbsp;🏟&nbsp;🏛&nbsp;🏗&nbsp;🧱&nbsp;🪨&nbsp;🪵&nbsp;🛖&nbsp;🏘&nbsp;🏚&nbsp;🏠&nbsp;🏡&nbsp;🏢&nbsp;🏣&nbsp;🏤&nbsp;🏥&nbsp;🏦&nbsp;🏨&nbsp;🏩&nbsp;🏪&nbsp;🏫&nbsp;🏬&nbsp;🏭&nbsp;🏯&nbsp;🏰&nbsp;💒&nbsp;*&nbsp;🗽&nbsp;⛪&nbsp;🕌&nbsp;🛕&nbsp;🕍&nbsp;⛩&nbsp;🕋&nbsp;⛲&nbsp;⛺&nbsp;🌁&nbsp;🌃&nbsp;🏙&nbsp;🌄&nbsp;🌅&nbsp;🌆&nbsp;🌇&nbsp;🌉&nbsp;♨&nbsp;🎠&nbsp;🎡&nbsp;🎢&nbsp;💈&nbsp;🎪&nbsp;🚂&nbsp;🚃&nbsp;🚄&nbsp;🚅&nbsp;🚆&nbsp;🚇&nbsp;🚈&nbsp;🚉&nbsp;🚊&nbsp;🚝&nbsp;🚞&nbsp;🚋&nbsp;🚌&nbsp;🚍&nbsp;🚎&nbsp;🚐&nbsp;🚑&nbsp;🚒&nbsp;🚓&nbsp;🚔&nbsp;🚕&nbsp;🚖&nbsp;🚗&nbsp;🚘&nbsp;🚙&nbsp;🛻&nbsp;🚚&nbsp;🚛&nbsp;🚜&nbsp;🏎&nbsp;🏍&nbsp;🛵&nbsp;🦽&nbsp;🦼&nbsp;🛺&nbsp;🚲&nbsp;🛴&nbsp;🛹&nbsp;🛼&nbsp;🚏&nbsp;🛣&nbsp;🛤&nbsp;🛢&nbsp;⛽&nbsp;🚨&nbsp;🚥&nbsp;🚦&nbsp;🛑&nbsp;🚧&nbsp;⚓&nbsp;⛵&nbsp;🛶&nbsp;🚤&nbsp;🛳&nbsp;⛴&nbsp;🛥&nbsp;🚢&nbsp;✈&nbsp;🛩&nbsp;🛫&nbsp;🛬&nbsp;🪂&nbsp;💺&nbsp;🚁&nbsp;🚟&nbsp;🚠&nbsp;🚡&nbsp;🛰&nbsp;🚀&nbsp;🛸&nbsp;🛎&nbsp;🧳&nbsp;⌛&nbsp;⏳&nbsp;⌚&nbsp;⏰&nbsp;⏱&nbsp;⏲&nbsp;🕰&nbsp;🕛&nbsp;🕧&nbsp;🕐&nbsp;🕜&nbsp;🕑&nbsp;🕝&nbsp;🕒&nbsp;🕞&nbsp;🕓&nbsp;🕟&nbsp;🕔&nbsp;🕠&nbsp;🕕&nbsp;🕡&nbsp;🕖&nbsp;🕢&nbsp;🕗&nbsp;🕣&nbsp;🕘&nbsp;🕤&nbsp;🕙&nbsp;🕥&nbsp;🕚&nbsp;🕦&nbsp;🌑&nbsp;🌒&nbsp;🌓&nbsp;🌔&nbsp;🌕&nbsp;🌖&nbsp;🌗&nbsp;🌘&nbsp;🌙&nbsp;🌚&nbsp;🌛&nbsp;🌜&nbsp;🌡&nbsp;☀&nbsp;🌝&nbsp;🌞&nbsp;🪐&nbsp;⭐&nbsp;🌟&nbsp;🌠&nbsp;🌌&nbsp;☁&nbsp;⛅&nbsp;⛈&nbsp;🌤&nbsp;🌥&nbsp;🌦&nbsp;🌧&nbsp;🌨&nbsp;🌩&nbsp;🌪&nbsp;🌫&nbsp;🌬&nbsp;🌀&nbsp;🌈&nbsp;🌂&nbsp;☂&nbsp;☔&nbsp;⛱&nbsp;⚡&nbsp;❄&nbsp;☃&nbsp;⛄&nbsp;☄&nbsp;*&nbsp;💧&nbsp;🌊</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>活动</strong></span></p><p style=\"line-height: 1;\">🎃&nbsp;🎄&nbsp;🎆&nbsp;🎇&nbsp;🧨&nbsp;✨&nbsp;🎈&nbsp;🎉&nbsp;🎊&nbsp;🎋&nbsp;🎍&nbsp;🎎&nbsp;🎏&nbsp;🎐&nbsp;🎑&nbsp;🧧&nbsp;🎀&nbsp;🎁&nbsp;🎗&nbsp;🎟&nbsp;🎫&nbsp;🎖&nbsp;🏆&nbsp;🏅&nbsp;🥇&nbsp;🥈&nbsp;🥉&nbsp;*&nbsp;⚾&nbsp;🥎&nbsp;*&nbsp;🏐&nbsp;🏈&nbsp;🏉&nbsp;🎾&nbsp;🥏&nbsp;🎳&nbsp;🏏&nbsp;🏑&nbsp;🏒&nbsp;🥍&nbsp;🏓&nbsp;🏸&nbsp;🥊&nbsp;🥋&nbsp;🥅&nbsp;⛳&nbsp;⛸&nbsp;🎣&nbsp;🤿&nbsp;🎽&nbsp;🎿&nbsp;🛷&nbsp;🥌&nbsp;🎯&nbsp;🪀&nbsp;🪁&nbsp;🎱&nbsp;🔮&nbsp;🪄&nbsp;🧿&nbsp;🎮&nbsp;🕹&nbsp;🎰&nbsp;🎲&nbsp;🧩&nbsp;🧸&nbsp;🪅&nbsp;🪆&nbsp;♠&nbsp;♥&nbsp;♦&nbsp;♣&nbsp;♟&nbsp;🃏&nbsp;🀄&nbsp;🎴&nbsp;🎭&nbsp;🖼&nbsp;🎨&nbsp;🧵&nbsp;🪡&nbsp;🧶&nbsp;🪢</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>物品</strong></span></p><p style=\"line-height: 1;\">👓&nbsp;🕶&nbsp;🥽&nbsp;🥼&nbsp;🦺&nbsp;👔&nbsp;👕&nbsp;👖&nbsp;🧣&nbsp;🧤&nbsp;🧥&nbsp;🧦&nbsp;👗&nbsp;👘&nbsp;🥻&nbsp;🩱&nbsp;🩲&nbsp;🩳&nbsp;👙&nbsp;👚&nbsp;👛&nbsp;👜&nbsp;👝&nbsp;🛍&nbsp;🎒&nbsp;🩴&nbsp;👞&nbsp;*&nbsp;🥾&nbsp;🥿&nbsp;👠&nbsp;👡&nbsp;🩰&nbsp;👢&nbsp;👑&nbsp;👒&nbsp;🎩&nbsp;🎓&nbsp;🧢&nbsp;🪖&nbsp;⛑&nbsp;📿&nbsp;💄&nbsp;💍&nbsp;💎&nbsp;🔇&nbsp;🔈&nbsp;🔉&nbsp;🔊&nbsp;📢&nbsp;📣&nbsp;📯&nbsp;🔔&nbsp;🔕&nbsp;🎼&nbsp;🎵&nbsp;🎶&nbsp;🎙&nbsp;🎚&nbsp;🎛&nbsp;🎤&nbsp;🎧&nbsp;📻&nbsp;🎷&nbsp;🪗&nbsp;🎸&nbsp;🎹&nbsp;🎺&nbsp;🎻&nbsp;🪕&nbsp;🥁&nbsp;🪘&nbsp;📱&nbsp;📲&nbsp;☎&nbsp;📞&nbsp;📟&nbsp;📠&nbsp;🔋&nbsp;🔌&nbsp;💻&nbsp;🖥&nbsp;🖨&nbsp;⌨&nbsp;🖱&nbsp;🖲&nbsp;💽&nbsp;💾&nbsp;💿&nbsp;📀&nbsp;🧮&nbsp;🎥&nbsp;🎞&nbsp;📽&nbsp;🎬&nbsp;📺&nbsp;📷&nbsp;📸&nbsp;📹&nbsp;📼&nbsp;🔍&nbsp;🔎&nbsp;🕯&nbsp;*&nbsp;🔦&nbsp;🏮&nbsp;🪔&nbsp;📔&nbsp;📕&nbsp;📖&nbsp;📗&nbsp;📘&nbsp;📙&nbsp;📚&nbsp;📓&nbsp;📒&nbsp;📃&nbsp;📜&nbsp;📄&nbsp;📰&nbsp;🗞&nbsp;📑&nbsp;🔖&nbsp;🏷&nbsp;💰&nbsp;🪙&nbsp;💴&nbsp;💵&nbsp;💶&nbsp;💷&nbsp;💸&nbsp;💳&nbsp;🧾&nbsp;💹&nbsp;✉&nbsp;📧&nbsp;📨&nbsp;📩&nbsp;📤&nbsp;📥&nbsp;📦&nbsp;📫&nbsp;📪&nbsp;📬&nbsp;📭&nbsp;📮&nbsp;🗳&nbsp;✏&nbsp;✒&nbsp;🖋&nbsp;🖊&nbsp;🖌&nbsp;🖍&nbsp;📝&nbsp;💼&nbsp;📁&nbsp;📂&nbsp;🗂&nbsp;📅&nbsp;📆&nbsp;🗒&nbsp;🗓&nbsp;📇&nbsp;📈&nbsp;📉&nbsp;📊&nbsp;📋&nbsp;*&nbsp;📍&nbsp;📎&nbsp;🖇&nbsp;📏&nbsp;📐&nbsp;✂&nbsp;🗃&nbsp;🗄&nbsp;🗑&nbsp;🔒&nbsp;🔓&nbsp;🔏&nbsp;🔐&nbsp;🔑&nbsp;🗝&nbsp;*&nbsp;🪓&nbsp;⛏&nbsp;⚒&nbsp;🛠&nbsp;🗡&nbsp;⚔&nbsp;*&nbsp;🪃&nbsp;🏹&nbsp;🛡&nbsp;🪚&nbsp;🔧&nbsp;🪛&nbsp;🔩&nbsp;⚙&nbsp;🗜&nbsp;⚖&nbsp;🦯&nbsp;🔗&nbsp;⛓&nbsp;🪝&nbsp;🧰&nbsp;🧲&nbsp;🪜&nbsp;⚗&nbsp;🧪&nbsp;🧫&nbsp;🧬&nbsp;🔬&nbsp;🔭&nbsp;📡&nbsp;*&nbsp;🩸&nbsp;💊&nbsp;🩹&nbsp;🩺&nbsp;🚪&nbsp;🛗&nbsp;🪞&nbsp;🪟&nbsp;🛏&nbsp;🛋&nbsp;🪑&nbsp;🚽&nbsp;🪠&nbsp;🚿&nbsp;🛁&nbsp;🪤&nbsp;🪒&nbsp;🧴&nbsp;🧷&nbsp;🧹&nbsp;🧺&nbsp;🧻&nbsp;🪣&nbsp;🧼&nbsp;🪥&nbsp;🧽&nbsp;🧯&nbsp;🛒&nbsp;🚬&nbsp;⚰&nbsp;🪦&nbsp;⚱&nbsp;🗿&nbsp;🪧</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>符号</strong></span></p><p style=\"line-height: 1;\">🏧&nbsp;🚮&nbsp;🚰&nbsp;♿&nbsp;🚹&nbsp;🚺&nbsp;🚻&nbsp;🚼&nbsp;🚾&nbsp;🛂&nbsp;🛃&nbsp;🛄&nbsp;🛅&nbsp;⚠&nbsp;🚸&nbsp;⛔&nbsp;🚫&nbsp;🚳&nbsp;🚭&nbsp;🚯&nbsp;🚱&nbsp;🚷&nbsp;📵&nbsp;🔞&nbsp;☢&nbsp;☣&nbsp;⬆&nbsp;↗&nbsp;➡&nbsp;↘&nbsp;⬇&nbsp;↙&nbsp;⬅&nbsp;↖&nbsp;↕&nbsp;↔&nbsp;↩&nbsp;↪&nbsp;⤴&nbsp;⤵&nbsp;🔃&nbsp;🔄&nbsp;🔙&nbsp;🔚&nbsp;🔛&nbsp;🔜&nbsp;🔝&nbsp;🛐&nbsp;⚛&nbsp;🕉&nbsp;✡&nbsp;☸&nbsp;☯&nbsp;✝&nbsp;☦&nbsp;☪&nbsp;☮&nbsp;🕎&nbsp;🔯&nbsp;♈&nbsp;♉&nbsp;♊&nbsp;♋&nbsp;♌&nbsp;♍&nbsp;♎&nbsp;♏&nbsp;♐&nbsp;♑&nbsp;♒&nbsp;♓&nbsp;⛎&nbsp;🔀&nbsp;🔁&nbsp;🔂&nbsp;▶&nbsp;⏩&nbsp;⏭&nbsp;⏯&nbsp;◀&nbsp;⏪&nbsp;⏮&nbsp;🔼&nbsp;⏫&nbsp;🔽&nbsp;⏬&nbsp;⏸&nbsp;⏹&nbsp;⏺&nbsp;⏏&nbsp;🎦&nbsp;🔅&nbsp;🔆&nbsp;📶&nbsp;📳&nbsp;📴&nbsp;♀&nbsp;♂&nbsp;⚧&nbsp;✖&nbsp;➕&nbsp;➖&nbsp;➗&nbsp;♾&nbsp;‼&nbsp;⁉&nbsp;❓&nbsp;❔&nbsp;❕&nbsp;❗&nbsp;〰&nbsp;💱&nbsp;💲&nbsp;⚕&nbsp;♻&nbsp;⚜&nbsp;🔱&nbsp;📛&nbsp;🔰&nbsp;⭕&nbsp;✅&nbsp;☑&nbsp;✔&nbsp;❌&nbsp;❎&nbsp;➰&nbsp;➿&nbsp;〽&nbsp;✳&nbsp;✴&nbsp;❇&nbsp;©&nbsp;®&nbsp;™&nbsp;#⃣&nbsp;*⃣&nbsp;0⃣&nbsp;1⃣&nbsp;2⃣&nbsp;3⃣&nbsp;4⃣&nbsp;5⃣&nbsp;6⃣&nbsp;7⃣&nbsp;8⃣&nbsp;9⃣&nbsp;🔟&nbsp;🔠&nbsp;🔡&nbsp;🔢&nbsp;🔣&nbsp;🔤&nbsp;🅰&nbsp;🆎&nbsp;🅱&nbsp;🆑&nbsp;🆒&nbsp;🆓&nbsp;ℹ&nbsp;🆔&nbsp;Ⓜ&nbsp;🆕&nbsp;🆖&nbsp;🅾&nbsp;🆗&nbsp;🅿&nbsp;🆘&nbsp;🆙&nbsp;🆚&nbsp;🈁&nbsp;🈂&nbsp;🈷&nbsp;🈶&nbsp;🈯&nbsp;🉐&nbsp;🈹&nbsp;🈚&nbsp;*&nbsp;🉑&nbsp;🈸&nbsp;🈴&nbsp;🈳&nbsp;㊗&nbsp;㊙&nbsp;🈺&nbsp;🈵&nbsp;🔴&nbsp;🟠&nbsp;🟡&nbsp;🟢&nbsp;🔵&nbsp;🟣&nbsp;🟤&nbsp;⚫&nbsp;⚪&nbsp;🟥&nbsp;🟧&nbsp;🟨&nbsp;🟩&nbsp;🟦&nbsp;🟪&nbsp;🟫&nbsp;⬛&nbsp;⬜&nbsp;◼&nbsp;◻&nbsp;◾&nbsp;◽&nbsp;▪&nbsp;▫&nbsp;🔶&nbsp;🔷&nbsp;🔸&nbsp;🔹&nbsp;🔺&nbsp;🔻&nbsp;💠&nbsp;🔘&nbsp;🔳&nbsp;🔲</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>旗帜</strong></span></p><p style=\"line-height: 1;\">🏁&nbsp;🚩&nbsp;🎌&nbsp;🏴&nbsp;🏳&nbsp;🏳‍🌈&nbsp;🏳‍⚧&nbsp;🏴‍☠&nbsp;🇦🇨&nbsp;🇦🇩&nbsp;🇦🇪&nbsp;🇦🇫&nbsp;🇦🇬&nbsp;🇦🇮&nbsp;🇦🇱&nbsp;🇦🇲&nbsp;🇦🇴&nbsp;🇦🇶&nbsp;🇦🇷&nbsp;🇦🇸&nbsp;🇦🇹&nbsp;🇦🇺&nbsp;🇦🇼&nbsp;🇦🇽&nbsp;🇦🇿&nbsp;🇧🇦&nbsp;🇧🇧&nbsp;🇧🇩&nbsp;🇧🇪&nbsp;🇧🇫&nbsp;🇧🇬&nbsp;🇧🇭&nbsp;🇧🇮&nbsp;🇧🇯&nbsp;🇧🇱&nbsp;🇧🇲&nbsp;🇧🇳&nbsp;🇧🇴&nbsp;🇧🇶&nbsp;🇧🇷&nbsp;🇧🇸&nbsp;🇧🇹&nbsp;🇧🇻&nbsp;🇧🇼&nbsp;🇧🇾&nbsp;🇧🇿&nbsp;🇨🇦&nbsp;🇨🇨&nbsp;🇨🇩&nbsp;🇨🇫&nbsp;🇨🇬&nbsp;🇨🇭&nbsp;🇨🇮&nbsp;🇨🇰&nbsp;🇨🇱&nbsp;🇨🇲&nbsp;🇨🇳&nbsp;🇨🇴&nbsp;🇨🇵&nbsp;🇨🇷&nbsp;🇨🇺&nbsp;🇨🇻&nbsp;🇨🇼&nbsp;🇨🇽&nbsp;🇨🇾&nbsp;🇨🇿&nbsp;🇩🇪&nbsp;🇩🇬&nbsp;🇩🇯&nbsp;🇩🇰&nbsp;🇩🇲&nbsp;🇩🇴&nbsp;🇩🇿&nbsp;🇪🇦&nbsp;🇪🇨&nbsp;🇪🇪&nbsp;🇪🇬&nbsp;🇪🇭&nbsp;🇪🇷&nbsp;🇪🇸&nbsp;🇪🇹&nbsp;🇪🇺&nbsp;🇫🇮&nbsp;🇫🇯&nbsp;🇫🇰&nbsp;🇫🇲&nbsp;🇫🇴&nbsp;🇫🇷&nbsp;🇬🇦&nbsp;🇬🇧&nbsp;🇬🇩&nbsp;🇬🇪&nbsp;🇬🇫&nbsp;🇬🇬&nbsp;🇬🇭&nbsp;🇬🇮&nbsp;🇬🇱&nbsp;🇬🇲&nbsp;🇬🇳&nbsp;🇬🇵&nbsp;🇬🇶&nbsp;🇬🇷&nbsp;🇬🇸&nbsp;🇬🇹&nbsp;🇬🇺&nbsp;🇬🇼&nbsp;🇬🇾&nbsp;🇭🇰&nbsp;🇭🇲&nbsp;🇭🇳&nbsp;🇭🇷&nbsp;🇭🇹&nbsp;🇭🇺&nbsp;🇮🇨&nbsp;🇮🇩&nbsp;🇮🇪&nbsp;🇮🇱&nbsp;🇮🇲&nbsp;🇮🇳&nbsp;🇮🇴&nbsp;🇮🇶&nbsp;🇮🇷&nbsp;🇮🇸&nbsp;🇮🇹&nbsp;🇯🇪&nbsp;🇯🇲&nbsp;🇯🇴&nbsp;🇯🇵&nbsp;🇰🇪&nbsp;🇰🇬&nbsp;🇰🇭&nbsp;🇰🇮&nbsp;🇰🇲&nbsp;🇰🇳&nbsp;🇰🇵&nbsp;🇰🇷&nbsp;🇰🇼&nbsp;🇰🇾&nbsp;🇰🇿&nbsp;🇱🇦&nbsp;🇱🇧&nbsp;🇱🇨&nbsp;🇱🇮&nbsp;🇱🇰&nbsp;🇱🇷&nbsp;🇱🇸&nbsp;🇱🇹&nbsp;🇱🇺&nbsp;🇱🇻&nbsp;🇱🇾&nbsp;🇲🇦&nbsp;🇲🇨&nbsp;🇲🇩&nbsp;🇲🇪&nbsp;🇲🇫&nbsp;🇲🇬&nbsp;🇲🇭&nbsp;🇲🇰&nbsp;🇲🇱&nbsp;🇲🇲&nbsp;🇲🇳&nbsp;🇲🇴&nbsp;🇲🇵&nbsp;🇲🇶&nbsp;🇲🇷&nbsp;🇲🇸&nbsp;🇲🇹&nbsp;🇲🇺&nbsp;🇲🇻&nbsp;🇲🇼&nbsp;🇲🇽&nbsp;🇲🇾&nbsp;🇲🇿&nbsp;🇳🇦&nbsp;🇳🇨&nbsp;🇳🇪&nbsp;🇳🇫&nbsp;🇳🇬&nbsp;🇳🇮&nbsp;🇳🇱&nbsp;🇳🇴&nbsp;🇳🇵&nbsp;🇳🇷&nbsp;🇳🇺&nbsp;🇳🇿&nbsp;🇴🇲&nbsp;🇵🇦&nbsp;🇵🇪&nbsp;🇵🇫&nbsp;🇵🇬&nbsp;🇵🇭&nbsp;🇵🇰&nbsp;🇵🇱&nbsp;🇵🇲&nbsp;🇵🇳&nbsp;🇵🇷&nbsp;🇵🇸&nbsp;🇵🇹&nbsp;🇵🇼&nbsp;🇵🇾&nbsp;🇶🇦&nbsp;🇷🇪&nbsp;🇷🇴&nbsp;🇷🇸&nbsp;🇷🇺&nbsp;🇷🇼&nbsp;🇸🇦&nbsp;🇸🇧&nbsp;🇸🇨&nbsp;🇸🇩&nbsp;🇸🇪&nbsp;🇸🇬&nbsp;🇸🇭&nbsp;🇸🇮&nbsp;🇸🇯&nbsp;🇸🇰&nbsp;🇸🇱&nbsp;🇸🇲&nbsp;🇸🇳&nbsp;🇸🇴&nbsp;🇸🇷&nbsp;🇸🇸&nbsp;🇸🇹&nbsp;🇸🇻&nbsp;🇸🇽&nbsp;🇸🇾&nbsp;🇸🇿&nbsp;🇹🇦&nbsp;🇹🇨&nbsp;🇹🇩&nbsp;🇹🇫&nbsp;🇹🇬&nbsp;🇹🇭&nbsp;🇹🇯&nbsp;🇹🇰&nbsp;🇹🇱&nbsp;🇹🇲&nbsp;🇹🇳&nbsp;🇹🇴&nbsp;🇹🇷&nbsp;🇹🇹&nbsp;🇹🇻&nbsp;🇹🇼&nbsp;🇹🇿&nbsp;🇺🇦&nbsp;🇺🇬&nbsp;🇺🇲&nbsp;🇺🇳&nbsp;🇺🇸&nbsp;🇺🇾&nbsp;🇺🇿&nbsp;🇻🇦&nbsp;🇻🇨&nbsp;🇻🇪&nbsp;🇻🇬&nbsp;🇻🇮&nbsp;🇻🇳&nbsp;🇻🇺&nbsp;🇼🇫&nbsp;🇼🇸&nbsp;🇽🇰&nbsp;🇾🇪&nbsp;🇾🇹&nbsp;🇿🇦&nbsp;🇿🇲&nbsp;🇿🇼&nbsp;🏴󠁧󠁢󠁥󠁮󠁧󠁿&nbsp;🏴󠁧󠁢󠁳󠁣󠁴󠁿&nbsp;🏴󠁧󠁢󠁷󠁬󠁳󠁿</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>手势类</strong></span></p><p style=\"line-height: 1;\">🤲&nbsp;👐&nbsp;🙌&nbsp;👏&nbsp;🤝&nbsp;👍&nbsp;👎&nbsp;👊&nbsp;✊&nbsp;🤛&nbsp;🤜&nbsp;🤞&nbsp;✌️&nbsp;🤟&nbsp;🤘&nbsp;👌&nbsp;👈&nbsp;👉&nbsp;👆&nbsp;👇&nbsp;☝️&nbsp;✋&nbsp;🤚&nbsp;🖐&nbsp;🖖&nbsp;*&nbsp;🤙&nbsp;💪🖕&nbsp;✍️&nbsp;🙏</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>人物</strong></span></p><p style=\"line-height: 1;\">👶&nbsp;👧&nbsp;🧒&nbsp;👦&nbsp;👩&nbsp;🧑&nbsp;👨&nbsp;👵&nbsp;🧓&nbsp;👴&nbsp;👲&nbsp;👳‍♀️&nbsp;👳‍♂️&nbsp;🧕&nbsp;🧔&nbsp;👱‍♂️&nbsp;👱‍♀️&nbsp;👮‍♀️&nbsp;👮‍♂️&nbsp;👷‍♀️&nbsp;👷‍♂️&nbsp;💂‍♀️&nbsp;💂‍♂️&nbsp;🕵️‍♀️&nbsp;🕵️‍♂️&nbsp;👩‍⚕️&nbsp;👨‍⚕️&nbsp;👩‍🌾&nbsp;👨‍🌾&nbsp;👩‍🍳&nbsp;👨‍🍳&nbsp;👩‍🎓&nbsp;👨‍🎓&nbsp;👩‍🎤&nbsp;👨‍🎤&nbsp;👩‍🏫&nbsp;👨‍🏫&nbsp;👩‍🏭&nbsp;👨‍🏭&nbsp;👩‍💻&nbsp;👨‍💻&nbsp;👩‍💼&nbsp;👨‍💼&nbsp;👩‍🔧&nbsp;👨‍🔧&nbsp;👩‍🔬&nbsp;👨‍🔬&nbsp;👩‍🎨&nbsp;👨‍🎨&nbsp;👩‍🚒&nbsp;👨‍🚒&nbsp;👩‍✈️&nbsp;👨‍✈️&nbsp;👩‍🚀&nbsp;👨‍🚀&nbsp;👩‍⚖️&nbsp;👨‍⚖️&nbsp;👰&nbsp;🤵&nbsp;👸&nbsp;🤴&nbsp;🤶&nbsp;🎅&nbsp;🧙‍♀️&nbsp;🧙‍♂️&nbsp;🧝‍♀️&nbsp;🧝‍♂️&nbsp;🧛‍♀️&nbsp;🧛‍♂️&nbsp;🧟‍♀️&nbsp;🧟‍♂️&nbsp;🧞‍♀️&nbsp;🧞‍♂️&nbsp;🧜‍♀️&nbsp;🧜‍♂️&nbsp;🧚‍♀️&nbsp;🧚‍♂️&nbsp;👼&nbsp;🤰&nbsp;🤱&nbsp;🙇‍♀️&nbsp;🙇‍♂️&nbsp;💁‍♀️&nbsp;💁‍♂️&nbsp;🙅‍♀️&nbsp;🙅‍♂️&nbsp;🙆‍♀️&nbsp;🙆‍♂️&nbsp;🙋‍♀️&nbsp;🙋‍♂️&nbsp;🤦‍♀️&nbsp;🤦‍♂️&nbsp;🤷‍♀️&nbsp;🤷‍♂️&nbsp;🙎‍♀️&nbsp;🙎‍♂️&nbsp;🙍‍♀️&nbsp;🙍‍♂️&nbsp;💇‍♀️&nbsp;💇‍♂️&nbsp;💆‍♀️&nbsp;💆‍♂️&nbsp;🧖‍♀️&nbsp;🧖‍♂️&nbsp;💅&nbsp;🤳&nbsp;💃&nbsp;🕺&nbsp;👯‍♀️&nbsp;👯‍♂️&nbsp;🕴&nbsp;🚶‍♀️&nbsp;🚶‍♂️&nbsp;🏃‍♀️&nbsp;🏃‍♂️&nbsp;👫&nbsp;👭&nbsp;👬&nbsp;💑&nbsp;👩‍❤️‍👩&nbsp;👨‍❤️‍👨&nbsp;💏&nbsp;👩‍❤️‍💋‍👩&nbsp;👨‍❤️‍💋‍👨&nbsp;👪&nbsp;👨‍👩‍👧&nbsp;👨‍👩‍👧‍👦&nbsp;👨‍👩‍👦‍👦&nbsp;👨‍👩‍👧‍👧&nbsp;👩‍👩‍👦&nbsp;👩‍👩‍👧&nbsp;👩‍👩‍👧‍👦&nbsp;👩‍👩‍👦‍👦&nbsp;👩‍👩‍👧‍👧&nbsp;👨‍👨‍👦&nbsp;👨‍👨‍👧&nbsp;👨‍👨‍👧‍👦&nbsp;👨‍👨‍👦‍👦&nbsp;👨‍👨‍👧‍👧&nbsp;👩‍👦&nbsp;👩‍👧&nbsp;👩‍👧‍👦&nbsp;👩‍👦‍👦&nbsp;👩‍👧‍👧&nbsp;👨‍👦&nbsp;👨‍👧&nbsp;👨‍👧‍👦&nbsp;👨‍👦‍👦&nbsp;👨‍👧‍👧</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>动植物&nbsp;自然</strong></span></p><p style=\"line-height: 1;\">*&nbsp;🐱&nbsp;🐭&nbsp;🐹&nbsp;🐰&nbsp;🦊&nbsp;🦝&nbsp;*&nbsp;🐼&nbsp;🦘&nbsp;🦡&nbsp;🐨&nbsp;🐯&nbsp;🦁&nbsp;*&nbsp;🐷&nbsp;🐽&nbsp;🐸&nbsp;🐵&nbsp;🙈&nbsp;🙉&nbsp;🙊&nbsp;🐒&nbsp;🐔&nbsp;*&nbsp;🐦&nbsp;🐤&nbsp;🐣&nbsp;🐥&nbsp;🦆&nbsp;🦢&nbsp;🦅&nbsp;🦉&nbsp;🦚&nbsp;🦜&nbsp;🦇&nbsp;🐺&nbsp;🐗&nbsp;*&nbsp;🦄&nbsp;🐝&nbsp;*&nbsp;🦋&nbsp;🐌&nbsp;🐚&nbsp;🐞&nbsp;🐜&nbsp;🦗&nbsp;🕷&nbsp;🕸&nbsp;🦂&nbsp;🦟&nbsp;🦠&nbsp;*&nbsp;🐍&nbsp;🦎&nbsp;🦖&nbsp;🦕&nbsp;🐙&nbsp;🦑&nbsp;🦐&nbsp;🦀&nbsp;🐡&nbsp;*&nbsp;🐟&nbsp;🐬&nbsp;🐳&nbsp;🐋&nbsp;🦈&nbsp;🐊&nbsp;🐅&nbsp;🐆&nbsp;🦓&nbsp;🦍&nbsp;🐘&nbsp;🦏&nbsp;🦛&nbsp;🐪&nbsp;🐫&nbsp;🦙&nbsp;🦒&nbsp;🐃&nbsp;🐂&nbsp;🐄&nbsp;*&nbsp;🐖&nbsp;🐏&nbsp;🐑&nbsp;🐐&nbsp;🦌&nbsp;🐕&nbsp;🐩&nbsp;🐈&nbsp;🐓&nbsp;🦃&nbsp;🕊&nbsp;🐇&nbsp;🐁&nbsp;🐀&nbsp;🐿&nbsp;🦔&nbsp;🐾&nbsp;🐉&nbsp;🐲&nbsp;🌵&nbsp;🎄&nbsp;🌲&nbsp;🌳&nbsp;🌴&nbsp;🌱&nbsp;🌿&nbsp;☘️&nbsp;🍀&nbsp;🎍&nbsp;🎋&nbsp;🍃&nbsp;🍂&nbsp;🍁&nbsp;🍄&nbsp;🌾&nbsp;💐&nbsp;🌷&nbsp;🌹&nbsp;🥀&nbsp;🌺&nbsp;🌸&nbsp;🌼&nbsp;🌻&nbsp;🌞&nbsp;🌝&nbsp;🌛&nbsp;🌜&nbsp;🌚&nbsp;🌕&nbsp;🌖&nbsp;🌗&nbsp;🌘&nbsp;🌑&nbsp;🌒&nbsp;🌓&nbsp;🌔&nbsp;🌙&nbsp;🌎&nbsp;🌍&nbsp;🌏&nbsp;💫&nbsp;⭐️&nbsp;🌟&nbsp;✨&nbsp;⚡️&nbsp;☄️&nbsp;💥&nbsp;*&nbsp;🌪&nbsp;🌈&nbsp;☀️&nbsp;🌤&nbsp;⛅️&nbsp;🌥&nbsp;☁️&nbsp;🌦&nbsp;🌧&nbsp;⛈&nbsp;🌩&nbsp;🌨&nbsp;</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>十二生肖</strong></span></p><p style=\"line-height: 1;\">🐁🐂🐅🐇🐉🐍*🐐🐒🐓🐕🐖</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>水果&nbsp;食物</strong></span></p><p style=\"line-height: 1;\">🍏&nbsp;🍎&nbsp;*&nbsp;🍊&nbsp;🍋&nbsp;🍌&nbsp;🍉&nbsp;🍇&nbsp;🍓&nbsp;🍈&nbsp;🍒&nbsp;🍑&nbsp;🍍&nbsp;🥭&nbsp;🥥&nbsp;🥝&nbsp;🍅&nbsp;🍆&nbsp;🥑&nbsp;🥦&nbsp;🥒&nbsp;🥬&nbsp;🌶&nbsp;🌽&nbsp;🥕&nbsp;🥔&nbsp;🍠&nbsp;🥐&nbsp;🍞&nbsp;🥖&nbsp;🥨&nbsp;🥯&nbsp;🧀&nbsp;🥚&nbsp;🍳&nbsp;🥞&nbsp;🥓&nbsp;🥩&nbsp;🍗&nbsp;🍖&nbsp;🌭&nbsp;🍔&nbsp;🍟&nbsp;🍕&nbsp;🥪&nbsp;🥙&nbsp;🌮&nbsp;🌯&nbsp;🥗&nbsp;🥘&nbsp;🥫&nbsp;🍝&nbsp;🍜&nbsp;🍲&nbsp;🍛&nbsp;🍣&nbsp;🍱&nbsp;🥟&nbsp;🍤&nbsp;🍙&nbsp;🍚&nbsp;🍘&nbsp;🍥&nbsp;🥮&nbsp;🥠&nbsp;🍢&nbsp;🍡&nbsp;🍧&nbsp;🍨&nbsp;🍦&nbsp;🥧&nbsp;🍰&nbsp;🎂&nbsp;🍮&nbsp;🍭&nbsp;🍬&nbsp;🍫&nbsp;🍿&nbsp;🧂&nbsp;🍩&nbsp;🍪&nbsp;🌰&nbsp;🥜&nbsp;🍯&nbsp;🥛&nbsp;🍼&nbsp;☕️&nbsp;🍵&nbsp;🥤&nbsp;🍶&nbsp;*&nbsp;🍻&nbsp;🥂&nbsp;🍷&nbsp;🥃&nbsp;🍸&nbsp;🍹&nbsp;🍾&nbsp;🥄&nbsp;🍴&nbsp;🍽&nbsp;🥣&nbsp;🥡&nbsp;🥢</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>运动&nbsp;娱乐</strong></span></p><p style=\"line-height: 1;\">*️&nbsp;*&nbsp;🏈&nbsp;⚾️&nbsp;🥎&nbsp;🏐&nbsp;🏉&nbsp;🎾&nbsp;🥏&nbsp;🎱&nbsp;🏓&nbsp;🏸&nbsp;🥅&nbsp;🏒&nbsp;🏑&nbsp;🥍&nbsp;🏏&nbsp;⛳️&nbsp;🏹&nbsp;🎣&nbsp;🥊&nbsp;🥋&nbsp;🎽&nbsp;⛸&nbsp;🥌&nbsp;🛷&nbsp;🛹&nbsp;🎿&nbsp;⛷&nbsp;🏂&nbsp;🏋️‍♀️&nbsp;🏋🏻‍♀️&nbsp;🏋🏼‍♀️&nbsp;🏋🏽‍♀️&nbsp;🏋🏾‍♀️&nbsp;🏋🏿‍♀️&nbsp;🏋️‍♂️&nbsp;🏋🏻‍♂️&nbsp;🏋🏼‍♂️&nbsp;🏋🏽‍♂️&nbsp;🏋🏾‍♂️&nbsp;🏋🏿‍♂️&nbsp;🤼‍♀️&nbsp;🤼‍♂️&nbsp;🤸‍♀️&nbsp;🤸🏻‍♀️&nbsp;🤸🏼‍♀️&nbsp;🤸🏽‍♀️&nbsp;🤸🏾‍♀️&nbsp;🤸🏿‍♀️&nbsp;🤸‍♂️&nbsp;🤸🏻‍♂️&nbsp;🤸🏼‍♂️&nbsp;🤸🏽‍♂️&nbsp;🤸🏾‍♂️&nbsp;🤸🏿‍♂️&nbsp;⛹️‍♀️&nbsp;⛹🏻‍♀️&nbsp;⛹🏼‍♀️&nbsp;⛹🏽‍♀️&nbsp;⛹🏾‍♀️&nbsp;⛹🏿‍♀️&nbsp;⛹️‍♂️&nbsp;⛹🏻‍♂️&nbsp;⛹🏼‍♂️&nbsp;⛹🏽‍♂️&nbsp;⛹🏾‍♂️&nbsp;⛹🏿‍♂️&nbsp;🤺&nbsp;🤾‍♀️&nbsp;🤾🏻‍♀️&nbsp;🤾🏼‍♀️&nbsp;🤾🏾‍♀️&nbsp;🤾🏾‍♀️&nbsp;🤾🏿‍♀️&nbsp;🤾‍♂️&nbsp;🤾🏻‍♂️&nbsp;🤾🏼‍♂️&nbsp;🤾🏽‍♂️&nbsp;🤾🏾‍♂️&nbsp;🤾🏿‍♂️&nbsp;🏌️‍♀️&nbsp;🏌🏻‍♀️&nbsp;🏌🏼‍♀️&nbsp;🏌🏽‍♀️&nbsp;🏌🏾‍♀️&nbsp;🏌🏿‍♀️&nbsp;🏌️‍♂️&nbsp;🏌🏻‍♂️&nbsp;🏌🏼‍♂️&nbsp;🏌🏽‍♂️&nbsp;🏌🏾‍♂️&nbsp;🏌🏿‍♂️&nbsp;🏇&nbsp;🏇🏻&nbsp;🏇🏼&nbsp;🏇🏽&nbsp;🏇🏾&nbsp;🏇🏿&nbsp;🧘‍♀️&nbsp;🧘🏻‍♀️&nbsp;🧘🏼‍♀️&nbsp;🧘🏽‍♀️&nbsp;🧘🏾‍♀️&nbsp;🧘🏿‍♀️&nbsp;🧘‍♂️&nbsp;🧘🏻‍♂️&nbsp;🧘🏼‍♂️&nbsp;🧘🏽‍♂️&nbsp;🧘🏾‍♂️&nbsp;🧘🏿‍♂️&nbsp;🏄‍♀️&nbsp;🏄🏻‍♀️&nbsp;🏄🏼‍♀️&nbsp;🏄🏽‍♀️&nbsp;🏄🏾‍♀️&nbsp;🏄🏿‍♀️&nbsp;🏄‍♂️&nbsp;🏄🏻‍♂️&nbsp;🏄🏼‍♂️&nbsp;🏄🏽‍♂️&nbsp;🏄🏾‍♂️&nbsp;🏄🏿‍♂️&nbsp;🏊‍♀️&nbsp;🏊🏻‍♀️&nbsp;🏊🏼‍♀️&nbsp;🏊🏽‍♀️&nbsp;🏊🏾‍♀️&nbsp;🏊🏿‍♀️&nbsp;🏊‍♂️&nbsp;🏊🏻‍♂️&nbsp;🏊🏼‍♂️&nbsp;🏊🏽‍♂️&nbsp;🏊🏾‍♂️&nbsp;🏊🏿‍♂️&nbsp;🤽‍♀️&nbsp;🤽🏻‍♀️&nbsp;🤽🏼‍♀️&nbsp;🤽🏽‍♀️&nbsp;🤽🏾‍♀️&nbsp;🤽🏿‍♀️&nbsp;🤽‍♂️&nbsp;🤽🏻‍♂️&nbsp;🤽🏼‍♂️&nbsp;🤽🏽‍♂️&nbsp;🤽🏾‍♂️&nbsp;🤽🏿‍♂️&nbsp;🚣‍♀️&nbsp;🚣🏻‍♀️&nbsp;🚣🏼‍♀️&nbsp;🚣🏽‍♀️&nbsp;🚣🏾‍♀️&nbsp;🚣🏿‍♀️&nbsp;🚣‍♂️&nbsp;🚣🏻‍♂️&nbsp;🚣🏼‍♂️&nbsp;🚣🏽‍♂️&nbsp;🚣🏾‍♂️&nbsp;🚣🏿‍♂️&nbsp;🧗‍♀️&nbsp;🧗🏻‍♀️&nbsp;🧗🏼‍♀️&nbsp;🧗🏽‍♀️&nbsp;🧗🏾‍♀️&nbsp;🧗🏿‍♀️&nbsp;🧗‍♂️&nbsp;🧗🏻‍♂️&nbsp;🧗🏼‍♂️&nbsp;🧗🏽‍♂️&nbsp;🧗🏾‍♂️&nbsp;🧗🏿‍♂️&nbsp;🚵‍♀️&nbsp;🚵🏻‍♀️&nbsp;🚵🏼‍♀️&nbsp;🚵🏽‍♀️&nbsp;🚵🏾‍♀️&nbsp;🚵🏿‍♀️&nbsp;🚵‍♂️&nbsp;🚵🏻‍♂️&nbsp;🚵🏼‍♂️&nbsp;🚵🏽‍♂️&nbsp;🚵🏾‍♂️&nbsp;🚵🏿‍♂️&nbsp;🚴‍♀️&nbsp;🚴🏻‍♀️&nbsp;🚴🏼‍♀️&nbsp;🚴🏽‍♀️&nbsp;🚴🏾‍♀️&nbsp;🚴🏿‍♀️&nbsp;🚴‍♂️&nbsp;🚴🏻‍♂️&nbsp;🚴🏼‍♂️&nbsp;🚴🏽‍♂️&nbsp;🚴🏾‍♂️&nbsp;🚴🏿‍♂️&nbsp;🏆&nbsp;🥇&nbsp;🥈&nbsp;🥉&nbsp;🏅&nbsp;🎖&nbsp;🏵&nbsp;🎗&nbsp;🎫&nbsp;🎟&nbsp;🎪&nbsp;🤹‍♀️&nbsp;🤹🏻‍♀️&nbsp;🤹🏼‍♀️&nbsp;🤹🏽‍♀️&nbsp;🤹🏾‍♀️&nbsp;🤹🏿‍♀️&nbsp;🤹‍♂️&nbsp;🤹🏻‍♂️&nbsp;🤹🏼‍♂️&nbsp;🤹🏽‍♂️&nbsp;🤹🏾‍♂️&nbsp;🤹🏿‍♂️&nbsp;🎭&nbsp;🎨&nbsp;🎬&nbsp;🎤&nbsp;🎧&nbsp;🎼&nbsp;🎹&nbsp;🥁&nbsp;🎷&nbsp;🎺&nbsp;🎸&nbsp;🎻&nbsp;🎲&nbsp;🧩&nbsp;♟&nbsp;🎯&nbsp;🎳&nbsp;🎮&nbsp;🎰</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>交通&nbsp;旅行</strong></span></p><p style=\"line-height: 1;\">🚗&nbsp;🚕&nbsp;🚙&nbsp;🚌&nbsp;🚎&nbsp;🏎&nbsp;🚓&nbsp;🚑&nbsp;🚒&nbsp;🚐&nbsp;🚚&nbsp;🚛&nbsp;🚜&nbsp;🛴&nbsp;🚲&nbsp;🛵&nbsp;🏍&nbsp;🚨&nbsp;🚔&nbsp;🚍&nbsp;🚘&nbsp;🚖&nbsp;🚡&nbsp;🚠&nbsp;🚟&nbsp;🚃&nbsp;🚋&nbsp;🚞&nbsp;🚝&nbsp;🚄&nbsp;🚅&nbsp;🚈&nbsp;🚂&nbsp;🚆&nbsp;🚇&nbsp;🚊&nbsp;🚉&nbsp;✈️&nbsp;🛫&nbsp;🛬&nbsp;🛩&nbsp;💺&nbsp;🛰&nbsp;🚀&nbsp;🛸&nbsp;🚁&nbsp;🛶&nbsp;⛵️&nbsp;🚤&nbsp;🛥&nbsp;🛳&nbsp;⛴&nbsp;🚢&nbsp;⚓️&nbsp;⛽️&nbsp;🚧&nbsp;🚦&nbsp;🚥&nbsp;🚏&nbsp;🗺&nbsp;🗿&nbsp;🗽&nbsp;*&nbsp;🏰&nbsp;🏯&nbsp;🏟&nbsp;🎡&nbsp;🎢&nbsp;🎠&nbsp;⛲️&nbsp;⛱&nbsp;🏖&nbsp;🏝&nbsp;🏜&nbsp;🌋&nbsp;⛰&nbsp;🏔&nbsp;🗻&nbsp;🏕&nbsp;⛺️&nbsp;🏠&nbsp;🏡&nbsp;🏘&nbsp;🏚&nbsp;🏗&nbsp;🏭&nbsp;🏢&nbsp;🏬&nbsp;🏣&nbsp;🏤&nbsp;🏥&nbsp;🏦&nbsp;🏨&nbsp;🏪&nbsp;🏫&nbsp;🏩&nbsp;💒&nbsp;🏛&nbsp;⛪️&nbsp;🕌&nbsp;🕍&nbsp;🕋&nbsp;⛩&nbsp;🛤&nbsp;🛣&nbsp;🗾&nbsp;🎑&nbsp;🏞&nbsp;🌅&nbsp;🌄&nbsp;🌠&nbsp;🎇&nbsp;🎆&nbsp;🌇&nbsp;🌆&nbsp;🏙&nbsp;🌃&nbsp;🌌&nbsp;🌉&nbsp;🌁</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>物体</strong></span></p><p style=\"line-height: 1;\">⌚️&nbsp;📱&nbsp;📲&nbsp;💻&nbsp;⌨️&nbsp;🖥&nbsp;🖨&nbsp;🖱&nbsp;🖲&nbsp;🕹&nbsp;🗜&nbsp;💽&nbsp;💾&nbsp;💿&nbsp;📀&nbsp;📼&nbsp;📷&nbsp;📸&nbsp;📹&nbsp;🎥&nbsp;📽&nbsp;🎞&nbsp;📞&nbsp;☎️&nbsp;📟&nbsp;📠&nbsp;📺&nbsp;📻&nbsp;🎙&nbsp;🎚&nbsp;🎛&nbsp;⏱&nbsp;⏲&nbsp;⏰&nbsp;🕰&nbsp;⌛️&nbsp;⏳&nbsp;📡&nbsp;🔋&nbsp;🔌&nbsp;*&nbsp;🔦&nbsp;🕯&nbsp;🗑&nbsp;🛢&nbsp;💸&nbsp;💵&nbsp;💴&nbsp;💶&nbsp;💷&nbsp;💰&nbsp;💳&nbsp;🧾&nbsp;💎&nbsp;⚖️&nbsp;🔧&nbsp;*&nbsp;⚒&nbsp;🛠&nbsp;⛏&nbsp;🔩&nbsp;⚙️&nbsp;⛓&nbsp;*&nbsp;💣&nbsp;🔪&nbsp;🗡&nbsp;⚔️&nbsp;🛡&nbsp;🚬&nbsp;⚰️&nbsp;⚱️&nbsp;🏺</p><p style=\"line-height: 1;\"><br></p><p style=\"text-align: center; line-height: 1;\"><span style=\"color: rgb(225, 60, 57); font-size: 22px; font-family: 华文楷体;\"><strong>标志</strong></span></p><p style=\"line-height: 1;\">❤️&nbsp;🧡&nbsp;💛&nbsp;💚&nbsp;💙&nbsp;💜&nbsp;🖤&nbsp;💔&nbsp;❣️&nbsp;💕&nbsp;💞&nbsp;💓&nbsp;💗&nbsp;💖&nbsp;💘&nbsp;💝&nbsp;💟&nbsp;☮️&nbsp;✝️&nbsp;☪️&nbsp;🕉&nbsp;☸️&nbsp;✡️&nbsp;🔯&nbsp;🕎&nbsp;☯️&nbsp;☦️&nbsp;🛐&nbsp;⛎&nbsp;♈️&nbsp;♉️&nbsp;♊️&nbsp;♋️&nbsp;♌️&nbsp;♍️&nbsp;♎️&nbsp;♏️&nbsp;♐️&nbsp;♑️&nbsp;♒️&nbsp;♓️&nbsp;🆔&nbsp;⚛️&nbsp;🉑&nbsp;☢️&nbsp;☣️&nbsp;📴&nbsp;📳&nbsp;🈶&nbsp;🈚️&nbsp;🈸&nbsp;🈺&nbsp;🈷️&nbsp;✴️&nbsp;🆚&nbsp;💮&nbsp;🉐&nbsp;㊙️&nbsp;㊗️&nbsp;🈴&nbsp;🈵&nbsp;🈹&nbsp;*&nbsp;🅰️&nbsp;🅱️&nbsp;🆎&nbsp;🆑&nbsp;🅾️&nbsp;🆘&nbsp;❌&nbsp;⭕️&nbsp;🛑&nbsp;⛔️&nbsp;📛&nbsp;🚫&nbsp;💯&nbsp;💢&nbsp;♨️&nbsp;🚷&nbsp;🚯&nbsp;🚳&nbsp;🚱&nbsp;🔞&nbsp;📵&nbsp;🚭&nbsp;❗️&nbsp;❕&nbsp;❓&nbsp;❔&nbsp;‼️&nbsp;⁉️&nbsp;🔅&nbsp;🔆&nbsp;〽️&nbsp;⚠️&nbsp;🚸&nbsp;🔱&nbsp;⚜️&nbsp;🔰&nbsp;♻️&nbsp;✅&nbsp;🈯️&nbsp;💹&nbsp;❇️&nbsp;✳️&nbsp;❎&nbsp;🌐&nbsp;💠&nbsp;Ⓜ️&nbsp;🌀&nbsp;💤&nbsp;🏧&nbsp;🚾&nbsp;♿️&nbsp;🅿️&nbsp;🈳&nbsp;🈂️&nbsp;🛂&nbsp;🛃&nbsp;🛄&nbsp;🛅&nbsp;🚹&nbsp;🚺&nbsp;🚼&nbsp;🚻&nbsp;🚮&nbsp;🎦&nbsp;📶&nbsp;🈁&nbsp;🔣&nbsp;ℹ️&nbsp;🔤&nbsp;🔡&nbsp;🔠&nbsp;🆖&nbsp;🆗&nbsp;🆙&nbsp;🆒&nbsp;🆕&nbsp;🆓&nbsp;0️⃣&nbsp;1️⃣&nbsp;2️⃣&nbsp;3️⃣&nbsp;4️⃣&nbsp;5️⃣&nbsp;6️⃣&nbsp;7️⃣&nbsp;8️⃣&nbsp;9️⃣&nbsp;🔟&nbsp;🔢&nbsp;#️⃣&nbsp;*️⃣&nbsp;⏏️&nbsp;▶️&nbsp;⏸&nbsp;⏯&nbsp;⏹&nbsp;⏺&nbsp;⏭&nbsp;⏮&nbsp;⏩&nbsp;⏪&nbsp;⏫&nbsp;⏬&nbsp;◀️&nbsp;🔼&nbsp;🔽&nbsp;➡️&nbsp;⬅️&nbsp;⬆️&nbsp;⬇️&nbsp;↗️&nbsp;↘️&nbsp;↙️&nbsp;↖️&nbsp;↕️&nbsp;↔️&nbsp;↪️&nbsp;↩️&nbsp;⤴️&nbsp;⤵️&nbsp;🔀&nbsp;🔁&nbsp;🔂&nbsp;🔄&nbsp;🔃&nbsp;🎵&nbsp;🎶&nbsp;➕&nbsp;➖&nbsp;➗&nbsp;✖️&nbsp;♾&nbsp;💲&nbsp;💱&nbsp;™️&nbsp;©️&nbsp;®️&nbsp;〰️&nbsp;➰&nbsp;➿&nbsp;🔚&nbsp;🔙&nbsp;🔛&nbsp;🔝&nbsp;🔜&nbsp;✔️&nbsp;☑️&nbsp;🔘&nbsp;⚪️&nbsp;⚫️&nbsp;🔴&nbsp;🔵&nbsp;🔺&nbsp;🔻&nbsp;🔸&nbsp;🔹&nbsp;🔶&nbsp;🔷&nbsp;🔳&nbsp;🔲&nbsp;▪️&nbsp;▫️&nbsp;◾️&nbsp;◽️&nbsp;◼️&nbsp;◻️&nbsp;⬛️&nbsp;⬜️&nbsp;🔈&nbsp;🔇&nbsp;🔉&nbsp;🔊&nbsp;🔔&nbsp;🔕&nbsp;📣&nbsp;📢&nbsp;👁‍🗨&nbsp;💬&nbsp;💭&nbsp;🗯&nbsp;♠️&nbsp;♣️&nbsp;♥️&nbsp;♦️&nbsp;🃏&nbsp;🎴&nbsp;🀄️&nbsp;🕐&nbsp;🕑&nbsp;🕒&nbsp;🕓&nbsp;🕔&nbsp;🕕&nbsp;🕖&nbsp;🕗&nbsp;🕘&nbsp;🕙&nbsp;🕚&nbsp;🕛&nbsp;🕜&nbsp;🕝&nbsp;🕞&nbsp;🕟&nbsp;🕠&nbsp;🕡&nbsp;🕢&nbsp;🕣&nbsp;🕤&nbsp;🕥&nbsp;🕦&nbsp;🕧&nbsp;⬆&nbsp;↗&nbsp;➡&nbsp;↘&nbsp;⬇&nbsp;↙&nbsp;⬅&nbsp;↖&nbsp;↕&nbsp;↔&nbsp;↩&nbsp;↪&nbsp;⤴&nbsp;⤵&nbsp;🔃&nbsp;🔄&nbsp;🔙&nbsp;🔚&nbsp;🔛&nbsp;🔜&nbsp;🔝&nbsp;&nbsp;❄️&nbsp;☃️&nbsp;⛄️&nbsp;🌬&nbsp;💨&nbsp;💧&nbsp;</p><p style=\"line-height: 1;\"><br></p>', '0', '3', '1', '1', '2022-07-09 20:15:03', '2022-07-09 17:37:34');
INSERT INTO `ly_article` VALUES (1547817667610562562, '中国科学院：我国南方古人类与古印第安人基因组相近', '中国科学院：我国南方古人类与古印第安人基因组相近', '中国科学院：我国南方古人类与古印第安人基因组相近', '/ly/ly-images\\2022/07/15/9a4bedc5ba80d08dfb3654557f554822.jpg', 'rgba(79,91,74)', '<p style=\"text-indent: 36px; text-align: justify;\">记者从中国科学院获悉，中科院昆明动物所专家运用科学手段，对在云南蒙自发现的“马鹿洞人”头骨化石开展古DNA遗传学分析研究。结果发现，<em>中国南方的古人类和最早的美洲土著人类之间存在深度的古老祖源遗传联系。</em>这一科研成果于北京时间7月14日在国际学术期刊《当代生物学》上发表。</p><p style=\"text-indent: 36px; text-align: center;\"><img src=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/6306b64b0aacdf1bcab548d8fc8f056b.jpg\" alt=\"6306b64b0aacdf1bcab548d8fc8f056b.jpg\" data-href=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/6306b64b0aacdf1bcab548d8fc8f056b.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\">在云南蒙自出土的“马鹿洞人”，是1989年在一个露天遗址被发现的。当时经过联合科考发掘，出土了包括一个无边帽头盖骨、被火烧过的头盖骨和比较完整的近端股骨等人类化石30多件，以及大量马鹿、麝、猕猴、熊、野猪等动物化石。</p><p style=\"text-indent: 36px; text-align: justify;\"><em>中科院昆明动物研究所研究员&nbsp;吉学平：</em>头骨有独有特征，跟当时认为的晚期智人有一定区别的特征。</p><p style=\"text-indent: 36px; text-align: center;\"><img src=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/b1ea8c6d9591d962b988801fbc90bb72.jpg\" alt=\"b1ea8c6d9591d962b988801fbc90bb72.jpg\" data-href=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/b1ea8c6d9591d962b988801fbc90bb72.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\"><em>中科院昆明动物研究所研究员&nbsp;宿兵：</em>我们发现蒙自人（马鹿洞人）人群跟早期的，最早到达美洲的印第安人土著其实是有很深的渊源关系。</p><p style=\"text-indent: 36px; text-align: center;\"><img src=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/1365c84ad8eae491bf3f77cc700f0755.jpg\" alt=\"1365c84ad8eae491bf3f77cc700f0755.jpg\" data-href=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/15/1365c84ad8eae491bf3f77cc700f0755.jpg\" style=\"\"></p><p style=\"text-indent: 36px; text-align: justify;\">先前的科研结果显示，美洲土著人群印第安人的起源，分为东亚和中亚两个来源。根据用“马鹿洞人”的基因组和古印第安人的基因组进行比较，科研人员发现两者之间在基因组上有相近的特征。这就表明东亚人群在一万两千年前左右，确实可能从东亚大陆跨过白令海峡，最后迁徙到美洲，成为古印第安人的祖先。</p>', '0', '1', '1', '0', '2022-07-15 13:37:44', '2022-07-15 13:37:44');
INSERT INTO `ly_article` VALUES (1552664964232175618, '12', '12', '12', '/ly/ly-images/文章.png', 'rgba(255, 0, 0, 0)', '<p><img src=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/28/d9d867f59f0836e8815a6fecb835da2f.png\" alt=\"d9d867f59f0836e8815a6fecb835da2f.png\" data-href=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/28/d9d867f59f0836e8815a6fecb835da2f.png\" style=\"\"/></p>', '0', '1', '1', '0', '2022-07-28 22:39:09', '2022-07-28 22:39:09');
INSERT INTO `ly_article` VALUES (1552672113284931586, '1', '1', '1', '/ly/ly-images/文章.png', 'rgba(4,4,4)', '<p style=\"text-indent: 36px; text-align: center;\"><img src=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/28/4d50f364fd28cc8370fa9f8b974761fb.png\" alt=\"4d50f364fd28cc8370fa9f8b974761fb.png\" data-href=\"http://localhost/zkc-dev/ly/ly-images\\2022/07/28/4d50f364fd28cc8370fa9f8b974761fb.png\" style=\"\"></p>', '0', '1', '1', NULL, '2022-07-28 23:07:34', '2022-07-28 23:07:34');

-- ----------------------------
-- Table structure for ly_article_browse
-- ----------------------------
DROP TABLE IF EXISTS `ly_article_browse`;
CREATE TABLE `ly_article_browse`  (
  `article_id` bigint(30) NULL DEFAULT NULL COMMENT '文章id',
  `browse_id` bigint(30) NULL DEFAULT NULL COMMENT '浏览日志id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章浏览日志关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_article_browse
-- ----------------------------

-- ----------------------------
-- Table structure for ly_article_classify
-- ----------------------------
DROP TABLE IF EXISTS `ly_article_classify`;
CREATE TABLE `ly_article_classify`  (
  `article_id` bigint(20) NULL DEFAULT NULL COMMENT '文章id',
  `classify_id` bigint(20) NULL DEFAULT NULL COMMENT '分类id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_article_classify
-- ----------------------------
INSERT INTO `ly_article_classify` VALUES (1536271379299622913, 1);
INSERT INTO `ly_article_classify` VALUES (1536271527991894018, 1);
INSERT INTO `ly_article_classify` VALUES (1542512386559197186, 4);
INSERT INTO `ly_article_classify` VALUES (1541974057610674177, 1);
INSERT INTO `ly_article_classify` VALUES (1536271301700804609, 2);
INSERT INTO `ly_article_classify` VALUES (1541591168259756034, 2);
INSERT INTO `ly_article_classify` VALUES (1545336911646715905, 1);
INSERT INTO `ly_article_classify` VALUES (1545703699827941377, 2);
INSERT INTO `ly_article_classify` VALUES (1547817667610562562, 1);
INSERT INTO `ly_article_classify` VALUES (1552672113284931586, 1);
INSERT INTO `ly_article_classify` VALUES (1552664964232175618, 2);

-- ----------------------------
-- Table structure for ly_article_label
-- ----------------------------
DROP TABLE IF EXISTS `ly_article_label`;
CREATE TABLE `ly_article_label`  (
  `article_id` bigint(20) NULL DEFAULT NULL,
  `label_id` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_article_label
-- ----------------------------

-- ----------------------------
-- Table structure for ly_browse_log
-- ----------------------------
DROP TABLE IF EXISTS `ly_browse_log`;
CREATE TABLE `ly_browse_log`  (
  `browse_log_id` bigint(20) NOT NULL COMMENT '浏览量日志id',
  `create_date` date NULL DEFAULT NULL COMMENT '创建时间',
  `browse_ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问ip',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`browse_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章浏览日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_browse_log
-- ----------------------------

-- ----------------------------
-- Table structure for ly_city
-- ----------------------------
DROP TABLE IF EXISTS `ly_city`;
CREATE TABLE `ly_city`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `province_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份代码',
  `city_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市代码',
  `city_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市名称',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改者',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  `is_deleted` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否已删除(0:否 1:是)',
  `delete_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除者',
  `delete_date` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 334 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '城市表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ly_city
-- ----------------------------
INSERT INTO `ly_city` VALUES (1, '110000', '110100', '市辖区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (2, '110000', '110200', '县', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (3, '120000', '120100', '市辖区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (4, '120000', '120200', '县', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (5, '130000', '130100', '石家庄市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (6, '130000', '130200', '唐山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (7, '130000', '130300', '秦皇岛', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (8, '130000', '130400', '邯郸市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (9, '130000', '130500', '邢台市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (10, '130000', '130600', '保定市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (11, '130000', '130700', '张家口', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (12, '130000', '130800', '承德市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (13, '130000', '130900', '沧州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (14, '130000', '131000', '廊坊市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (15, '130000', '131100', '衡水市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (16, '140000', '140100', '太原市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (17, '140000', '140200', '大同市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (18, '140000', '140300', '阳泉市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (19, '140000', '140400', '长治市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (20, '140000', '140500', '晋城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (21, '140000', '140600', '朔州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (22, '140000', '140700', '晋中市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (23, '140000', '140800', '运城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (24, '140000', '140900', '忻州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (25, '140000', '141000', '临汾市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (26, '140000', '141100', '吕梁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (27, '150000', '150100', '呼和浩特市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (28, '150000', '150200', '包头市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (29, '150000', '150300', '乌海市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (30, '150000', '150400', '赤峰市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (31, '150000', '150500', '通辽市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (32, '150000', '150600', '鄂尔多斯市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (33, '150000', '150700', '呼伦贝尔', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (34, '150000', '150800', '巴彦淖尔市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (35, '150000', '150900', '乌兰察布市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (36, '150000', '152200', '兴安盟', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (37, '150000', '152500', '锡林郭勒盟', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (38, '150000', '152900', '阿拉善盟', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (39, '210000', '210100', '沈阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (40, '210000', '210200', '大连市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (41, '210000', '210300', '鞍山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (42, '210000', '210400', '抚顺市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (43, '210000', '210500', '本溪市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (44, '210000', '210600', '丹东市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (45, '210000', '210700', '锦州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (46, '210000', '210800', '营口市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (47, '210000', '210900', '阜新市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (48, '210000', '211000', '辽阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (49, '210000', '211100', '盘锦市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (50, '210000', '211200', '铁岭市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (51, '210000', '211300', '朝阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (52, '210000', '211400', '葫芦岛市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (53, '220000', '220100', '长春市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (54, '220000', '220200', '吉林市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (55, '220000', '220300', '四平市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (56, '220000', '220400', '辽源市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (57, '220000', '220500', '通化市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (58, '220000', '220600', '白山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (59, '220000', '220700', '松原市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (60, '220000', '220800', '白城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (61, '220000', '222400', '延边朝鲜族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (62, '230000', '230100', '哈尔滨市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (63, '230000', '230200', '齐齐哈尔市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (64, '230000', '230300', '鸡西市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (65, '230000', '230400', '鹤岗市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (66, '230000', '230500', '双鸭山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (67, '230000', '230600', '大庆市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (68, '230000', '230700', '伊春市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (69, '230000', '230800', '佳木斯市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (70, '230000', '230900', '七台河市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (71, '230000', '231000', '牡丹江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (72, '230000', '231100', '黑河市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (73, '230000', '231200', '绥化市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (74, '230000', '232700', '大兴安岭', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (75, '310000', '310100', '市辖区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (76, '310000', '310200', '县', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (77, '320000', '320100', '南京市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (78, '320000', '320200', '无锡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (79, '320000', '320300', '徐州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (80, '320000', '320400', '常州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (81, '320000', '320500', '苏州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (82, '320000', '320600', '南通市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (83, '320000', '320700', '连云港市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (84, '320000', '320800', '淮安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (85, '320000', '320900', '盐城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (86, '320000', '321000', '扬州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (87, '320000', '321100', '镇江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (88, '320000', '321200', '泰州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (89, '320000', '321300', '宿迁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (90, '330000', '330100', '杭州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (91, '330000', '330200', '宁波市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (92, '330000', '330300', '温州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (93, '330000', '330400', '嘉兴市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (94, '330000', '330500', '湖州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (95, '330000', '330600', '绍兴市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (96, '330000', '330700', '金华市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (97, '330000', '330800', '衢州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (98, '330000', '330900', '舟山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (99, '330000', '331000', '台州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (100, '330000', '331100', '丽水市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (101, '340000', '340100', '合肥市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (102, '340000', '340200', '芜湖市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (103, '340000', '340300', '蚌埠市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (104, '340000', '340400', '淮南市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (105, '340000', '340500', '马鞍山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (106, '340000', '340600', '淮北市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (107, '340000', '340700', '铜陵市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (108, '340000', '340800', '安庆市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (109, '340000', '341000', '黄山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (110, '340000', '341100', '滁州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (111, '340000', '341200', '阜阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (112, '340000', '341300', '宿州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (113, '340000', '341400', '巢湖市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (114, '340000', '341500', '六安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (115, '340000', '341600', '亳州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (116, '340000', '341700', '池州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (117, '340000', '341800', '宣城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (118, '350000', '350100', '福州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (119, '350000', '350200', '厦门市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (120, '350000', '350300', '莆田市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (121, '350000', '350400', '三明市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (122, '350000', '350500', '泉州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (123, '350000', '350600', '漳州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (124, '350000', '350700', '南平市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (125, '350000', '350800', '龙岩市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (126, '350000', '350900', '宁德市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (127, '360000', '360100', '南昌市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (128, '360000', '360200', '景德镇市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (129, '360000', '360300', '萍乡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (130, '360000', '360400', '九江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (131, '360000', '360500', '新余市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (132, '360000', '360600', '鹰潭市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (133, '360000', '360700', '赣州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (134, '360000', '360800', '吉安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (135, '360000', '360900', '宜春市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (136, '360000', '361000', '抚州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (137, '360000', '361100', '上饶市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (138, '370000', '370100', '济南市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (139, '370000', '370200', '青岛市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (140, '370000', '370300', '淄博市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (141, '370000', '370400', '枣庄市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (142, '370000', '370500', '东营市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (143, '370000', '370600', '烟台市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (144, '370000', '370700', '潍坊市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (145, '370000', '370800', '济宁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (146, '370000', '370900', '泰安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (147, '370000', '371000', '威海市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (148, '370000', '371100', '日照市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (149, '370000', '371200', '莱芜市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (150, '370000', '371300', '临沂市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (151, '370000', '371400', '德州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (152, '370000', '371500', '聊城市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (153, '370000', '371600', '滨州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (154, '370000', '371700', '荷泽市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (155, '410000', '410100', '郑州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (156, '410000', '410200', '开封市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (157, '410000', '410300', '洛阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (158, '410000', '410400', '平顶山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (159, '410000', '410500', '安阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (160, '410000', '410600', '鹤壁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (161, '410000', '410700', '新乡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (162, '410000', '410800', '焦作市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (163, '410000', '410900', '濮阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (164, '410000', '411000', '许昌市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (165, '410000', '411100', '漯河市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (166, '410000', '411200', '三门峡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (167, '410000', '411300', '南阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (168, '410000', '411400', '商丘市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (169, '410000', '411500', '信阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (170, '410000', '411600', '周口市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (171, '410000', '411700', '驻马店市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (172, '420000', '420100', '武汉市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (173, '420000', '420200', '黄石市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (174, '420000', '420300', '十堰市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (175, '420000', '420500', '宜昌市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (176, '420000', '420600', '襄樊市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (177, '420000', '420700', '鄂州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (178, '420000', '420800', '荆门市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (179, '420000', '420900', '孝感市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (180, '420000', '421000', '荆州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (181, '420000', '421100', '黄冈市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (182, '420000', '421200', '咸宁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (183, '420000', '421300', '随州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (184, '420000', '422800', '恩施土家族苗族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (185, '420000', '429000', '省直辖行政单位', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (186, '430000', '430100', '长沙市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (187, '430000', '430200', '株洲市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (188, '430000', '430300', '湘潭市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (189, '430000', '430400', '衡阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (190, '430000', '430500', '邵阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (191, '430000', '430600', '岳阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (192, '430000', '430700', '常德市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (193, '430000', '430800', '张家界市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (194, '430000', '430900', '益阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (195, '430000', '431000', '郴州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (196, '430000', '431100', '永州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (197, '430000', '431200', '怀化市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (198, '430000', '431300', '娄底市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (199, '430000', '433100', '湘西土家族苗族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (200, '440000', '440100', '广州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (201, '440000', '440200', '韶关市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (202, '440000', '440300', '深圳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (203, '440000', '440400', '珠海市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (204, '440000', '440500', '汕头市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (205, '440000', '440600', '佛山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (206, '440000', '440700', '江门市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (207, '440000', '440800', '湛江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (208, '440000', '440900', '茂名市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (209, '440000', '441200', '肇庆市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (210, '440000', '441300', '惠州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (211, '440000', '441400', '梅州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (212, '440000', '441500', '汕尾市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (213, '440000', '441600', '河源市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (214, '440000', '441700', '阳江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (215, '440000', '441800', '清远市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (216, '440000', '441900', '东莞市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (217, '440000', '442000', '中山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (218, '440000', '445100', '潮州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (219, '440000', '445200', '揭阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (220, '440000', '445300', '云浮市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (221, '450000', '450100', '南宁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (222, '450000', '450200', '柳州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (223, '450000', '450300', '桂林市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (224, '450000', '450400', '梧州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (225, '450000', '450500', '北海市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (226, '450000', '450600', '防城港市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (227, '450000', '450700', '钦州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (228, '450000', '450800', '贵港市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (229, '450000', '450900', '玉林市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (230, '450000', '451000', '百色市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (231, '450000', '451100', '贺州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (232, '450000', '451200', '河池市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (233, '450000', '451300', '来宾市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (234, '450000', '451400', '崇左市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (235, '460000', '460100', '海口市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (236, '460000', '460200', '三亚市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (237, '460000', '469000', '省直辖县级行政单位', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (238, '500000', '500100', '市辖区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (239, '500000', '500200', '县', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (240, '500000', '500300', '市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (241, '510000', '510100', '成都市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (242, '510000', '510300', '自贡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (243, '510000', '510400', '攀枝花市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (244, '510000', '510500', '泸州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (245, '510000', '510600', '德阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (246, '510000', '510700', '绵阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (247, '510000', '510800', '广元市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (248, '510000', '510900', '遂宁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (249, '510000', '511000', '内江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (250, '510000', '511100', '乐山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (251, '510000', '511300', '南充市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (252, '510000', '511400', '眉山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (253, '510000', '511500', '宜宾市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (254, '510000', '511600', '广安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (255, '510000', '511700', '达州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (256, '510000', '511800', '雅安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (257, '510000', '511900', '巴中市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (258, '510000', '512000', '资阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (259, '510000', '513200', '阿坝藏族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (260, '510000', '513300', '甘孜藏族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (261, '510000', '513400', '凉山彝族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (262, '520000', '520100', '贵阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (263, '520000', '520200', '六盘水市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (264, '520000', '520300', '遵义市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (265, '520000', '520400', '安顺市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (266, '520000', '522200', '铜仁', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (267, '520000', '522300', '黔西南布依族苗族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (268, '520000', '522400', '毕节区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (269, '520000', '522600', '黔东南苗族侗族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (270, '520000', '522700', '黔南布依族苗族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (271, '530000', '530100', '昆明市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (272, '530000', '530300', '曲靖市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (273, '530000', '530400', '玉溪市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (274, '530000', '530500', '保山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (275, '530000', '530600', '昭通市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (276, '530000', '530700', '丽江市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (277, '530000', '530900', '临沧市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (278, '530000', '532300', '楚雄彝族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (279, '530000', '532500', '红河哈尼族彝族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (280, '540000', '540100', '拉萨市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (281, '540000', '542100', '昌都', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (282, '540000', '542200', '山南', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (283, '540000', '542300', '日喀则', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (284, '540000', '542400', '那曲地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (285, '540000', '542500', '阿里地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (286, '540000', '542600', '林芝地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (287, '610000', '610100', '西安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (288, '610000', '610200', '铜川市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (289, '610000', '610300', '宝鸡市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (290, '610000', '610400', '咸阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (291, '610000', '610500', '渭南市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (292, '610000', '610600', '延安市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (293, '610000', '610700', '汉中市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (294, '610000', '610800', '榆林市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (295, '610000', '610900', '安康市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (296, '610000', '611000', '商洛市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (297, '620000', '620100', '兰州市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (298, '620000', '620200', '嘉峪关市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (299, '620000', '620300', '金昌市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (300, '620000', '620400', '白银市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (301, '620000', '620500', '天水市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (302, '620000', '620600', '武威市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (303, '620000', '620700', '张掖市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (304, '620000', '620800', '平凉市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (305, '620000', '620900', '酒泉市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (306, '620000', '621000', '庆阳市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (307, '620000', '621100', '定西市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (308, '620000', '621200', '陇南市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (309, '620000', '622900', '临夏回族自治州', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (310, '620000', '623000', '甘南藏族自治州', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (311, '630000', '630100', '西宁市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (312, '630000', '632100', '海东', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (313, '630000', '632200', '海北', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (314, '630000', '632300', '黄南', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (315, '630000', '632500', '海南', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (316, '630000', '632600', '果洛', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (317, '630000', '632700', '玉树', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (318, '630000', '632800', '海西', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (319, '640000', '640100', '银川市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (320, '640000', '640200', '石嘴山市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (321, '640000', '640300', '吴忠市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (322, '640000', '640400', '固原市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (323, '640000', '640500', '中卫市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (324, '650000', '650100', '乌鲁木齐市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (325, '650000', '650200', '克拉玛依市', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (326, '650000', '652100', '吐鲁番地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (327, '650000', '652200', '哈密地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (328, '650000', '652300', '昌吉回族', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (329, '650000', '652700', '博尔塔拉', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (330, '650000', '652800', '巴音郭楞', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (331, '650000', '652900', '阿克苏地区', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (332, '710000', '710100', '台湾', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (333, '810000', '810100', '香港', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);
INSERT INTO `ly_city` VALUES (334, '820000', '820100', '澳门', '1', '2021-08-24 19:07:44', '1', '2021-08-24 19:07:44', '0', NULL, NULL);

-- ----------------------------
-- Table structure for ly_classify
-- ----------------------------
DROP TABLE IF EXISTS `ly_classify`;
CREATE TABLE `ly_classify`  (
  `classify_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `classify_parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父级id',
  `classify_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名字',
  `visible` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否显示 0 显示  1 隐藏',
  `classify_type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型 A 网站分类  B首页顶部菜单 C 专栏  ',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`classify_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_classify
-- ----------------------------
INSERT INTO `ly_classify` VALUES (1, 0, '科技', '0', 'A', '2022-06-09 16:52:00', '2022-06-09 16:52:02');
INSERT INTO `ly_classify` VALUES (2, 0, '娱乐', '0', 'A', '2022-06-09 16:52:04', '2022-06-09 16:52:07');
INSERT INTO `ly_classify` VALUES (3, 0, '美食', '0', 'A', '2022-06-09 16:52:09', '2022-06-09 16:52:11');
INSERT INTO `ly_classify` VALUES (4, 0, '汽车', '0', 'A', '2022-06-09 16:52:14', '2022-06-09 16:52:16');
INSERT INTO `ly_classify` VALUES (5, 0, '综艺', '0', 'A', '2022-06-09 16:52:18', '2022-06-09 16:52:20');
INSERT INTO `ly_classify` VALUES (6, 0, '咨询', '0', 'A', '2022-06-09 16:52:24', '2022-06-09 16:52:26');
INSERT INTO `ly_classify` VALUES (7, 0, 'JAVA', '0', 'A', '2022-06-09 16:52:29', '2022-06-09 16:52:32');
INSERT INTO `ly_classify` VALUES (8, 0, 'Python', '0', 'A', '2022-06-29 10:42:49', '2022-06-29 10:42:51');
INSERT INTO `ly_classify` VALUES (9, 0, 'Vue', '0', 'A', '2022-06-29 10:42:53', '2022-06-29 10:42:55');

-- ----------------------------
-- Table structure for ly_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `ly_dict_data`;
CREATE TABLE `ly_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 1 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_dict_data
-- ----------------------------
INSERT INTO `ly_dict_data` VALUES (1, 1, '男', '0', 'user_sex', '', 'default', '0', '2022-02-24 00:07:41', '2022-07-12 21:06:47', '性别男');
INSERT INTO `ly_dict_data` VALUES (2, 2, '女', '1', 'user_sex', '', 'default', '0', '2022-02-24 00:07:41', '2022-07-12 21:06:50', '性别女');
INSERT INTO `ly_dict_data` VALUES (3, 3, '未知', '2', 'user_sex', '', 'warning', '0', '2022-02-24 00:07:41', '2022-07-12 21:06:53', '性别未知');
INSERT INTO `ly_dict_data` VALUES (4, 1, '系统用户', '0', 'user_type', NULL, 'default', '0', '2022-07-13 15:55:41', '2022-07-13 15:55:43', '系统用户');
INSERT INTO `ly_dict_data` VALUES (5, 2, '注册用户', '1', 'user_type', NULL, 'default', '0', '2022-07-13 15:56:43', '2022-07-13 15:56:46', '注册用户');
INSERT INTO `ly_dict_data` VALUES (6, 1, '正常', '0', 'user_status', NULL, 'default', '0', '2022-07-13 23:12:30', '2022-07-13 23:12:33', '状态正常');
INSERT INTO `ly_dict_data` VALUES (7, 2, '停用', '1', 'user_status', NULL, 'danger', '0', '2022-07-13 23:13:05', '2022-07-13 23:13:07', '状态停用');
INSERT INTO `ly_dict_data` VALUES (8, 1, '启用', '0', 'role_status', NULL, 'default', '0', '2022-07-13 23:35:04', '2022-07-13 23:35:06', '状态启用');
INSERT INTO `ly_dict_data` VALUES (9, 2, '停用', '1', 'role_status', NULL, 'danger', '0', '2022-07-13 23:36:03', '2022-07-13 23:36:05', '状态停用');
INSERT INTO `ly_dict_data` VALUES (10, 1, '暂存', '0', 'article_status', NULL, 'warning', '0', '2022-07-14 21:48:12', '2022-07-14 21:48:14', '状态暂存');
INSERT INTO `ly_dict_data` VALUES (11, 2, '正常', '1', 'article_status', NULL, 'default', '0', '2022-07-14 21:48:38', '2022-07-14 21:48:40', '状态正常');
INSERT INTO `ly_dict_data` VALUES (12, 3, '隐藏', '2', 'article_status', NULL, 'info', '0', '2022-07-14 21:48:55', '2022-07-14 21:48:58', '状态隐藏');
INSERT INTO `ly_dict_data` VALUES (13, 4, '违规', '3', 'article_status', NULL, 'danger', '0', '2022-07-14 21:49:12', '2022-07-14 21:49:15', '状态违规');
INSERT INTO `ly_dict_data` VALUES (15, 1, '默认', 'default', 'list_class', NULL, 'default', '0', '2022-07-14 22:18:45', '2022-07-14 22:19:02', '标签回显（默认）');
INSERT INTO `ly_dict_data` VALUES (17, 2, '成功', 'success', 'list_class', NULL, 'success', '0', '2022-07-14 22:18:50', '2022-07-14 22:19:09', '标签回显（成功）');
INSERT INTO `ly_dict_data` VALUES (18, 3, '信息', 'info', 'list_class', NULL, 'info', '0', '2022-07-14 22:18:53', '2022-07-14 22:19:12', '标签回显（信息）');
INSERT INTO `ly_dict_data` VALUES (19, 4, '警告', 'warning', 'list_class', NULL, 'warning', '0', '2022-07-14 22:18:56', '2022-07-14 22:19:14', '标签回显（警告）');
INSERT INTO `ly_dict_data` VALUES (20, 6, '危险', 'danger', 'list_class', NULL, 'danger', '0', '2022-07-14 22:18:59', '2022-07-14 22:19:17', '标签回显（危险）');
INSERT INTO `ly_dict_data` VALUES (21, 1, '正常', '0', 'status', NULL, 'default', '0', '2022-08-26 18:56:21', '2022-08-26 18:56:24', '状态正常');
INSERT INTO `ly_dict_data` VALUES (22, 2, '停用', '1', 'status', NULL, 'danger', '0', '2022-08-26 18:57:28', '2022-09-10 00:28:30', '状态停用');
INSERT INTO `ly_dict_data` VALUES (23, 100, '1', '1', 'ceshi1', NULL, 'danger', '0', '2022-09-10 00:18:21', '2022-09-10 00:18:21', '1');
INSERT INTO `ly_dict_data` VALUES (24, 102, 'e', 'e', 'tianxia', 'color:#fff', 'danger', '1', '2022-09-10 00:21:37', '2022-09-10 00:27:51', 'eeeeeee');
INSERT INTO `ly_dict_data` VALUES (28, 0, 'q', 'q', 'tianxia', NULL, 'default', '0', '2022-09-10 00:36:38', '2022-09-10 00:36:38', 'q');
INSERT INTO `ly_dict_data` VALUES (29, 0, 'w4', 'w4', 'tianxia', 'color:red', 'success', '0', '2022-09-10 00:36:43', '2022-09-10 00:41:48', 'w4');
INSERT INTO `ly_dict_data` VALUES (30, 0, 'e', 'e', 'tianxia', NULL, 'default', '0', '2022-09-10 00:36:48', '2022-09-10 00:36:48', 'e');

-- ----------------------------
-- Table structure for ly_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `ly_dict_type`;
CREATE TABLE `ly_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_dict_type
-- ----------------------------
INSERT INTO `ly_dict_type` VALUES (1, '用户性别', 'user_sex', '0', '2022-07-12 21:04:21', '2022-07-12 21:04:24', '用户性别列表');
INSERT INTO `ly_dict_type` VALUES (2, '用户类型', 'user_type', '0', '2022-07-13 15:55:06', '2022-07-13 15:55:08', '用户类型列表');
INSERT INTO `ly_dict_type` VALUES (3, '用户状态', 'user_status', '0', '2022-07-13 23:11:17', '2022-07-13 23:11:20', '用户状态列表');
INSERT INTO `ly_dict_type` VALUES (4, '角色状态', 'role_status', '0', '2022-07-13 23:36:40', '2022-07-13 23:36:42', '角色状态列表');
INSERT INTO `ly_dict_type` VALUES (5, '文章状态', 'article_status', '0', '2022-07-14 21:47:39', '2022-07-14 21:47:41', '文章状态列表');
INSERT INTO `ly_dict_type` VALUES (6, '标签回显样式', 'list_class', '0', '2022-07-14 22:22:25', '2022-07-14 22:22:28', '标签回显列表');
INSERT INTO `ly_dict_type` VALUES (7, '字典状态', 'status', '0', '2022-08-26 17:22:18', '2022-08-26 17:22:21', '字典状态列表');
INSERT INTO `ly_dict_type` VALUES (18, 'ceshi1', 'ceshi1', '0', '2022-09-08 22:10:08', '2022-09-08 22:10:08', 'ceshi1');
INSERT INTO `ly_dict_type` VALUES (19, 'ceshi2', 'ceshi2', '0', '2022-09-08 22:10:15', '2022-09-08 22:10:15', 'ceshi2');
INSERT INTO `ly_dict_type` VALUES (25, '天下', 'tianxia', '0', '2022-09-10 00:20:28', '2022-09-10 00:20:28', '天下');

-- ----------------------------
-- Table structure for ly_label
-- ----------------------------
DROP TABLE IF EXISTS `ly_label`;
CREATE TABLE `ly_label`  (
  `label_id` int(20) NOT NULL COMMENT '标签ID',
  `label_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`label_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ly_label
-- ----------------------------

-- ----------------------------
-- Table structure for ly_menu
-- ----------------------------
DROP TABLE IF EXISTS `ly_menu`;
CREATE TABLE `ly_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` bigint(20) NOT NULL COMMENT '父编号',
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称/功能名称',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由地址',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单状态（1显示 0隐藏）',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `sort` bigint(20) NULL DEFAULT NULL COMMENT '排序字段（越小越靠前）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单类型（ A 目录 B 菜单  C表单按钮）',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开方式（tab页签 new新窗口）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1546364785459765277 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_menu
-- ----------------------------
INSERT INTO `ly_menu` VALUES (1, 0, '首页', '/admin', '1', 'el-icon-s-home', 1, 'A', 'tab', '', '2021-12-12 00:44:15', '首页');
INSERT INTO `ly_menu` VALUES (2, 0, '系统管理', '/system', '1', 'el-icon-setting', 2, 'A', 'tab', '', '2021-12-12 00:44:15', NULL);
INSERT INTO `ly_menu` VALUES (3, 0, '系统监控', '/monitor', '1', 'el-icon-monitor', 3, 'A', 'tab', '', '2021-12-12 00:44:15', NULL);
INSERT INTO `ly_menu` VALUES (4, 0, '凌云官网', 'https://meishibiexuejava.cn', '1', 'el-icon-s-promotion', 4, 'B', 'new', '', '2021-12-12 00:44:15', NULL);
INSERT INTO `ly_menu` VALUES (5, 2, '用户管理', '/system/user', '1', 'el-icon-s-custom', 5, 'B', 'tab', 'system:user:getuserlist', '2022-09-12 23:24:58', '用户列表');
INSERT INTO `ly_menu` VALUES (6, 2, '文章管理', '/system/article', '1', 'el-icon-document', 6, 'B', 'tab', 'system:article:articlelist', '2022-09-13 00:14:06', '文章管理');
INSERT INTO `ly_menu` VALUES (19, 5, '删除用户', NULL, '', NULL, 7, 'C', NULL, 'system:user:deletuserbyid', '2022-09-12 23:54:50', '删除用户');
INSERT INTO `ly_menu` VALUES (21, 5, '修改用户', NULL, '', NULL, 8, 'C', NULL, 'system:user:updateuser', '2022-09-13 00:04:22', '修改用户');
INSERT INTO `ly_menu` VALUES (22, 5, '批量删除用户', NULL, '', NULL, 9, 'C', NULL, 'system:user:deletuserbyids', '2022-09-12 23:55:01', '批量删除用户');
INSERT INTO `ly_menu` VALUES (23, 5, '添加用户', NULL, '', NULL, 10, 'C', NULL, 'system:user:adduser', '2022-09-13 00:05:36', '添加用户');
INSERT INTO `ly_menu` VALUES (24, 5, '更具ID查询用户', NULL, '', NULL, 11, 'C', NULL, 'system:user:selectuserbyid', '2022-09-12 23:47:46', '更具ID查询用户');
INSERT INTO `ly_menu` VALUES (25, 2, '角色管理', '/system/role', '1', 'el-icon-user', 12, 'B', 'tab', 'system:role:rolelist', '2022-09-12 00:56:09', '角色列表');
INSERT INTO `ly_menu` VALUES (26, 3, '服务监控', '/monitor/server', '1', 'el-icon-document', 13, 'B', 'tab', 'monitor:server', '2022-03-12 00:36:49', '服务监控');
INSERT INTO `ly_menu` VALUES (27, 5, '用户导出excel', NULL, '', NULL, 14, 'C', NULL, 'system:user:userexportexcel', '2022-04-23 22:51:48', '用户导出excel');
INSERT INTO `ly_menu` VALUES (28, 25, '删除角色', NULL, '', NULL, 15, 'C', NULL, 'system:role:deleterolebyid', '2022-09-12 01:12:59', '删除角色');
INSERT INTO `ly_menu` VALUES (29, 25, '批量删除角色', NULL, '', NULL, 16, 'C', NULL, 'system:role:deleterolebyids', '2022-09-12 01:13:30', '批量删除角色');
INSERT INTO `ly_menu` VALUES (30, 25, '根据ID查询角色', NULL, '', NULL, 17, 'C', NULL, 'system:role:selectrolebyid', '2022-09-12 01:25:03', '根据ID查询角色');
INSERT INTO `ly_menu` VALUES (31, 25, '修改角色', NULL, '', NULL, 18, 'C', NULL, 'system:role:updaterole', '2022-04-23 23:00:20', '修改角色');
INSERT INTO `ly_menu` VALUES (32, 25, '修改角色状态', NULL, '', NULL, 19, 'C', NULL, 'system:role:updaterolestatus', '2022-04-23 23:01:20', '修改角色状态');
INSERT INTO `ly_menu` VALUES (33, 25, '添加角色', NULL, '', NULL, 20, 'C', NULL, 'system:role:addrole', '2022-04-23 23:02:42', '添加角色');
INSERT INTO `ly_menu` VALUES (34, 25, '角色信息导出为Excel', NULL, '', NULL, 21, 'C', NULL, 'system:role:roleexportexcel', '2022-09-12 22:58:40', '角色导出excel');
INSERT INTO `ly_menu` VALUES (35, 6, '根据id查看文章', NULL, '', NULL, 22, 'C', NULL, 'system:article:getarticlebyid', '2022-06-09 15:52:56', '根据id查看文章');
INSERT INTO `ly_menu` VALUES (36, 6, '修改文章信息', NULL, '', NULL, 23, 'C', NULL, 'system:article:updatearticle', '2022-06-09 15:53:57', '修改文章信息');
INSERT INTO `ly_menu` VALUES (37, 6, '文章单个删除', NULL, '', NULL, 24, 'C', NULL, 'system:article:deletearticlebyid', '2022-06-09 15:55:13', '文章单个删除');
INSERT INTO `ly_menu` VALUES (38, 6, '添加文章', NULL, '', NULL, 25, 'C', NULL, 'system:article:insertarticle', '2022-06-12 18:45:26', '添加文章');
INSERT INTO `ly_menu` VALUES (39, 6, '文章多个删除', NULL, '', NULL, 26, 'C', NULL, 'system:article:deletearticlebyids', '2022-06-28 22:33:13', '文章多个删除');
INSERT INTO `ly_menu` VALUES (40, 6, '轮播图状态修改', NULL, '', NULL, 27, 'C', NULL, 'system:article:updateisshuffling', '2022-07-09 19:02:38', '轮播图状态修改');
INSERT INTO `ly_menu` VALUES (41, 6, '推广状态修改', NULL, '', NULL, 28, 'C', NULL, 'system:article:updateispopularize', '2022-07-09 19:03:07', '推广状态修改');
INSERT INTO `ly_menu` VALUES (42, 2, '菜单管理', '/system/menu', '1', 'el-icon-menu', 29, 'B', 'tab', 'system:menu:querymenulist', '2022-07-09 21:01:42', '功能管理');
INSERT INTO `ly_menu` VALUES (43, 42, '添加菜单', NULL, '', NULL, 30, 'C', NULL, 'system:menu:addmenu', '2022-07-10 23:32:20', '添加菜单');
INSERT INTO `ly_menu` VALUES (44, 42, '修改菜单', NULL, '', NULL, 31, 'C', NULL, 'system:menu:updatemenu', '2022-07-11 13:09:22', '修改菜单');
INSERT INTO `ly_menu` VALUES (45, 42, '删除菜单', NULL, '', NULL, 32, 'C', NULL, 'system:menu:deletemenu', '2022-07-11 23:54:02', '删除菜单');
INSERT INTO `ly_menu` VALUES (46, 42, '更具菜单id查询菜单信息', NULL, '', NULL, 33, 'C', NULL, 'system:menu:getmenubyId', '2022-07-11 23:55:08', '更具菜单id查询菜单信息');
INSERT INTO `ly_menu` VALUES (1546364785459765262, 2, '字典管理', '/system/dict', '1', 'el-icon-s-management', 30, 'B', 'tab', 'system:dict:dictlist', '2022-08-26 17:46:43', '字典管理');
INSERT INTO `ly_menu` VALUES (1546364785459765263, 2, '日志管理', '/system/logo', '1', 'el-icon-tickets', 31, 'B', 'tab', NULL, '2022-07-18 10:46:56', '日志管理');
INSERT INTO `ly_menu` VALUES (1546364785459765264, 2, '字典数据', '/system/dict-data', '0', 'el-icon-s-management', 32, 'B', 'tab', 'system:dict:dictdatalist', '2022-08-30 21:13:47', NULL);
INSERT INTO `ly_menu` VALUES (1546364785459765265, 1546364785459765262, '更具id查询字典类型', NULL, NULL, NULL, 33, 'C', NULL, 'system:dict:selectdictbyid', '2022-08-30 18:06:30', '更具id查询字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765266, 1546364785459765264, '查询所有字典类型', NULL, NULL, NULL, 34, 'C', NULL, 'system:dict:selectdicttypeall', '2022-08-30 21:45:53', '查询所有字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765267, 1546364785459765262, '添加字典类型', NULL, NULL, NULL, 34, 'C', NULL, 'system:dict:adddicttype', '2022-09-07 22:58:31', '添加字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765268, 1546364785459765262, '修改字典类型', NULL, NULL, NULL, 35, 'C', NULL, 'system:dict:updatedicttype', '2022-09-07 23:27:03', '修改字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765270, 1546364785459765262, '批量删除字典类型', NULL, NULL, NULL, 36, 'C', NULL, 'system:dict:deletedicttypebyids', '2022-09-08 22:01:55', '批量删除字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765271, 1546364785459765262, '删除字典类型', NULL, NULL, NULL, 37, 'C', NULL, 'system:dict:deletedicttypebyid', '2022-09-08 22:20:00', '删除字典类型');
INSERT INTO `ly_menu` VALUES (1546364785459765272, 1546364785459765264, '添加字典数据', NULL, NULL, NULL, 38, 'C', NULL, 'system:dict:adddictdata', '2022-09-08 23:57:47', '添加字典数据');
INSERT INTO `ly_menu` VALUES (1546364785459765273, 1546364785459765264, '修改字典数据', NULL, NULL, NULL, 39, 'C', NULL, 'system:dict:updatedictdata', '2022-09-08 23:58:08', '修改字典数据');
INSERT INTO `ly_menu` VALUES (1546364785459765274, 1546364785459765264, '根据字典数据编码查询字典数据', NULL, NULL, NULL, 40, 'C', NULL, 'system:dict:selectDictdatabyid', '2022-09-08 23:58:33', '根据字典数据编码查询字典数据');
INSERT INTO `ly_menu` VALUES (1546364785459765275, 1546364785459765264, '删除字典数据', NULL, NULL, NULL, 41, 'C', NULL, 'system:dict:deletedictdatabyid', '2022-09-08 23:58:56', '删除字典数据');
INSERT INTO `ly_menu` VALUES (1546364785459765276, 1546364785459765264, '批量删除字典数据', NULL, NULL, NULL, 42, 'C', NULL, 'system:dict:deletedictdatabyids', '2022-09-08 23:59:19', '批量删除字典数据');

-- ----------------------------
-- Table structure for ly_province
-- ----------------------------
DROP TABLE IF EXISTS `ly_province`;
CREATE TABLE `ly_province`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `province_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份代码',
  `province_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份名称',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改者',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  `is_deleted` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否已删除(0:否 1:是)',
  `delete_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除者',
  `delete_date` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '省份表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ly_province
-- ----------------------------
INSERT INTO `ly_province` VALUES (1, '110000', '北京市', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (2, '120000', '天津市', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (3, '130000', '河北省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (4, '140000', '山西省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (5, '150000', '内蒙古', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (6, '210000', '辽宁省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (7, '220000', '吉林省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (8, '230000', '黑龙江省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (9, '310000', '上海市', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (10, '320000', '江苏省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (11, '330000', '浙江省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (12, '340000', '安徽省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (13, '350000', '福建省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (14, '360000', '江西省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (15, '370000', '山东省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (16, '410000', '河南省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (17, '420000', '湖北省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (18, '430000', '湖南省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (19, '440000', '广东省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (20, '450000', '广西壮族自治区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (21, '460000', '海南省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (22, '500000', '重庆市', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (23, '510000', '四川省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (24, '520000', '贵州省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (25, '530000', '云南省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (26, '540000', '西藏自治区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (27, '610000', '陕西省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (28, '620000', '甘肃省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (29, '630000', '青海省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (30, '640000', '宁夏回族自治区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (31, '650000', '新疆维吾尔自治区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (32, '710000', '台湾省', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (33, '810000', '香港特别行政区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);
INSERT INTO `ly_province` VALUES (34, '820000', '澳门特别行政区', '1', '2021-08-24 19:03:44', '1', '2021-08-24 19:03:44', '0', NULL, NULL);

-- ----------------------------
-- Table structure for ly_region
-- ----------------------------
DROP TABLE IF EXISTS `ly_region`;
CREATE TABLE `ly_region`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `province_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份代码',
  `city_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市代码',
  `region_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区县代码',
  `region_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区县名称',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改者',
  `update_date` datetime NOT NULL COMMENT '修改时间',
  `is_deleted` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否已删除(0:否 1:是)',
  `delete_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除者',
  `delete_date` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3274 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '区县表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ly_region
-- ----------------------------
INSERT INTO `ly_region` VALUES (1, '110000', '110100', '110101', '东城区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2, '110000', '110100', '110102', '西城区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3, '110000', '110100', '110105', '朝阳区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (4, '110000', '110100', '110106', '丰台区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (5, '110000', '110100', '110107', '石景山区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (6, '110000', '110100', '110108', '海淀区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (7, '110000', '110100', '110109', '门头沟区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (8, '110000', '110100', '110111', '房山区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (9, '110000', '110100', '110112', '通州区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (10, '110000', '110100', '110113', '顺义区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (11, '110000', '110100', '110114', '昌平区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (12, '110000', '110100', '110115', '大兴区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (13, '110000', '110100', '110116', '怀柔区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (14, '110000', '110100', '110117', '平谷区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (15, '110000', '110100', '110118', '密云区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (16, '110000', '110100', '110119', '延庆区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (17, '120000', '120100', '120101', '和平区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (18, '120000', '120100', '120102', '河东区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (19, '120000', '120100', '120103', '河西区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (20, '120000', '120100', '120104', '南开区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (21, '120000', '120100', '120105', '河北区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (22, '120000', '120100', '120106', '红桥区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (23, '120000', '120100', '120110', '东丽区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (24, '120000', '120100', '120111', '西青区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (25, '120000', '120100', '120112', '津南区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (26, '120000', '120100', '120113', '北辰区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (27, '120000', '120100', '120114', '武清区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (28, '120000', '120100', '120115', '宝坻区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (29, '120000', '120100', '120116', '滨海新区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (30, '120000', '120100', '120117', '宁河区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (31, '120000', '120100', '120118', '静海区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (32, '120000', '120100', '120119', '蓟州区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (33, '130000', '130100', '130101', '市辖区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (34, '130000', '130100', '130102', '长安区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (35, '130000', '130100', '130104', '桥西区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (36, '130000', '130100', '130105', '新华区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (37, '130000', '130100', '130107', '井陉矿区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (38, '130000', '130100', '130108', '裕华区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (39, '130000', '130100', '130109', '藁城区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (40, '130000', '130100', '130110', '鹿泉区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (41, '130000', '130100', '130111', '栾城区', '1', '2021-08-24 19:04:46', '1', '2021-08-24 19:04:46', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (42, '130000', '130100', '130121', '井陉县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (43, '130000', '130100', '130123', '正定县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (44, '130000', '130100', '130125', '行唐县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (45, '130000', '130100', '130126', '灵寿县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (46, '130000', '130100', '130127', '高邑县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (47, '130000', '130100', '130128', '深泽县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (48, '130000', '130100', '130129', '赞皇县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (49, '130000', '130100', '130130', '无极县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (50, '130000', '130100', '130131', '平山县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (51, '130000', '130100', '130132', '元氏县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (52, '130000', '130100', '130133', '赵县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (53, '130000', '130100', '130171', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (54, '130000', '130100', '130172', '循环化工园区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (55, '130000', '130100', '130181', '辛集市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (56, '130000', '130100', '130183', '晋州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (57, '130000', '130100', '130184', '新乐市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (58, '130000', '130200', '130201', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (59, '130000', '130200', '130202', '路南区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (60, '130000', '130200', '130203', '路北区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (61, '130000', '130200', '130204', '古冶区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (62, '130000', '130200', '130205', '开平区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (63, '130000', '130200', '130207', '丰南区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (64, '130000', '130200', '130208', '丰润区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (65, '130000', '130200', '130209', '曹妃甸区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (66, '130000', '130200', '130224', '滦南县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (67, '130000', '130200', '130225', '乐亭县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (68, '130000', '130200', '130227', '迁西县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (69, '130000', '130200', '130229', '玉田县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (70, '130000', '130200', '130271', '芦台经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (71, '130000', '130200', '130272', '汉沽管理区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (72, '130000', '130200', '130273', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (73, '130000', '130200', '130274', '海港经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (74, '130000', '130200', '130281', '遵化市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (75, '130000', '130200', '130283', '迁安市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (76, '130000', '130200', '130284', '滦州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (77, '130000', '130300', '130301', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (78, '130000', '130300', '130302', '海港区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (79, '130000', '130300', '130303', '山海关区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (80, '130000', '130300', '130304', '北戴河区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (81, '130000', '130300', '130306', '抚宁区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (82, '130000', '130300', '130321', '青龙满族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (83, '130000', '130300', '130322', '昌黎县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (84, '130000', '130300', '130324', '卢龙县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (85, '130000', '130300', '130371', '经济技术开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (86, '130000', '130300', '130372', '北戴河新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (87, '130000', '130400', '130401', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (88, '130000', '130400', '130402', '邯山区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (89, '130000', '130400', '130403', '丛台区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (90, '130000', '130400', '130404', '复兴区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (91, '130000', '130400', '130406', '峰峰矿区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (92, '130000', '130400', '130407', '肥乡区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (93, '130000', '130400', '130408', '永年区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (94, '130000', '130400', '130423', '临漳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (95, '130000', '130400', '130424', '成安县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (96, '130000', '130400', '130425', '大名县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (97, '130000', '130400', '130426', '涉县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (98, '130000', '130400', '130427', '磁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (99, '130000', '130400', '130430', '邱县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (100, '130000', '130400', '130431', '鸡泽县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (101, '130000', '130400', '130432', '广平县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (102, '130000', '130400', '130433', '馆陶县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (103, '130000', '130400', '130434', '魏县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (104, '130000', '130400', '130435', '曲周县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (105, '130000', '130400', '130471', '经济技术开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (106, '130000', '130400', '130473', '冀南新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (107, '130000', '130400', '130481', '武安市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (108, '130000', '130500', '130501', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (109, '130000', '130500', '130502', '襄都区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (110, '130000', '130500', '130503', '信都区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (111, '130000', '130500', '130505', '任泽区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (112, '130000', '130500', '130506', '南和区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (113, '130000', '130500', '130522', '临城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (114, '130000', '130500', '130523', '内丘县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (115, '130000', '130500', '130524', '柏乡县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (116, '130000', '130500', '130525', '隆尧县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (117, '130000', '130500', '130528', '宁晋县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (118, '130000', '130500', '130529', '巨鹿县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (119, '130000', '130500', '130530', '新河县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (120, '130000', '130500', '130531', '广宗县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (121, '130000', '130500', '130532', '平乡县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (122, '130000', '130500', '130533', '威县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (123, '130000', '130500', '130534', '清河县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (124, '130000', '130500', '130535', '临西县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (125, '130000', '130500', '130571', '经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (126, '130000', '130500', '130581', '南宫市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (127, '130000', '130500', '130582', '沙河市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (128, '130000', '130600', '130601', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (129, '130000', '130600', '130602', '竞秀区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (130, '130000', '130600', '130606', '莲池区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (131, '130000', '130600', '130607', '满城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (132, '130000', '130600', '130608', '清苑区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (133, '130000', '130600', '130609', '徐水区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (134, '130000', '130600', '130623', '涞水县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (135, '130000', '130600', '130624', '阜平县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (136, '130000', '130600', '130626', '定兴县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (137, '130000', '130600', '130627', '唐县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (138, '130000', '130600', '130628', '高阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (139, '130000', '130600', '130629', '容城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (140, '130000', '130600', '130630', '涞源县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (141, '130000', '130600', '130631', '望都县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (142, '130000', '130600', '130632', '安新县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (143, '130000', '130600', '130633', '易县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (144, '130000', '130600', '130634', '曲阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (145, '130000', '130600', '130635', '蠡县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (146, '130000', '130600', '130636', '顺平县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (147, '130000', '130600', '130637', '博野县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (148, '130000', '130600', '130638', '雄县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (149, '130000', '130600', '130671', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (150, '130000', '130600', '130672', '保定白沟新城', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (151, '130000', '130600', '130681', '涿州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (152, '130000', '130600', '130682', '定州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (153, '130000', '130600', '130683', '安国市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (154, '130000', '130600', '130684', '高碑店市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (155, '130000', '130700', '130701', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (156, '130000', '130700', '130702', '桥东区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (157, '130000', '130700', '130703', '桥西区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (158, '130000', '130700', '130705', '宣化区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (159, '130000', '130700', '130706', '下花园区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (160, '130000', '130700', '130708', '万全区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (161, '130000', '130700', '130709', '崇礼区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (162, '130000', '130700', '130722', '张北县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (163, '130000', '130700', '130723', '康保县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (164, '130000', '130700', '130724', '沽源县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (165, '130000', '130700', '130725', '尚义县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (166, '130000', '130700', '130726', '蔚县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (167, '130000', '130700', '130727', '阳原县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (168, '130000', '130700', '130728', '怀安县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (169, '130000', '130700', '130730', '怀来县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (170, '130000', '130700', '130731', '涿鹿县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (171, '130000', '130700', '130732', '赤城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (172, '130000', '130700', '130771', '经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (173, '130000', '130700', '130772', '察北管理区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (174, '130000', '130700', '130773', '塞北管理区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (175, '130000', '130800', '130801', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (176, '130000', '130800', '130802', '双桥区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (177, '130000', '130800', '130803', '双滦区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (178, '130000', '130800', '130804', '鹰手营子矿区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (179, '130000', '130800', '130821', '承德县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (180, '130000', '130800', '130822', '兴隆县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (181, '130000', '130800', '130824', '滦平县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (182, '130000', '130800', '130825', '隆化县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (183, '130000', '130800', '130826', '丰宁满族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (184, '130000', '130800', '130827', '宽城满族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (185, '130000', '130800', '130828', '围场满族蒙古族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (186, '130000', '130800', '130871', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (187, '130000', '130800', '130881', '平泉市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (188, '130000', '130900', '130901', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (189, '130000', '130900', '130902', '新华区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (190, '130000', '130900', '130903', '运河区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (191, '130000', '130900', '130921', '沧县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (192, '130000', '130900', '130922', '青县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (193, '130000', '130900', '130923', '东光县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (194, '130000', '130900', '130924', '海兴县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (195, '130000', '130900', '130925', '盐山县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (196, '130000', '130900', '130926', '肃宁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (197, '130000', '130900', '130927', '南皮县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (198, '130000', '130900', '130928', '吴桥县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (199, '130000', '130900', '130929', '献县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (200, '130000', '130900', '130930', '孟村回族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (201, '130000', '130900', '130971', '经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (202, '130000', '130900', '130972', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (203, '130000', '130900', '130973', '渤海新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (204, '130000', '130900', '130981', '泊头市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (205, '130000', '130900', '130982', '任丘市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (206, '130000', '130900', '130983', '黄骅市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (207, '130000', '130900', '130984', '河间市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (208, '130000', '131000', '131001', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (209, '130000', '131000', '131002', '安次区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (210, '130000', '131000', '131003', '广阳区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (211, '130000', '131000', '131022', '固安县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (212, '130000', '131000', '131023', '永清县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (213, '130000', '131000', '131024', '香河县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (214, '130000', '131000', '131025', '大城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (215, '130000', '131000', '131026', '文安县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (216, '130000', '131000', '131028', '大厂回族自治县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (217, '130000', '131000', '131071', '经济技术开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (218, '130000', '131000', '131081', '霸州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (219, '130000', '131000', '131082', '三河市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (220, '130000', '131100', '131101', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (221, '130000', '131100', '131102', '桃城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (222, '130000', '131100', '131103', '冀州区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (223, '130000', '131100', '131121', '枣强县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (224, '130000', '131100', '131122', '武邑县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (225, '130000', '131100', '131123', '武强县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (226, '130000', '131100', '131124', '饶阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (227, '130000', '131100', '131125', '安平县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (228, '130000', '131100', '131126', '故城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (229, '130000', '131100', '131127', '景县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (230, '130000', '131100', '131128', '阜城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (231, '130000', '131100', '131171', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (232, '130000', '131100', '131172', '滨湖新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (233, '130000', '131100', '131182', '深州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (234, '140000', '140100', '140101', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (235, '140000', '140100', '140105', '小店区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (236, '140000', '140100', '140106', '迎泽区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (237, '140000', '140100', '140107', '杏花岭区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (238, '140000', '140100', '140108', '尖草坪区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (239, '140000', '140100', '140109', '万柏林区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (240, '140000', '140100', '140110', '晋源区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (241, '140000', '140100', '140121', '清徐县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (242, '140000', '140100', '140122', '阳曲县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (243, '140000', '140100', '140123', '娄烦县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (244, '140000', '140100', '140171', '山西转型综合改革示范区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (245, '140000', '140100', '140181', '古交市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (246, '140000', '140200', '140201', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (247, '140000', '140200', '140212', '新荣区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (248, '140000', '140200', '140213', '平城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (249, '140000', '140200', '140214', '云冈区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (250, '140000', '140200', '140215', '云州区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (251, '140000', '140200', '140221', '阳高县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (252, '140000', '140200', '140222', '天镇县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (253, '140000', '140200', '140223', '广灵县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (254, '140000', '140200', '140224', '灵丘县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (255, '140000', '140200', '140225', '浑源县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (256, '140000', '140200', '140226', '左云县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (257, '140000', '140200', '140271', '经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (258, '140000', '140300', '140301', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (259, '140000', '140300', '140302', '城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (260, '140000', '140300', '140303', '矿区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (261, '140000', '140300', '140311', '郊区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (262, '140000', '140300', '140321', '平定县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (263, '140000', '140300', '140322', '盂县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (264, '140000', '140400', '140401', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (265, '140000', '140400', '140403', '潞州区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (266, '140000', '140400', '140404', '上党区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (267, '140000', '140400', '140405', '屯留区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (268, '140000', '140400', '140406', '潞城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (269, '140000', '140400', '140423', '襄垣县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (270, '140000', '140400', '140425', '平顺县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (271, '140000', '140400', '140426', '黎城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (272, '140000', '140400', '140427', '壶关县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (273, '140000', '140400', '140428', '长子县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (274, '140000', '140400', '140429', '武乡县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (275, '140000', '140400', '140430', '沁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (276, '140000', '140400', '140431', '沁源县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (277, '140000', '140400', '140471', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (278, '140000', '140500', '140501', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (279, '140000', '140500', '140502', '城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (280, '140000', '140500', '140521', '沁水县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (281, '140000', '140500', '140522', '阳城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (282, '140000', '140500', '140524', '陵川县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (283, '140000', '140500', '140525', '泽州县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (284, '140000', '140500', '140581', '高平市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (285, '140000', '140600', '140601', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (286, '140000', '140600', '140602', '朔城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (287, '140000', '140600', '140603', '平鲁区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (288, '140000', '140600', '140621', '山阴县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (289, '140000', '140600', '140622', '应县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (290, '140000', '140600', '140623', '右玉县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (291, '140000', '140600', '140671', '经济开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (292, '140000', '140600', '140681', '怀仁市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (293, '140000', '140700', '140701', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (294, '140000', '140700', '140702', '榆次区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (295, '140000', '140700', '140703', '太谷区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (296, '140000', '140700', '140721', '榆社县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (297, '140000', '140700', '140722', '左权县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (298, '140000', '140700', '140723', '和顺县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (299, '140000', '140700', '140724', '昔阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (300, '140000', '140700', '140725', '寿阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (301, '140000', '140700', '140727', '祁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (302, '140000', '140700', '140728', '平遥县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (303, '140000', '140700', '140729', '灵石县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (304, '140000', '140700', '140781', '介休市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (305, '140000', '140800', '140801', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (306, '140000', '140800', '140802', '盐湖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (307, '140000', '140800', '140821', '临猗县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (308, '140000', '140800', '140822', '万荣县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (309, '140000', '140800', '140823', '闻喜县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (310, '140000', '140800', '140824', '稷山县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (311, '140000', '140800', '140825', '新绛县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (312, '140000', '140800', '140826', '绛县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (313, '140000', '140800', '140827', '垣曲县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (314, '140000', '140800', '140828', '夏县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (315, '140000', '140800', '140829', '平陆县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (316, '140000', '140800', '140830', '芮城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (317, '140000', '140800', '140881', '永济市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (318, '140000', '140800', '140882', '河津市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (319, '140000', '140900', '140901', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (320, '140000', '140900', '140902', '忻府区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (321, '140000', '140900', '140921', '定襄县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (322, '140000', '140900', '140922', '五台县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (323, '140000', '140900', '140923', '代县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (324, '140000', '140900', '140924', '繁峙县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (325, '140000', '140900', '140925', '宁武县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (326, '140000', '140900', '140926', '静乐县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (327, '140000', '140900', '140927', '神池县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (328, '140000', '140900', '140928', '五寨县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (329, '140000', '140900', '140929', '岢岚县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (330, '140000', '140900', '140930', '河曲县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (331, '140000', '140900', '140931', '保德县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (332, '140000', '140900', '140932', '偏关县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (333, '140000', '140900', '140971', '五台山风景名胜区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (334, '140000', '140900', '140981', '原平市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (335, '140000', '141000', '141001', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (336, '140000', '141000', '141002', '尧都区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (337, '140000', '141000', '141021', '曲沃县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (338, '140000', '141000', '141022', '翼城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (339, '140000', '141000', '141023', '襄汾县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (340, '140000', '141000', '141024', '洪洞县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (341, '140000', '141000', '141025', '古县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (342, '140000', '141000', '141026', '安泽县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (343, '140000', '141000', '141027', '浮山县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (344, '140000', '141000', '141028', '吉县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (345, '140000', '141000', '141029', '乡宁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (346, '140000', '141000', '141030', '大宁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (347, '140000', '141000', '141031', '隰县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (348, '140000', '141000', '141032', '永和县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (349, '140000', '141000', '141033', '蒲县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (350, '140000', '141000', '141034', '汾西县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (351, '140000', '141000', '141081', '侯马市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (352, '140000', '141000', '141082', '霍州市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (353, '140000', '141100', '141101', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (354, '140000', '141100', '141102', '离石区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (355, '140000', '141100', '141121', '文水县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (356, '140000', '141100', '141122', '交城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (357, '140000', '141100', '141123', '兴县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (358, '140000', '141100', '141124', '临县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (359, '140000', '141100', '141125', '柳林县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (360, '140000', '141100', '141126', '石楼县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (361, '140000', '141100', '141127', '岚县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (362, '140000', '141100', '141128', '方山县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (363, '140000', '141100', '141129', '中阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (364, '140000', '141100', '141130', '交口县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (365, '140000', '141100', '141181', '孝义市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (366, '140000', '141100', '141182', '汾阳市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (367, '150000', '150100', '150101', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (368, '150000', '150100', '150102', '新城区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (369, '150000', '150100', '150103', '回民区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (370, '150000', '150100', '150104', '玉泉区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (371, '150000', '150100', '150105', '赛罕区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (372, '150000', '150100', '150121', '土默特左旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (373, '150000', '150100', '150122', '托克托县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (374, '150000', '150100', '150123', '和林格尔县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (375, '150000', '150100', '150124', '清水河县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (376, '150000', '150100', '150125', '武川县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (377, '150000', '150100', '150172', '经济技术开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (378, '150000', '150200', '150201', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (379, '150000', '150200', '150202', '东河区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (380, '150000', '150200', '150203', '昆都仑区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (381, '150000', '150200', '150204', '青山区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (382, '150000', '150200', '150205', '石拐区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (383, '150000', '150200', '150206', '白云鄂博矿区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (384, '150000', '150200', '150207', '九原区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (385, '150000', '150200', '150221', '土默特右旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (386, '150000', '150200', '150222', '固阳县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (387, '150000', '150200', '150223', '达尔罕茂明安联合旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (388, '150000', '150200', '150271', '高新区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (389, '150000', '150300', '150301', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (390, '150000', '150300', '150302', '海勃湾区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (391, '150000', '150300', '150303', '海南区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (392, '150000', '150300', '150304', '乌达区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (393, '150000', '150400', '150401', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (394, '150000', '150400', '150402', '红山区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (395, '150000', '150400', '150403', '元宝山区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (396, '150000', '150400', '150404', '松山区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (397, '150000', '150400', '150421', '阿鲁科尔沁旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (398, '150000', '150400', '150422', '巴林左旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (399, '150000', '150400', '150423', '巴林右旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (400, '150000', '150400', '150424', '林西县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (401, '150000', '150400', '150425', '克什克腾旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (402, '150000', '150400', '150426', '翁牛特旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (403, '150000', '150400', '150428', '喀喇沁旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (404, '150000', '150400', '150429', '宁城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (405, '150000', '150400', '150430', '敖汉旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (406, '150000', '150500', '150501', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (407, '150000', '150500', '150502', '科尔沁区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (408, '150000', '150500', '150521', '科尔沁左翼中旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (409, '150000', '150500', '150522', '科尔沁左翼后旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (410, '150000', '150500', '150523', '开鲁县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (411, '150000', '150500', '150524', '库伦旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (412, '150000', '150500', '150525', '奈曼旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (413, '150000', '150500', '150526', '扎鲁特旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (414, '150000', '150500', '150571', '经济技术开发区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (415, '150000', '150500', '150581', '霍林郭勒市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (416, '150000', '150600', '150601', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (417, '150000', '150600', '150602', '东胜区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (418, '150000', '150600', '150603', '康巴什区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (419, '150000', '150600', '150621', '达拉特旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (420, '150000', '150600', '150622', '准格尔旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (421, '150000', '150600', '150623', '鄂托克前旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (422, '150000', '150600', '150624', '鄂托克旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (423, '150000', '150600', '150625', '杭锦旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (424, '150000', '150600', '150626', '乌审旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (425, '150000', '150600', '150627', '伊金霍洛旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (426, '150000', '150700', '150701', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (427, '150000', '150700', '150702', '海拉尔区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (428, '150000', '150700', '150703', '扎赉诺尔区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (429, '150000', '150700', '150721', '阿荣旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (430, '150000', '150700', '150722', '莫力达瓦达斡尔族自治旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (431, '150000', '150700', '150723', '鄂伦春自治旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (432, '150000', '150700', '150724', '鄂温克族自治旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (433, '150000', '150700', '150725', '陈巴尔虎旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (434, '150000', '150700', '150726', '新巴尔虎左旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (435, '150000', '150700', '150727', '新巴尔虎右旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (436, '150000', '150700', '150781', '满洲里市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (437, '150000', '150700', '150782', '牙克石市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (438, '150000', '150700', '150783', '扎兰屯市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (439, '150000', '150700', '150784', '额尔古纳市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (440, '150000', '150700', '150785', '根河市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (441, '150000', '150800', '150801', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (442, '150000', '150800', '150802', '临河区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (443, '150000', '150800', '150821', '五原县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (444, '150000', '150800', '150822', '磴口县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (445, '150000', '150800', '150823', '乌拉特前旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (446, '150000', '150800', '150824', '乌拉特中旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (447, '150000', '150800', '150825', '乌拉特后旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (448, '150000', '150800', '150826', '杭锦后旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (449, '150000', '150900', '150901', '市辖区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (450, '150000', '150900', '150902', '集宁区', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (451, '150000', '150900', '150921', '卓资县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (452, '150000', '150900', '150922', '化德县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (453, '150000', '150900', '150923', '商都县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (454, '150000', '150900', '150924', '兴和县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (455, '150000', '150900', '150925', '凉城县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (456, '150000', '150900', '150926', '察哈尔右翼前旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (457, '150000', '150900', '150927', '察哈尔右翼中旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (458, '150000', '150900', '150928', '察哈尔右翼后旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (459, '150000', '150900', '150929', '四子王旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (460, '150000', '150900', '150981', '丰镇市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (461, '150000', '152200', '152201', '乌兰浩特市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (462, '150000', '152200', '152202', '阿尔山市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (463, '150000', '152200', '152221', '科尔沁右翼前旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (464, '150000', '152200', '152222', '科尔沁右翼中旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (465, '150000', '152200', '152223', '扎赉特旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (466, '150000', '152200', '152224', '突泉县', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (467, '150000', '152500', '152501', '二连浩特市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (468, '150000', '152500', '152502', '锡林浩特市', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (469, '150000', '152500', '152522', '阿巴嘎旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (470, '150000', '152500', '152523', '苏尼特左旗', '1', '2021-08-24 19:04:47', '1', '2021-08-24 19:04:47', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (471, '150000', '152500', '152524', '苏尼特右旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (472, '150000', '152500', '152525', '东乌珠穆沁旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (473, '150000', '152500', '152526', '西乌珠穆沁旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (474, '150000', '152500', '152527', '太仆寺旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (475, '150000', '152500', '152528', '镶黄旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (476, '150000', '152500', '152529', '正镶白旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (477, '150000', '152500', '152530', '正蓝旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (478, '150000', '152500', '152531', '多伦县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (479, '150000', '152500', '152571', '乌拉盖管委会', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (480, '150000', '152900', '152921', '阿拉善左旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (481, '150000', '152900', '152922', '阿拉善右旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (482, '150000', '152900', '152923', '额济纳旗', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (483, '150000', '152900', '152971', '经济开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (484, '210000', '210100', '210101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (485, '210000', '210100', '210102', '和平区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (486, '210000', '210100', '210103', '沈河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (487, '210000', '210100', '210104', '大东区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (488, '210000', '210100', '210105', '皇姑区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (489, '210000', '210100', '210106', '铁西区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (490, '210000', '210100', '210111', '苏家屯区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (491, '210000', '210100', '210112', '浑南区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (492, '210000', '210100', '210113', '沈北新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (493, '210000', '210100', '210114', '于洪区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (494, '210000', '210100', '210115', '辽中区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (495, '210000', '210100', '210123', '康平县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (496, '210000', '210100', '210124', '法库县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (497, '210000', '210100', '210181', '新民市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (498, '210000', '210200', '210201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (499, '210000', '210200', '210202', '中山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (500, '210000', '210200', '210203', '西岗区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (501, '210000', '210200', '210204', '沙河口区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (502, '210000', '210200', '210211', '甘井子区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (503, '210000', '210200', '210212', '旅顺口区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (504, '210000', '210200', '210213', '金州区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (505, '210000', '210200', '210214', '普兰店区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (506, '210000', '210200', '210224', '长海县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (507, '210000', '210200', '210281', '瓦房店市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (508, '210000', '210200', '210283', '庄河市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (509, '210000', '210300', '210301', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (510, '210000', '210300', '210302', '铁东区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (511, '210000', '210300', '210303', '铁西区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (512, '210000', '210300', '210304', '立山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (513, '210000', '210300', '210311', '千山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (514, '210000', '210300', '210321', '台安县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (515, '210000', '210300', '210323', '岫岩满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (516, '210000', '210300', '210381', '海城市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (517, '210000', '210400', '210401', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (518, '210000', '210400', '210402', '新抚区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (519, '210000', '210400', '210403', '东洲区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (520, '210000', '210400', '210404', '望花区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (521, '210000', '210400', '210411', '顺城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (522, '210000', '210400', '210421', '抚顺县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (523, '210000', '210400', '210422', '新宾满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (524, '210000', '210400', '210423', '清原满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (525, '210000', '210500', '210501', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (526, '210000', '210500', '210502', '平山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (527, '210000', '210500', '210503', '溪湖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (528, '210000', '210500', '210504', '明山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (529, '210000', '210500', '210505', '南芬区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (530, '210000', '210500', '210521', '本溪满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (531, '210000', '210500', '210522', '桓仁满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (532, '210000', '210600', '210601', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (533, '210000', '210600', '210602', '元宝区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (534, '210000', '210600', '210603', '振兴区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (535, '210000', '210600', '210604', '振安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (536, '210000', '210600', '210624', '宽甸满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (537, '210000', '210600', '210681', '东港市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (538, '210000', '210600', '210682', '凤城市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (539, '210000', '210700', '210701', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (540, '210000', '210700', '210702', '古塔区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (541, '210000', '210700', '210703', '凌河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (542, '210000', '210700', '210711', '太和区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (543, '210000', '210700', '210726', '黑山县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (544, '210000', '210700', '210727', '义县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (545, '210000', '210700', '210781', '凌海市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (546, '210000', '210700', '210782', '北镇市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (547, '210000', '210800', '210801', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (548, '210000', '210800', '210802', '站前区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (549, '210000', '210800', '210803', '西市区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (550, '210000', '210800', '210804', '鲅鱼圈区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (551, '210000', '210800', '210811', '老边区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (552, '210000', '210800', '210881', '盖州市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (553, '210000', '210800', '210882', '大石桥市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (554, '210000', '210900', '210901', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (555, '210000', '210900', '210902', '海州区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (556, '210000', '210900', '210903', '新邱区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (557, '210000', '210900', '210904', '太平区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (558, '210000', '210900', '210905', '清河门区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (559, '210000', '210900', '210911', '细河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (560, '210000', '210900', '210921', '阜新蒙古族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (561, '210000', '210900', '210922', '彰武县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (562, '210000', '211000', '211001', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (563, '210000', '211000', '211002', '白塔区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (564, '210000', '211000', '211003', '文圣区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (565, '210000', '211000', '211004', '宏伟区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (566, '210000', '211000', '211005', '弓长岭区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (567, '210000', '211000', '211011', '太子河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (568, '210000', '211000', '211021', '辽阳县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (569, '210000', '211000', '211081', '灯塔市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (570, '210000', '211100', '211101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (571, '210000', '211100', '211102', '双台子区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (572, '210000', '211100', '211103', '兴隆台区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (573, '210000', '211100', '211104', '大洼区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (574, '210000', '211100', '211122', '盘山县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (575, '210000', '211200', '211201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (576, '210000', '211200', '211202', '银州区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (577, '210000', '211200', '211204', '清河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (578, '210000', '211200', '211221', '铁岭县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (579, '210000', '211200', '211223', '西丰县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (580, '210000', '211200', '211224', '昌图县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (581, '210000', '211200', '211281', '调兵山市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (582, '210000', '211200', '211282', '开原市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (583, '210000', '211300', '211301', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (584, '210000', '211300', '211302', '双塔区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (585, '210000', '211300', '211303', '龙城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (586, '210000', '211300', '211321', '朝阳县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (587, '210000', '211300', '211322', '建平县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (588, '210000', '211300', '211324', '喀喇沁左翼蒙古族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (589, '210000', '211300', '211381', '北票市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (590, '210000', '211300', '211382', '凌源市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (591, '210000', '211400', '211401', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (592, '210000', '211400', '211402', '连山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (593, '210000', '211400', '211403', '龙港区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (594, '210000', '211400', '211404', '南票区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (595, '210000', '211400', '211421', '绥中县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (596, '210000', '211400', '211422', '建昌县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (597, '210000', '211400', '211481', '兴城市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (598, '220000', '220100', '220101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (599, '220000', '220100', '220102', '南关区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (600, '220000', '220100', '220103', '宽城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (601, '220000', '220100', '220104', '朝阳区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (602, '220000', '220100', '220105', '二道区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (603, '220000', '220100', '220106', '绿园区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (604, '220000', '220100', '220112', '双阳区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (605, '220000', '220100', '220113', '九台区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (606, '220000', '220100', '220122', '农安县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (607, '220000', '220100', '220171', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (608, '220000', '220100', '220172', '净月高新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (609, '220000', '220100', '220173', '高新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (610, '220000', '220100', '220174', '汽车经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (611, '220000', '220100', '220182', '榆树市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (612, '220000', '220100', '220183', '德惠市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (613, '220000', '220100', '220184', '公主岭市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (614, '220000', '220200', '220201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (615, '220000', '220200', '220202', '昌邑区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (616, '220000', '220200', '220203', '龙潭区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (617, '220000', '220200', '220204', '船营区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (618, '220000', '220200', '220211', '丰满区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (619, '220000', '220200', '220221', '永吉县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (620, '220000', '220200', '220271', '经济开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (621, '220000', '220200', '220272', '高新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (622, '220000', '220200', '220273', '中国新加坡食品区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (623, '220000', '220200', '220281', '蛟河市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (624, '220000', '220200', '220282', '桦甸市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (625, '220000', '220200', '220283', '舒兰市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (626, '220000', '220200', '220284', '磐石市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (627, '220000', '220300', '220301', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (628, '220000', '220300', '220302', '铁西区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (629, '220000', '220300', '220303', '铁东区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (630, '220000', '220300', '220322', '梨树县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (631, '220000', '220300', '220323', '伊通满族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (632, '220000', '220300', '220382', '双辽市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (633, '220000', '220400', '220401', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (634, '220000', '220400', '220402', '龙山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (635, '220000', '220400', '220403', '西安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (636, '220000', '220400', '220421', '东丰县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (637, '220000', '220400', '220422', '东辽县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (638, '220000', '220500', '220501', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (639, '220000', '220500', '220502', '东昌区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (640, '220000', '220500', '220503', '二道江区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (641, '220000', '220500', '220521', '通化县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (642, '220000', '220500', '220523', '辉南县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (643, '220000', '220500', '220524', '柳河县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (644, '220000', '220500', '220581', '梅河口市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (645, '220000', '220500', '220582', '集安市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (646, '220000', '220600', '220601', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (647, '220000', '220600', '220602', '浑江区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (648, '220000', '220600', '220605', '江源区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (649, '220000', '220600', '220621', '抚松县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (650, '220000', '220600', '220622', '靖宇县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (651, '220000', '220600', '220623', '长白朝鲜族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (652, '220000', '220600', '220681', '临江市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (653, '220000', '220700', '220701', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (654, '220000', '220700', '220702', '宁江区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (655, '220000', '220700', '220721', '前郭尔罗斯蒙古族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (656, '220000', '220700', '220722', '长岭县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (657, '220000', '220700', '220723', '乾安县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (658, '220000', '220700', '220771', '经济开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (659, '220000', '220700', '220781', '扶余市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (660, '220000', '220800', '220801', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (661, '220000', '220800', '220802', '洮北区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (662, '220000', '220800', '220821', '镇赉县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (663, '220000', '220800', '220822', '通榆县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (664, '220000', '220800', '220871', '经济开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (665, '220000', '220800', '220881', '洮南市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (666, '220000', '220800', '220882', '大安市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (667, '220000', '222400', '222401', '延吉市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (668, '220000', '222400', '222402', '图们市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (669, '220000', '222400', '222403', '敦化市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (670, '220000', '222400', '222404', '珲春市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (671, '220000', '222400', '222405', '龙井市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (672, '220000', '222400', '222406', '和龙市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (673, '220000', '222400', '222424', '汪清县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (674, '220000', '222400', '222426', '安图县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (675, '230000', '230100', '230101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (676, '230000', '230100', '230102', '道里区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (677, '230000', '230100', '230103', '南岗区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (678, '230000', '230100', '230104', '道外区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (679, '230000', '230100', '230108', '平房区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (680, '230000', '230100', '230109', '松北区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (681, '230000', '230100', '230110', '香坊区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (682, '230000', '230100', '230111', '呼兰区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (683, '230000', '230100', '230112', '阿城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (684, '230000', '230100', '230113', '双城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (685, '230000', '230100', '230123', '依兰县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (686, '230000', '230100', '230124', '方正县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (687, '230000', '230100', '230125', '宾县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (688, '230000', '230100', '230126', '巴彦县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (689, '230000', '230100', '230127', '木兰县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (690, '230000', '230100', '230128', '通河县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (691, '230000', '230100', '230129', '延寿县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (692, '230000', '230100', '230183', '尚志市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (693, '230000', '230100', '230184', '五常市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (694, '230000', '230200', '230201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (695, '230000', '230200', '230202', '龙沙区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (696, '230000', '230200', '230203', '建华区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (697, '230000', '230200', '230204', '铁锋区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (698, '230000', '230200', '230205', '昂昂溪区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (699, '230000', '230200', '230206', '富拉尔基区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (700, '230000', '230200', '230207', '碾子山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (701, '230000', '230200', '230208', '梅里斯达斡尔族区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (702, '230000', '230200', '230221', '龙江县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (703, '230000', '230200', '230223', '依安县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (704, '230000', '230200', '230224', '泰来县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (705, '230000', '230200', '230225', '甘南县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (706, '230000', '230200', '230227', '富裕县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (707, '230000', '230200', '230229', '克山县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (708, '230000', '230200', '230230', '克东县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (709, '230000', '230200', '230231', '拜泉县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (710, '230000', '230200', '230281', '讷河市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (711, '230000', '230300', '230301', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (712, '230000', '230300', '230302', '鸡冠区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (713, '230000', '230300', '230303', '恒山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (714, '230000', '230300', '230304', '滴道区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (715, '230000', '230300', '230305', '梨树区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (716, '230000', '230300', '230306', '城子河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (717, '230000', '230300', '230307', '麻山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (718, '230000', '230300', '230321', '鸡东县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (719, '230000', '230300', '230381', '虎林市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (720, '230000', '230300', '230382', '密山市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (721, '230000', '230400', '230401', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (722, '230000', '230400', '230402', '向阳区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (723, '230000', '230400', '230403', '工农区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (724, '230000', '230400', '230404', '南山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (725, '230000', '230400', '230405', '兴安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (726, '230000', '230400', '230406', '东山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (727, '230000', '230400', '230407', '兴山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (728, '230000', '230400', '230421', '萝北县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (729, '230000', '230400', '230422', '绥滨县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (730, '230000', '230500', '230501', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (731, '230000', '230500', '230502', '尖山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (732, '230000', '230500', '230503', '岭东区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (733, '230000', '230500', '230505', '四方台区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (734, '230000', '230500', '230506', '宝山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (735, '230000', '230500', '230521', '集贤县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (736, '230000', '230500', '230522', '友谊县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (737, '230000', '230500', '230523', '宝清县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (738, '230000', '230500', '230524', '饶河县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (739, '230000', '230600', '230601', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (740, '230000', '230600', '230602', '萨尔图区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (741, '230000', '230600', '230603', '龙凤区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (742, '230000', '230600', '230604', '让胡路区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (743, '230000', '230600', '230605', '红岗区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (744, '230000', '230600', '230606', '大同区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (745, '230000', '230600', '230621', '肇州县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (746, '230000', '230600', '230622', '肇源县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (747, '230000', '230600', '230623', '林甸县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (748, '230000', '230600', '230624', '杜尔伯特蒙古族自治县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (749, '230000', '230600', '230671', '高新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (750, '230000', '230700', '230701', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (751, '230000', '230700', '230717', '伊美区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (752, '230000', '230700', '230718', '乌翠区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (753, '230000', '230700', '230719', '友好区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (754, '230000', '230700', '230722', '嘉荫县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (755, '230000', '230700', '230723', '汤旺县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (756, '230000', '230700', '230724', '丰林县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (757, '230000', '230700', '230725', '大箐山县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (758, '230000', '230700', '230726', '南岔县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (759, '230000', '230700', '230751', '金林区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (760, '230000', '230700', '230781', '铁力市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (761, '230000', '230800', '230801', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (762, '230000', '230800', '230803', '向阳区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (763, '230000', '230800', '230804', '前进区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (764, '230000', '230800', '230805', '东风区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (765, '230000', '230800', '230811', '郊区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (766, '230000', '230800', '230822', '桦南县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (767, '230000', '230800', '230826', '桦川县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (768, '230000', '230800', '230828', '汤原县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (769, '230000', '230800', '230881', '同江市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (770, '230000', '230800', '230882', '富锦市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (771, '230000', '230800', '230883', '抚远市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (772, '230000', '230900', '230901', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (773, '230000', '230900', '230902', '新兴区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (774, '230000', '230900', '230903', '桃山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (775, '230000', '230900', '230904', '茄子河区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (776, '230000', '230900', '230921', '勃利县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (777, '230000', '231000', '231001', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (778, '230000', '231000', '231002', '东安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (779, '230000', '231000', '231003', '阳明区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (780, '230000', '231000', '231004', '爱民区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (781, '230000', '231000', '231005', '西安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (782, '230000', '231000', '231025', '林口县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (783, '230000', '231000', '231071', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (784, '230000', '231000', '231081', '绥芬河市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (785, '230000', '231000', '231083', '海林市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (786, '230000', '231000', '231084', '宁安市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (787, '230000', '231000', '231085', '穆棱市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (788, '230000', '231000', '231086', '东宁市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (789, '230000', '231100', '231101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (790, '230000', '231100', '231102', '爱辉区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (791, '230000', '231100', '231123', '逊克县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (792, '230000', '231100', '231124', '孙吴县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (793, '230000', '231100', '231181', '北安市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (794, '230000', '231100', '231182', '五大连池市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (795, '230000', '231100', '231183', '嫩江市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (796, '230000', '231200', '231201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (797, '230000', '231200', '231202', '北林区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (798, '230000', '231200', '231221', '望奎县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (799, '230000', '231200', '231222', '兰西县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (800, '230000', '231200', '231223', '青冈县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (801, '230000', '231200', '231224', '庆安县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (802, '230000', '231200', '231225', '明水县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (803, '230000', '231200', '231226', '绥棱县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (804, '230000', '231200', '231281', '安达市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (805, '230000', '231200', '231282', '肇东市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (806, '230000', '231200', '231283', '海伦市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (807, '230000', '232700', '232701', '漠河市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (808, '230000', '232700', '232721', '呼玛县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (809, '230000', '232700', '232722', '塔河县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (810, '230000', '232700', '232761', '加格达奇区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (811, '230000', '232700', '232762', '松岭区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (812, '230000', '232700', '232763', '新林区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (813, '230000', '232700', '232764', '呼中区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (814, '310000', '310100', '310101', '黄浦区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (815, '310000', '310100', '310104', '徐汇区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (816, '310000', '310100', '310105', '长宁区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (817, '310000', '310100', '310106', '静安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (818, '310000', '310100', '310107', '普陀区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (819, '310000', '310100', '310109', '虹口区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (820, '310000', '310100', '310110', '杨浦区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (821, '310000', '310100', '310112', '闵行区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (822, '310000', '310100', '310113', '宝山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (823, '310000', '310100', '310114', '嘉定区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (824, '310000', '310100', '310115', '浦东新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (825, '310000', '310100', '310116', '金山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (826, '310000', '310100', '310117', '松江区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (827, '310000', '310100', '310118', '青浦区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (828, '310000', '310100', '310120', '奉贤区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (829, '310000', '310100', '310151', '崇明区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (830, '320000', '320100', '320101', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (831, '320000', '320100', '320102', '玄武区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (832, '320000', '320100', '320104', '秦淮区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (833, '320000', '320100', '320105', '建邺区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (834, '320000', '320100', '320106', '鼓楼区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (835, '320000', '320100', '320111', '浦口区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (836, '320000', '320100', '320113', '栖霞区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (837, '320000', '320100', '320114', '雨花台区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (838, '320000', '320100', '320115', '江宁区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (839, '320000', '320100', '320116', '六合区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (840, '320000', '320100', '320117', '溧水区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (841, '320000', '320100', '320118', '高淳区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (842, '320000', '320200', '320201', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (843, '320000', '320200', '320205', '锡山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (844, '320000', '320200', '320206', '惠山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (845, '320000', '320200', '320211', '滨湖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (846, '320000', '320200', '320213', '梁溪区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (847, '320000', '320200', '320214', '新吴区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (848, '320000', '320200', '320281', '江阴市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (849, '320000', '320200', '320282', '宜兴市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (850, '320000', '320300', '320301', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (851, '320000', '320300', '320302', '鼓楼区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (852, '320000', '320300', '320303', '云龙区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (853, '320000', '320300', '320305', '贾汪区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (854, '320000', '320300', '320311', '泉山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (855, '320000', '320300', '320312', '铜山区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (856, '320000', '320300', '320321', '丰县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (857, '320000', '320300', '320322', '沛县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (858, '320000', '320300', '320324', '睢宁县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (859, '320000', '320300', '320371', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (860, '320000', '320300', '320381', '新沂市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (861, '320000', '320300', '320382', '邳州市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (862, '320000', '320400', '320401', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (863, '320000', '320400', '320402', '天宁区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (864, '320000', '320400', '320404', '钟楼区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (865, '320000', '320400', '320411', '新北区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (866, '320000', '320400', '320412', '武进区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (867, '320000', '320400', '320413', '金坛区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (868, '320000', '320400', '320481', '溧阳市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (869, '320000', '320500', '320501', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (870, '320000', '320500', '320505', '虎丘区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (871, '320000', '320500', '320506', '吴中区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (872, '320000', '320500', '320507', '相城区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (873, '320000', '320500', '320508', '姑苏区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (874, '320000', '320500', '320509', '吴江区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (875, '320000', '320500', '320571', '苏州工业园区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (876, '320000', '320500', '320581', '常熟市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (877, '320000', '320500', '320582', '张家港市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (878, '320000', '320500', '320583', '昆山市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (879, '320000', '320500', '320585', '太仓市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (880, '320000', '320600', '320601', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (881, '320000', '320600', '320602', '崇川区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (882, '320000', '320600', '320611', '港闸区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (883, '320000', '320600', '320612', '通州区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (884, '320000', '320600', '320623', '如东县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (885, '320000', '320600', '320671', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (886, '320000', '320600', '320681', '启东市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (887, '320000', '320600', '320682', '如皋市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (888, '320000', '320600', '320684', '海门市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (889, '320000', '320600', '320685', '海安市', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (890, '320000', '320700', '320701', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (891, '320000', '320700', '320703', '连云区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (892, '320000', '320700', '320706', '海州区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (893, '320000', '320700', '320707', '赣榆区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (894, '320000', '320700', '320722', '东海县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (895, '320000', '320700', '320723', '灌云县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (896, '320000', '320700', '320724', '灌南县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (897, '320000', '320700', '320771', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (898, '320000', '320700', '320772', '高新区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (899, '320000', '320800', '320801', '市辖区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (900, '320000', '320800', '320803', '淮安区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (901, '320000', '320800', '320804', '淮阴区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (902, '320000', '320800', '320812', '清江浦区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (903, '320000', '320800', '320813', '洪泽区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (904, '320000', '320800', '320826', '涟水县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (905, '320000', '320800', '320830', '盱眙县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (906, '320000', '320800', '320831', '金湖县', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (907, '320000', '320800', '320871', '经济技术开发区', '1', '2021-08-24 19:04:48', '1', '2021-08-24 19:04:48', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (908, '320000', '320900', '320901', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (909, '320000', '320900', '320902', '亭湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (910, '320000', '320900', '320903', '盐都区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (911, '320000', '320900', '320904', '大丰区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (912, '320000', '320900', '320921', '响水县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (913, '320000', '320900', '320922', '滨海县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (914, '320000', '320900', '320923', '阜宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (915, '320000', '320900', '320924', '射阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (916, '320000', '320900', '320925', '建湖县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (917, '320000', '320900', '320971', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (918, '320000', '320900', '320981', '东台市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (919, '320000', '321000', '321001', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (920, '320000', '321000', '321002', '广陵区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (921, '320000', '321000', '321003', '邗江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (922, '320000', '321000', '321012', '江都区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (923, '320000', '321000', '321023', '宝应县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (924, '320000', '321000', '321071', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (925, '320000', '321000', '321081', '仪征市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (926, '320000', '321000', '321084', '高邮市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (927, '320000', '321100', '321101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (928, '320000', '321100', '321102', '京口区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (929, '320000', '321100', '321111', '润州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (930, '320000', '321100', '321112', '丹徒区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (931, '320000', '321100', '321171', '镇江新区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (932, '320000', '321100', '321181', '丹阳市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (933, '320000', '321100', '321182', '扬中市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (934, '320000', '321100', '321183', '句容市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (935, '320000', '321200', '321201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (936, '320000', '321200', '321202', '海陵区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (937, '320000', '321200', '321203', '高港区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (938, '320000', '321200', '321204', '姜堰区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (939, '320000', '321200', '321271', '高新区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (940, '320000', '321200', '321281', '兴化市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (941, '320000', '321200', '321282', '靖江市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (942, '320000', '321200', '321283', '泰兴市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (943, '320000', '321300', '321301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (944, '320000', '321300', '321302', '宿城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (945, '320000', '321300', '321311', '宿豫区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (946, '320000', '321300', '321322', '沭阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (947, '320000', '321300', '321323', '泗阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (948, '320000', '321300', '321324', '泗洪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (949, '320000', '321300', '321371', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (950, '330000', '330100', '330101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (951, '330000', '330100', '330102', '上城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (952, '330000', '330100', '330103', '下城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (953, '330000', '330100', '330104', '江干区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (954, '330000', '330100', '330105', '拱墅区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (955, '330000', '330100', '330106', '西湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (956, '330000', '330100', '330108', '滨江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (957, '330000', '330100', '330109', '萧山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (958, '330000', '330100', '330110', '余杭区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (959, '330000', '330100', '330111', '富阳区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (960, '330000', '330100', '330112', '临安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (961, '330000', '330100', '330122', '桐庐县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (962, '330000', '330100', '330127', '淳安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (963, '330000', '330100', '330182', '建德市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (964, '330000', '330200', '330201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (965, '330000', '330200', '330203', '海曙区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (966, '330000', '330200', '330205', '江北区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (967, '330000', '330200', '330206', '北仑区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (968, '330000', '330200', '330211', '镇海区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (969, '330000', '330200', '330212', '鄞州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (970, '330000', '330200', '330213', '奉化区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (971, '330000', '330200', '330225', '象山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (972, '330000', '330200', '330226', '宁海县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (973, '330000', '330200', '330281', '余姚市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (974, '330000', '330200', '330282', '慈溪市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (975, '330000', '330300', '330301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (976, '330000', '330300', '330302', '鹿城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (977, '330000', '330300', '330303', '龙湾区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (978, '330000', '330300', '330304', '瓯海区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (979, '330000', '330300', '330305', '洞头区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (980, '330000', '330300', '330324', '永嘉县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (981, '330000', '330300', '330326', '平阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (982, '330000', '330300', '330327', '苍南县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (983, '330000', '330300', '330328', '文成县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (984, '330000', '330300', '330329', '泰顺县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (985, '330000', '330300', '330371', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (986, '330000', '330300', '330381', '瑞安市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (987, '330000', '330300', '330382', '乐清市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (988, '330000', '330300', '330383', '龙港市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (989, '330000', '330400', '330401', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (990, '330000', '330400', '330402', '南湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (991, '330000', '330400', '330411', '秀洲区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (992, '330000', '330400', '330421', '嘉善县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (993, '330000', '330400', '330424', '海盐县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (994, '330000', '330400', '330481', '海宁市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (995, '330000', '330400', '330482', '平湖市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (996, '330000', '330400', '330483', '桐乡市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (997, '330000', '330500', '330501', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (998, '330000', '330500', '330502', '吴兴区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (999, '330000', '330500', '330503', '南浔区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1000, '330000', '330500', '330521', '德清县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1001, '330000', '330500', '330522', '长兴县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1002, '330000', '330500', '330523', '安吉县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1003, '330000', '330600', '330601', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1004, '330000', '330600', '330602', '越城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1005, '330000', '330600', '330603', '柯桥区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1006, '330000', '330600', '330604', '上虞区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1007, '330000', '330600', '330624', '新昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1008, '330000', '330600', '330681', '诸暨市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1009, '330000', '330600', '330683', '嵊州市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1010, '330000', '330700', '330701', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1011, '330000', '330700', '330702', '婺城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1012, '330000', '330700', '330703', '金东区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1013, '330000', '330700', '330723', '武义县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1014, '330000', '330700', '330726', '浦江县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1015, '330000', '330700', '330727', '磐安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1016, '330000', '330700', '330781', '兰溪市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1017, '330000', '330700', '330782', '义乌市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1018, '330000', '330700', '330783', '东阳市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1019, '330000', '330700', '330784', '永康市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1020, '330000', '330800', '330801', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1021, '330000', '330800', '330802', '柯城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1022, '330000', '330800', '330803', '衢江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1023, '330000', '330800', '330822', '常山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1024, '330000', '330800', '330824', '开化县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1025, '330000', '330800', '330825', '龙游县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1026, '330000', '330800', '330881', '江山市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1027, '330000', '330900', '330901', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1028, '330000', '330900', '330902', '定海区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1029, '330000', '330900', '330903', '普陀区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1030, '330000', '330900', '330921', '岱山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1031, '330000', '330900', '330922', '嵊泗县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1032, '330000', '331000', '331001', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1033, '330000', '331000', '331002', '椒江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1034, '330000', '331000', '331003', '黄岩区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1035, '330000', '331000', '331004', '路桥区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1036, '330000', '331000', '331022', '三门县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1037, '330000', '331000', '331023', '天台县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1038, '330000', '331000', '331024', '仙居县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1039, '330000', '331000', '331081', '温岭市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1040, '330000', '331000', '331082', '临海市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1041, '330000', '331000', '331083', '玉环市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1042, '330000', '331100', '331101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1043, '330000', '331100', '331102', '莲都区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1044, '330000', '331100', '331121', '青田县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1045, '330000', '331100', '331122', '缙云县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1046, '330000', '331100', '331123', '遂昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1047, '330000', '331100', '331124', '松阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1048, '330000', '331100', '331125', '云和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1049, '330000', '331100', '331126', '庆元县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1050, '330000', '331100', '331127', '景宁畲族自治县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1051, '330000', '331100', '331181', '龙泉市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1052, '340000', '340100', '340101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1053, '340000', '340100', '340102', '瑶海区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1054, '340000', '340100', '340103', '庐阳区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1055, '340000', '340100', '340104', '蜀山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1056, '340000', '340100', '340111', '包河区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1057, '340000', '340100', '340121', '长丰县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1058, '340000', '340100', '340122', '肥东县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1059, '340000', '340100', '340123', '肥西县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1060, '340000', '340100', '340124', '庐江县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1061, '340000', '340100', '340171', '高新区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1062, '340000', '340100', '340172', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1063, '340000', '340100', '340173', '新站高新区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1064, '340000', '340100', '340181', '巢湖市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1065, '340000', '340200', '340201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1066, '340000', '340200', '340202', '镜湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1067, '340000', '340200', '340203', '弋江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1068, '340000', '340200', '340207', '鸠江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1069, '340000', '340200', '340208', '三山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1070, '340000', '340200', '340221', '芜湖县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1071, '340000', '340200', '340222', '繁昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1072, '340000', '340200', '340223', '南陵县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1073, '340000', '340200', '340271', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1074, '340000', '340200', '340272', '长江大桥经济开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1075, '340000', '340200', '340281', '无为市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1076, '340000', '340300', '340301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1077, '340000', '340300', '340302', '龙子湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1078, '340000', '340300', '340303', '蚌山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1079, '340000', '340300', '340304', '禹会区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1080, '340000', '340300', '340311', '淮上区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1081, '340000', '340300', '340321', '怀远县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1082, '340000', '340300', '340322', '五河县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1083, '340000', '340300', '340323', '固镇县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1084, '340000', '340300', '340371', '高新区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1085, '340000', '340300', '340372', '经济开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1086, '340000', '340400', '340401', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1087, '340000', '340400', '340402', '大通区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1088, '340000', '340400', '340403', '田家庵区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1089, '340000', '340400', '340404', '谢家集区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1090, '340000', '340400', '340405', '八公山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1091, '340000', '340400', '340406', '潘集区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1092, '340000', '340400', '340421', '凤台县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1093, '340000', '340400', '340422', '寿县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1094, '340000', '340500', '340501', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1095, '340000', '340500', '340503', '花山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1096, '340000', '340500', '340504', '雨山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1097, '340000', '340500', '340506', '博望区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1098, '340000', '340500', '340521', '当涂县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1099, '340000', '340500', '340522', '含山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1100, '340000', '340500', '340523', '和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1101, '340000', '340600', '340601', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1102, '340000', '340600', '340602', '杜集区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1103, '340000', '340600', '340603', '相山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1104, '340000', '340600', '340604', '烈山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1105, '340000', '340600', '340621', '濉溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1106, '340000', '340700', '340701', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1107, '340000', '340700', '340705', '铜官区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1108, '340000', '340700', '340706', '义安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1109, '340000', '340700', '340711', '郊区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1110, '340000', '340700', '340722', '枞阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1111, '340000', '340800', '340801', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1112, '340000', '340800', '340802', '迎江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1113, '340000', '340800', '340803', '大观区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1114, '340000', '340800', '340811', '宜秀区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1115, '340000', '340800', '340822', '怀宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1116, '340000', '340800', '340825', '太湖县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1117, '340000', '340800', '340826', '宿松县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1118, '340000', '340800', '340827', '望江县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1119, '340000', '340800', '340828', '岳西县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1120, '340000', '340800', '340871', '经济开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1121, '340000', '340800', '340881', '桐城市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1122, '340000', '340800', '340882', '潜山市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1123, '340000', '341000', '341001', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1124, '340000', '341000', '341002', '屯溪区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1125, '340000', '341000', '341003', '黄山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1126, '340000', '341000', '341004', '徽州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1127, '340000', '341000', '341021', '歙县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1128, '340000', '341000', '341022', '休宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1129, '340000', '341000', '341023', '黟县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1130, '340000', '341000', '341024', '祁门县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1131, '340000', '341100', '341101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1132, '340000', '341100', '341102', '琅琊区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1133, '340000', '341100', '341103', '南谯区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1134, '340000', '341100', '341122', '来安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1135, '340000', '341100', '341124', '全椒县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1136, '340000', '341100', '341125', '定远县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1137, '340000', '341100', '341126', '凤阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1138, '340000', '341100', '341171', '现代产业园', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1139, '340000', '341100', '341172', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1140, '340000', '341100', '341181', '天长市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1141, '340000', '341100', '341182', '明光市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1142, '340000', '341200', '341201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1143, '340000', '341200', '341202', '颍州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1144, '340000', '341200', '341203', '颍东区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1145, '340000', '341200', '341204', '颍泉区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1146, '340000', '341200', '341221', '临泉县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1147, '340000', '341200', '341222', '太和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1148, '340000', '341200', '341225', '阜南县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1149, '340000', '341200', '341226', '颍上县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1150, '340000', '341200', '341271', '现代产业园区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1151, '340000', '341200', '341272', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1152, '340000', '341200', '341282', '界首市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1153, '340000', '341300', '341301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1154, '340000', '341300', '341302', '埇桥区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1155, '340000', '341300', '341321', '砀山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1156, '340000', '341300', '341322', '萧县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1157, '340000', '341300', '341323', '灵璧县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1158, '340000', '341300', '341324', '泗县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1159, '340000', '341300', '341371', '现代产业园区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1160, '340000', '341300', '341372', '经济技术开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1161, '340000', '341500', '341501', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1162, '340000', '341500', '341502', '金安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1163, '340000', '341500', '341503', '裕安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1164, '340000', '341500', '341504', '叶集区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1165, '340000', '341500', '341522', '霍邱县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1166, '340000', '341500', '341523', '舒城县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1167, '340000', '341500', '341524', '金寨县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1168, '340000', '341500', '341525', '霍山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1169, '340000', '341600', '341601', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1170, '340000', '341600', '341602', '谯城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1171, '340000', '341600', '341621', '涡阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1172, '340000', '341600', '341622', '蒙城县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1173, '340000', '341600', '341623', '利辛县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1174, '340000', '341700', '341701', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1175, '340000', '341700', '341702', '贵池区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1176, '340000', '341700', '341721', '东至县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1177, '340000', '341700', '341722', '石台县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1178, '340000', '341700', '341723', '青阳县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1179, '340000', '341800', '341801', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1180, '340000', '341800', '341802', '宣州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1181, '340000', '341800', '341821', '郎溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1182, '340000', '341800', '341823', '泾县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1183, '340000', '341800', '341824', '绩溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1184, '340000', '341800', '341825', '旌德县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1185, '340000', '341800', '341871', '经济开发区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1186, '340000', '341800', '341881', '宁国市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1187, '340000', '341800', '341882', '广德市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1188, '350000', '350100', '350101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1189, '350000', '350100', '350102', '鼓楼区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1190, '350000', '350100', '350103', '台江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1191, '350000', '350100', '350104', '仓山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1192, '350000', '350100', '350105', '马尾区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1193, '350000', '350100', '350111', '晋安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1194, '350000', '350100', '350112', '长乐区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1195, '350000', '350100', '350121', '闽侯县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1196, '350000', '350100', '350122', '连江县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1197, '350000', '350100', '350123', '罗源县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1198, '350000', '350100', '350124', '闽清县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1199, '350000', '350100', '350125', '永泰县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1200, '350000', '350100', '350128', '平潭县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1201, '350000', '350100', '350181', '福清市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1202, '350000', '350200', '350201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1203, '350000', '350200', '350203', '思明区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1204, '350000', '350200', '350205', '海沧区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1205, '350000', '350200', '350206', '湖里区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1206, '350000', '350200', '350211', '集美区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1207, '350000', '350200', '350212', '同安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1208, '350000', '350200', '350213', '翔安区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1209, '350000', '350300', '350301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1210, '350000', '350300', '350302', '城厢区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1211, '350000', '350300', '350303', '涵江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1212, '350000', '350300', '350304', '荔城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1213, '350000', '350300', '350305', '秀屿区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1214, '350000', '350300', '350322', '仙游县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1215, '350000', '350400', '350401', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1216, '350000', '350400', '350402', '梅列区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1217, '350000', '350400', '350403', '三元区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1218, '350000', '350400', '350421', '明溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1219, '350000', '350400', '350423', '清流县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1220, '350000', '350400', '350424', '宁化县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1221, '350000', '350400', '350425', '大田县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1222, '350000', '350400', '350426', '尤溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1223, '350000', '350400', '350427', '沙县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1224, '350000', '350400', '350428', '将乐县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1225, '350000', '350400', '350429', '泰宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1226, '350000', '350400', '350430', '建宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1227, '350000', '350400', '350481', '永安市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1228, '350000', '350500', '350501', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1229, '350000', '350500', '350502', '鲤城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1230, '350000', '350500', '350503', '丰泽区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1231, '350000', '350500', '350504', '洛江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1232, '350000', '350500', '350505', '泉港区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1233, '350000', '350500', '350521', '惠安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1234, '350000', '350500', '350524', '安溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1235, '350000', '350500', '350525', '永春县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1236, '350000', '350500', '350526', '德化县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1237, '350000', '350500', '350527', '金门县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1238, '350000', '350500', '350581', '石狮市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1239, '350000', '350500', '350582', '晋江市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1240, '350000', '350500', '350583', '南安市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1241, '350000', '350600', '350601', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1242, '350000', '350600', '350602', '芗城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1243, '350000', '350600', '350603', '龙文区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1244, '350000', '350600', '350622', '云霄县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1245, '350000', '350600', '350623', '漳浦县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1246, '350000', '350600', '350624', '诏安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1247, '350000', '350600', '350625', '长泰县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1248, '350000', '350600', '350626', '东山县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1249, '350000', '350600', '350627', '南靖县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1250, '350000', '350600', '350628', '平和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1251, '350000', '350600', '350629', '华安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1252, '350000', '350600', '350681', '龙海市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1253, '350000', '350700', '350701', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1254, '350000', '350700', '350702', '延平区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1255, '350000', '350700', '350703', '建阳区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1256, '350000', '350700', '350721', '顺昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1257, '350000', '350700', '350722', '浦城县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1258, '350000', '350700', '350723', '光泽县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1259, '350000', '350700', '350724', '松溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1260, '350000', '350700', '350725', '政和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1261, '350000', '350700', '350781', '邵武市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1262, '350000', '350700', '350782', '武夷山市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1263, '350000', '350700', '350783', '建瓯市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1264, '350000', '350800', '350801', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1265, '350000', '350800', '350802', '新罗区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1266, '350000', '350800', '350803', '永定区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1267, '350000', '350800', '350821', '长汀县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1268, '350000', '350800', '350823', '上杭县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1269, '350000', '350800', '350824', '武平县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1270, '350000', '350800', '350825', '连城县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1271, '350000', '350800', '350881', '漳平市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1272, '350000', '350900', '350901', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1273, '350000', '350900', '350902', '蕉城区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1274, '350000', '350900', '350921', '霞浦县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1275, '350000', '350900', '350922', '古田县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1276, '350000', '350900', '350923', '屏南县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1277, '350000', '350900', '350924', '寿宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1278, '350000', '350900', '350925', '周宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1279, '350000', '350900', '350926', '柘荣县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1280, '350000', '350900', '350981', '福安市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1281, '350000', '350900', '350982', '福鼎市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1282, '360000', '360100', '360101', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1283, '360000', '360100', '360102', '东湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1284, '360000', '360100', '360103', '西湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1285, '360000', '360100', '360104', '青云谱区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1286, '360000', '360100', '360111', '青山湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1287, '360000', '360100', '360112', '新建区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1288, '360000', '360100', '360113', '红谷滩区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1289, '360000', '360100', '360121', '南昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1290, '360000', '360100', '360123', '安义县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1291, '360000', '360100', '360124', '进贤县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1292, '360000', '360200', '360201', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1293, '360000', '360200', '360202', '昌江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1294, '360000', '360200', '360203', '珠山区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1295, '360000', '360200', '360222', '浮梁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1296, '360000', '360200', '360281', '乐平市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1297, '360000', '360300', '360301', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1298, '360000', '360300', '360302', '安源区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1299, '360000', '360300', '360313', '湘东区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1300, '360000', '360300', '360321', '莲花县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1301, '360000', '360300', '360322', '上栗县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1302, '360000', '360300', '360323', '芦溪县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1303, '360000', '360400', '360401', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1304, '360000', '360400', '360402', '濂溪区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1305, '360000', '360400', '360403', '浔阳区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1306, '360000', '360400', '360404', '柴桑区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1307, '360000', '360400', '360423', '武宁县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1308, '360000', '360400', '360424', '修水县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1309, '360000', '360400', '360425', '永修县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1310, '360000', '360400', '360426', '德安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1311, '360000', '360400', '360428', '都昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1312, '360000', '360400', '360429', '湖口县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1313, '360000', '360400', '360430', '彭泽县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1314, '360000', '360400', '360481', '瑞昌市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1315, '360000', '360400', '360482', '共青城市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1316, '360000', '360400', '360483', '庐山市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1317, '360000', '360500', '360501', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1318, '360000', '360500', '360502', '渝水区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1319, '360000', '360500', '360521', '分宜县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1320, '360000', '360600', '360601', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1321, '360000', '360600', '360602', '月湖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1322, '360000', '360600', '360603', '余江区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1323, '360000', '360600', '360681', '贵溪市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1324, '360000', '360700', '360701', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1325, '360000', '360700', '360702', '章贡区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1326, '360000', '360700', '360703', '南康区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1327, '360000', '360700', '360704', '赣县区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1328, '360000', '360700', '360722', '信丰县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1329, '360000', '360700', '360723', '大余县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1330, '360000', '360700', '360724', '上犹县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1331, '360000', '360700', '360725', '崇义县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1332, '360000', '360700', '360726', '安远县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1333, '360000', '360700', '360728', '定南县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1334, '360000', '360700', '360729', '全南县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1335, '360000', '360700', '360730', '宁都县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1336, '360000', '360700', '360731', '于都县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1337, '360000', '360700', '360732', '兴国县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1338, '360000', '360700', '360733', '会昌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1339, '360000', '360700', '360734', '寻乌县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1340, '360000', '360700', '360735', '石城县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1341, '360000', '360700', '360781', '瑞金市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1342, '360000', '360700', '360783', '龙南市', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1343, '360000', '360800', '360801', '市辖区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1344, '360000', '360800', '360802', '吉州区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1345, '360000', '360800', '360803', '青原区', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1346, '360000', '360800', '360821', '吉安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1347, '360000', '360800', '360822', '吉水县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1348, '360000', '360800', '360823', '峡江县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1349, '360000', '360800', '360824', '新干县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1350, '360000', '360800', '360825', '永丰县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1351, '360000', '360800', '360826', '泰和县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1352, '360000', '360800', '360827', '遂川县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1353, '360000', '360800', '360828', '万安县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1354, '360000', '360800', '360829', '安福县', '1', '2021-08-24 19:04:49', '1', '2021-08-24 19:04:49', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1355, '360000', '360800', '360830', '永新县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1356, '360000', '360800', '360881', '井冈山市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1357, '360000', '360900', '360901', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1358, '360000', '360900', '360902', '袁州区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1359, '360000', '360900', '360921', '奉新县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1360, '360000', '360900', '360922', '万载县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1361, '360000', '360900', '360923', '上高县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1362, '360000', '360900', '360924', '宜丰县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1363, '360000', '360900', '360925', '靖安县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1364, '360000', '360900', '360926', '铜鼓县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1365, '360000', '360900', '360981', '丰城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1366, '360000', '360900', '360982', '樟树市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1367, '360000', '360900', '360983', '高安市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1368, '360000', '361000', '361001', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1369, '360000', '361000', '361002', '临川区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1370, '360000', '361000', '361003', '东乡区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1371, '360000', '361000', '361021', '南城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1372, '360000', '361000', '361022', '黎川县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1373, '360000', '361000', '361023', '南丰县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1374, '360000', '361000', '361024', '崇仁县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1375, '360000', '361000', '361025', '乐安县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1376, '360000', '361000', '361026', '宜黄县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1377, '360000', '361000', '361027', '金溪县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1378, '360000', '361000', '361028', '资溪县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1379, '360000', '361000', '361030', '广昌县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1380, '360000', '361100', '361101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1381, '360000', '361100', '361102', '信州区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1382, '360000', '361100', '361103', '广丰区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1383, '360000', '361100', '361104', '广信区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1384, '360000', '361100', '361123', '玉山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1385, '360000', '361100', '361124', '铅山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1386, '360000', '361100', '361125', '横峰县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1387, '360000', '361100', '361126', '弋阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1388, '360000', '361100', '361127', '余干县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1389, '360000', '361100', '361128', '鄱阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1390, '360000', '361100', '361129', '万年县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1391, '360000', '361100', '361130', '婺源县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1392, '360000', '361100', '361181', '德兴市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1393, '370000', '370100', '370101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1394, '370000', '370100', '370102', '历下区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1395, '370000', '370100', '370103', '市中区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1396, '370000', '370100', '370104', '槐荫区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1397, '370000', '370100', '370105', '天桥区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1398, '370000', '370100', '370112', '历城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1399, '370000', '370100', '370113', '长清区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1400, '370000', '370100', '370114', '章丘区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1401, '370000', '370100', '370115', '济阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1402, '370000', '370100', '370116', '莱芜区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1403, '370000', '370100', '370117', '钢城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1404, '370000', '370100', '370124', '平阴县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1405, '370000', '370100', '370126', '商河县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1406, '370000', '370100', '370171', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1407, '370000', '370200', '370201', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1408, '370000', '370200', '370202', '市南区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1409, '370000', '370200', '370203', '市北区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1410, '370000', '370200', '370211', '黄岛区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1411, '370000', '370200', '370212', '崂山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1412, '370000', '370200', '370213', '李沧区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1413, '370000', '370200', '370214', '城阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1414, '370000', '370200', '370215', '即墨区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1415, '370000', '370200', '370271', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1416, '370000', '370200', '370281', '胶州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1417, '370000', '370200', '370283', '平度市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1418, '370000', '370200', '370285', '莱西市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1419, '370000', '370300', '370301', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1420, '370000', '370300', '370302', '淄川区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1421, '370000', '370300', '370303', '张店区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1422, '370000', '370300', '370304', '博山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1423, '370000', '370300', '370305', '临淄区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1424, '370000', '370300', '370306', '周村区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1425, '370000', '370300', '370321', '桓台县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1426, '370000', '370300', '370322', '高青县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1427, '370000', '370300', '370323', '沂源县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1428, '370000', '370400', '370401', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1429, '370000', '370400', '370402', '市中区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1430, '370000', '370400', '370403', '薛城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1431, '370000', '370400', '370404', '峄城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1432, '370000', '370400', '370405', '台儿庄区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1433, '370000', '370400', '370406', '山亭区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1434, '370000', '370400', '370481', '滕州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1435, '370000', '370500', '370501', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1436, '370000', '370500', '370502', '东营区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1437, '370000', '370500', '370503', '河口区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1438, '370000', '370500', '370505', '垦利区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1439, '370000', '370500', '370522', '利津县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1440, '370000', '370500', '370523', '广饶县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1441, '370000', '370500', '370571', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1442, '370000', '370500', '370572', '东营港经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1443, '370000', '370600', '370601', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1444, '370000', '370600', '370602', '芝罘区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1445, '370000', '370600', '370611', '福山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1446, '370000', '370600', '370612', '牟平区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1447, '370000', '370600', '370613', '莱山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1448, '370000', '370600', '370614', '蓬莱区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1449, '370000', '370600', '370671', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1450, '370000', '370600', '370672', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1451, '370000', '370600', '370681', '龙口市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1452, '370000', '370600', '370682', '莱阳市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1453, '370000', '370600', '370683', '莱州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1454, '370000', '370600', '370685', '招远市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1455, '370000', '370600', '370686', '栖霞市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1456, '370000', '370600', '370687', '海阳市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1457, '370000', '370700', '370701', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1458, '370000', '370700', '370702', '潍城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1459, '370000', '370700', '370703', '寒亭区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1460, '370000', '370700', '370704', '坊子区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1461, '370000', '370700', '370705', '奎文区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1462, '370000', '370700', '370724', '临朐县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1463, '370000', '370700', '370725', '昌乐县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1464, '370000', '370700', '370772', '滨海经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1465, '370000', '370700', '370781', '青州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1466, '370000', '370700', '370782', '诸城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1467, '370000', '370700', '370783', '寿光市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1468, '370000', '370700', '370784', '安丘市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1469, '370000', '370700', '370785', '高密市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1470, '370000', '370700', '370786', '昌邑市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1471, '370000', '370800', '370801', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1472, '370000', '370800', '370811', '任城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1473, '370000', '370800', '370812', '兖州区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1474, '370000', '370800', '370826', '微山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1475, '370000', '370800', '370827', '鱼台县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1476, '370000', '370800', '370828', '金乡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1477, '370000', '370800', '370829', '嘉祥县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1478, '370000', '370800', '370830', '汶上县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1479, '370000', '370800', '370831', '泗水县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1480, '370000', '370800', '370832', '梁山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1481, '370000', '370800', '370871', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1482, '370000', '370800', '370881', '曲阜市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1483, '370000', '370800', '370883', '邹城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1484, '370000', '370900', '370901', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1485, '370000', '370900', '370902', '泰山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1486, '370000', '370900', '370911', '岱岳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1487, '370000', '370900', '370921', '宁阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1488, '370000', '370900', '370923', '东平县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1489, '370000', '370900', '370982', '新泰市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1490, '370000', '370900', '370983', '肥城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1491, '370000', '371000', '371001', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1492, '370000', '371000', '371002', '环翠区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1493, '370000', '371000', '371003', '文登区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1494, '370000', '371000', '371071', '高技术产业开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1495, '370000', '371000', '371072', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1496, '370000', '371000', '371073', '临港经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1497, '370000', '371000', '371082', '荣成市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1498, '370000', '371000', '371083', '乳山市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1499, '370000', '371100', '371101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1500, '370000', '371100', '371102', '东港区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1501, '370000', '371100', '371103', '岚山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1502, '370000', '371100', '371121', '五莲县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1503, '370000', '371100', '371122', '莒县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1504, '370000', '371100', '371171', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1505, '370000', '371300', '371301', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1506, '370000', '371300', '371302', '兰山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1507, '370000', '371300', '371311', '罗庄区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1508, '370000', '371300', '371312', '河东区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1509, '370000', '371300', '371321', '沂南县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1510, '370000', '371300', '371322', '郯城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1511, '370000', '371300', '371323', '沂水县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1512, '370000', '371300', '371324', '兰陵县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1513, '370000', '371300', '371325', '费县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1514, '370000', '371300', '371326', '平邑县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1515, '370000', '371300', '371327', '莒南县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1516, '370000', '371300', '371328', '蒙阴县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1517, '370000', '371300', '371329', '临沭县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1518, '370000', '371300', '371371', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1519, '370000', '371400', '371401', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1520, '370000', '371400', '371402', '德城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1521, '370000', '371400', '371403', '陵城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1522, '370000', '371400', '371422', '宁津县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1523, '370000', '371400', '371423', '庆云县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1524, '370000', '371400', '371424', '临邑县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1525, '370000', '371400', '371425', '齐河县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1526, '370000', '371400', '371426', '平原县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1527, '370000', '371400', '371427', '夏津县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1528, '370000', '371400', '371428', '武城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1529, '370000', '371400', '371471', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1530, '370000', '371400', '371472', '运河经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1531, '370000', '371400', '371481', '乐陵市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1532, '370000', '371400', '371482', '禹城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1533, '370000', '371500', '371501', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1534, '370000', '371500', '371502', '东昌府区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1535, '370000', '371500', '371503', '茌平区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1536, '370000', '371500', '371521', '阳谷县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1537, '370000', '371500', '371522', '莘县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1538, '370000', '371500', '371524', '东阿县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1539, '370000', '371500', '371525', '冠县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1540, '370000', '371500', '371526', '高唐县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1541, '370000', '371500', '371581', '临清市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1542, '370000', '371600', '371601', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1543, '370000', '371600', '371602', '滨城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1544, '370000', '371600', '371603', '沾化区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1545, '370000', '371600', '371621', '惠民县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1546, '370000', '371600', '371622', '阳信县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1547, '370000', '371600', '371623', '无棣县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1548, '370000', '371600', '371625', '博兴县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1549, '370000', '371600', '371681', '邹平市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1550, '370000', '371700', '371701', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1551, '370000', '371700', '371702', '牡丹区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1552, '370000', '371700', '371703', '定陶区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1553, '370000', '371700', '371721', '曹县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1554, '370000', '371700', '371722', '单县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1555, '370000', '371700', '371723', '成武县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1556, '370000', '371700', '371724', '巨野县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1557, '370000', '371700', '371725', '郓城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1558, '370000', '371700', '371726', '鄄城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1559, '370000', '371700', '371728', '东明县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1560, '370000', '371700', '371771', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1561, '370000', '371700', '371772', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1562, '410000', '410100', '410101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1563, '410000', '410100', '410102', '中原区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1564, '410000', '410100', '410103', '二七区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1565, '410000', '410100', '410104', '管城回族区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1566, '410000', '410100', '410105', '金水区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1567, '410000', '410100', '410106', '上街区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1568, '410000', '410100', '410108', '惠济区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1569, '410000', '410100', '410122', '中牟县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1570, '410000', '410100', '410171', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1571, '410000', '410100', '410172', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1572, '410000', '410100', '410173', '航空港经济综合实验区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1573, '410000', '410100', '410181', '巩义市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1574, '410000', '410100', '410182', '荥阳市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1575, '410000', '410100', '410183', '新密市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1576, '410000', '410100', '410184', '新郑市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1577, '410000', '410100', '410185', '登封市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1578, '410000', '410200', '410201', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1579, '410000', '410200', '410202', '龙亭区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1580, '410000', '410200', '410203', '顺河回族区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1581, '410000', '410200', '410204', '鼓楼区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1582, '410000', '410200', '410205', '禹王台区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1583, '410000', '410200', '410212', '祥符区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1584, '410000', '410200', '410221', '杞县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1585, '410000', '410200', '410222', '通许县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1586, '410000', '410200', '410223', '尉氏县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1587, '410000', '410200', '410225', '兰考县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1588, '410000', '410300', '410301', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1589, '410000', '410300', '410302', '老城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1590, '410000', '410300', '410303', '西工区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1591, '410000', '410300', '410304', '瀍河回族区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1592, '410000', '410300', '410305', '涧西区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1593, '410000', '410300', '410306', '吉利区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1594, '410000', '410300', '410311', '洛龙区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1595, '410000', '410300', '410322', '孟津县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1596, '410000', '410300', '410323', '新安县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1597, '410000', '410300', '410324', '栾川县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1598, '410000', '410300', '410325', '嵩县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1599, '410000', '410300', '410326', '汝阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1600, '410000', '410300', '410327', '宜阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1601, '410000', '410300', '410328', '洛宁县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1602, '410000', '410300', '410329', '伊川县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1603, '410000', '410300', '410371', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1604, '410000', '410300', '410381', '偃师市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1605, '410000', '410400', '410401', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1606, '410000', '410400', '410402', '新华区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1607, '410000', '410400', '410403', '卫东区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1608, '410000', '410400', '410404', '石龙区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1609, '410000', '410400', '410411', '湛河区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1610, '410000', '410400', '410421', '宝丰县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1611, '410000', '410400', '410422', '叶县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1612, '410000', '410400', '410423', '鲁山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1613, '410000', '410400', '410425', '郏县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1614, '410000', '410400', '410471', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1615, '410000', '410400', '410472', '城乡一体化示范区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1616, '410000', '410400', '410481', '舞钢市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1617, '410000', '410400', '410482', '汝州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1618, '410000', '410500', '410501', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1619, '410000', '410500', '410502', '文峰区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1620, '410000', '410500', '410503', '北关区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1621, '410000', '410500', '410505', '殷都区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1622, '410000', '410500', '410506', '龙安区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1623, '410000', '410500', '410522', '安阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1624, '410000', '410500', '410523', '汤阴县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1625, '410000', '410500', '410526', '滑县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1626, '410000', '410500', '410527', '内黄县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1627, '410000', '410500', '410571', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1628, '410000', '410500', '410581', '林州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1629, '410000', '410600', '410601', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1630, '410000', '410600', '410602', '鹤山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1631, '410000', '410600', '410603', '山城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1632, '410000', '410600', '410611', '淇滨区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1633, '410000', '410600', '410621', '浚县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1634, '410000', '410600', '410622', '淇县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1635, '410000', '410600', '410671', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1636, '410000', '410700', '410701', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1637, '410000', '410700', '410702', '红旗区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1638, '410000', '410700', '410703', '卫滨区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1639, '410000', '410700', '410704', '凤泉区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1640, '410000', '410700', '410711', '牧野区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1641, '410000', '410700', '410721', '新乡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1642, '410000', '410700', '410724', '获嘉县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1643, '410000', '410700', '410725', '原阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1644, '410000', '410700', '410726', '延津县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1645, '410000', '410700', '410727', '封丘县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1646, '410000', '410700', '410771', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1647, '410000', '410700', '410772', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1648, '410000', '410700', '410773', '平原城乡一体化示范区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1649, '410000', '410700', '410781', '卫辉市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1650, '410000', '410700', '410782', '辉县市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1651, '410000', '410700', '410783', '长垣市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1652, '410000', '410800', '410801', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1653, '410000', '410800', '410802', '解放区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1654, '410000', '410800', '410803', '中站区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1655, '410000', '410800', '410804', '马村区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1656, '410000', '410800', '410811', '山阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1657, '410000', '410800', '410821', '修武县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1658, '410000', '410800', '410822', '博爱县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1659, '410000', '410800', '410823', '武陟县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1660, '410000', '410800', '410825', '温县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1661, '410000', '410800', '410871', '焦作城乡一体化示范区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1662, '410000', '410800', '410882', '沁阳市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1663, '410000', '410800', '410883', '孟州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1664, '410000', '410900', '410901', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1665, '410000', '410900', '410902', '华龙区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1666, '410000', '410900', '410922', '清丰县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1667, '410000', '410900', '410923', '南乐县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1668, '410000', '410900', '410926', '范县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1669, '410000', '410900', '410927', '台前县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1670, '410000', '410900', '410928', '濮阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1671, '410000', '410900', '410971', '工业园区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1672, '410000', '410900', '410972', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1673, '410000', '411000', '411001', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1674, '410000', '411000', '411002', '魏都区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1675, '410000', '411000', '411003', '建安区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1676, '410000', '411000', '411024', '鄢陵县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1677, '410000', '411000', '411025', '襄城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1678, '410000', '411000', '411071', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1679, '410000', '411000', '411081', '禹州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1680, '410000', '411000', '411082', '长葛市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1681, '410000', '411100', '411101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1682, '410000', '411100', '411102', '源汇区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1683, '410000', '411100', '411103', '郾城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1684, '410000', '411100', '411104', '召陵区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1685, '410000', '411100', '411121', '舞阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1686, '410000', '411100', '411122', '临颍县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1687, '410000', '411100', '411171', '经济技术开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1688, '410000', '411200', '411201', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1689, '410000', '411200', '411202', '湖滨区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1690, '410000', '411200', '411203', '陕州区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1691, '410000', '411200', '411221', '渑池县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1692, '410000', '411200', '411224', '卢氏县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1693, '410000', '411200', '411271', '经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1694, '410000', '411200', '411281', '义马市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1695, '410000', '411200', '411282', '灵宝市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1696, '410000', '411300', '411301', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1697, '410000', '411300', '411302', '宛城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1698, '410000', '411300', '411303', '卧龙区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1699, '410000', '411300', '411321', '南召县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1700, '410000', '411300', '411322', '方城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1701, '410000', '411300', '411323', '西峡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1702, '410000', '411300', '411324', '镇平县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1703, '410000', '411300', '411325', '内乡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1704, '410000', '411300', '411326', '淅川县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1705, '410000', '411300', '411327', '社旗县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1706, '410000', '411300', '411328', '唐河县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1707, '410000', '411300', '411329', '新野县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1708, '410000', '411300', '411330', '桐柏县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1709, '410000', '411300', '411371', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1710, '410000', '411300', '411372', '城乡一体化示范区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1711, '410000', '411300', '411381', '邓州市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1712, '410000', '411400', '411401', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1713, '410000', '411400', '411402', '梁园区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1714, '410000', '411400', '411403', '睢阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1715, '410000', '411400', '411421', '民权县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1716, '410000', '411400', '411422', '睢县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1717, '410000', '411400', '411423', '宁陵县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1718, '410000', '411400', '411424', '柘城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1719, '410000', '411400', '411425', '虞城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1720, '410000', '411400', '411426', '夏邑县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1721, '410000', '411400', '411471', '豫东综合物流产业聚集区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1722, '410000', '411400', '411472', '经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1723, '410000', '411400', '411481', '永城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1724, '410000', '411500', '411501', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1725, '410000', '411500', '411502', '浉河区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1726, '410000', '411500', '411503', '平桥区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1727, '410000', '411500', '411521', '罗山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1728, '410000', '411500', '411522', '光山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1729, '410000', '411500', '411523', '新县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1730, '410000', '411500', '411524', '商城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1731, '410000', '411500', '411525', '固始县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1732, '410000', '411500', '411526', '潢川县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1733, '410000', '411500', '411527', '淮滨县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1734, '410000', '411500', '411528', '息县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1735, '410000', '411500', '411571', '高新区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1736, '410000', '411600', '411601', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1737, '410000', '411600', '411602', '川汇区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1738, '410000', '411600', '411603', '淮阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1739, '410000', '411600', '411621', '扶沟县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1740, '410000', '411600', '411622', '西华县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1741, '410000', '411600', '411623', '商水县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1742, '410000', '411600', '411624', '沈丘县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1743, '410000', '411600', '411625', '郸城县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1744, '410000', '411600', '411627', '太康县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1745, '410000', '411600', '411628', '鹿邑县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1746, '410000', '411600', '411671', '经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1747, '410000', '411600', '411681', '项城市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1748, '410000', '411700', '411701', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1749, '410000', '411700', '411702', '驿城区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1750, '410000', '411700', '411721', '西平县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1751, '410000', '411700', '411722', '上蔡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1752, '410000', '411700', '411723', '平舆县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1753, '410000', '411700', '411724', '正阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1754, '410000', '411700', '411725', '确山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1755, '410000', '411700', '411726', '泌阳县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1756, '410000', '411700', '411727', '汝南县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1757, '410000', '411700', '411728', '遂平县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1758, '410000', '411700', '411729', '新蔡县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1759, '410000', '411700', '411771', '经济开发区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1760, '410000', '419000', '419001', '济源市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1761, '420000', '420100', '420101', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1762, '420000', '420100', '420102', '江岸区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1763, '420000', '420100', '420103', '江汉区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1764, '420000', '420100', '420104', '硚口区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1765, '420000', '420100', '420105', '汉阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1766, '420000', '420100', '420106', '武昌区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1767, '420000', '420100', '420107', '青山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1768, '420000', '420100', '420111', '洪山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1769, '420000', '420100', '420112', '东西湖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1770, '420000', '420100', '420113', '汉南区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1771, '420000', '420100', '420114', '蔡甸区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1772, '420000', '420100', '420115', '江夏区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1773, '420000', '420100', '420116', '黄陂区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1774, '420000', '420100', '420117', '新洲区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1775, '420000', '420200', '420201', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1776, '420000', '420200', '420202', '黄石港区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1777, '420000', '420200', '420203', '西塞山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1778, '420000', '420200', '420204', '下陆区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1779, '420000', '420200', '420205', '铁山区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1780, '420000', '420200', '420222', '阳新县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1781, '420000', '420200', '420281', '大冶市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1782, '420000', '420300', '420301', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1783, '420000', '420300', '420302', '茅箭区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1784, '420000', '420300', '420303', '张湾区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1785, '420000', '420300', '420304', '郧阳区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1786, '420000', '420300', '420322', '郧西县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1787, '420000', '420300', '420323', '竹山县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1788, '420000', '420300', '420324', '竹溪县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1789, '420000', '420300', '420325', '房县', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1790, '420000', '420300', '420381', '丹江口市', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1791, '420000', '420500', '420501', '市辖区', '1', '2021-08-24 19:04:50', '1', '2021-08-24 19:04:50', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1792, '420000', '420500', '420502', '西陵区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1793, '420000', '420500', '420503', '伍家岗区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1794, '420000', '420500', '420504', '点军区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1795, '420000', '420500', '420505', '猇亭区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1796, '420000', '420500', '420506', '夷陵区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1797, '420000', '420500', '420525', '远安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1798, '420000', '420500', '420526', '兴山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1799, '420000', '420500', '420527', '秭归县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1800, '420000', '420500', '420528', '长阳土家族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1801, '420000', '420500', '420529', '五峰土家族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1802, '420000', '420500', '420581', '宜都市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1803, '420000', '420500', '420582', '当阳市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1804, '420000', '420500', '420583', '枝江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1805, '420000', '420600', '420601', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1806, '420000', '420600', '420602', '襄城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1807, '420000', '420600', '420606', '樊城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1808, '420000', '420600', '420607', '襄州区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1809, '420000', '420600', '420624', '南漳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1810, '420000', '420600', '420625', '谷城县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1811, '420000', '420600', '420626', '保康县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1812, '420000', '420600', '420682', '老河口市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1813, '420000', '420600', '420683', '枣阳市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1814, '420000', '420600', '420684', '宜城市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1815, '420000', '420700', '420701', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1816, '420000', '420700', '420702', '梁子湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1817, '420000', '420700', '420703', '华容区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1818, '420000', '420700', '420704', '鄂城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1819, '420000', '420800', '420801', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1820, '420000', '420800', '420802', '东宝区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1821, '420000', '420800', '420804', '掇刀区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1822, '420000', '420800', '420822', '沙洋县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1823, '420000', '420800', '420881', '钟祥市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1824, '420000', '420800', '420882', '京山市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1825, '420000', '420900', '420901', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1826, '420000', '420900', '420902', '孝南区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1827, '420000', '420900', '420921', '孝昌县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1828, '420000', '420900', '420922', '大悟县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1829, '420000', '420900', '420923', '云梦县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1830, '420000', '420900', '420981', '应城市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1831, '420000', '420900', '420982', '安陆市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1832, '420000', '420900', '420984', '汉川市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1833, '420000', '421000', '421001', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1834, '420000', '421000', '421002', '沙市区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1835, '420000', '421000', '421003', '荆州区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1836, '420000', '421000', '421022', '公安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1837, '420000', '421000', '421023', '监利县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1838, '420000', '421000', '421024', '江陵县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1839, '420000', '421000', '421071', '经济技术开发区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1840, '420000', '421000', '421081', '石首市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1841, '420000', '421000', '421083', '洪湖市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1842, '420000', '421000', '421087', '松滋市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1843, '420000', '421100', '421101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1844, '420000', '421100', '421102', '黄州区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1845, '420000', '421100', '421121', '团风县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1846, '420000', '421100', '421122', '红安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1847, '420000', '421100', '421123', '罗田县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1848, '420000', '421100', '421124', '英山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1849, '420000', '421100', '421125', '浠水县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1850, '420000', '421100', '421126', '蕲春县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1851, '420000', '421100', '421127', '黄梅县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1852, '420000', '421100', '421171', '龙感湖管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1853, '420000', '421100', '421181', '麻城市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1854, '420000', '421100', '421182', '武穴市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1855, '420000', '421200', '421201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1856, '420000', '421200', '421202', '咸安区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1857, '420000', '421200', '421221', '嘉鱼县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1858, '420000', '421200', '421222', '通城县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1859, '420000', '421200', '421223', '崇阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1860, '420000', '421200', '421224', '通山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1861, '420000', '421200', '421281', '赤壁市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1862, '420000', '421300', '421301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1863, '420000', '421300', '421303', '曾都区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1864, '420000', '421300', '421321', '随县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1865, '420000', '421300', '421381', '广水市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1866, '420000', '422800', '422801', '恩施市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1867, '420000', '422800', '422802', '利川市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1868, '420000', '422800', '422822', '建始县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1869, '420000', '422800', '422823', '巴东县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1870, '420000', '422800', '422825', '宣恩县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1871, '420000', '422800', '422826', '咸丰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1872, '420000', '422800', '422827', '来凤县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1873, '420000', '422800', '422828', '鹤峰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1874, '420000', '429000', '429004', '仙桃市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1875, '420000', '429000', '429005', '潜江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1876, '420000', '429000', '429006', '天门市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1877, '420000', '429000', '429021', '神农架林区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1878, '430000', '430100', '430101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1879, '430000', '430100', '430102', '芙蓉区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1880, '430000', '430100', '430103', '天心区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1881, '430000', '430100', '430104', '岳麓区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1882, '430000', '430100', '430105', '开福区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1883, '430000', '430100', '430111', '雨花区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1884, '430000', '430100', '430112', '望城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1885, '430000', '430100', '430121', '长沙县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1886, '430000', '430100', '430181', '浏阳市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1887, '430000', '430100', '430182', '宁乡市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1888, '430000', '430200', '430201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1889, '430000', '430200', '430202', '荷塘区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1890, '430000', '430200', '430203', '芦淞区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1891, '430000', '430200', '430204', '石峰区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1892, '430000', '430200', '430211', '天元区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1893, '430000', '430200', '430212', '渌口区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1894, '430000', '430200', '430223', '攸县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1895, '430000', '430200', '430224', '茶陵县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1896, '430000', '430200', '430225', '炎陵县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1897, '430000', '430200', '430271', '云龙示范区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1898, '430000', '430200', '430281', '醴陵市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1899, '430000', '430300', '430301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1900, '430000', '430300', '430302', '雨湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1901, '430000', '430300', '430304', '岳塘区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1902, '430000', '430300', '430321', '湘潭县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1903, '430000', '430300', '430371', '高新区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1904, '430000', '430300', '430372', '昭山示范区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1905, '430000', '430300', '430373', '九华示范区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1906, '430000', '430300', '430381', '湘乡市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1907, '430000', '430300', '430382', '韶山市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1908, '430000', '430400', '430401', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1909, '430000', '430400', '430405', '珠晖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1910, '430000', '430400', '430406', '雁峰区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1911, '430000', '430400', '430407', '石鼓区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1912, '430000', '430400', '430408', '蒸湘区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1913, '430000', '430400', '430412', '南岳区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1914, '430000', '430400', '430421', '衡阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1915, '430000', '430400', '430422', '衡南县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1916, '430000', '430400', '430423', '衡山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1917, '430000', '430400', '430424', '衡东县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1918, '430000', '430400', '430426', '祁东县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1919, '430000', '430400', '430471', '综合保税区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1920, '430000', '430400', '430472', '高新区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1921, '430000', '430400', '430473', '松木经济开发区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1922, '430000', '430400', '430481', '耒阳市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1923, '430000', '430400', '430482', '常宁市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1924, '430000', '430500', '430501', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1925, '430000', '430500', '430502', '双清区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1926, '430000', '430500', '430503', '大祥区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1927, '430000', '430500', '430511', '北塔区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1928, '430000', '430500', '430522', '新邵县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1929, '430000', '430500', '430523', '邵阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1930, '430000', '430500', '430524', '隆回县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1931, '430000', '430500', '430525', '洞口县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1932, '430000', '430500', '430527', '绥宁县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1933, '430000', '430500', '430528', '新宁县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1934, '430000', '430500', '430529', '城步苗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1935, '430000', '430500', '430581', '武冈市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1936, '430000', '430500', '430582', '邵东市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1937, '430000', '430600', '430601', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1938, '430000', '430600', '430602', '岳阳楼区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1939, '430000', '430600', '430603', '云溪区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1940, '430000', '430600', '430611', '君山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1941, '430000', '430600', '430621', '岳阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1942, '430000', '430600', '430623', '华容县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1943, '430000', '430600', '430624', '湘阴县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1944, '430000', '430600', '430626', '平江县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1945, '430000', '430600', '430671', '岳阳市屈原管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1946, '430000', '430600', '430681', '汨罗市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1947, '430000', '430600', '430682', '临湘市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1948, '430000', '430700', '430701', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1949, '430000', '430700', '430702', '武陵区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1950, '430000', '430700', '430703', '鼎城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1951, '430000', '430700', '430721', '安乡县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1952, '430000', '430700', '430722', '汉寿县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1953, '430000', '430700', '430723', '澧县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1954, '430000', '430700', '430724', '临澧县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1955, '430000', '430700', '430725', '桃源县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1956, '430000', '430700', '430726', '石门县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1957, '430000', '430700', '430771', '常德市西洞庭管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1958, '430000', '430700', '430781', '津市市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1959, '430000', '430800', '430801', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1960, '430000', '430800', '430802', '永定区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1961, '430000', '430800', '430811', '武陵源区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1962, '430000', '430800', '430821', '慈利县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1963, '430000', '430800', '430822', '桑植县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1964, '430000', '430900', '430901', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1965, '430000', '430900', '430902', '资阳区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1966, '430000', '430900', '430903', '赫山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1967, '430000', '430900', '430921', '南县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1968, '430000', '430900', '430922', '桃江县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1969, '430000', '430900', '430923', '安化县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1970, '430000', '430900', '430971', '大通湖管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1971, '430000', '430900', '430972', '高新区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1972, '430000', '430900', '430981', '沅江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1973, '430000', '431000', '431001', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1974, '430000', '431000', '431002', '北湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1975, '430000', '431000', '431003', '苏仙区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1976, '430000', '431000', '431021', '桂阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1977, '430000', '431000', '431022', '宜章县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1978, '430000', '431000', '431023', '永兴县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1979, '430000', '431000', '431024', '嘉禾县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1980, '430000', '431000', '431025', '临武县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1981, '430000', '431000', '431026', '汝城县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1982, '430000', '431000', '431027', '桂东县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1983, '430000', '431000', '431028', '安仁县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1984, '430000', '431000', '431081', '资兴市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1985, '430000', '431100', '431101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1986, '430000', '431100', '431102', '零陵区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1987, '430000', '431100', '431103', '冷水滩区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1988, '430000', '431100', '431121', '祁阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1989, '430000', '431100', '431122', '东安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1990, '430000', '431100', '431123', '双牌县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1991, '430000', '431100', '431124', '道县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1992, '430000', '431100', '431125', '江永县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1993, '430000', '431100', '431126', '宁远县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1994, '430000', '431100', '431127', '蓝山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1995, '430000', '431100', '431128', '新田县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1996, '430000', '431100', '431129', '江华瑶族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1997, '430000', '431100', '431171', '经济技术开发区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1998, '430000', '431100', '431172', '金洞管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (1999, '430000', '431100', '431173', '回龙圩管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2000, '430000', '431200', '431201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2001, '430000', '431200', '431202', '鹤城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2002, '430000', '431200', '431221', '中方县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2003, '430000', '431200', '431222', '沅陵县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2004, '430000', '431200', '431223', '辰溪县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2005, '430000', '431200', '431224', '溆浦县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2006, '430000', '431200', '431225', '会同县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2007, '430000', '431200', '431226', '麻阳苗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2008, '430000', '431200', '431227', '新晃侗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2009, '430000', '431200', '431228', '芷江侗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2010, '430000', '431200', '431229', '靖州苗族侗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2011, '430000', '431200', '431230', '通道侗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2012, '430000', '431200', '431271', '怀化市洪江管理区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2013, '430000', '431200', '431281', '洪江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2014, '430000', '431300', '431301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2015, '430000', '431300', '431302', '娄星区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2016, '430000', '431300', '431321', '双峰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2017, '430000', '431300', '431322', '新化县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2018, '430000', '431300', '431381', '冷水江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2019, '430000', '431300', '431382', '涟源市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2020, '430000', '433100', '433101', '吉首市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2021, '430000', '433100', '433122', '泸溪县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2022, '430000', '433100', '433123', '凤凰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2023, '430000', '433100', '433124', '花垣县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2024, '430000', '433100', '433125', '保靖县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2025, '430000', '433100', '433126', '古丈县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2026, '430000', '433100', '433127', '永顺县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2027, '430000', '433100', '433130', '龙山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2028, '440000', '440100', '440101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2029, '440000', '440100', '440103', '荔湾区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2030, '440000', '440100', '440104', '越秀区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2031, '440000', '440100', '440105', '海珠区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2032, '440000', '440100', '440106', '天河区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2033, '440000', '440100', '440111', '白云区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2034, '440000', '440100', '440112', '黄埔区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2035, '440000', '440100', '440113', '番禺区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2036, '440000', '440100', '440114', '花都区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2037, '440000', '440100', '440115', '南沙区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2038, '440000', '440100', '440117', '从化区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2039, '440000', '440100', '440118', '增城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2040, '440000', '440200', '440201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2041, '440000', '440200', '440203', '武江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2042, '440000', '440200', '440204', '浈江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2043, '440000', '440200', '440205', '曲江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2044, '440000', '440200', '440222', '始兴县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2045, '440000', '440200', '440224', '仁化县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2046, '440000', '440200', '440229', '翁源县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2047, '440000', '440200', '440232', '乳源瑶族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2048, '440000', '440200', '440233', '新丰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2049, '440000', '440200', '440281', '乐昌市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2050, '440000', '440200', '440282', '南雄市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2051, '440000', '440300', '440301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2052, '440000', '440300', '440303', '罗湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2053, '440000', '440300', '440304', '福田区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2054, '440000', '440300', '440305', '南山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2055, '440000', '440300', '440306', '宝安区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2056, '440000', '440300', '440307', '龙岗区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2057, '440000', '440300', '440308', '盐田区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2058, '440000', '440300', '440309', '龙华区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2059, '440000', '440300', '440310', '坪山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2060, '440000', '440300', '440311', '光明区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2061, '440000', '440400', '440401', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2062, '440000', '440400', '440402', '香洲区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2063, '440000', '440400', '440403', '斗门区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2064, '440000', '440400', '440404', '金湾区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2065, '440000', '440500', '440501', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2066, '440000', '440500', '440507', '龙湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2067, '440000', '440500', '440511', '金平区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2068, '440000', '440500', '440512', '濠江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2069, '440000', '440500', '440513', '潮阳区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2070, '440000', '440500', '440514', '潮南区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2071, '440000', '440500', '440515', '澄海区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2072, '440000', '440500', '440523', '南澳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2073, '440000', '440600', '440601', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2074, '440000', '440600', '440604', '禅城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2075, '440000', '440600', '440605', '南海区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2076, '440000', '440600', '440606', '顺德区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2077, '440000', '440600', '440607', '三水区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2078, '440000', '440600', '440608', '高明区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2079, '440000', '440700', '440701', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2080, '440000', '440700', '440703', '蓬江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2081, '440000', '440700', '440704', '江海区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2082, '440000', '440700', '440705', '新会区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2083, '440000', '440700', '440781', '台山市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2084, '440000', '440700', '440783', '开平市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2085, '440000', '440700', '440784', '鹤山市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2086, '440000', '440700', '440785', '恩平市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2087, '440000', '440800', '440801', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2088, '440000', '440800', '440802', '赤坎区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2089, '440000', '440800', '440803', '霞山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2090, '440000', '440800', '440804', '坡头区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2091, '440000', '440800', '440811', '麻章区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2092, '440000', '440800', '440823', '遂溪县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2093, '440000', '440800', '440825', '徐闻县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2094, '440000', '440800', '440881', '廉江市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2095, '440000', '440800', '440882', '雷州市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2096, '440000', '440800', '440883', '吴川市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2097, '440000', '440900', '440901', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2098, '440000', '440900', '440902', '茂南区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2099, '440000', '440900', '440904', '电白区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2100, '440000', '440900', '440981', '高州市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2101, '440000', '440900', '440982', '化州市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2102, '440000', '440900', '440983', '信宜市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2103, '440000', '441200', '441201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2104, '440000', '441200', '441202', '端州区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2105, '440000', '441200', '441203', '鼎湖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2106, '440000', '441200', '441204', '高要区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2107, '440000', '441200', '441223', '广宁县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2108, '440000', '441200', '441224', '怀集县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2109, '440000', '441200', '441225', '封开县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2110, '440000', '441200', '441226', '德庆县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2111, '440000', '441200', '441284', '四会市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2112, '440000', '441300', '441301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2113, '440000', '441300', '441302', '惠城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2114, '440000', '441300', '441303', '惠阳区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2115, '440000', '441300', '441322', '博罗县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2116, '440000', '441300', '441323', '惠东县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2117, '440000', '441300', '441324', '龙门县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2118, '440000', '441400', '441401', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2119, '440000', '441400', '441402', '梅江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2120, '440000', '441400', '441403', '梅县区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2121, '440000', '441400', '441422', '大埔县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2122, '440000', '441400', '441423', '丰顺县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2123, '440000', '441400', '441424', '五华县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2124, '440000', '441400', '441426', '平远县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2125, '440000', '441400', '441427', '蕉岭县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2126, '440000', '441400', '441481', '兴宁市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2127, '440000', '441500', '441501', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2128, '440000', '441500', '441502', '城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2129, '440000', '441500', '441521', '海丰县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2130, '440000', '441500', '441523', '陆河县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2131, '440000', '441500', '441581', '陆丰市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2132, '440000', '441600', '441601', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2133, '440000', '441600', '441602', '源城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2134, '440000', '441600', '441621', '紫金县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2135, '440000', '441600', '441622', '龙川县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2136, '440000', '441600', '441623', '连平县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2137, '440000', '441600', '441624', '和平县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2138, '440000', '441600', '441625', '东源县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2139, '440000', '441700', '441701', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2140, '440000', '441700', '441702', '江城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2141, '440000', '441700', '441704', '阳东区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2142, '440000', '441700', '441721', '阳西县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2143, '440000', '441700', '441781', '阳春市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2144, '440000', '441800', '441801', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2145, '440000', '441800', '441802', '清城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2146, '440000', '441800', '441803', '清新区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2147, '440000', '441800', '441821', '佛冈县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2148, '440000', '441800', '441823', '阳山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2149, '440000', '441800', '441825', '连山壮族瑶族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2150, '440000', '441800', '441826', '连南瑶族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2151, '440000', '441800', '441881', '英德市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2152, '440000', '441800', '441882', '连州市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2153, '440000', '445100', '445101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2154, '440000', '445100', '445102', '湘桥区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2155, '440000', '445100', '445103', '潮安区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2156, '440000', '445100', '445122', '饶平县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2157, '440000', '445200', '445201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2158, '440000', '445200', '445202', '榕城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2159, '440000', '445200', '445203', '揭东区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2160, '440000', '445200', '445222', '揭西县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2161, '440000', '445200', '445224', '惠来县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2162, '440000', '445200', '445281', '普宁市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2163, '440000', '445300', '445301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2164, '440000', '445300', '445302', '云城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2165, '440000', '445300', '445303', '云安区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2166, '440000', '445300', '445321', '新兴县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2167, '440000', '445300', '445322', '郁南县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2168, '440000', '445300', '445381', '罗定市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2169, '450000', '450100', '450101', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2170, '450000', '450100', '450102', '兴宁区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2171, '450000', '450100', '450103', '青秀区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2172, '450000', '450100', '450105', '江南区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2173, '450000', '450100', '450107', '西乡塘区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2174, '450000', '450100', '450108', '良庆区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2175, '450000', '450100', '450109', '邕宁区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2176, '450000', '450100', '450110', '武鸣区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2177, '450000', '450100', '450123', '隆安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2178, '450000', '450100', '450124', '马山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2179, '450000', '450100', '450125', '上林县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2180, '450000', '450100', '450126', '宾阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2181, '450000', '450100', '450127', '横县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2182, '450000', '450200', '450201', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2183, '450000', '450200', '450202', '城中区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2184, '450000', '450200', '450203', '鱼峰区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2185, '450000', '450200', '450204', '柳南区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2186, '450000', '450200', '450205', '柳北区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2187, '450000', '450200', '450206', '柳江区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2188, '450000', '450200', '450222', '柳城县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2189, '450000', '450200', '450223', '鹿寨县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2190, '450000', '450200', '450224', '融安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2191, '450000', '450200', '450225', '融水苗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2192, '450000', '450200', '450226', '三江侗族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2193, '450000', '450300', '450301', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2194, '450000', '450300', '450302', '秀峰区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2195, '450000', '450300', '450303', '叠彩区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2196, '450000', '450300', '450304', '象山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2197, '450000', '450300', '450305', '七星区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2198, '450000', '450300', '450311', '雁山区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2199, '450000', '450300', '450312', '临桂区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2200, '450000', '450300', '450321', '阳朔县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2201, '450000', '450300', '450323', '灵川县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2202, '450000', '450300', '450324', '全州县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2203, '450000', '450300', '450325', '兴安县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2204, '450000', '450300', '450326', '永福县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2205, '450000', '450300', '450327', '灌阳县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2206, '450000', '450300', '450328', '龙胜各族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2207, '450000', '450300', '450329', '资源县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2208, '450000', '450300', '450330', '平乐县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2209, '450000', '450300', '450332', '恭城瑶族自治县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2210, '450000', '450300', '450381', '荔浦市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2211, '450000', '450400', '450401', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2212, '450000', '450400', '450403', '万秀区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2213, '450000', '450400', '450405', '长洲区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2214, '450000', '450400', '450406', '龙圩区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2215, '450000', '450400', '450421', '苍梧县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2216, '450000', '450400', '450422', '藤县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2217, '450000', '450400', '450423', '蒙山县', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2218, '450000', '450400', '450481', '岑溪市', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2219, '450000', '450500', '450501', '市辖区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2220, '450000', '450500', '450502', '海城区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2221, '450000', '450500', '450503', '银海区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2222, '450000', '450500', '450512', '铁山港区', '1', '2021-08-24 19:04:51', '1', '2021-08-24 19:04:51', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2223, '450000', '450500', '450521', '合浦县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2224, '450000', '450600', '450601', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2225, '450000', '450600', '450602', '港口区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2226, '450000', '450600', '450603', '防城区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2227, '450000', '450600', '450621', '上思县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2228, '450000', '450600', '450681', '东兴市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2229, '450000', '450700', '450701', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2230, '450000', '450700', '450702', '钦南区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2231, '450000', '450700', '450703', '钦北区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2232, '450000', '450700', '450721', '灵山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2233, '450000', '450700', '450722', '浦北县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2234, '450000', '450800', '450801', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2235, '450000', '450800', '450802', '港北区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2236, '450000', '450800', '450803', '港南区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2237, '450000', '450800', '450804', '覃塘区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2238, '450000', '450800', '450821', '平南县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2239, '450000', '450800', '450881', '桂平市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2240, '450000', '450900', '450901', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2241, '450000', '450900', '450902', '玉州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2242, '450000', '450900', '450903', '福绵区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2243, '450000', '450900', '450921', '容县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2244, '450000', '450900', '450922', '陆川县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2245, '450000', '450900', '450923', '博白县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2246, '450000', '450900', '450924', '兴业县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2247, '450000', '450900', '450981', '北流市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2248, '450000', '451000', '451001', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2249, '450000', '451000', '451002', '右江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2250, '450000', '451000', '451003', '田阳区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2251, '450000', '451000', '451022', '田东县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2252, '450000', '451000', '451024', '德保县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2253, '450000', '451000', '451026', '那坡县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2254, '450000', '451000', '451027', '凌云县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2255, '450000', '451000', '451028', '乐业县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2256, '450000', '451000', '451029', '田林县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2257, '450000', '451000', '451030', '西林县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2258, '450000', '451000', '451031', '隆林各族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2259, '450000', '451000', '451081', '靖西市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2260, '450000', '451000', '451082', '平果市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2261, '450000', '451100', '451101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2262, '450000', '451100', '451102', '八步区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2263, '450000', '451100', '451103', '平桂区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2264, '450000', '451100', '451121', '昭平县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2265, '450000', '451100', '451122', '钟山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2266, '450000', '451100', '451123', '富川瑶族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2267, '450000', '451200', '451201', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2268, '450000', '451200', '451202', '金城江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2269, '450000', '451200', '451203', '宜州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2270, '450000', '451200', '451221', '南丹县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2271, '450000', '451200', '451222', '天峨县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2272, '450000', '451200', '451223', '凤山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2273, '450000', '451200', '451224', '东兰县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2274, '450000', '451200', '451225', '罗城仫佬族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2275, '450000', '451200', '451226', '环江毛南族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2276, '450000', '451200', '451227', '巴马瑶族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2277, '450000', '451200', '451228', '都安瑶族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2278, '450000', '451200', '451229', '大化瑶族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2279, '450000', '451300', '451301', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2280, '450000', '451300', '451302', '兴宾区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2281, '450000', '451300', '451321', '忻城县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2282, '450000', '451300', '451322', '象州县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2283, '450000', '451300', '451323', '武宣县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2284, '450000', '451300', '451324', '金秀瑶族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2285, '450000', '451300', '451381', '合山市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2286, '450000', '451400', '451401', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2287, '450000', '451400', '451402', '江州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2288, '450000', '451400', '451421', '扶绥县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2289, '450000', '451400', '451422', '宁明县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2290, '450000', '451400', '451423', '龙州县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2291, '450000', '451400', '451424', '大新县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2292, '450000', '451400', '451425', '天等县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2293, '450000', '451400', '451481', '凭祥市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2294, '460000', '460100', '460101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2295, '460000', '460100', '460105', '秀英区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2296, '460000', '460100', '460106', '龙华区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2297, '460000', '460100', '460107', '琼山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2298, '460000', '460100', '460108', '美兰区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2299, '460000', '460200', '460201', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2300, '460000', '460200', '460202', '海棠区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2301, '460000', '460200', '460203', '吉阳区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2302, '460000', '460200', '460204', '天涯区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2303, '460000', '460200', '460205', '崖州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2304, '460000', '460300', '460321', '西沙群岛', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2305, '460000', '460300', '460322', '南沙群岛', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2306, '460000', '460300', '460323', '中沙群岛的岛礁及其海域', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2307, '460000', '469000', '469001', '五指山市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2308, '460000', '469000', '469002', '琼海市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2309, '460000', '469000', '469005', '文昌市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2310, '460000', '469000', '469006', '万宁市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2311, '460000', '469000', '469007', '东方市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2312, '460000', '469000', '469021', '定安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2313, '460000', '469000', '469022', '屯昌县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2314, '460000', '469000', '469023', '澄迈县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2315, '460000', '469000', '469024', '临高县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2316, '460000', '469000', '469025', '白沙黎族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2317, '460000', '469000', '469026', '昌江黎族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2318, '460000', '469000', '469027', '乐东黎族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2319, '460000', '469000', '469028', '陵水黎族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2320, '460000', '469000', '469029', '保亭黎族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2321, '460000', '469000', '469030', '琼中黎族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2322, '500000', '500100', '500101', '万州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2323, '500000', '500100', '500102', '涪陵区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2324, '500000', '500100', '500103', '渝中区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2325, '500000', '500100', '500104', '大渡口区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2326, '500000', '500100', '500105', '江北区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2327, '500000', '500100', '500106', '沙坪坝区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2328, '500000', '500100', '500107', '九龙坡区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2329, '500000', '500100', '500108', '南岸区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2330, '500000', '500100', '500109', '北碚区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2331, '500000', '500100', '500110', '綦江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2332, '500000', '500100', '500111', '大足区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2333, '500000', '500100', '500112', '渝北区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2334, '500000', '500100', '500113', '巴南区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2335, '500000', '500100', '500114', '黔江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2336, '500000', '500100', '500115', '长寿区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2337, '500000', '500100', '500116', '江津区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2338, '500000', '500100', '500117', '合川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2339, '500000', '500100', '500118', '永川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2340, '500000', '500100', '500119', '南川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2341, '500000', '500100', '500120', '璧山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2342, '500000', '500100', '500151', '铜梁区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2343, '500000', '500100', '500152', '潼南区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2344, '500000', '500100', '500153', '荣昌区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2345, '500000', '500100', '500154', '开州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2346, '500000', '500100', '500155', '梁平区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2347, '500000', '500100', '500156', '武隆区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2348, '500000', '500200', '500229', '城口县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2349, '500000', '500200', '500230', '丰都县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2350, '500000', '500200', '500231', '垫江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2351, '500000', '500200', '500233', '忠县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2352, '500000', '500200', '500235', '云阳县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2353, '500000', '500200', '500236', '奉节县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2354, '500000', '500200', '500237', '巫山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2355, '500000', '500200', '500238', '巫溪县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2356, '500000', '500200', '500240', '石柱土家族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2357, '500000', '500200', '500241', '秀山土家族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2358, '500000', '500200', '500242', '酉阳土家族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2359, '500000', '500200', '500243', '彭水苗族土家族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2360, '510000', '510100', '510101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2361, '510000', '510100', '510104', '锦江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2362, '510000', '510100', '510105', '青羊区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2363, '510000', '510100', '510106', '金牛区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2364, '510000', '510100', '510107', '武侯区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2365, '510000', '510100', '510108', '成华区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2366, '510000', '510100', '510112', '龙泉驿区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2367, '510000', '510100', '510113', '青白江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2368, '510000', '510100', '510114', '新都区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2369, '510000', '510100', '510115', '温江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2370, '510000', '510100', '510116', '双流区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2371, '510000', '510100', '510117', '郫都区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2372, '510000', '510100', '510118', '新津区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2373, '510000', '510100', '510121', '金堂县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2374, '510000', '510100', '510129', '大邑县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2375, '510000', '510100', '510131', '蒲江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2376, '510000', '510100', '510181', '都江堰市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2377, '510000', '510100', '510182', '彭州市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2378, '510000', '510100', '510183', '邛崃市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2379, '510000', '510100', '510184', '崇州市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2380, '510000', '510100', '510185', '简阳市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2381, '510000', '510300', '510301', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2382, '510000', '510300', '510302', '自流井区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2383, '510000', '510300', '510303', '贡井区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2384, '510000', '510300', '510304', '大安区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2385, '510000', '510300', '510311', '沿滩区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2386, '510000', '510300', '510321', '荣县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2387, '510000', '510300', '510322', '富顺县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2388, '510000', '510400', '510401', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2389, '510000', '510400', '510402', '东区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2390, '510000', '510400', '510403', '西区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2391, '510000', '510400', '510411', '仁和区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2392, '510000', '510400', '510421', '米易县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2393, '510000', '510400', '510422', '盐边县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2394, '510000', '510500', '510501', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2395, '510000', '510500', '510502', '江阳区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2396, '510000', '510500', '510503', '纳溪区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2397, '510000', '510500', '510504', '龙马潭区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2398, '510000', '510500', '510521', '泸县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2399, '510000', '510500', '510522', '合江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2400, '510000', '510500', '510524', '叙永县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2401, '510000', '510500', '510525', '古蔺县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2402, '510000', '510600', '510601', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2403, '510000', '510600', '510603', '旌阳区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2404, '510000', '510600', '510604', '罗江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2405, '510000', '510600', '510623', '中江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2406, '510000', '510600', '510681', '广汉市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2407, '510000', '510600', '510682', '什邡市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2408, '510000', '510600', '510683', '绵竹市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2409, '510000', '510700', '510701', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2410, '510000', '510700', '510703', '涪城区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2411, '510000', '510700', '510704', '游仙区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2412, '510000', '510700', '510705', '安州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2413, '510000', '510700', '510722', '三台县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2414, '510000', '510700', '510723', '盐亭县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2415, '510000', '510700', '510725', '梓潼县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2416, '510000', '510700', '510726', '北川羌族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2417, '510000', '510700', '510727', '平武县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2418, '510000', '510700', '510781', '江油市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2419, '510000', '510800', '510801', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2420, '510000', '510800', '510802', '利州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2421, '510000', '510800', '510811', '昭化区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2422, '510000', '510800', '510812', '朝天区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2423, '510000', '510800', '510821', '旺苍县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2424, '510000', '510800', '510822', '青川县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2425, '510000', '510800', '510823', '剑阁县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2426, '510000', '510800', '510824', '苍溪县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2427, '510000', '510900', '510901', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2428, '510000', '510900', '510903', '船山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2429, '510000', '510900', '510904', '安居区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2430, '510000', '510900', '510921', '蓬溪县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2431, '510000', '510900', '510923', '大英县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2432, '510000', '510900', '510981', '射洪市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2433, '510000', '511000', '511001', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2434, '510000', '511000', '511002', '市中区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2435, '510000', '511000', '511011', '东兴区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2436, '510000', '511000', '511024', '威远县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2437, '510000', '511000', '511025', '资中县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2438, '510000', '511000', '511071', '经济开发区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2439, '510000', '511000', '511083', '隆昌市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2440, '510000', '511100', '511101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2441, '510000', '511100', '511102', '市中区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2442, '510000', '511100', '511111', '沙湾区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2443, '510000', '511100', '511112', '五通桥区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2444, '510000', '511100', '511113', '金口河区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2445, '510000', '511100', '511123', '犍为县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2446, '510000', '511100', '511124', '井研县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2447, '510000', '511100', '511126', '夹江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2448, '510000', '511100', '511129', '沐川县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2449, '510000', '511100', '511132', '峨边彝族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2450, '510000', '511100', '511133', '马边彝族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2451, '510000', '511100', '511181', '峨眉山市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2452, '510000', '511300', '511301', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2453, '510000', '511300', '511302', '顺庆区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2454, '510000', '511300', '511303', '高坪区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2455, '510000', '511300', '511304', '嘉陵区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2456, '510000', '511300', '511321', '南部县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2457, '510000', '511300', '511322', '营山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2458, '510000', '511300', '511323', '蓬安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2459, '510000', '511300', '511324', '仪陇县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2460, '510000', '511300', '511325', '西充县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2461, '510000', '511300', '511381', '阆中市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2462, '510000', '511400', '511401', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2463, '510000', '511400', '511402', '东坡区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2464, '510000', '511400', '511403', '彭山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2465, '510000', '511400', '511421', '仁寿县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2466, '510000', '511400', '511423', '洪雅县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2467, '510000', '511400', '511424', '丹棱县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2468, '510000', '511400', '511425', '青神县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2469, '510000', '511500', '511501', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2470, '510000', '511500', '511502', '翠屏区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2471, '510000', '511500', '511503', '南溪区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2472, '510000', '511500', '511504', '叙州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2473, '510000', '511500', '511523', '江安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2474, '510000', '511500', '511524', '长宁县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2475, '510000', '511500', '511525', '高县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2476, '510000', '511500', '511526', '珙县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2477, '510000', '511500', '511527', '筠连县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2478, '510000', '511500', '511528', '兴文县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2479, '510000', '511500', '511529', '屏山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2480, '510000', '511600', '511601', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2481, '510000', '511600', '511602', '广安区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2482, '510000', '511600', '511603', '前锋区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2483, '510000', '511600', '511621', '岳池县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2484, '510000', '511600', '511622', '武胜县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2485, '510000', '511600', '511623', '邻水县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2486, '510000', '511600', '511681', '华蓥市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2487, '510000', '511700', '511701', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2488, '510000', '511700', '511702', '通川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2489, '510000', '511700', '511703', '达川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2490, '510000', '511700', '511722', '宣汉县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2491, '510000', '511700', '511723', '开江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2492, '510000', '511700', '511724', '大竹县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2493, '510000', '511700', '511725', '渠县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2494, '510000', '511700', '511771', '经济开发区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2495, '510000', '511700', '511781', '万源市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2496, '510000', '511800', '511801', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2497, '510000', '511800', '511802', '雨城区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2498, '510000', '511800', '511803', '名山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2499, '510000', '511800', '511822', '荥经县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2500, '510000', '511800', '511823', '汉源县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2501, '510000', '511800', '511824', '石棉县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2502, '510000', '511800', '511825', '天全县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2503, '510000', '511800', '511826', '芦山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2504, '510000', '511800', '511827', '宝兴县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2505, '510000', '511900', '511901', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2506, '510000', '511900', '511902', '巴州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2507, '510000', '511900', '511903', '恩阳区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2508, '510000', '511900', '511921', '通江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2509, '510000', '511900', '511922', '南江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2510, '510000', '511900', '511923', '平昌县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2511, '510000', '511900', '511971', '经济开发区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2512, '510000', '512000', '512001', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2513, '510000', '512000', '512002', '雁江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2514, '510000', '512000', '512021', '安岳县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2515, '510000', '512000', '512022', '乐至县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2516, '510000', '513200', '513201', '马尔康市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2517, '510000', '513200', '513221', '汶川县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2518, '510000', '513200', '513222', '理县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2519, '510000', '513200', '513223', '茂县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2520, '510000', '513200', '513224', '松潘县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2521, '510000', '513200', '513225', '九寨沟县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2522, '510000', '513200', '513226', '金川县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2523, '510000', '513200', '513227', '小金县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2524, '510000', '513200', '513228', '黑水县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2525, '510000', '513200', '513230', '壤塘县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2526, '510000', '513200', '513231', '阿坝县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2527, '510000', '513200', '513232', '若尔盖县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2528, '510000', '513200', '513233', '红原县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2529, '510000', '513300', '513301', '康定市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2530, '510000', '513300', '513322', '泸定县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2531, '510000', '513300', '513323', '丹巴县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2532, '510000', '513300', '513324', '九龙县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2533, '510000', '513300', '513325', '雅江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2534, '510000', '513300', '513326', '道孚县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2535, '510000', '513300', '513327', '炉霍县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2536, '510000', '513300', '513328', '甘孜县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2537, '510000', '513300', '513329', '新龙县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2538, '510000', '513300', '513330', '德格县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2539, '510000', '513300', '513331', '白玉县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2540, '510000', '513300', '513332', '石渠县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2541, '510000', '513300', '513333', '色达县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2542, '510000', '513300', '513334', '理塘县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2543, '510000', '513300', '513335', '巴塘县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2544, '510000', '513300', '513336', '乡城县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2545, '510000', '513300', '513337', '稻城县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2546, '510000', '513300', '513338', '得荣县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2547, '510000', '513400', '513401', '西昌市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2548, '510000', '513400', '513422', '木里藏族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2549, '510000', '513400', '513423', '盐源县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2550, '510000', '513400', '513424', '德昌县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2551, '510000', '513400', '513425', '会理县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2552, '510000', '513400', '513426', '会东县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2553, '510000', '513400', '513427', '宁南县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2554, '510000', '513400', '513428', '普格县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2555, '510000', '513400', '513429', '布拖县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2556, '510000', '513400', '513430', '金阳县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2557, '510000', '513400', '513431', '昭觉县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2558, '510000', '513400', '513432', '喜德县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2559, '510000', '513400', '513433', '冕宁县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2560, '510000', '513400', '513434', '越西县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2561, '510000', '513400', '513435', '甘洛县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2562, '510000', '513400', '513436', '美姑县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2563, '510000', '513400', '513437', '雷波县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2564, '520000', '520100', '520101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2565, '520000', '520100', '520102', '南明区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2566, '520000', '520100', '520103', '云岩区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2567, '520000', '520100', '520111', '花溪区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2568, '520000', '520100', '520112', '乌当区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2569, '520000', '520100', '520113', '白云区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2570, '520000', '520100', '520115', '观山湖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2571, '520000', '520100', '520121', '开阳县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2572, '520000', '520100', '520122', '息烽县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2573, '520000', '520100', '520123', '修文县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2574, '520000', '520100', '520181', '清镇市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2575, '520000', '520200', '520201', '钟山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2576, '520000', '520200', '520203', '六枝特区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2577, '520000', '520200', '520221', '水城县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2578, '520000', '520200', '520281', '盘州市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2579, '520000', '520300', '520301', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2580, '520000', '520300', '520302', '红花岗区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2581, '520000', '520300', '520303', '汇川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2582, '520000', '520300', '520304', '播州区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2583, '520000', '520300', '520322', '桐梓县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2584, '520000', '520300', '520323', '绥阳县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2585, '520000', '520300', '520324', '正安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2586, '520000', '520300', '520325', '道真仡佬族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2587, '520000', '520300', '520326', '务川仡佬族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2588, '520000', '520300', '520327', '凤冈县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2589, '520000', '520300', '520328', '湄潭县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2590, '520000', '520300', '520329', '余庆县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2591, '520000', '520300', '520330', '习水县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2592, '520000', '520300', '520381', '赤水市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2593, '520000', '520300', '520382', '仁怀市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2594, '520000', '520400', '520401', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2595, '520000', '520400', '520402', '西秀区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2596, '520000', '520400', '520403', '平坝区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2597, '520000', '520400', '520422', '普定县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2598, '520000', '520400', '520423', '镇宁布依族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2599, '520000', '520400', '520424', '关岭布依族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2600, '520000', '520400', '520425', '紫云苗族布依族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2601, '520000', '520500', '520501', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2602, '520000', '520500', '520502', '七星关区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2603, '520000', '520500', '520521', '大方县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2604, '520000', '520500', '520522', '黔西县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2605, '520000', '520500', '520523', '金沙县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2606, '520000', '520500', '520524', '织金县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2607, '520000', '520500', '520525', '纳雍县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2608, '520000', '520500', '520526', '威宁彝族回族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2609, '520000', '520500', '520527', '赫章县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2610, '520000', '520600', '520601', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2611, '520000', '520600', '520602', '碧江区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2612, '520000', '520600', '520603', '万山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2613, '520000', '520600', '520621', '江口县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2614, '520000', '520600', '520622', '玉屏侗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2615, '520000', '520600', '520623', '石阡县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2616, '520000', '520600', '520624', '思南县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2617, '520000', '520600', '520625', '印江土家族苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2618, '520000', '520600', '520626', '德江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2619, '520000', '520600', '520627', '沿河土家族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2620, '520000', '520600', '520628', '松桃苗族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2621, '520000', '522300', '522301', '兴义市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2622, '520000', '522300', '522302', '兴仁市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2623, '520000', '522300', '522323', '普安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2624, '520000', '522300', '522324', '晴隆县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2625, '520000', '522300', '522325', '贞丰县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2626, '520000', '522300', '522326', '望谟县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2627, '520000', '522300', '522327', '册亨县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2628, '520000', '522300', '522328', '安龙县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2629, '520000', '522600', '522601', '凯里市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2630, '520000', '522600', '522622', '黄平县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2631, '520000', '522600', '522623', '施秉县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2632, '520000', '522600', '522624', '三穗县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2633, '520000', '522600', '522625', '镇远县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2634, '520000', '522600', '522626', '岑巩县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2635, '520000', '522600', '522627', '天柱县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2636, '520000', '522600', '522628', '锦屏县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2637, '520000', '522600', '522629', '剑河县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2638, '520000', '522600', '522630', '台江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2639, '520000', '522600', '522631', '黎平县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2640, '520000', '522600', '522632', '榕江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2641, '520000', '522600', '522633', '从江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2642, '520000', '522600', '522634', '雷山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2643, '520000', '522600', '522635', '麻江县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2644, '520000', '522600', '522636', '丹寨县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2645, '520000', '522700', '522701', '都匀市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2646, '520000', '522700', '522702', '福泉市', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2647, '520000', '522700', '522722', '荔波县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2648, '520000', '522700', '522723', '贵定县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2649, '520000', '522700', '522725', '瓮安县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2650, '520000', '522700', '522726', '独山县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2651, '520000', '522700', '522727', '平塘县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2652, '520000', '522700', '522728', '罗甸县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2653, '520000', '522700', '522729', '长顺县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2654, '520000', '522700', '522730', '龙里县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2655, '520000', '522700', '522731', '惠水县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2656, '520000', '522700', '522732', '三都水族自治县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2657, '530000', '530100', '530101', '市辖区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2658, '530000', '530100', '530102', '五华区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2659, '530000', '530100', '530103', '盘龙区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2660, '530000', '530100', '530111', '官渡区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2661, '530000', '530100', '530112', '西山区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2662, '530000', '530100', '530113', '东川区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2663, '530000', '530100', '530114', '呈贡区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2664, '530000', '530100', '530115', '晋宁区', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2665, '530000', '530100', '530124', '富民县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2666, '530000', '530100', '530125', '宜良县', '1', '2021-08-24 19:04:52', '1', '2021-08-24 19:04:52', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2667, '530000', '530100', '530126', '石林彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2668, '530000', '530100', '530127', '嵩明县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2669, '530000', '530100', '530128', '禄劝彝族苗族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2670, '530000', '530100', '530129', '寻甸回族彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2671, '530000', '530100', '530181', '安宁市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2672, '530000', '530300', '530301', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2673, '530000', '530300', '530302', '麒麟区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2674, '530000', '530300', '530303', '沾益区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2675, '530000', '530300', '530304', '马龙区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2676, '530000', '530300', '530322', '陆良县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2677, '530000', '530300', '530323', '师宗县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2678, '530000', '530300', '530324', '罗平县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2679, '530000', '530300', '530325', '富源县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2680, '530000', '530300', '530326', '会泽县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2681, '530000', '530300', '530381', '宣威市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2682, '530000', '530400', '530401', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2683, '530000', '530400', '530402', '红塔区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2684, '530000', '530400', '530403', '江川区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2685, '530000', '530400', '530423', '通海县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2686, '530000', '530400', '530424', '华宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2687, '530000', '530400', '530425', '易门县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2688, '530000', '530400', '530426', '峨山彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2689, '530000', '530400', '530427', '新平彝族傣族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2690, '530000', '530400', '530428', '元江哈尼族彝族傣族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2691, '530000', '530400', '530481', '澄江市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2692, '530000', '530500', '530501', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2693, '530000', '530500', '530502', '隆阳区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2694, '530000', '530500', '530521', '施甸县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2695, '530000', '530500', '530523', '龙陵县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2696, '530000', '530500', '530524', '昌宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2697, '530000', '530500', '530581', '腾冲市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2698, '530000', '530600', '530601', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2699, '530000', '530600', '530602', '昭阳区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2700, '530000', '530600', '530621', '鲁甸县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2701, '530000', '530600', '530622', '巧家县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2702, '530000', '530600', '530623', '盐津县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2703, '530000', '530600', '530624', '大关县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2704, '530000', '530600', '530625', '永善县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2705, '530000', '530600', '530626', '绥江县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2706, '530000', '530600', '530627', '镇雄县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2707, '530000', '530600', '530628', '彝良县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2708, '530000', '530600', '530629', '威信县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2709, '530000', '530600', '530681', '水富市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2710, '530000', '530700', '530701', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2711, '530000', '530700', '530702', '古城区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2712, '530000', '530700', '530721', '玉龙纳西族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2713, '530000', '530700', '530722', '永胜县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2714, '530000', '530700', '530723', '华坪县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2715, '530000', '530700', '530724', '宁蒗彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2716, '530000', '530800', '530801', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2717, '530000', '530800', '530802', '思茅区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2718, '530000', '530800', '530821', '宁洱哈尼族彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2719, '530000', '530800', '530822', '墨江哈尼族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2720, '530000', '530800', '530823', '景东彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2721, '530000', '530800', '530824', '景谷傣族彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2722, '530000', '530800', '530825', '镇沅彝族哈尼族拉祜族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2723, '530000', '530800', '530826', '江城哈尼族彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2724, '530000', '530800', '530827', '孟连傣族拉祜族佤族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2725, '530000', '530800', '530828', '澜沧拉祜族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2726, '530000', '530800', '530829', '西盟佤族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2727, '530000', '530900', '530901', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2728, '530000', '530900', '530902', '临翔区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2729, '530000', '530900', '530921', '凤庆县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2730, '530000', '530900', '530922', '云县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2731, '530000', '530900', '530923', '永德县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2732, '530000', '530900', '530924', '镇康县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2733, '530000', '530900', '530925', '双江拉祜族佤族布朗族傣族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2734, '530000', '530900', '530926', '耿马傣族佤族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2735, '530000', '530900', '530927', '沧源佤族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2736, '530000', '532300', '532301', '楚雄市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2737, '530000', '532300', '532322', '双柏县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2738, '530000', '532300', '532323', '牟定县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2739, '530000', '532300', '532324', '南华县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2740, '530000', '532300', '532325', '姚安县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2741, '530000', '532300', '532326', '大姚县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2742, '530000', '532300', '532327', '永仁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2743, '530000', '532300', '532328', '元谋县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2744, '530000', '532300', '532329', '武定县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2745, '530000', '532300', '532331', '禄丰县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2746, '530000', '532500', '532501', '个旧市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2747, '530000', '532500', '532502', '开远市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2748, '530000', '532500', '532503', '蒙自市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2749, '530000', '532500', '532504', '弥勒市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2750, '530000', '532500', '532523', '屏边苗族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2751, '530000', '532500', '532524', '建水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2752, '530000', '532500', '532525', '石屏县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2753, '530000', '532500', '532527', '泸西县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2754, '530000', '532500', '532528', '元阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2755, '530000', '532500', '532529', '红河县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2756, '530000', '532500', '532530', '金平苗族瑶族傣族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2757, '530000', '532500', '532531', '绿春县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2758, '530000', '532500', '532532', '河口瑶族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2759, '530000', '532600', '532601', '文山市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2760, '530000', '532600', '532622', '砚山县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2761, '530000', '532600', '532623', '西畴县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2762, '530000', '532600', '532624', '麻栗坡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2763, '530000', '532600', '532625', '马关县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2764, '530000', '532600', '532626', '丘北县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2765, '530000', '532600', '532627', '广南县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2766, '530000', '532600', '532628', '富宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2767, '530000', '532800', '532801', '景洪市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2768, '530000', '532800', '532822', '勐海县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2769, '530000', '532800', '532823', '勐腊县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2770, '530000', '532900', '532901', '大理市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2771, '530000', '532900', '532922', '漾濞彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2772, '530000', '532900', '532923', '祥云县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2773, '530000', '532900', '532924', '宾川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2774, '530000', '532900', '532925', '弥渡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2775, '530000', '532900', '532926', '南涧彝族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2776, '530000', '532900', '532927', '巍山彝族回族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2777, '530000', '532900', '532928', '永平县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2778, '530000', '532900', '532929', '云龙县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2779, '530000', '532900', '532930', '洱源县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2780, '530000', '532900', '532931', '剑川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2781, '530000', '532900', '532932', '鹤庆县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2782, '530000', '533100', '533102', '瑞丽市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2783, '530000', '533100', '533103', '芒市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2784, '530000', '533100', '533122', '梁河县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2785, '530000', '533100', '533123', '盈江县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2786, '530000', '533100', '533124', '陇川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2787, '530000', '533300', '533301', '泸水市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2788, '530000', '533300', '533323', '福贡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2789, '530000', '533300', '533324', '贡山独龙族怒族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2790, '530000', '533300', '533325', '兰坪白族普米族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2791, '530000', '533400', '533401', '香格里拉市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2792, '530000', '533400', '533422', '德钦县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2793, '530000', '533400', '533423', '维西傈僳族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2794, '540000', '540100', '540101', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2795, '540000', '540100', '540102', '城关区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2796, '540000', '540100', '540103', '堆龙德庆区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2797, '540000', '540100', '540104', '达孜区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2798, '540000', '540100', '540121', '林周县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2799, '540000', '540100', '540122', '当雄县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2800, '540000', '540100', '540123', '尼木县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2801, '540000', '540100', '540124', '曲水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2802, '540000', '540100', '540127', '墨竹工卡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2803, '540000', '540100', '540171', '格尔木藏青工业园区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2804, '540000', '540100', '540172', '经济技术开发区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2805, '540000', '540100', '540173', '西藏文化旅游创意园区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2806, '540000', '540100', '540174', '达孜工业园区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2807, '540000', '540200', '540202', '桑珠孜区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2808, '540000', '540200', '540221', '南木林县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2809, '540000', '540200', '540222', '江孜县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2810, '540000', '540200', '540223', '定日县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2811, '540000', '540200', '540224', '萨迦县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2812, '540000', '540200', '540225', '拉孜县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2813, '540000', '540200', '540226', '昂仁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2814, '540000', '540200', '540227', '谢通门县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2815, '540000', '540200', '540228', '白朗县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2816, '540000', '540200', '540229', '仁布县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2817, '540000', '540200', '540230', '康马县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2818, '540000', '540200', '540231', '定结县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2819, '540000', '540200', '540232', '仲巴县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2820, '540000', '540200', '540233', '亚东县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2821, '540000', '540200', '540234', '吉隆县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2822, '540000', '540200', '540235', '聂拉木县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2823, '540000', '540200', '540236', '萨嘎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2824, '540000', '540200', '540237', '岗巴县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2825, '540000', '540300', '540302', '卡若区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2826, '540000', '540300', '540321', '江达县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2827, '540000', '540300', '540322', '贡觉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2828, '540000', '540300', '540323', '类乌齐县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2829, '540000', '540300', '540324', '丁青县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2830, '540000', '540300', '540325', '察雅县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2831, '540000', '540300', '540326', '八宿县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2832, '540000', '540300', '540327', '左贡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2833, '540000', '540300', '540328', '芒康县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2834, '540000', '540300', '540329', '洛隆县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2835, '540000', '540300', '540330', '边坝县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2836, '540000', '540400', '540402', '巴宜区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2837, '540000', '540400', '540421', '工布江达县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2838, '540000', '540400', '540422', '米林县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2839, '540000', '540400', '540423', '墨脱县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2840, '540000', '540400', '540424', '波密县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2841, '540000', '540400', '540425', '察隅县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2842, '540000', '540400', '540426', '朗县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2843, '540000', '540500', '540501', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2844, '540000', '540500', '540502', '乃东区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2845, '540000', '540500', '540521', '扎囊县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2846, '540000', '540500', '540522', '贡嘎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2847, '540000', '540500', '540523', '桑日县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2848, '540000', '540500', '540524', '琼结县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2849, '540000', '540500', '540525', '曲松县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2850, '540000', '540500', '540526', '措美县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2851, '540000', '540500', '540527', '洛扎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2852, '540000', '540500', '540528', '加查县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2853, '540000', '540500', '540529', '隆子县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2854, '540000', '540500', '540530', '错那县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2855, '540000', '540500', '540531', '浪卡子县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2856, '540000', '540600', '540602', '色尼区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2857, '540000', '540600', '540621', '嘉黎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2858, '540000', '540600', '540622', '比如县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2859, '540000', '540600', '540623', '聂荣县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2860, '540000', '540600', '540624', '安多县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2861, '540000', '540600', '540625', '申扎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2862, '540000', '540600', '540626', '索县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2863, '540000', '540600', '540627', '班戈县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2864, '540000', '540600', '540628', '巴青县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2865, '540000', '540600', '540629', '尼玛县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2866, '540000', '540600', '540630', '双湖县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2867, '540000', '542500', '542521', '普兰县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2868, '540000', '542500', '542522', '札达县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2869, '540000', '542500', '542523', '噶尔县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2870, '540000', '542500', '542524', '日土县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2871, '540000', '542500', '542525', '革吉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2872, '540000', '542500', '542526', '改则县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2873, '540000', '542500', '542527', '措勤县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2874, '610000', '610100', '610101', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2875, '610000', '610100', '610102', '新城区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2876, '610000', '610100', '610103', '碑林区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2877, '610000', '610100', '610104', '莲湖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2878, '610000', '610100', '610111', '灞桥区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2879, '610000', '610100', '610112', '未央区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2880, '610000', '610100', '610113', '雁塔区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2881, '610000', '610100', '610114', '阎良区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2882, '610000', '610100', '610115', '临潼区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2883, '610000', '610100', '610116', '长安区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2884, '610000', '610100', '610117', '高陵区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2885, '610000', '610100', '610118', '鄠邑区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2886, '610000', '610100', '610122', '蓝田县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2887, '610000', '610100', '610124', '周至县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2888, '610000', '610200', '610201', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2889, '610000', '610200', '610202', '王益区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2890, '610000', '610200', '610203', '印台区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2891, '610000', '610200', '610204', '耀州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2892, '610000', '610200', '610222', '宜君县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2893, '610000', '610300', '610301', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2894, '610000', '610300', '610302', '渭滨区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2895, '610000', '610300', '610303', '金台区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2896, '610000', '610300', '610304', '陈仓区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2897, '610000', '610300', '610322', '凤翔县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2898, '610000', '610300', '610323', '岐山县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2899, '610000', '610300', '610324', '扶风县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2900, '610000', '610300', '610326', '眉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2901, '610000', '610300', '610327', '陇县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2902, '610000', '610300', '610328', '千阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2903, '610000', '610300', '610329', '麟游县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2904, '610000', '610300', '610330', '凤县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2905, '610000', '610300', '610331', '太白县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2906, '610000', '610400', '610401', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2907, '610000', '610400', '610402', '秦都区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2908, '610000', '610400', '610403', '杨陵区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2909, '610000', '610400', '610404', '渭城区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2910, '610000', '610400', '610422', '三原县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2911, '610000', '610400', '610423', '泾阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2912, '610000', '610400', '610424', '乾县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2913, '610000', '610400', '610425', '礼泉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2914, '610000', '610400', '610426', '永寿县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2915, '610000', '610400', '610428', '长武县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2916, '610000', '610400', '610429', '旬邑县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2917, '610000', '610400', '610430', '淳化县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2918, '610000', '610400', '610431', '武功县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2919, '610000', '610400', '610481', '兴平市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2920, '610000', '610400', '610482', '彬州市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2921, '610000', '610500', '610501', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2922, '610000', '610500', '610502', '临渭区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2923, '610000', '610500', '610503', '华州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2924, '610000', '610500', '610522', '潼关县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2925, '610000', '610500', '610523', '大荔县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2926, '610000', '610500', '610524', '合阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2927, '610000', '610500', '610525', '澄城县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2928, '610000', '610500', '610526', '蒲城县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2929, '610000', '610500', '610527', '白水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2930, '610000', '610500', '610528', '富平县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2931, '610000', '610500', '610581', '韩城市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2932, '610000', '610500', '610582', '华阴市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2933, '610000', '610600', '610601', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2934, '610000', '610600', '610602', '宝塔区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2935, '610000', '610600', '610603', '安塞区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2936, '610000', '610600', '610621', '延长县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2937, '610000', '610600', '610622', '延川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2938, '610000', '610600', '610625', '志丹县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2939, '610000', '610600', '610626', '吴起县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2940, '610000', '610600', '610627', '甘泉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2941, '610000', '610600', '610628', '富县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2942, '610000', '610600', '610629', '洛川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2943, '610000', '610600', '610630', '宜川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2944, '610000', '610600', '610631', '黄龙县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2945, '610000', '610600', '610632', '黄陵县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2946, '610000', '610600', '610681', '子长市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2947, '610000', '610700', '610701', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2948, '610000', '610700', '610702', '汉台区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2949, '610000', '610700', '610703', '南郑区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2950, '610000', '610700', '610722', '城固县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2951, '610000', '610700', '610723', '洋县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2952, '610000', '610700', '610724', '西乡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2953, '610000', '610700', '610725', '勉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2954, '610000', '610700', '610726', '宁强县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2955, '610000', '610700', '610727', '略阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2956, '610000', '610700', '610728', '镇巴县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2957, '610000', '610700', '610729', '留坝县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2958, '610000', '610700', '610730', '佛坪县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2959, '610000', '610800', '610801', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2960, '610000', '610800', '610802', '榆阳区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2961, '610000', '610800', '610803', '横山区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2962, '610000', '610800', '610822', '府谷县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2963, '610000', '610800', '610824', '靖边县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2964, '610000', '610800', '610825', '定边县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2965, '610000', '610800', '610826', '绥德县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2966, '610000', '610800', '610827', '米脂县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2967, '610000', '610800', '610828', '佳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2968, '610000', '610800', '610829', '吴堡县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2969, '610000', '610800', '610830', '清涧县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2970, '610000', '610800', '610831', '子洲县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2971, '610000', '610800', '610881', '神木市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2972, '610000', '610900', '610901', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2973, '610000', '610900', '610902', '汉滨区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2974, '610000', '610900', '610921', '汉阴县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2975, '610000', '610900', '610922', '石泉县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2976, '610000', '610900', '610923', '宁陕县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2977, '610000', '610900', '610924', '紫阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2978, '610000', '610900', '610925', '岚皋县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2979, '610000', '610900', '610926', '平利县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2980, '610000', '610900', '610927', '镇坪县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2981, '610000', '610900', '610928', '旬阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2982, '610000', '610900', '610929', '白河县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2983, '610000', '611000', '611001', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2984, '610000', '611000', '611002', '商州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2985, '610000', '611000', '611021', '洛南县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2986, '610000', '611000', '611022', '丹凤县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2987, '610000', '611000', '611023', '商南县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2988, '610000', '611000', '611024', '山阳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2989, '610000', '611000', '611025', '镇安县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2990, '610000', '611000', '611026', '柞水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2991, '620000', '620100', '620101', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2992, '620000', '620100', '620102', '城关区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2993, '620000', '620100', '620103', '七里河区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2994, '620000', '620100', '620104', '西固区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2995, '620000', '620100', '620105', '安宁区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2996, '620000', '620100', '620111', '红古区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2997, '620000', '620100', '620121', '永登县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2998, '620000', '620100', '620122', '皋兰县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (2999, '620000', '620100', '620123', '榆中县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3000, '620000', '620100', '620171', '兰州新区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3001, '620000', '620200', '620201', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3002, '620000', '620300', '620301', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3003, '620000', '620300', '620302', '金川区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3004, '620000', '620300', '620321', '永昌县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3005, '620000', '620400', '620401', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3006, '620000', '620400', '620402', '白银区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3007, '620000', '620400', '620403', '平川区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3008, '620000', '620400', '620421', '靖远县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3009, '620000', '620400', '620422', '会宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3010, '620000', '620400', '620423', '景泰县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3011, '620000', '620500', '620501', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3012, '620000', '620500', '620502', '秦州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3013, '620000', '620500', '620503', '麦积区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3014, '620000', '620500', '620521', '清水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3015, '620000', '620500', '620522', '秦安县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3016, '620000', '620500', '620523', '甘谷县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3017, '620000', '620500', '620524', '武山县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3018, '620000', '620500', '620525', '张家川回族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3019, '620000', '620600', '620601', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3020, '620000', '620600', '620602', '凉州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3021, '620000', '620600', '620621', '民勤县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3022, '620000', '620600', '620622', '古浪县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3023, '620000', '620600', '620623', '天祝藏族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3024, '620000', '620700', '620701', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3025, '620000', '620700', '620702', '甘州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3026, '620000', '620700', '620721', '肃南裕固族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3027, '620000', '620700', '620722', '民乐县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3028, '620000', '620700', '620723', '临泽县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3029, '620000', '620700', '620724', '高台县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3030, '620000', '620700', '620725', '山丹县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3031, '620000', '620800', '620801', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3032, '620000', '620800', '620802', '崆峒区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3033, '620000', '620800', '620821', '泾川县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3034, '620000', '620800', '620822', '灵台县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3035, '620000', '620800', '620823', '崇信县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3036, '620000', '620800', '620825', '庄浪县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3037, '620000', '620800', '620826', '静宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3038, '620000', '620800', '620881', '华亭市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3039, '620000', '620900', '620901', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3040, '620000', '620900', '620902', '肃州区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3041, '620000', '620900', '620921', '金塔县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3042, '620000', '620900', '620922', '瓜州县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3043, '620000', '620900', '620923', '肃北蒙古族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3044, '620000', '620900', '620924', '阿克塞哈萨克族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3045, '620000', '620900', '620981', '玉门市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3046, '620000', '620900', '620982', '敦煌市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3047, '620000', '621000', '621001', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3048, '620000', '621000', '621002', '西峰区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3049, '620000', '621000', '621021', '庆城县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3050, '620000', '621000', '621022', '环县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3051, '620000', '621000', '621023', '华池县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3052, '620000', '621000', '621024', '合水县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3053, '620000', '621000', '621025', '正宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3054, '620000', '621000', '621026', '宁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3055, '620000', '621000', '621027', '镇原县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3056, '620000', '621100', '621101', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3057, '620000', '621100', '621102', '安定区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3058, '620000', '621100', '621121', '通渭县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3059, '620000', '621100', '621122', '陇西县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3060, '620000', '621100', '621123', '渭源县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3061, '620000', '621100', '621124', '临洮县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3062, '620000', '621100', '621125', '漳县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3063, '620000', '621100', '621126', '岷县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3064, '620000', '621200', '621201', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3065, '620000', '621200', '621202', '武都区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3066, '620000', '621200', '621221', '成县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3067, '620000', '621200', '621222', '文县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3068, '620000', '621200', '621223', '宕昌县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3069, '620000', '621200', '621224', '康县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3070, '620000', '621200', '621225', '西和县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3071, '620000', '621200', '621226', '礼县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3072, '620000', '621200', '621227', '徽县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3073, '620000', '621200', '621228', '两当县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3074, '620000', '622900', '622901', '临夏市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3075, '620000', '622900', '622921', '临夏县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3076, '620000', '622900', '622922', '康乐县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3077, '620000', '622900', '622923', '永靖县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3078, '620000', '622900', '622924', '广河县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3079, '620000', '622900', '622925', '和政县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3080, '620000', '622900', '622926', '东乡族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3081, '620000', '622900', '622927', '积石山保安族东乡族撒拉族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3082, '620000', '623000', '623001', '合作市', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3083, '620000', '623000', '623021', '临潭县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3084, '620000', '623000', '623022', '卓尼县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3085, '620000', '623000', '623023', '舟曲县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3086, '620000', '623000', '623024', '迭部县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3087, '620000', '623000', '623025', '玛曲县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3088, '620000', '623000', '623026', '碌曲县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3089, '620000', '623000', '623027', '夏河县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3090, '630000', '630100', '630101', '市辖区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3091, '630000', '630100', '630102', '城东区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3092, '630000', '630100', '630103', '城中区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3093, '630000', '630100', '630104', '城西区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3094, '630000', '630100', '630105', '城北区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3095, '630000', '630100', '630106', '湟中区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3096, '630000', '630100', '630121', '大通回族土族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3097, '630000', '630100', '630123', '湟源县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3098, '630000', '630200', '630202', '乐都区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3099, '630000', '630200', '630203', '平安区', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3100, '630000', '630200', '630222', '民和回族土族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3101, '630000', '630200', '630223', '互助土族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3102, '630000', '630200', '630224', '化隆回族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3103, '630000', '630200', '630225', '循化撒拉族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3104, '630000', '632200', '632221', '门源回族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3105, '630000', '632200', '632222', '祁连县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3106, '630000', '632200', '632223', '海晏县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3107, '630000', '632200', '632224', '刚察县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3108, '630000', '632300', '632321', '同仁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3109, '630000', '632300', '632322', '尖扎县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3110, '630000', '632300', '632323', '泽库县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3111, '630000', '632300', '632324', '河南蒙古族自治县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3112, '630000', '632500', '632521', '共和县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3113, '630000', '632500', '632522', '同德县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3114, '630000', '632500', '632523', '贵德县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3115, '630000', '632500', '632524', '兴海县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3116, '630000', '632500', '632525', '贵南县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3117, '630000', '632600', '632621', '玛沁县', '1', '2021-08-24 19:04:53', '1', '2021-08-24 19:04:53', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3118, '630000', '632600', '632622', '班玛县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3119, '630000', '632600', '632623', '甘德县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3120, '630000', '632600', '632624', '达日县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3121, '630000', '632600', '632625', '久治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3122, '630000', '632600', '632626', '玛多县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3123, '630000', '632700', '632701', '玉树市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3124, '630000', '632700', '632722', '杂多县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3125, '630000', '632700', '632723', '称多县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3126, '630000', '632700', '632724', '治多县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3127, '630000', '632700', '632725', '囊谦县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3128, '630000', '632700', '632726', '曲麻莱县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3129, '630000', '632800', '632801', '格尔木市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3130, '630000', '632800', '632802', '德令哈市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3131, '630000', '632800', '632803', '茫崖市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3132, '630000', '632800', '632821', '乌兰县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3133, '630000', '632800', '632822', '都兰县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3134, '630000', '632800', '632823', '天峻县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3135, '630000', '632800', '632857', '大柴旦行政委员会', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3136, '640000', '640100', '640101', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3137, '640000', '640100', '640104', '兴庆区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3138, '640000', '640100', '640105', '西夏区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3139, '640000', '640100', '640106', '金凤区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3140, '640000', '640100', '640121', '永宁县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3141, '640000', '640100', '640122', '贺兰县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3142, '640000', '640100', '640181', '灵武市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3143, '640000', '640200', '640201', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3144, '640000', '640200', '640202', '大武口区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3145, '640000', '640200', '640205', '惠农区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3146, '640000', '640200', '640221', '平罗县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3147, '640000', '640300', '640301', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3148, '640000', '640300', '640302', '利通区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3149, '640000', '640300', '640303', '红寺堡区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3150, '640000', '640300', '640323', '盐池县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3151, '640000', '640300', '640324', '同心县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3152, '640000', '640300', '640381', '青铜峡市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3153, '640000', '640400', '640401', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3154, '640000', '640400', '640402', '原州区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3155, '640000', '640400', '640422', '西吉县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3156, '640000', '640400', '640423', '隆德县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3157, '640000', '640400', '640424', '泾源县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3158, '640000', '640400', '640425', '彭阳县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3159, '640000', '640500', '640501', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3160, '640000', '640500', '640502', '沙坡头区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3161, '640000', '640500', '640521', '中宁县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3162, '640000', '640500', '640522', '海原县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3163, '650000', '650100', '650101', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3164, '650000', '650100', '650102', '天山区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3165, '650000', '650100', '650103', '沙依巴克区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3166, '650000', '650100', '650104', '新市区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3167, '650000', '650100', '650105', '水磨沟区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3168, '650000', '650100', '650106', '头屯河区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3169, '650000', '650100', '650107', '达坂城区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3170, '650000', '650100', '650109', '米东区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3171, '650000', '650100', '650121', '乌鲁木齐县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3172, '650000', '650200', '650201', '市辖区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3173, '650000', '650200', '650202', '独山子区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3174, '650000', '650200', '650203', '克拉玛依区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3175, '650000', '650200', '650204', '白碱滩区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3176, '650000', '650200', '650205', '乌尔禾区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3177, '650000', '650400', '650402', '高昌区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3178, '650000', '650400', '650421', '鄯善县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3179, '650000', '650400', '650422', '托克逊县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3180, '650000', '650500', '650502', '伊州区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3181, '650000', '650500', '650521', '巴里坤哈萨克自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3182, '650000', '650500', '650522', '伊吾县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3183, '650000', '652300', '652301', '昌吉市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3184, '650000', '652300', '652302', '阜康市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3185, '650000', '652300', '652323', '呼图壁县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3186, '650000', '652300', '652324', '玛纳斯县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3187, '650000', '652300', '652325', '奇台县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3188, '650000', '652300', '652327', '吉木萨尔县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3189, '650000', '652300', '652328', '木垒哈萨克自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3190, '650000', '652700', '652701', '博乐市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3191, '650000', '652700', '652702', '阿拉山口市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3192, '650000', '652700', '652722', '精河县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3193, '650000', '652700', '652723', '温泉县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3194, '650000', '652800', '652801', '库尔勒市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3195, '650000', '652800', '652822', '轮台县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3196, '650000', '652800', '652823', '尉犁县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3197, '650000', '652800', '652824', '若羌县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3198, '650000', '652800', '652825', '且末县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3199, '650000', '652800', '652826', '焉耆回族自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3200, '650000', '652800', '652827', '和静县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3201, '650000', '652800', '652828', '和硕县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3202, '650000', '652800', '652829', '博湖县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3203, '650000', '652800', '652871', '经济技术开发区', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3204, '650000', '652900', '652901', '阿克苏市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3205, '650000', '652900', '652902', '库车市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3206, '650000', '652900', '652922', '温宿县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3207, '650000', '652900', '652924', '沙雅县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3208, '650000', '652900', '652925', '新和县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3209, '650000', '652900', '652926', '拜城县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3210, '650000', '652900', '652927', '乌什县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3211, '650000', '652900', '652928', '阿瓦提县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3212, '650000', '652900', '652929', '柯坪县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3213, '650000', '653000', '653001', '阿图什市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3214, '650000', '653000', '653022', '阿克陶县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3215, '650000', '653000', '653023', '阿合奇县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3216, '650000', '653000', '653024', '乌恰县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3217, '650000', '653100', '653101', '喀什市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3218, '650000', '653100', '653121', '疏附县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3219, '650000', '653100', '653122', '疏勒县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3220, '650000', '653100', '653123', '英吉沙县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3221, '650000', '653100', '653124', '泽普县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3222, '650000', '653100', '653125', '莎车县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3223, '650000', '653100', '653126', '叶城县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3224, '650000', '653100', '653127', '麦盖提县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3225, '650000', '653100', '653128', '岳普湖县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3226, '650000', '653100', '653129', '伽师县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3227, '650000', '653100', '653130', '巴楚县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3228, '650000', '653100', '653131', '塔什库尔干塔吉克自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3229, '650000', '653200', '653201', '和田市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3230, '650000', '653200', '653221', '和田县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3231, '650000', '653200', '653222', '墨玉县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3232, '650000', '653200', '653223', '皮山县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3233, '650000', '653200', '653224', '洛浦县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3234, '650000', '653200', '653225', '策勒县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3235, '650000', '653200', '653226', '于田县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3236, '650000', '653200', '653227', '民丰县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3237, '650000', '654000', '654002', '伊宁市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3238, '650000', '654000', '654003', '奎屯市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3239, '650000', '654000', '654004', '霍尔果斯市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3240, '650000', '654000', '654021', '伊宁县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3241, '650000', '654000', '654022', '察布查尔锡伯自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3242, '650000', '654000', '654023', '霍城县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3243, '650000', '654000', '654024', '巩留县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3244, '650000', '654000', '654025', '新源县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3245, '650000', '654000', '654026', '昭苏县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3246, '650000', '654000', '654027', '特克斯县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3247, '650000', '654000', '654028', '尼勒克县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3248, '650000', '654200', '654201', '塔城市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3249, '650000', '654200', '654202', '乌苏市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3250, '650000', '654200', '654221', '额敏县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3251, '650000', '654200', '654223', '沙湾县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3252, '650000', '654200', '654224', '托里县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3253, '650000', '654200', '654225', '裕民县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3254, '650000', '654200', '654226', '和布克赛尔蒙古自治县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3255, '650000', '654300', '654301', '阿勒泰市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3256, '650000', '654300', '654321', '布尔津县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3257, '650000', '654300', '654322', '富蕴县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3258, '650000', '654300', '654323', '福海县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3259, '650000', '654300', '654324', '哈巴河县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3260, '650000', '654300', '654325', '青河县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3261, '650000', '654300', '654326', '吉木乃县', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3262, '650000', '659000', '659001', '石河子市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3263, '650000', '659000', '659002', '阿拉尔市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3264, '650000', '659000', '659003', '图木舒克市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3265, '650000', '659000', '659004', '五家渠市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3266, '650000', '659000', '659005', '北屯市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3267, '650000', '659000', '659006', '铁门关市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3268, '650000', '659000', '659007', '双河市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3269, '650000', '659000', '659008', '可克达拉市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3270, '650000', '659000', '659009', '昆玉市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3271, '650000', '659000', '659010', '胡杨河市', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3272, '710000', '710100', '710101', '台湾', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3273, '810000', '810100', '810101', '香港', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);
INSERT INTO `ly_region` VALUES (3274, '820000', '820100', '820101', '澳门', '1', '2021-08-24 19:04:54', '1', '2021-08-24 19:04:54', '0', NULL, NULL);

-- ----------------------------
-- Table structure for ly_role
-- ----------------------------
DROP TABLE IF EXISTS `ly_role`;
CREATE TABLE `ly_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：角色数据权限）',
  `role_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0启用 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0正常 2删除）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_role
-- ----------------------------
INSERT INTO `ly_role` VALUES (1, '超级管理员', 'admin', '1', '0', '0', '2022-03-16 21:00:18', '2022-03-16 21:00:20');
INSERT INTO `ly_role` VALUES (2, '普通角色', 'common', '3', '1', '0', '2022-03-16 21:00:18', '2022-07-09 20:58:12');
INSERT INTO `ly_role` VALUES (10, '发文24', 'fawen24', '1', '1', '0', '2022-03-27 12:35:00', '2022-09-12 23:06:45');

-- ----------------------------
-- Table structure for ly_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ly_role_menu`;
CREATE TABLE `ly_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单 关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_role_menu
-- ----------------------------
INSERT INTO `ly_role_menu` VALUES (2, 2);
INSERT INTO `ly_role_menu` VALUES (2, 25);
INSERT INTO `ly_role_menu` VALUES (2, 6);
INSERT INTO `ly_role_menu` VALUES (2, 35);
INSERT INTO `ly_role_menu` VALUES (2, 36);
INSERT INTO `ly_role_menu` VALUES (2, 37);
INSERT INTO `ly_role_menu` VALUES (2, 38);
INSERT INTO `ly_role_menu` VALUES (2, 39);
INSERT INTO `ly_role_menu` VALUES (2, 40);
INSERT INTO `ly_role_menu` VALUES (2, 41);
INSERT INTO `ly_role_menu` VALUES (2, 28);
INSERT INTO `ly_role_menu` VALUES (11, 1);
INSERT INTO `ly_role_menu` VALUES (13, 1);
INSERT INTO `ly_role_menu` VALUES (13, 2);
INSERT INTO `ly_role_menu` VALUES (13, 5);
INSERT INTO `ly_role_menu` VALUES (13, 6);
INSERT INTO `ly_role_menu` VALUES (13, 25);
INSERT INTO `ly_role_menu` VALUES (13, 42);
INSERT INTO `ly_role_menu` VALUES (13, 1546364785459765262);
INSERT INTO `ly_role_menu` VALUES (13, 1546364785459765263);
INSERT INTO `ly_role_menu` VALUES (13, 1546364785459765264);
INSERT INTO `ly_role_menu` VALUES (14, 2);
INSERT INTO `ly_role_menu` VALUES (14, 5);
INSERT INTO `ly_role_menu` VALUES (14, 6);
INSERT INTO `ly_role_menu` VALUES (14, 25);
INSERT INTO `ly_role_menu` VALUES (14, 42);
INSERT INTO `ly_role_menu` VALUES (14, 1546364785459765262);
INSERT INTO `ly_role_menu` VALUES (14, 1546364785459765263);
INSERT INTO `ly_role_menu` VALUES (14, 1546364785459765264);
INSERT INTO `ly_role_menu` VALUES (16, 1);
INSERT INTO `ly_role_menu` VALUES (16, 3);
INSERT INTO `ly_role_menu` VALUES (16, 26);
INSERT INTO `ly_role_menu` VALUES (17, 1);

-- ----------------------------
-- Table structure for ly_user
-- ----------------------------
DROP TABLE IF EXISTS `ly_user`;
CREATE TABLE `ly_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `user_type` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户类型（0系统用户 1注册用户）',
  `user_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 （0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `user_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '账号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录ip',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 696 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_user
-- ----------------------------
INSERT INTO `ly_user` VALUES (24, '2418870649@qq.com', '没事别学JAVA1', '0', '2418870649@qq.com', '15037576473', '1', '/ly/ly-images/2021/11/24/3a246134a17fdc24fe285dd6a8c6f37f.jpg', '$2a$10$WVBOGEKPAG0u3O0G.r1HveJUvwgv6mihWXjrysJF/XoNsTM6uezz.', '0', '0', '127.0.0.1', '2021-11-14 21:03:36');
INSERT INTO `ly_user` VALUES (49, '486612180@qq.com', '天下11111111', '1', '2418870649@qq.com', '15037576473', '1', '/ly/ly-images/2021/11/24/a15c06463bf859e6c3ac524d62e3d21e.png', '$2a$10$WVBOGEKPAG0u3O0G.r1HveJUvwgv6mihWXjrysJF/XoNsTM6uezz.', '0', '0', '127.0.0.1', '2021-11-14 21:03:36');
INSERT INTO `ly_user` VALUES (687, '632330628@qq.com', '测试11', '1', '632330628@qq.com', '15037576473', '1', '/ly/ly-images\\2022/06/12/59ed4b08eafbd579c5cecaf7265075df.jpg', '$2a$10$eowRHHf7fXCnuPCiOvNvNeobjvu7E9KOGJkAuMCSuNnvddtNJWxR2', '1', '0', NULL, NULL);
INSERT INTO `ly_user` VALUES (688, '26541254@qq.com', '26541254@qq.com', '1', '26541254@qq.com', NULL, '0', '/ly/ly-images/凌云.png', '$2a$10$E27ZTmzHwkR.1t8mTJdVIebHLafS6j2vwsQbBaFXBT3fE6nX4b6AW', '0', '0', NULL, NULL);
INSERT INTO `ly_user` VALUES (689, '13938653722@qq.com', '13938653722@qq.com', '0', '13938653722@qq.com', '13938653722', '0', '/ly/ly-images\\2022/07/14/42628f4c6cb605aaab8fdb858eaeafd4.jpg', '$2a$10$NfXxsQkFOcrxjS5M21q37u2QU9ZQx9PACBmglFu7NXLUtNrN51DP6', '0', '0', NULL, NULL);
INSERT INTO `ly_user` VALUES (690, '1718812578@qq.com', '无心', '0', '11111111@qq.com', '15037576473', '1', '/ly/ly-images\\2022/07/14/e3a2c8a62bf8552698a5490d4d0810d4.jpg', '$2a$10$mXtzLP7k7uLHaROlQRt5yO1Fk.35V84kpZ.Yc37QSszVz6LSuYe7i', '0', '0', NULL, NULL);
INSERT INTO `ly_user` VALUES (694, '5465@qq.com', '5465@qq.com', '0', '5465@qq.com', '15037576475', '0', '/ly/ly-images\\2022/07/14/a15cb95dafec9ba9a5996da1287f7f31.jpg', '$2a$10$NIcEBTke1Vo8kdpwVfVDcez0v4XvlIUi87b/cAk4hxfPSBXr4Wmfu', '0', '0', NULL, NULL);
INSERT INTO `ly_user` VALUES (696, '8', '8', '0', '7@qq.com', '15037576473', '1', '/ly/ly-images\\2022/09/13/5d6de26a6310649e69f948d3cacf42a4.png', '$2a$10$/XYNFIyxLZl9HHWAs/o.LOEweS2WsUSmmAacd/yGO0LQoAr.PTPsa', '0', '0', NULL, NULL);

-- ----------------------------
-- Table structure for ly_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ly_user_role`;
CREATE TABLE `ly_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色 关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ly_user_role
-- ----------------------------
INSERT INTO `ly_user_role` VALUES (24, 1);
INSERT INTO `ly_user_role` VALUES (49, 2);
INSERT INTO `ly_user_role` VALUES (683, 10);
INSERT INTO `ly_user_role` VALUES (684, 2);
INSERT INTO `ly_user_role` VALUES (685, 1);
INSERT INTO `ly_user_role` VALUES (686, 1);
INSERT INTO `ly_user_role` VALUES (687, 10);
INSERT INTO `ly_user_role` VALUES (688, 1);
INSERT INTO `ly_user_role` VALUES (689, 2);
INSERT INTO `ly_user_role` VALUES (690, 2);
INSERT INTO `ly_user_role` VALUES (691, 1);
INSERT INTO `ly_user_role` VALUES (692, 2);
INSERT INTO `ly_user_role` VALUES (693, 2);
INSERT INTO `ly_user_role` VALUES (694, 1);
INSERT INTO `ly_user_role` VALUES (696, 2);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `SCHED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(6) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
