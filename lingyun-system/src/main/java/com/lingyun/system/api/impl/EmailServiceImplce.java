package com.lingyun.system.api.impl;


import com.lingyun.system.api.EmailService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 * @author 没事别学JAVA
 *
 */
@Service("EmailService")
public class EmailServiceImplce implements EmailService {


    @Resource
    private JavaMailSender mailSender;

    @Override
    public void Email(String from, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        // 发送人的邮箱
        message.setFrom("zkc4918@163.com");
        //发给谁  对方邮箱
        message.setTo(from);
        //标题
        message.setSubject("欢迎注册凌云系统:");
        //内容
        message.setText(code);
        //发送
        mailSender.send(message);
    }



}
