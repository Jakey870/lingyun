package com.lingyun.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.mapper.LyArticleClassifyMapper;
import com.lingyun.common.mapper.LyArticleMapper;
import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.pojo.LyArticleClassify;
import com.lingyun.common.vo.article.ArticleSearch;
import com.lingyun.system.service.LyArticleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * (LyArticle)表服务实现类
 *
 * @author 没事别学JAVA
 * @since 2021-04-29 12:46:38
 */
@Service("lyArticleService")
public class LyArticleServiceImpl extends ServiceImpl<LyArticleMapper, LyArticle> implements LyArticleService {


    @Resource
    LyArticleMapper lyArticleMapper;

    @Resource
    LyArticleClassifyMapper lyArticleClassifyMapper;

    @Override
    public IPage<LyArticle> articleList(ArticleSearch articleSearch) {
        QueryWrapper<LyArticle> queryWrapper = new QueryWrapper<>();
        if (!StrUtil.hasEmpty(articleSearch.getArticleName())) {
            queryWrapper.like("article.article_name", articleSearch.getArticleName());
        }
        if (!StrUtil.hasEmpty(articleSearch.getArticleStatus())) {
            queryWrapper.eq("article.status", articleSearch.getArticleStatus());
        }
        if (articleSearch.getClassifyId()!=null) {
            queryWrapper.eq("classify.classify_id", articleSearch.getClassifyId());
        }
        queryWrapper.orderByAsc("create_date");  //以文章创建时间排序
        Page<LyArticle> page = new Page<>(articleSearch.getCurrent(),articleSearch.getSize());
        IPage<LyArticle> iPage = lyArticleMapper.articleList(page,queryWrapper);
        return iPage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer insertArticle(LyArticle lyArticle) {
        lyArticle.setDelDelete("0");
        lyArticle.setArticleStatus("1");
        lyArticle.setCreateDate(new Date());
        lyArticle.setUpdateDate(new Date());

        // 添加文章
        int row=lyArticleMapper.insert(lyArticle);


        // 判断是否有 选择分类
        if(lyArticle.getClassifyId()!=null){
            // 添加文章与分类关联
            LyArticleClassify lyArticleClassify=new LyArticleClassify();
            lyArticleClassify.setArticleId(lyArticle.getArticleId());
            lyArticleClassify.setClassifyId(lyArticle.getClassifyId());
            lyArticleClassifyMapper.insert(lyArticleClassify);
        }
        return row;
    }

    @Override
    public LyArticle getArticleById(Long articleId) {
        LyArticle lyArticle = lyArticleMapper.getArticleById(articleId);
        if(ObjectUtil.isNotNull(lyArticle.getLyClassify())){
            lyArticle.setClassifyId(lyArticle.getLyClassify().getClassifyId());
        }
        return lyArticle;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateArticleId(LyArticle lyArticle) {
        int row= lyArticleMapper.updateById(lyArticle);
        // 判断是否有 选择分类
        if(lyArticle.getClassifyId()!=null){


            lyArticleClassifyMapper.deleteByArticleId(lyArticle.getArticleId());


            // 添加文章与分类关联
            LyArticleClassify lyArticleClassify=new LyArticleClassify();
            lyArticleClassify.setArticleId(lyArticle.getArticleId());
            lyArticleClassify.setClassifyId(lyArticle.getClassifyId());
            lyArticleClassifyMapper.insert(lyArticleClassify);
        }
        return row;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteArticleById(Long articleId) {
        lyArticleClassifyMapper.deleteByArticleId(articleId);
        int row= lyArticleMapper.deleteById(articleId);
        if(row>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteArticleByIds(List<Long> articleId) {

        for (Long id : articleId) {
            lyArticleClassifyMapper.deleteByArticleId(id);
        }

        int row= lyArticleMapper.deleteBatchIds(articleId);
        if(row>0){
            return true;
        }
        return false;
    }




}
