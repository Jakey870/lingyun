package com.lingyun.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyMenu;
import com.lingyun.common.vo.menu.Router;

import java.util.List;
import java.util.Set;

/**
 * (LyLeftNav)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2021-11-10 16:19:26
 */
public interface LyMenuService extends IService<LyMenu> {

    /**
     * 查询登录角色拥有的菜单
     * @return  菜单集合
     */
    public List<LyMenu> menuList();

    /**
     * 查询对应角色的菜单列表
     * @return  对应角色的菜单列表
     */
    public List<LyMenu> roleMenuTress();

    /**
     * 构建前端路由所需要的左侧导航栏菜单
     */
    public List<Router> buildMenu(List<LyMenu> menus);

    /**
     * 更具用户ID获取权限
     * @param userId 用户id
     * @return 当前用户 校色的权限
     */
    public Set<String> selectUserPermsByUserId(Long userId);

    /**
     * 为 admin 角色获取全部权限
     * @return 返回全部权限
     */
    public Set<String> selectAdminUserPermsAll();


    /**
     * 根据角色id 获取角色所拥有的导航 id 列表
     * @param roleId 角色id
     * @return
     */
    public List<Long> selectMenuByRoleId(Long roleId);


    /**
     * 根据父id获取 子菜单 和 子按钮
     *
     * @param parentId  父id
     * @return 集合
     */
    public List<Long> selectParentId(Long parentId);






}
