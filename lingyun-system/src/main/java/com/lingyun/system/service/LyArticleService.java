package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.vo.article.ArticleSearch;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * (LyArticle)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2021-04-29 12:46:38
 */
public interface LyArticleService extends IService<LyArticle> {


    /**
     * 文章分页查询
     * @param articleSearch 搜索条件表单
     * @return 分页列表
     */
    public IPage<LyArticle> articleList(ArticleSearch articleSearch);


    /**
     * 添加文章
     * @param lyArticle 文章表单
     * @return
     */
    public Integer insertArticle(LyArticle lyArticle);


    /**
     * 根据id查看文章
     *
     * @param articleId 文章id
     * @return 返回当前返回的文章
     */
    public LyArticle getArticleById(Long articleId);


    /**
     * 根据id修改文章信息
     *
     * @param lyArticle 文章表单
     * @return
     */
    public Integer updateArticleId(LyArticle lyArticle);


    /**
     * 根据id单个删除
     * @param articleId 文章id
     * @return
     */
    public boolean deleteArticleById(Long articleId);

    /**
     * 根据id集合批量删除
     *
     * @param articleIds  文章id集合
     * @return
     */
    public boolean deleteArticleByIds(List<Long> articleIds);









}
