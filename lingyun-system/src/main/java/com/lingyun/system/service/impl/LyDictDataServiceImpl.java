package com.lingyun.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.mapper.LyDictDataMapper;
import com.lingyun.common.pojo.LyDictData;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.vo.dict.DictDataPage;
import com.lingyun.system.service.LyDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (LyDictData)表服务实现类
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:18:48
 */
@Service("lyDictDataService")
public class LyDictDataServiceImpl extends ServiceImpl<LyDictDataMapper, LyDictData> implements LyDictDataService {

    @Resource
    LyDictDataMapper lyDictDataMapper;

    @Override
    public List<LyDictData> queryDictDataByType(String dictType) {
        return lyDictDataMapper.queryDictDataByType(dictType);
    }

    @Override
    public IPage<LyDictData> dictDataList(DictDataPage dictDataPage) {
        Page<LyDictData> page = new Page<>(dictDataPage.getCurrent(), dictDataPage.getSize());
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("dict_type",dictDataPage.getDictType());

        if(!StrUtil.hasEmpty(dictDataPage.getDictLabel())){
            queryWrapper.eq("dict_label",dictDataPage.getDictLabel());
        }
        if(!StrUtil.hasEmpty(dictDataPage.getStatus())){
            queryWrapper.eq("status",dictDataPage.getStatus());
        }
        if(!StrUtil.hasEmpty(dictDataPage.getDateTimeRangeCreate())){
            queryWrapper.between("create_time",dictDataPage.getDateTimeRangeCreate()[0],dictDataPage.getDateTimeRangeCreate()[1]);
        }
        if(!StrUtil.hasEmpty(dictDataPage.getDateTimeRangeUpdate())){
            queryWrapper.between("update_time",dictDataPage.getDateTimeRangeUpdate()[0],dictDataPage.getDateTimeRangeUpdate()[1]);
        }
        queryWrapper.orderByAsc("dict_code");
        IPage<LyDictData> iPage = lyDictDataMapper.dictDataList(page,queryWrapper);
        return iPage;
    }

    @Override
    public LyDictData selectDictDataById(Long dictCode) {
        return lyDictDataMapper.selectDictDataById(dictCode);
    }


}
