package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.exception.dict.DictHaveDataException;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.vo.dict.DictPage;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * (LyDictType)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:18:48
 */
public interface LyDictTypeService extends IService<LyDictType> {


    /**
     * 字典类型列表
     * @param dictPage 字典类型搜索表单
     * @return
     */
    IPage<LyDictType> dictList(DictPage dictPage);


    /**
     * 更具id 查询字典类型
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    public LyDictType selectDictById(Long dictId);


    /**
     * 查询所有字典类型
     * @return  字典类型列表
     */
    public List<LyDictType> selectDictTypeAll();


    /**
     * 根据id 删除 字典类型
     * @param dictId 字典id
     * @return
     */
    public Integer deleteDictTypeById(Long dictId) throws DictHaveDataException;


    /**
     * 批量删除字典类型
     * @param dictId 字典id集合
     * @return
     */
    public Boolean deleteDictTypeByIds(List<Long> dictId)throws DictHaveDataException;

}
