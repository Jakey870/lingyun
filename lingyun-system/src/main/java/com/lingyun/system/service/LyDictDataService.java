package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyDictData;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.vo.dict.DictDataPage;

import java.util.List;

/**
 * (LyDictData)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:18:48
 */
public interface LyDictDataService extends IService<LyDictData> {

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    public List<LyDictData> queryDictDataByType(String dictType);


    /**
     * 更具字典类型 查询 字典数据列表
     * @param dictDataPage 字典数据搜索表单
     * @return 字典数据列表
     */
    IPage<LyDictData> dictDataList(DictDataPage dictDataPage);


    /**
     *根据字典数据编码查询字典数据
     * @param dictCode 字典编码
     * @return 字典数据信息
     */
    public LyDictData selectDictDataById(Long dictCode);


}
