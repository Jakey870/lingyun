package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.exception.role.RoleHaveUserCanNotDelException;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.pojo.LyUserRole;
import com.lingyun.common.vo.role.RoleVagueQuery;
import com.lingyun.common.vo.user.UserQueryCriteria;

import java.util.List;

/**
 * @author 没事别学JAVA
 * 2021/12/16 0:56
 */
public interface LyRoleService extends IService<LyRole> {


    /**
     * 查询角色列表（下拉框）
     *
     * @return  角色集合
     */
    public List<LyRole> selectRole();



    /**
     * 获取所有角色
     * @return 角色集合
     */
    public IPage<LyRole> roleList(RoleVagueQuery roleVagueQueryVo);



    /**
     * 角色单个删除
     * @return 是否删除成功
     */
    public boolean deleteRoleById(Long roleId) throws RoleHaveUserCanNotDelException;

    /**
     * 根据id获取校色信息
     * @return 角色信息
     */
    public LyRole selectRoleById(Long RoleId);


    /**
     * 修改 保存校色信息
     */
    public Integer updateRole(LyRole lyRole);


    /**
     * 保存角色信息
     */
    public Integer addRole(LyRole lyRole);


    /**
     *导出角色Excel信息-也可添加条件
     *
     */
    public List<LyRole> roleExportExcel(RoleVagueQuery roleVagueQueryVo);


    /**
     * 根据角色id 查询 用户
     * @param userVagueQuery 用户搜索条件
     * @return  用户集合
     */
    public IPage<LyUser> selectRoleUser(UserQueryCriteria userVagueQuery);


    /**
     * 取消绑定（将用户与角色解除关联）
     * @param lyUserRole 角色id 和 用户id
     * @return 是否解除成功
     */
    public Boolean cancelBinding(LyUserRole lyUserRole);


    /**
     * 批量取消绑定
     * @param roleId 角色id
     * @param userIds 用户id
     * @return 是否解除成功
     */
    public Boolean batchCancelBinding(Long roleId, Long[] userIds);

    /**
     *
     * 绑定用户
     * @param roleId 角色id
     * @param userIds 用户id
     * @return 是否绑定成功
     */
    public Boolean bindingUser(Long roleId, Long[] userIds);

}
