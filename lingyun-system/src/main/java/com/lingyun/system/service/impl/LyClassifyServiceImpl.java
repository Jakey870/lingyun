package com.lingyun.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.mapper.LyClassifyMapper;
import com.lingyun.common.pojo.LyClassify;
import com.lingyun.system.service.LyClassifyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("lyClassifyService")
public class LyClassifyServiceImpl extends ServiceImpl<LyClassifyMapper, LyClassify> implements LyClassifyService {


    @Resource
    LyClassifyMapper lyClassifyMapper;

    @Override
    public List<LyClassify> queryAllClassifyList() {
        return lyClassifyMapper.queryAllClassifyList();
    }
}
