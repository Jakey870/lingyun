package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.exception.ServiceException;
import com.lingyun.common.exception.user.CaptchaException;
import com.lingyun.common.exception.user.CaptchaExpireException;
import com.lingyun.common.exception.user.UserNotFoundException;
import com.lingyun.common.exception.user.UserPasswordNotMatchException;
import com.lingyun.common.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.exception.LyException;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.common.vo.user.UserRegister;
import com.lingyun.common.vo.user.UserQueryCriteria;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * (LyUser)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2021-03-07 17:06:47
 */
public interface LyUserService extends IService<LyUser> {
    /**
     *
     * 邮箱注册-发送验证码
     *
     * */
    void SendEmailVerificationCode(String Email) throws LyException;
    /**
     *
     * 邮箱注册
     *
     * */
    void lyRegistered(UserRegister userRegisterVO) throws LyException;
    /**
     *
     * 登录
     *
     * */
    String lyLogin(LoginFrom loginFrom, HttpServletRequest request) throws CaptchaExpireException, CaptchaException, UserPasswordNotMatchException, ServiceException, UserNotFoundException;

    /**
     *
     * @param userQueryCriteria 用户查询条件
     * @return 后台用户全部查询接口
     */
    IPage<UserAll> getUserList(UserQueryCriteria userQueryCriteria);
    /**
     *
     * 添加用户
     *
     */
    public boolean addUser(UserFrom userFromVo) throws LyException;
    /**
     *
     * 根据id查询用户详细信息
     *
     * @return
     */
    public UserFrom selectUserById(Long userId);

    /**
     * 根据用户id 获取角色列表
     * @param userid 用户id
     * @return
     */
    List<Long> getUserRole(Long userid);

    /**
     * 修改用户信息
     * @param userFromVo 提交的用户信息
     * @return 结果
     */
    public Boolean updateUser(UserFrom userFromVo);


    /**
     * 导出用户Excel信息
     * @param userVagueQueryVo 用户查询条件
     * @return 文件名字
     */
    public List<UserAll> userExportExcel(UserQueryCriteria userVagueQueryVo);


    /**
     * 授权用户列表
     * @param userQueryCriteria 用户查询条件
     * @return 授权用户列表
     */
    public IPage<UserAll> authorizationUserList(UserQueryCriteria userQueryCriteria);

















}
