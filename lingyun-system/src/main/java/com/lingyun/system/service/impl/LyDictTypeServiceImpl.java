package com.lingyun.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.exception.dict.DictHaveDataException;
import com.lingyun.common.mapper.LyDictDataMapper;
import com.lingyun.common.mapper.LyDictTypeMapper;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.vo.dict.DictPage;
import com.lingyun.system.service.LyDictTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (LyDictType)表服务实现类
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:18:48
 */
@Service("lyDictTypeService")
public class LyDictTypeServiceImpl extends ServiceImpl<LyDictTypeMapper, LyDictType> implements LyDictTypeService {


    @Resource
    LyDictTypeMapper lyDictTypeMapper;

    @Resource
    LyDictDataMapper lyDictDataMapper;


    @Override
    public IPage<LyDictType> dictList(DictPage dictPage) {
        Page<LyDictType> page = new Page<>(dictPage.getCurrent(), dictPage.getSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        if(!StrUtil.hasEmpty(dictPage.getDictName())){
            queryWrapper.eq("dict_name",dictPage.getDictName());
        }
        if(!StrUtil.hasEmpty(dictPage.getDictType())){
            queryWrapper.eq("dict_type",dictPage.getDictType());
        }
        if(!StrUtil.hasEmpty(dictPage.getStatus())){
            queryWrapper.eq("status",dictPage.getStatus());
        }
        if(!StrUtil.hasEmpty(dictPage.getDateTimeRangeCreate())){
            queryWrapper.between("create_time",dictPage.getDateTimeRangeCreate()[0],dictPage.getDateTimeRangeCreate()[1]);
        }
        if(!StrUtil.hasEmpty(dictPage.getDateTimeRangeUpdate())){
            queryWrapper.between("update_time",dictPage.getDateTimeRangeUpdate()[0],dictPage.getDateTimeRangeUpdate()[1]);
        }
        queryWrapper.orderByAsc("dict_id");
        IPage<LyDictType> iPage = lyDictTypeMapper.dictList(page,queryWrapper);
        return iPage;
    }


    @Override
    public LyDictType selectDictById(Long dictId) {

        return lyDictTypeMapper.selectDictById(dictId);

    }

    @Override
    public List<LyDictType> selectDictTypeAll() {

        return lyDictTypeMapper.selectDictTypeAll();

    }

    @Override
    public Integer deleteDictTypeById(Long dictId) throws DictHaveDataException {

        LyDictType dictType = lyDictTypeMapper.selectDictById(dictId);

        if(ObjectUtils.isNotEmpty(dictType)){
            if(lyDictDataMapper.queryDictDataByType(dictType.getDictType()).size() == 0){
                return lyDictTypeMapper.deleteById(dictId);
            }else{
                throw new DictHaveDataException();
            }
        }
        return 0;

    }

    @Override
    public Boolean deleteDictTypeByIds(List<Long> dictId) throws DictHaveDataException {

        boolean is = false;

        for (Long id: dictId) {

            LyDictType dictType = lyDictTypeMapper.selectDictById(id);

            if(ObjectUtils.isNotEmpty(dictType)){
                if(lyDictDataMapper.queryDictDataByType(dictType.getDictType()).size() == 0){
                    if(lyDictTypeMapper.deleteById(id)>0){
                        is = true;
                    }
                }else{
                    throw new DictHaveDataException();
                }
            }

        }

        return is;
    }


}
