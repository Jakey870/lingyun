package com.lingyun.admin.controller.system.nav;


import com.lingyun.common.pojo.LyMenu;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.tree.TreeSelect;
import com.lingyun.system.service.LyMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/menu")
@Api(value = "菜单管理", description = "")
public class MenuController {


    @Resource
    LyMenuService menuService;



    /**
     * @return 获取左侧导航栏
     */
    @ApiOperation(value = "获取左侧导航栏")
    @GetMapping("/getAllRouters")
    public AjaxResult getAllRouters(){
        List<LyMenu> navlist= menuService.menuList();
        return AjaxResult.success(menuService.buildMenu(navlist));
    }

    /**
     * 获取当前登录角色的 所拥有的菜单权限  和 根据角色id获取修改的角色的菜单id
     */
    @ApiOperation(value = "获取当前登录角色的 所拥有的菜单权限  和 根据角色id获取修改的角色的菜单id")
    @GetMapping("/roleMenuTress/{roleId}")
    public AjaxResult roleMenuTress(@PathVariable Long roleId){
        List<LyMenu> navlist=menuService.roleMenuTress();
        AjaxResult ajax = AjaxResult.success();
        ajax.put("role_menu_data",navlist.stream().map(TreeSelect::new).collect(Collectors.toList()));
        ajax.put("menu_id",menuService.selectMenuByRoleId(roleId));
        return ajax;
    }

    /**
     * 获取当前角色所拥有的 菜单
     */
    @ApiOperation(value = "获取当前角色所拥有的 菜单")
    @GetMapping("/treeSelect")
    public AjaxResult treeselect() {
        List<LyMenu> navlist=menuService.roleMenuTress();
        return AjaxResult.success(navlist.stream().map(TreeSelect::new).collect(Collectors.toList()));
    }


















    /**
     *
     * @return  菜单列表
     */
    @PreAuthorize("hasRole('system:menu:querymenulist')")
    @ApiOperation(value = "菜单列表")
    @GetMapping("/queryMenuList")
    public AjaxResult queryMenuList(){
        return AjaxResult.success(menuService.list());
    }


    /**
     * 添加菜单
     * @param lyNav 添加菜单表单
     * @return 添加成功  或者 添加失败
     */
    @PreAuthorize("hasRole('system:menu:addmenu')")
    @ApiOperation(value = "添加菜单")
    @PostMapping("/addMenu")
    public AjaxResult addMenu(@RequestBody LyMenu lyNav){

        if(lyNav.getParentId()==null){  // 如果 parentId == null 说明 本次创建为菜单   要给parentId赋值0
            lyNav.setParentId(0L);
        }

        if(lyNav.getParentId()!=0){
            if(menuService.getById(lyNav.getParentId()).getMenuType().equals("C")){  // 拿着添加的 父id（ParentId ），查询出父类信息，如果父类是C（按钮） 则无法添加
                return AjaxResult.error("无法添加，按钮无法在添加下级目录、菜单、按钮");
            }
        }

        // 添加更新时间
        lyNav.setUpdateTime(new Date());
        if(menuService.save(lyNav)){
            return AjaxResult.success("保存成功");
        }else{
            return AjaxResult.error("保存失败");
        }

    }

    /**
     * 更具菜单id查询菜单信息
     * @param navId 菜单id
     * @return 菜单信息
     */
    @PreAuthorize("hasRole('system:menu:getmenubyId')")
    @ApiOperation(value = "更具菜单id查询菜单信息")
    @GetMapping("/getMenuById/{navId}")
    public AjaxResult getMenuById(@PathVariable("navId") Long navId){
        return AjaxResult.success(menuService.getById(navId));
    }

    /**
     * 修改菜单
     * @param lyNav 修改菜单表单
     * @return  修改成功 或者 修改失败
     */
    @PreAuthorize("hasRole('system:menu:updatemenu')")
    @ApiOperation(value = "修改菜单")
    @PutMapping("/updateMenu")
    public AjaxResult updateMenu(@RequestBody LyMenu lyNav){

        // 如果 parentId == null 说明 本次创建为菜单   要给parentId赋值0
        if(lyNav.getParentId()==null){
            lyNav.setParentId(0L);
        }

        if(lyNav.getParentId()!=0){
            if(menuService.getById(lyNav.getParentId()).getMenuType().equals("C")){  // 拿着添加的 父id（ParentId ），查询出父类信息，如果父类是C（按钮） 则无法添加
                return AjaxResult.error("无法添加，按钮无法在添加下级目录、菜单、按钮");
            }
        }

        // 添加更新时间
        lyNav.setUpdateTime(new Date());
        if(menuService.updateById(lyNav)){
            return AjaxResult.success("保存成功");
        }
        return AjaxResult.error("保存失败");

    }

    /**
     * 删除菜单
     * @param navId 菜单id
     * @return  删除成功 或者 删除失败
     */
    @PreAuthorize("hasRole('system:menu:deletemenu')")
    @ApiOperation(value = "删除菜单")
    @DeleteMapping("/deleteMenu/{navId}")
    public AjaxResult deleteMenu(@PathVariable("navId") Long navId){
        // 获取当前要删除的 菜单信息 是 目录 还是 菜单 还是 按钮
        LyMenu lyNav = menuService.getById(navId);
        if(menuService.selectParentId(navId).size()!=0 ){ // 查看当前菜单是否有 菜单 或者 按钮
            if(lyNav.getMenuType().equals("A")){
                return AjaxResult.error("当前目录还拥有菜单，无法进行删除！");
            }else if(lyNav.getMenuType().equals("B")){
                return AjaxResult.error("当前菜单还拥有按钮，无法进行删除！");
            }else{
                return AjaxResult.error("数据异常！"+lyNav.getMenuType());
            }
        }else{
            if(menuService.removeById(navId)){
                return AjaxResult.success("删除成功");
            }else{
                return AjaxResult.error("删除失败");
            }
        }

    }








}
