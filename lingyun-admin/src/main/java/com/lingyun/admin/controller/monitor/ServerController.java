package com.lingyun.admin.controller.monitor;


import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.monitor.server.Server;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

@RestController
@RequestMapping("/monitor")
public class ServerController {


    @PreAuthorize("hasRole('monitor:server')")
    @GetMapping("/server")
    public AjaxResult server(){
        Server server=new Server();
        try {
            server.copyTo();
        } catch (UnknownHostException e) {
            return AjaxResult.success(e.getMessage());
        } catch (Exception e) {
            return AjaxResult.success(e.getMessage());
        }
        return AjaxResult.success(server);
    }


}
