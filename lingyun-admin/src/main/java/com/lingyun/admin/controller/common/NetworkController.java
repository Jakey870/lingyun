package com.lingyun.admin.controller.common;


import cn.hutool.core.net.NetUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.lingyun.common.utils.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
@RequestMapping("/common")
public class NetworkController {


    @GetMapping("/network")
    public AjaxResult clientsInformation(HttpServletRequest request){

        AjaxResult ajax =  AjaxResult.success();
        ajax.put("服务端主机名",NetUtil.getLocalHostName());
        ajax.put("服务端ip-1",NetUtil.getLocalhost());
        ajax.put("服务端ip-2",NetUtil.getLocalhost().getHostAddress());
        try {
            ajax.put("服务端ip-3", InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            ajax.put("服务端ip-3", e.getMessage());
        }
        ajax.put("服务端主机名/服务端ip",NetUtil.getIpByHost(String.valueOf(NetUtil.getLocalhost())));
        ajax.put("客户端ip", ServletUtil.getClientIP(request));
        return ajax;
    }



}
