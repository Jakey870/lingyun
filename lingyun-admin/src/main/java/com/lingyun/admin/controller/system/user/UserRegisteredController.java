package com.lingyun.admin.controller.system.user;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.lingyun.common.exception.LyException;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.system.service.LyUserService;
import com.lingyun.common.vo.user.UserRegister;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


/**
 *
 * @author 没事别学JAVA
 *
 */
@RestController
@RequestMapping("/system/registered")
@Api(value = "后台注册",description="")
public class UserRegisteredController extends ApiController {

    @Resource
    private LyUserService lyUserService;

    /**
     *
     *邮箱注册-发送验证码
     *
     * */
    @ApiOperation(value = "邮箱注册-发送验证码")
    @PostMapping("/sendemailverificationcode")
    public AjaxResult SendEmailVerificationCode(@ApiParam(value = "账号-邮箱") @RequestParam("Email") String Email){
        try {
            lyUserService.SendEmailVerificationCode(Email);
        } catch (LyException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("发送成功");
    }

    /**
     *
     * 注册-数据入库
     *
     * */
    @ApiOperation(value = "注册-数据入库")
    @PostMapping("/lyregistered")
    public AjaxResult lyRegistered(@RequestBody UserRegister userRegisterVO){
        try {
            lyUserService.lyRegistered(userRegisterVO);
        } catch (LyException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("注册成功");
    }







}
