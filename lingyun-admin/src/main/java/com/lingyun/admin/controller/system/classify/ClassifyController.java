package com.lingyun.admin.controller.system.classify;


import com.lingyun.common.utils.AjaxResult;
import com.lingyun.system.service.LyClassifyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/system/classify")
public class ClassifyController {


    @Resource
    LyClassifyService lyClassifyService;


    @ApiOperation(value = "分类列表")
    @GetMapping("/queryAllClassifyList")
    public AjaxResult queryAllClassifyList(){
        return AjaxResult.success(lyClassifyService.queryAllClassifyList());
    }



















}
