package com.lingyun.admin.controller.system.dict;


import com.lingyun.common.exception.dict.DictHaveDataException;
import com.lingyun.common.pojo.LyDictData;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.dict.DictDataPage;
import com.lingyun.common.vo.dict.DictPage;
import com.lingyun.system.service.LyDictDataService;
import com.lingyun.system.service.LyDictTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/dict")
public class DictController {



    @Resource
    LyDictTypeService lyDictTypeService;

    @Resource
    LyDictDataService lyDictDataService;

    /***
     * 字典类型列表
     * @param dictPage 字典类型搜索表单
     * @return 字典分页数据
     */
    @ApiOperation(value = "字典类型列表")
    @PreAuthorize("hasRole('system:dict:dictlist')")
    @GetMapping("/dictList")
    public AjaxResult dictList(DictPage dictPage){

        return AjaxResult.success("查询成功",lyDictTypeService.dictList(dictPage));

    }


    /**
     * 更具id 查询字典类型
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    @ApiOperation(value = "字典数据列表")
    @PreAuthorize("hasRole('system:dict:selectdictbyid')")
    @GetMapping("/selectDictByID/{dictId}")
    public AjaxResult selectDictByID(@PathVariable Long dictId){

        return AjaxResult.success("查询成功",lyDictTypeService.selectDictById(dictId));

    }

    /**
     * 添加字典类型
     * @param dictType 字典类型表单
     * @return 是否添加成功
     */
    @ApiOperation(value = "添加字典类型")
    @PreAuthorize("hasRole('system:dict:adddicttype')")
    @PostMapping("/addDictType")
    public AjaxResult addDictType(@RequestBody LyDictType dictType){
        dictType.setCreateTime(new Date());
        dictType.setUpdateTime(new Date());
        if(lyDictTypeService.save(dictType)){
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.success("添加失败");
    }


    /**
     * 修改字典类型
     * @param dictType 字典类型表单
     * @return  是否修改成功
     */
    @ApiOperation(value = "修改字典类型")
    @PreAuthorize("hasRole('system:dict:updatedicttype')")
    @PutMapping("/updateDictType")
    public AjaxResult updateDictType(@RequestBody LyDictType dictType){
        dictType.setUpdateTime(new Date());
        if(lyDictTypeService.updateById(dictType)){
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }


    /**
     * 删除字典类型
     *
     * @param dictId  字典类型id
     * @return  是否删除成功
     */
    @ApiOperation(value = "删除字典类型")
    @PreAuthorize("hasRole('system:dict:deletedicttypebyid')")
    @DeleteMapping("/deleteDictTypeById/{dictId}")
    public AjaxResult deleteDictTypeById(@PathVariable Long dictId){

        try {
            if(lyDictTypeService.deleteDictTypeById(dictId)>0){
                return AjaxResult.success("删除成功");
            }
        } catch (DictHaveDataException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.error("删除失败");

    }


    /**
     * 批量删除字典类型
     *
     * @param ids   字典类型id 集合
     * @return 是否删除成功
     */
    @ApiOperation(value = "批量删除字典类型")
    @PreAuthorize("hasRole('system:dict:deletedicttypebyids')")
    @DeleteMapping("/deleteDictTypeByIds")
    public AjaxResult deleteDictTypeByIds(@RequestBody List<Long> ids){

        try {
            if(lyDictTypeService.deleteDictTypeByIds(ids)){
                return AjaxResult.success("删除成功");
            }
        } catch (DictHaveDataException e) {
            return AjaxResult.error("选中的部分"+e.getMessage());
        }
        return AjaxResult.error("删除失败");

    }













    /**
     * ----------------------------------------------------------------------------------------------------------------------------------------------------
     */


    /**
     * 查询所有字典类型
     * @return  字典类型列表
     */
    @ApiOperation(value = "查询所有字典类型")
    @PreAuthorize("hasRole('system:dict:selectdicttypeall')")
    @GetMapping("/selectDictTypeAll")
    public AjaxResult selectDictTypeAll(){
        return AjaxResult.success("查询成功",lyDictTypeService.selectDictTypeAll());
    }





    /**
     * 根据字典类型查询字典数据
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @ApiOperation(value = "按字典类型查询数据")
    @GetMapping("/queryDictDataByType/{dictType}")
    public AjaxResult queryDictDataByType(@PathVariable("dictType") String dictType){
        return AjaxResult.success(lyDictDataService.queryDictDataByType(dictType));
    }


    /**
     * 更具字典类型 查询 字典数据列表
     * @param dictDataPage 字典数据搜索表单
     * @return 字典数据列表
     */
    @ApiOperation(value = "更具字典类型 查询 字典数据列表")
    @PreAuthorize("hasRole('system:dict:dictdatalist')")
    @GetMapping("/dictDataList")
    public AjaxResult dictDataList(DictDataPage dictDataPage){
        return AjaxResult.success("查询成功",lyDictDataService.dictDataList(dictDataPage));
    }


    /**
     * 添加字典数据
     * @param dictData  字典数据表
     * @return  是否添加成功
     */
    @ApiOperation(value = "添加字典数据")
    @PreAuthorize("hasRole('system:dict:adddictdata')")
    @PostMapping("/addDictData")
    public AjaxResult addDictData(@RequestBody LyDictData dictData){
        dictData.setCreateTime(new Date());
        dictData.setUpdateTime(new Date());
        if(lyDictDataService.save(dictData)){
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.success("添加失败");
    }

    /**
     * 修改字典数据
     *
     * @param dictData 字典数据表
     * @return 是否修改成功
     */
    @ApiOperation(value = "修改字典数据")
    @PreAuthorize("hasRole('system:dict:updatedictdata')")
    @PutMapping("/updateDictData")
    public AjaxResult updateDictData(@RequestBody LyDictData dictData){
        dictData.setUpdateTime(new Date());
        if(lyDictDataService.updateById(dictData)){
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }

    /**
     * 根据字典数据编码查询字典数据
     * @param dictCode  字典编码
     * @return 字典数据信息
     */
    @ApiOperation(value = "根据字典数据编码查询字典数据")
    @PreAuthorize("hasRole('system:dict:selectDictdatabyid')")
    @GetMapping("/selectDictDataById/{dictCode}")
    public AjaxResult selectDictDataById(@PathVariable Long dictCode){
        return AjaxResult.success(lyDictDataService.selectDictDataById(dictCode));
    }

    /**
     * 删除字典数据
     * @param dictCode 字典编码
     * @return 是否删除成功
     */
    @ApiOperation(value = "删除字典数据")
    @PreAuthorize("hasRole('system:dict:deletedictdatabyid')")
    @DeleteMapping("/deleteDictDataById/{dictCode}")
    public AjaxResult deleteDictDataById(@PathVariable Long dictCode){
        if(lyDictDataService.removeById(dictCode)){
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.success("删除失败");
    }

    /**
     * 批量删除字典数据
     * @param ids 字典编码集合
     * @return 是否删除成功
     */
    @ApiOperation(value = "批量删除字典数据")
    @PreAuthorize("hasRole('system:dict:deletedictdatabyids')")
    @DeleteMapping("/deleteDictDataByIds")
    public AjaxResult deleteDictDataByIds(@RequestBody List<Long> ids){
        if(lyDictDataService.removeByIds(ids)){
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.success("删除失败");
    }


















}
