package com.lingyun.admin.controller.system.article;

import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.article.ArticleSearch;
import com.lingyun.system.service.LyArticleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 没事别学JAVA
 * 2021/7/14 17:09
 */
@RestController
@RequestMapping("/system/article")
public class ArticleContorller {

    @Resource
    LyArticleService lyArticleService;

    /**
     * 文章分页查询
     *
     * @param articleSearch 文章搜索表单
     * @return 返回文章分页集合
     */
    @PreAuthorize("hasRole('system:article:articlelist')")
    @ApiOperation(value = "查看文章列表")
    @GetMapping("/articleList")
    public AjaxResult articleList(ArticleSearch articleSearch) {
        return AjaxResult.success("查询成功", lyArticleService.articleList(articleSearch));
    }


    /**
     * 添加文章
     * @param lyArticle 文章表单
     * @return 添加成功 或 添加失败
     */
    @PreAuthorize("hasRole('system:article:insertarticle')")
    @ApiOperation(value = "添加文章")
    @PostMapping("/insertArticle")
    public AjaxResult insertArticle(@RequestBody LyArticle lyArticle) {
        if(lyArticleService.insertArticle(lyArticle)>0){
            return AjaxResult.success("成功");
        }
        return AjaxResult.success("添加失败");
    }


    /**
     * 根据id查看文章
     *
     * @param articleId 文章id
     * @return 返回当前返回的文章
     */
    @PreAuthorize("hasRole('system:article:getarticlebyid')")
    @ApiOperation(value = "根据id查看文章")
    @GetMapping("/getArticleById/{articleId}")
    public AjaxResult getArticleById(@PathVariable("articleId") Long articleId) {
        return AjaxResult.success(lyArticleService.getArticleById(articleId));
    }


    /**
     * 修改文章信息
     *
     * @param lyArticle 文章实体类
     * @return 直接返回  修改成功 或者  修改失败
     */
    @PreAuthorize("hasRole('system:article:updatearticle')")
    @ApiOperation(value = "修改文章信息")
    @PutMapping("/updateArticle")
    public AjaxResult updateArticleId(@RequestBody LyArticle lyArticle) {
        if (lyArticleService.updateArticleId(lyArticle)>0) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }


    /**
     * 文章单个删除
     *
     * @param articleId 文章id
     * @return 直接返回 删除成功 或者 删除失败
     */
    @PreAuthorize("hasRole('system:article:deletearticlebyid')")
    @ApiOperation(value = "文章单个删除")
    @DeleteMapping("/deleteArticleById/{articleId}")
    public AjaxResult deleteArticleById(@PathVariable("articleId") Long articleId) {
        if (lyArticleService.deleteArticleById(articleId)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.success("删除失败");
        }
    }


    /**
     * 文章多个删除
     * @param articleIds
     * @return
     */
    @PreAuthorize("hasRole('system:article:deletearticlebyids')")
    @ApiOperation(value = "文章多个删除")
    @DeleteMapping("/deleteArticleByIds")
    public AjaxResult deleteArticleByIds(@RequestBody List<Long> articleIds){

        if (lyArticleService.deleteArticleByIds(articleIds)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.success("删除失败");
        }

    }


    /**
     * 修改 轮播图状态
     * @param lyArticle 只有 文章id 轮播图状态
     * @return  成功 或者 失败
     */
    @PreAuthorize("hasRole('system:article:updateisshuffling')")
    @ApiOperation(value = "修改 轮播图状态")
    @PutMapping("/updateIsShuffling")
    public AjaxResult updateIsShuffling(@RequestBody LyArticle lyArticle){

        if (lyArticleService.updateById(lyArticle)) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.success("修改失败");
        }

    }

    /**
     * 修改 推广状态
     * @param lyArticle 只有 文章id  推广状态
     * @return 成功 或者 失败
     */
    @PreAuthorize("hasRole('system:article:updateispopularize')")
    @ApiOperation(value = "修改 推广状态")
    @PutMapping("/updateIsPopularize")
    public AjaxResult updateIsPopularize(@RequestBody LyArticle lyArticle){

        if (lyArticleService.updateById(lyArticle)) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.success("修改失败");
        }

    }




}
