package com.lingyun.admin.controller.system.article;

import com.lingyun.common.pojo.JobFrom;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.framework.quartz.JobService;
import io.swagger.annotations.ApiParam;
import org.quartz.SchedulerException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 *
 *  @author 没事别学JAVA
 *  2021/4/27 16:31
 */
@RestController
@RequestMapping("/system/quartz")
public class QuartzController {

    @Resource
    private JobService jobService;

    /**
     * 分页查询 定时任务
     * */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/quartzlist")
    public AjaxResult quartzlist(@ApiParam(value = "当前页") @RequestParam("current") Integer current, @ApiParam(value = "页大小") @RequestParam("size") Integer size){
        return AjaxResult.success(jobService.quartzlist(current,size));
    }

    /**
     * 添加定时任务
     * */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/quartzadd")
    public AjaxResult quartzadd(@RequestBody JobFrom job){
        try {
            jobService.quartzadd(job);
        } catch (SchedulerException e) {
            e.printStackTrace();
            return AjaxResult.success(e.getMessage());
        } catch (ClassNotFoundException e) {
            return AjaxResult.success(e.getMessage());
        } catch (InstantiationException e) {
            return AjaxResult.success(e.getMessage());
        } catch (IllegalAccessException e) {
            return AjaxResult.success(e.getMessage());
        }
        return AjaxResult.success("添加成功");
    }

    /**
     *后台文章定时任务的删除接口
     * */
    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("/deleteJob")
    public AjaxResult deleteJob(@RequestBody JobFrom job) {
        try {
            jobService.deleteJob(job);
        } catch (Exception e) {
            return AjaxResult.success(e.getMessage());
        }
        return AjaxResult.success("删除成功");
    }



}
