//package com.lingyun.admin.controller.system.nav;
//
//import com.lingyun.common.vo.tree.TreeSelect;
//import com.lingyun.common.pojo.LyMenu;
//import com.lingyun.common.utils.AjaxResult;
//import com.lingyun.system.service.LyMenuService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @author 没事别学JAVA
// * 2021/11/10 16:32
// */
//@RestController
//@RequestMapping("/system/menu")
//@Api(value = "导航菜单操作", description = "")
//public class NavController {
//
//    @Resource
//    LyMenuService lyNavService;
//
//    /**
//     * @return 获取左侧导航栏
//     */
//    @ApiOperation(value = "获取左侧导航栏")
//    @GetMapping("/getAllRouters")
//    public AjaxResult selectNavAll(){
//        List<LyMenu> navlist= lyNavService.selectNavAll();
//        return AjaxResult.success(lyNavService.buildNav(navlist));
//    }
//    /**
//     * 查询对应角色的菜单列表
//     */
//    @ApiOperation(value = "查询对应角色的菜单列表")
//    @GetMapping("/roleMenuTress/{roleId}")
//    public AjaxResult roleNavTress(@PathVariable Long roleId){
//        List<LyMenu> navlist=lyNavService.roleNavTress();
//        AjaxResult ajax = AjaxResult.success();
//        ajax.put("role_menu_data",navlist.stream().map(TreeSelect::new).collect(Collectors.toList()));
//        ajax.put("menu_id",lyNavService.selectNavByRoleId(roleId));
//        return ajax;
//    }
//
//    /**
//     * 获取菜单下拉树列表
//     */
//    @ApiOperation(value = "获取菜单下拉树列表")
//    @GetMapping("/treeSelect")
//    public AjaxResult treeselect() {
//        List<LyMenu> navlist=lyNavService.selectNavAll();
//        return AjaxResult.success(navlist.stream().map(TreeSelect::new).collect(Collectors.toList()));
//    }
//
//
//
//
//
//
//
//
//}
