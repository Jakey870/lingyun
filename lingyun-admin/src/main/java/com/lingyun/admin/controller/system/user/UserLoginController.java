package com.lingyun.admin.controller.system.user;



import com.lingyun.common.exception.ServiceException;
import com.lingyun.common.exception.user.CaptchaException;
import com.lingyun.common.exception.user.CaptchaExpireException;
import com.lingyun.common.exception.user.UserNotFoundException;
import com.lingyun.common.exception.user.UserPasswordNotMatchException;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.LoginFrom;
import com.lingyun.system.service.LyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 没事别学JAVA
 *
 */
@RestController
@RequestMapping("/system")
@Api(value = "后台登录",description="")
public class UserLoginController {

    @Resource
    private LyUserService lyUserService;

    /**
     *  登录
     * */
    @ApiOperation(value = "邮箱登录  登录后返回token")
    @PostMapping("/login")
    public AjaxResult lyLogin(@RequestBody LoginFrom loginFrom, HttpServletRequest request){
        String token = null;
        try {
            token = lyUserService.lyLogin(loginFrom,request);
        } catch (CaptchaExpireException e) {
            return AjaxResult.error(e.getMessage());
        } catch (CaptchaException e) {
            return AjaxResult.error(e.getMessage());
        } catch (UserPasswordNotMatchException e) {
            return AjaxResult.error(e.getMessage());
        } catch (UserNotFoundException e) {
            return AjaxResult.error(e.getMessage());
        } catch (ServiceException e) {
            return AjaxResult.error(e.getMessage());
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("token",token);
        return ajax;
    }

































}
