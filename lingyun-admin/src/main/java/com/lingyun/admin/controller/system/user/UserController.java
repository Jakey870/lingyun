package com.lingyun.admin.controller.system.user;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.utils.file.FileDownloadUtils;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserQueryCriteria;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.system.service.LyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/user")
@Api(value = "用户操作", description = "用户操作")
public class UserController {

    @Resource
    LyUserService lyUserService;

    /**
     *
     * @param userQueryCriteria 用户查询条件
     * @return 后台用户全部查询接口
     */
    @ApiOperation(value = "用户全部查询")
    @PreAuthorize("hasRole('system:user:getuserlist')")
    @GetMapping("/getUserList")
    public AjaxResult getUserList(UserQueryCriteria userQueryCriteria) {
        return AjaxResult.success("查询成功", lyUserService.getUserList(userQueryCriteria));
    }


    @ApiOperation(value = "用户单删")
    @PreAuthorize("hasRole('system:user:deletuserbyid')")
    @DeleteMapping("/deletUserById/{userid}")
    public AjaxResult deletUserById(@PathVariable Long userid){
        if(lyUserService.removeById(userid)){
            return AjaxResult.success("删除成功");
        }else {
            return AjaxResult.error("删除成功");
        }
    }

    @ApiOperation(value = "批量删除用户")
    @PreAuthorize("hasRole('system:user:deletuserbyids')")
    @DeleteMapping("/deletUserByIds")
    public AjaxResult deletUserByIds(@RequestBody List<String> userIds){
        if(lyUserService.removeByIds(userIds)){
            return AjaxResult.success("删除成功");
        }else{
            return AjaxResult.error("删除成功");
        }
    }

    @ApiOperation(value = "添加用户")
    @PreAuthorize("hasRole('system:user:adduser')")
    @PostMapping("/addUser")
    public AjaxResult addUser(@RequestBody UserFrom userFrom){
        try {
            lyUserService.addUser(userFrom);
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("添加成功");
    }

    @ApiOperation(value = "根据id查询用户详细信息")
    @PreAuthorize("hasRole('system:user:selectuserbyid')")
    @GetMapping("/selectUserById/{userId}")
    public AjaxResult selectUserById(@PathVariable("userId") Long userId){
        AjaxResult ajax = AjaxResult.success();
        ajax.put("data",lyUserService.selectUserById(userId));
        ajax.put("roleIds",lyUserService.getUserRole(userId));
        return ajax;
    }

    @ApiOperation(value = "修改用户信息")
    @PreAuthorize("hasRole('system:user:updateuser')")
    @PutMapping("/updateUser")
    public AjaxResult updateUser(@RequestBody UserFrom userFromVo){
        if(lyUserService.updateUser(userFromVo)){
            return AjaxResult.success("修改成功");
        }else {
            return AjaxResult.error("修改失败");
        }
    }

    /**
     *
     * @param userVagueQueryVo 用户查询条件
     * @return 文件名字
     */
    @ApiOperation(value = "导出用户Excel信息")
    @PreAuthorize("hasRole('system:user:userexportexcel')")
    @GetMapping("/userExportExcel")
    public AjaxResult userExportExcel(UserQueryCriteria userVagueQueryVo){
        List<UserAll> list = lyUserService.userExportExcel(userVagueQueryVo);
        Workbook work= ExcelExportUtil.exportExcel(new ExportParams(), UserAll.class,list);
        String filename = UUID.randomUUID().toString() + "_" + "用户数据" + ".xlsx";
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(FileDownloadUtils.getAbsoluteFile(filename));
            work.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.success("导出成功",filename);
    }

    /**
     * 授权用户列表
     * @param userQueryCriteria 用户查询条件
     * @return 授权用户列表
     */
    @ApiOperation( value = "授权用户列表")
    @PreAuthorize("hasRole('system:user:authorizationuserlist')")
    @GetMapping("/authorizationUserList")
    public AjaxResult authorizationUserList(UserQueryCriteria userQueryCriteria){
        return AjaxResult.success(lyUserService.authorizationUserList(userQueryCriteria));
    }




}
