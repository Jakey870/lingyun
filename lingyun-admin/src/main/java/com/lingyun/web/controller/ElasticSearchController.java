package com.lingyun.web.controller;

import com.lingyun.common.exception.es.IndexException;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.framework.elasticsearch.ElasticSearchService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 没事别学JAVA
 * 2021/6/2 18:06
 */

@RestController
@RequestMapping("/web/es")
public class ElasticSearchController {


    @Resource
    ElasticSearchService elasticSearchService;


    /**
     * 轮播图
     *
     * @return 轮播图集合
     */
    @GetMapping("/shufflingFigure")
    public AjaxResult shufflingFigure(){
        List<Map<String, Object>> list=null;
        try {
             list= elasticSearchService.shufflingFigure();
        } catch (IOException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success(list);
    }

    /**
     * 推广内容
     *
     * @return 推广集合
     */
    @GetMapping("/queryPopularize")
    public AjaxResult queryPopularize(){
        List<Map<String, Object>> list=null;
        try {
            list= elasticSearchService.shufflingFigure();
        } catch (IOException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success(list);
    }


    /**
     * 根据文章id 获取内容
     *
     * @return
     */
    @GetMapping("/queryArticle/{Id}")
    public AjaxResult queryArticle(@PathVariable("Id") Long articleId){
        try {
            return AjaxResult.success(elasticSearchService.queryArticle(articleId));
        } catch (IOException e) {
//            throw new RuntimeException(e);
            return AjaxResult.error(e.getMessage());
        }
    }



    /**
     *
     * @param keyword 查询内容（标题）
     * @param current  当前页
     * @param size 页大小
     *
     */
    @GetMapping("/es_search")
    public AjaxResult search(@RequestParam String keyword, @RequestParam Integer current, @RequestParam Integer size){
        try {
            return AjaxResult.success(elasticSearchService.search(keyword,current,size));
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.warn(e.getMessage());
        } catch (IndexException e) {
            return AjaxResult.warn(e.getMessage());
        }
    }


    /**
     * es批量添加
     * @return 更新成功 或 更新失败
     */
    @GetMapping("/batchAdd")
    public AjaxResult fullUpdate(){
        Integer status= elasticSearchService.reset();
        if(status==200){
            return AjaxResult.success("更新成功");
        }
        return AjaxResult.error("更新失败");
    }









}
