package com.lingyun.web.controller;


import com.lingyun.common.utils.AjaxResult;
import com.lingyun.framework.solr.SolrSearchService;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * @author 24188
 */

@RestController
@RequestMapping("/web/solr")
public class SolrController {

    @Resource
    private SolrSearchService articleSearchService;

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/solrList")
    public AjaxResult solrFuzzy(@ApiParam(value = "当前页") @RequestParam("current") Integer current,
                                @ApiParam(value = "页大小") @RequestParam("size") Integer size,
                                @ApiParam(value = "文章标题") String name) {
        return AjaxResult.success(articleSearchService.searchArticlePage(current,size,name));
    }



}




















