// 引入vue
import Vue from 'vue'
// 路由
import Router from 'vue-router'
// 应用路由
Vue.use(Router)

// 引入首页页面
import Layout from '@/layout'
import web from "@/web";


/**
 *
 * hidden              是否显示
 * name
 * meta{
 *     title           标题
 *     icon            图片
 *     isAuth          是否身份验证
 *     affix           页签默认固定
 * }
 *
 * */
// 公共路由
export const constantRoutes = [
    {
        path: '*',
        redirect: '/404',
        hidden: false
    },
    {
        path: '/login',
        component: (resolve) => require(['@/views/login'], resolve),
        meta: {title: '登录', icon: 'dashboard', isAuth: false},
        hidden: true
    },
    {
        path: '/register',
        component: (resolve) => require(['@/views/register'], resolve),
        meta: {title: '注册', icon: 'dashboard', isAuth: false},
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/error/404'),
        // meta: {title: '404', icon: 'dashboard', isAuth: false},
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/error/401'),
        // meta: {title: '401', icon: 'dashboard', isAuth: false},
        hidden: true
    },
    {
        path: '/admin',
        component: Layout,
        hidden: true,
        redirect: '/admin',
        children: [
            {
                path: '/admin',
                name: 'admin',
                component: (resolve) => require(['@/views/index'], resolve),
                meta: {title: '首页', icon: 'dashboard', isAuth: true, affix: true},
                hidden: true,
            },
            {
                path: '/system/user',
                name: 'system_user',
                component: (resolve) => require(['@/views/user'], resolve),
                meta: {title: '用户管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/system/article',
                name: 'system_article',
                component: (resolve) => require(['@/views/article'], resolve),
                meta: {title: '文章管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/system/role',
                name: 'system_role',
                component: (resolve) => require(['@/views/role'], resolve),
                meta: {title: '角色管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/system/role-user/:roleId',
                name: 'system_role-user',
                component: (resolve) => require(['@/views/role/allocationUser'], resolve),
                meta: {title: '分配用户', icon: 'dashboard', isAuth: true},
                hidden: false,
            },
            {
                path: '/system/role-data/:roleId',
                name: 'system_role-data',
                component: (resolve) => require(['@/views/role/dataPermissions'], resolve),
                meta: {title: '数据权限', icon: 'dashboard', isAuth: true},
                hidden: false,
            },
            {
                path: '/system/menu',
                name: 'system_menu',
                component: (resolve) => require(['@/views/menu'], resolve),
                meta: {title: '菜单管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/system/dict',
                name: 'system_dict',
                component: (resolve) => require(['@/views/dict'], resolve),
                meta: {title: '字典管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/system/dict-data/:dictId',
                name: 'system_dict-data',
                component: (resolve) => require(['@/views/dict/dictData'], resolve),
                meta: {title: '字典数据', icon: 'dashboard', isAuth: true},
                hidden: false,
            },
            {
                path: '/system/logo',
                name: 'system_logo',
                component: (resolve) => require(['@/views/logo'], resolve),
                meta: {title: '日志管理', icon: 'dashboard', isAuth: true},
                hidden: true,
            },
            {
                path: '/monitor/server',
                name: 'monitor_server',
                component: (resolve) => require(['@/views/monitor/server'], resolve),
                meta: {title: '服务监控', icon: 'dashboard', isAuth: true},
                hidden: true,
            }
        ]
    }
]

export const webRoutes = [
    {
        path: '/',
        component: web,
        hidden: true,
        redirect: '/',
        children: [
            {
                path: '/',
                component: (resolve) => require(['@/web/index'], resolve),
                meta: {title: '没事别学JAVA', icon: 'dashboard', isAuth: false}
            }
        ]
    },
    {
        path: '/news/:id',
        component: (resolve) => require(['@/web/news/index'], resolve),
        meta: {title: '文章页', icon: 'dashboard', isAuth: false},
        hidden: true,
    }
]


export default new Router({
    mode: 'history', // 去掉url中的#
    scrollBehavior: () => ({y: 0}),
    routes: constantRoutes.concat(webRoutes)
})
