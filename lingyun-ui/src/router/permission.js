import router from '@/router'
import store from '@/store'
import {getToken} from "@/utils/cookie";


router.beforeEach((to, from, next) => {
    console.log('前置路由守卫', to, from)
    if(to.meta.isAuth){  // 判断是否需要权限
        if(getToken()){
            if(to.path === '/login'){
                next({ path: '/admin' })
            }else{
                store.dispatch('GenerateRoutes')
                next()
            }
        }else{
            if(to.path === '/login'){
                next()
            }else{
                next({ path: '/login' }) // 否则全部重定向到登录页
            }
        }
    }else{
        next()
    }
})

router.afterEach((to, from) => {
    console.log('后置路由守卫', to, from)
})
