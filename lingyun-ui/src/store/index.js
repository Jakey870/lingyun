// 引入vue
import Vue from 'vue'
// 引入vuex
import Vuex from 'vuex'
// 引入自定义getters
import getters from '@/store/getters'

import permission from './permission'
import user from './user'
import tagsView from "@/store/tagsView";
import settings from "@/store/settings";


// 应用vuex
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    permission,
    tagsView,
    settings,
    user
  },
  getters
})
export default store
