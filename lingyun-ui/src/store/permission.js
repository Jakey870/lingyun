import { getRouters } from '@/api/admin/menu'

import {constantRoutes} from '@/router'

const permission = {

  state: {
    // addRoutes: [],
    permissionRoutes: [],
    // defaultRoutes: [],
    // topbarRouters: [],

    // 侧边栏路由（用于生成侧边栏的数据）
    sidebarRouters:[]
  },


  mutations: {
    SET_PERMISSION_ROUTES: (state, routes) => {
      // state.addRoutes = routes
      state.permissionRoutes = constantRoutes
    },
    // SET_DEFAULT_ROUTES: (state, routes) => {
    //   state.defaultRoutes = constantRoutes.concat(routes)
    // },
    // SET_TOPBAR_ROUTES: (state, routes) => {
    //   state.topbarRouters = routes
    // },


    SET_SIDEBAR_ROUTERS: (state, routes) => {
      state.sidebarRouters = routes
    },

  },


  actions: {
    // 生成路由
    GenerateRoutes({ commit }) {
      getRouters().then(res => {
        commit('SET_PERMISSION_ROUTES', res.data)
        // commit('SET_DEFAULT_ROUTES', res.data)
        // commit('SET_TOPBAR_ROUTES', res.data)
        commit('SET_SIDEBAR_ROUTERS', res.data)
      })
    }
  }





}
export default permission
