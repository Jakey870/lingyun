import request from '@/utils/request'

/**
 * 字典类型列表
 * @param query
 * @returns {*}
 */
export function dictList(query) {

    return request({
        url: '/system/dict/dictList',
        method: 'get',
        params: query
    })

}


/**
 * 更具id 查询字典类型
 * @param dictId 字典类型id
 * @returns {*}
 */
export function selectDictById(dictId) {

    return request({
        url: '/system/dict/selectDictByID/'+ dictId,
        method: 'get',
    })

}

/**
 * 查询所有字典类型
 * @returns {*}
 */
export function selectDictTypeAll() {

    return request({
        url: '/system/dict/selectDictTypeAll',
        method: 'get',
    })

}




/**
 * 添加字典类型
 * @param data  字典类型表单
 */
export function addDictType(data){

    return request({
        url: '/system/dict/addDictType',
        method: 'post',
        data: data
    })

}


/**
 * 修改字典类型
 * @param data  字典类型表单
 */
export function updateDicType(data){

    return request({
        url: '/system/dict/updateDictType',
        method: 'put',
        data: data
    })

}


/**
 * 删除字典类型
 *
 * @param dictId  字典类型id
 * @returns {*}
 */
export function deleteDictTypeById(dictId){

    return request({
        url: '/system/dict/deleteDictTypeById/'+ dictId,
        method: 'delete',
    })

}


/**
 * 批量删除字典类型
 *
 * @param ids  字典类型id 集合
 * @returns {*}
 */
export function deleteDictTypeByIds(ids){

    return request({
        url: '/system/dict/deleteDictTypeByIds',
        method: 'delete',
        data: ids
    })

}







