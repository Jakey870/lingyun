import request from '@/utils/request'


/**
 * 获取验证码
 * @returns {*}
 */
export function getCodeImg() {

    return request({
        url: '/common/captchaImage',
        method: 'get',
        timeout: 20000
    })

}


/**
 * 登录
 * @param data
 * @returns {*}
 */
export function login(data) {

    return request({
        url: '/system/login',
        method: 'post',
        data: data
    })

}


/**
 * 退出登录
 * @returns {*}
 */
export function logout() {

    return request({
        url: '/logout',
        method: 'post'
    })

}
