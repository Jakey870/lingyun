import request from '@/utils/request'


/**
 * 上传
 * @param file
 * @returns {*}
 */
export function upload(file) {

    return request({
        url: '/common/upload',
        method: 'post',
        data:file,
        headers:{'Content-Type':'multipart/form-data'}
    })

}