import request from '@/utils/request'


/**
 * 文章分页查询
 * @param query 查询条件
 * @returns {*}
 */
export function articleList(query) {

    return request({
        url: '/system/article/articleList',
        method: 'get',
        params: query
    })

}

/**
 * 添加文章
 * @param data 文章表单
 * @returns {*}
 */
export function insertArticle(data){

    return request({
        url: '/system/article/insertArticle',
        method: 'post',
        data:data
    })

}

/**
 * 根据id查询文章
 * @param articleId 文章id
 * @returns {*}
 */
export function getArticleById(articleId){

    return request({
        url: '/system/article/getArticleById/'+articleId,
        method: 'get',
    })

}


/**
 * 根据id修改文章信息
 * @param data
 * @returns {*}
 */
export function updateArticleId(data){

    return request({
        url: '/system/article/updateArticle',
        method: 'put',
        data:data
    })

}


/**
 * 根据id 删除文章
 * @param articleId
 */
export function deleteArticleById(articleId){

    return request({
        url: '/system/article/deleteArticleById/'+articleId,
        method: 'delete',
    })

}


/**
 * 根据id list列表 删除文章
 * @param ids
 */
export function deleteArticleByIds(ids){

    return request({
        url: '/system/article/deleteArticleByIds',
        method: 'delete',
        data:ids
    })

}

/**
 * es批量添加
 * @returns {*}
 */
export function batchAdd(){

    return request({
        url:'/web/es/batchAdd',
        method:'get',
    })

}


/**
 * 分类下拉数据
 *
 */
export function queryAllClassifyList(){
    return request({
        url:'/system/classify/queryAllClassifyList',
        method:'get',
    })
}


/**
 * 修改轮播状态
 * @param articleId 文章id
 * @param isShuffling 轮播状态
 * @returns {*}
 */
export function handleIsShufflingChange(articleId, isShuffling) {

    const data = {
        articleId,
        isShuffling
    }
    return request({
        url: '/system/article/updateIsShuffling',
        method: 'put',
        data: data
    })

}


/**
 * 修改推广状态
 * @param articleId  文章id
 * @param isPopularize 推广状态
 * @returns {*}
 */
export function handleIsPopularizeChange(articleId, isPopularize) {

    const data = {
        articleId,
        isPopularize
    }
    return request({
        url: '/system/article/updateIsPopularize',
        method: 'put',
        data: data
    })

}






