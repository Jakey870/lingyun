import request from '@/utils/request'

/**
 * 用户全部查询
 * @param query
 * @returns {*}
 */
export function getUserList(query) {

    return request({
        url: '/system/user/getUserList',
        method: 'get',
        params: query
    })

}

/**
 * 删除用户
 * @param userId
 * @returns {*}
 */
export function deletUserById(userId) {

    return request({
        url: '/system/user/deletUserById/'+userId,
        method: 'delete',
    })

}

/**
 * 批量删除
 * @param ids
 * @returns {*}
 */
export function deletUserByIds(ids) {

    return request({
        url: '/system/user/deletUserByIds',
        method: 'delete',
        data: ids
    })

}

/**
 * 添加用户
 * @param data
 * @returns {*}
 */
export function addUser(data) {

    return request({
        url: '/system/user/addUser',
        method: 'post',
        data:data
    })

}

/**
 * 根据id 查询用户信息
 * @param userId  用户id
 * @returns {*}
 */
export function selectUserById(userId) {

    return request({
        url: '/system/user/selectUserById/' + userId,
        method: 'get'
    })

}

/**
 * 修改用户
 * @param data
 * @returns {*}
 */
export function updateUser(data) {

    return request({
        url: '/system/user/updateUser',
        method: 'put',
        data:data
    })

}

/**
 * 用户Excel导出
 * @param query
 * @returns {*}
 */
export function userExportExcel(query) {

    return request({
        url: '/system/user/userExportExcel',
        method: 'get',
        params: query
    })

}


