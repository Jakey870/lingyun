import request from '@/utils/request'


/**
 * 获取推广内容
 * @returns {*}
 */
export function queryPopularize() {

    return request({
        url: '/web/es/queryPopularize',
        method: 'get',
    })

}