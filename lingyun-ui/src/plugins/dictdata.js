import {getDicts} from '@/api/admin/menu'


export default {
    install(Vue) {
        Vue.mixin({
            data() {
                if (this.$options === undefined || this.$options.dicts === undefined || this.$options.dicts === null) {
                    return {}
                }
                return {
                   dict:{
                       dictLabel:{},
                       dictType:{}
                   }
                }
            },
            created() {
                if (this.$options === undefined || this.$options.dicts === undefined || this.$options.dicts === null) {
                    return this.dict
                }else{
                    this.$options.dicts.forEach(d =>{
                        getDicts(d).then(res => {
                            Vue.set(this.dict.dictLabel, d,{})
                            res.data.forEach(res =>{
                                Vue.set(this.dict.dictLabel[d], res.dictValue, res.dictLabel)
                            })
                            Vue.set(this.dict.dictType, d, res.data)
                        })
                    })
                }
            }
        })
    },
}


