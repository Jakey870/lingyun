// 引入vue
import Vue from 'vue'
// 引入app.vue
import App from '@/App.vue'

/**
 * 解决 谷歌浏览器下 （ Added non-passive event listener to a scroll-blocking 'mousewheel'event.Consider marking event handler as 'passive' to make the page more responsive.
 * See https://www.chromestatus.com/feature/5745543795965952）
 *
 *   安装 default-passive-events
 *   yarn add default-passive-events
 *
 *   引入：import "default-passive-events";
 * */
import "default-passive-events";

// 引入ElementUI组件库
import ElementUI from 'element-ui'
// 引入ElementUI全部样式
import 'element-ui/lib/theme-chalk/index.css'

// 引入路由-(这里引入的是同级目录下的router下的index.js)
import router from '@/router'
// 引入vuex(这里引入的是同级目录下的store下的index.js)
import '@/router/permission'

import store from "@/store";




import { resetForm, handleTree }from '@/utils/linyun'
import plugins from './plugins/index'
import dictdata from './plugins/dictdata'




// 方法挂载
Vue.prototype.resetForm = resetForm
Vue.prototype.handleTree = handleTree


// 应用ElementUI
Vue.use(ElementUI)
// 应用插件
Vue.use(plugins)
Vue.use(dictdata)

// 关闭生产模式下给出的提示
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
