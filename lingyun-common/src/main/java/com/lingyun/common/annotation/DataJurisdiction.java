package com.lingyun.common.annotation;

import java.lang.annotation.*;

/**
 *
 * 数据权限
 *
 * @author 没事别学JAVA
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataJurisdiction {

}
