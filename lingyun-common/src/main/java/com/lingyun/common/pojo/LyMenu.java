package com.lingyun.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.corebase.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
/**
 * (LyMenu)表实体类
 *
 * @author 没事别学JAVA
 * @since 2021-11-10 17:10:28
 */
@Data
@ToString
@ApiModel(value = "LyMenu对象", description = "")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyMenu extends BaseEntity {

    /**
     *id
     */
    @ApiModelProperty(value = "menu_id")
    @TableId(value = "menu_id",type = IdType.AUTO)
    private Long menuId;

    /**
     *父编号
     */
    @ApiModelProperty(value = "父编号")
    private Long parentId;

    /**
     *菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    /**
     *路由地址
     */
    @ApiModelProperty(value = "路由地址")
    private String path;

    /**
     *排序字段（越小越靠前）
     */
    @ApiModelProperty(value = "排序字段（越小越靠前）")
    private Long sort;

    /**
     *菜单状态（1显示 0隐藏）
     */
    @ApiModelProperty(value = "菜单状态（1显示 0隐藏）")
    private String visible;

    /**
     *菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    /**
     * 菜单类型（ A 目录 B 菜单  ）
     */
    @ApiModelProperty(value = "菜单类型（ A 目录 B 菜单 C 按钮 ）")
    private String menuType;

    /**
     *打开方式（tab页签 new新窗口）
     */
    @ApiModelProperty(value = "打开方式（tab页签 new新窗口）")
    private String target;

    /**
     * 权限
     */
    @ApiModelProperty(value = "权限")
    private String perms;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /**
     * 附加属性
     *           子菜单
     */
    @TableField(exist = false)
    private List<LyMenu> children = new ArrayList<LyMenu>();

}
