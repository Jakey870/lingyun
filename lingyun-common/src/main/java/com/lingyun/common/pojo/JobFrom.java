package com.lingyun.common.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 没事别学JAVA
 * 2021/4/27 17:02
 *
 */
@Data
@ToString
public class JobFrom implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 定时任务全类名（就是指定定时任务文件的位置）
     * */
    private String jobClassName;

    /**
     * 任务组名
     */
    private String jobGroupName;

    /**
     * cron执行表达式
     */
    private String cronExpression;



    /**
     * 文章名字
     */
    @ApiModelProperty(value = "文章名字")
    private String articleName;

    /**
     * 文章关键字
     */
    @ApiModelProperty(value = "文章关键字")
    private String articleKeyword;

    /**
     * 文章描述
     */
    @ApiModelProperty(value = "文章描述")
    private String articleDescription;

    /**
     * 文章内容
     */
    @ApiModelProperty(value = "文章内容")
    private String artucleContent;

    /**
     * 文章缩略图
     */
    @ApiModelProperty(value = "文章缩略图")
    private String artucleThumbnailUrl;

    /**
     *分类id
     */
    @ApiModelProperty(value = "分类id")
    private Long classifyId;






}
