package com.lingyun.common.pojo;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 用户和角色关联表(LyUserRole)表实体类
 *
 * @author 没事别学JAVA
 * @since 2021-03-25 12:39:10
 */
@Data
@ToString
@ApiModel(value = "用户角色关系表", description = "用户角色关系表")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyUserRole implements Serializable{
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 附加属性
     * */
    @TableField(exist = false) //表示不是数据库字段
    private List<LyRole> lyRole;

}
