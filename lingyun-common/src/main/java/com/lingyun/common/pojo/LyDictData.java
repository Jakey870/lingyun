package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * (LyDictData)表实体类
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:13:15
 */
@Data
@ToString
@TableName(value = "ly_dict_data")
public class LyDictData implements Serializable {
    /**
     *字典编码
     */
    @TableId(value = "dict_code",type = IdType.AUTO)
    private Long dictCode;
    /**
     *字典排序
     */
    private Integer dictSort;
    /**
     *字典标签
     */
    private String dictLabel;
    /**
     *字典键值
     */
    private String dictValue;
    /**
     *字典类型
     */
    private String dictType;
    /**
     *样式属性（其他样式扩展）
     */
    private String cssClass;
    /**
     *表格回显样式
     */
    private String listClass;
    /**
     *状态（0正常 1停用）
     */
    private String status;
    /**
     *创建时间
     */
    private Date createTime;
    /**
     *更新时间
     */
    private Date updateTime;
    /**
     *备注
     */
    private String remark;
}
