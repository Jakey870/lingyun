package com.lingyun.common.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * (LyUser)表实体类
 * @author 没事别学JAVA
 * @since 2021-03-07 17:06:47
 */
@Data
@ToString
@ApiModel(value = "LyUser对象",description = "用户对象")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyUser implements Serializable{

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    @TableId(value = "user_id",type = IdType.AUTO)
    private Long userId;
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;
    /**
     * 用户类型（00系统用户 01注册用户）
     */
    @ApiModelProperty(value = "用户类型（0系统用户 1注册用户）")
    private String userType;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String userEmail;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
    /**
     * 性别 （0男 1女 2未知）
     */
    @ApiModelProperty(value = "性别 （0男 1女 2未知）")
    private String userSex;
    /**
     * 头像路径
     */
    @ApiModelProperty(value = "头像路径")
    private String avatar;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 账号状态（0正常 1停用）
     */
    @ApiModelProperty(value = "账号状态")
    private String userStatus;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;
    /**
     * 最后登录ip
     */
    @ApiModelProperty(value = "最后登录ip")
    private String loginIp;
    /**
     * 最后登录时间
     */
    @ApiModelProperty(value = "最后登录时间")
    private Date loginDate;
    /**
     *
     * 附加属性
     */
    @TableField(exist = false)
    private List<LyRole> lyRoles;
}
