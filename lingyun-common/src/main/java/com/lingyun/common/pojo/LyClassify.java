package com.lingyun.common.pojo;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * (LyClassify)表实体类
 *
 * @author 没事别学JAVA
 * @since 2021-07-25 00:49:17
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyClassify implements Serializable {

    /**
     * 分类id
     */
    @TableId(value = "classify_id",type = IdType.AUTO)
    private Long classifyId;

    /**
     * 分类名字
     */
    @Field(type = FieldType.Text, store = true)
    private String classifyName;

    /**
     *创建时间
     */
    private Date createDate;
    /**
     *更新时间
     */
    private Date updateDate;




}
