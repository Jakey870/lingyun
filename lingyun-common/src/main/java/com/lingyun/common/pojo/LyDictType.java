package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * (LyDictType)表实体类
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:13:15
 */
@Data
@ToString
@TableName(value = "ly_dict_type")
public class LyDictType implements Serializable {
    /**
     *字典主键
     */
    @TableId(value = "dict_id",type = IdType.AUTO)
    private Long dictId;
    /**
     *字典名称
     */
    private String dictName;
    /**
     *字典类型
     */
    private String dictType;
    /**
     *状态（0正常 1停用）
     */
    private String status;
    /**
     *创建时间
     */
    private Date createTime;
    /**
     *更新时间
     */
    private Date updateTime;
    /**
     *备注
     */
    private String remark;

}
