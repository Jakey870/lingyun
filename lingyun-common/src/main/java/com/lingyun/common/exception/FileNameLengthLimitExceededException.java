package com.lingyun.common.exception;

/**
 * @author 24188
 */
public class FileNameLengthLimitExceededException extends RuntimeException{


    public FileNameLengthLimitExceededException(String msg) {
        super(msg);
    }

}
