package com.lingyun.common.exception.dict;

import com.lingyun.common.exception.LyException;

public class DictException extends LyException {

    private static final long serialVersionUID = 1L;

    public DictException(String message) {
        super("dict", message, null);
    }

}
