package com.lingyun.common.exception.role;

import com.lingyun.common.utils.MessageUtils;

public class RoleHaveUserCanNotDelException extends RoleException{

    private static final long serialVersionUID = 1L;

    public RoleHaveUserCanNotDelException() {
        super(MessageUtils.message("role.have.user.can.not.del"));
    }
}
