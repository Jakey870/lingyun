package com.lingyun.common.exception.user;

import com.lingyun.common.utils.MessageUtils;

/**
 * 用户密码不正确或不符合规范异常类
 *
 * @author 没事别学JAVA
 * 2021/12/25 1:28
 */
public class UserPasswordNotMatchException extends UserException{
    private static final long serialVersionUID = 1L;
    public UserPasswordNotMatchException(){
        super(MessageUtils.message("user.password.not.match"));
    }
}
