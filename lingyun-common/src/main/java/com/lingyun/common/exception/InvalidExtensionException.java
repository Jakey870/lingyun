package com.lingyun.common.exception;

public class InvalidExtensionException extends RuntimeException{

    public InvalidExtensionException(String msg) {
        super(msg);
    }
}
