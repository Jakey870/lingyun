package com.lingyun.common.exception.role;


import com.lingyun.common.exception.LyException;

public class RoleException extends LyException {

    public RoleException(String message) {
        super("role", message, null);
    }

}
