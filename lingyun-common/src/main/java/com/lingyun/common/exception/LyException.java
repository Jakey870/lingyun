package com.lingyun.common.exception;


/**
 *
 * extends Runtime(后续 异步的时候改为 这个)
 *
 * @author 没事别学JAVA
 */
public class LyException extends Exception{

    /**
     * 所属模块
     */
    private String module;

    /**
     *错误码
     * */
    private String errorCode;


    public String getModule() {
        return module;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public LyException(String module, String message, String errorCode) {
        super(message);
        this.module=module;
        this.errorCode=errorCode;
    }

    public  LyException(String message, String errorCode){
        super(message);
        this.errorCode=errorCode;
    }

    public  LyException(String message){
        super(message);
    }






}
