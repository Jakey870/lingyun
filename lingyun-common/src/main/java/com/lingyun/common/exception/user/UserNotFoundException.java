package com.lingyun.common.exception.user;

import com.lingyun.common.utils.MessageUtils;

/**
 * @author 没事别学JAVA
 * 2021/12/25 2:16
 */
public class UserNotFoundException extends UserException{

    private static final long serialVersionUID = 1L;

    public UserNotFoundException() {
        super(MessageUtils.message("user.not.exists"));
    }
}
