package com.lingyun.common.exception;

/**
 * @author 没事别学JAVA
 */
public class FileSizeLimitExceededException extends RuntimeException{

    public FileSizeLimitExceededException(String msg) {
        super(msg);
    }

}
