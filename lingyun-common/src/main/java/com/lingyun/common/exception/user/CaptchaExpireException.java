package com.lingyun.common.exception.user;

import com.lingyun.common.utils.MessageUtils;

/**
 * 验证码失效异常类
 * @author ruoyi
 */
public class CaptchaExpireException extends UserException {
    private static final long serialVersionUID = 1L;

    public CaptchaExpireException() {
        super(MessageUtils.message("user.jcaptcha.expire"));
    }
}
