package com.lingyun.common.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目配置
 *
 */
@Component
@ConfigurationProperties(prefix = "lingyun")
public class LingYunConfig {
    /**
     *
     * 项目名称
     */
    private String name;
    /**
     *
     * 图片上传路径
     */
    private static String imagePath;

    /**
     * 文件下载路径（文件下载到本机的路径）
     */
    private static String filePath;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        LingYunConfig.imagePath = imagePath;
    }

    public static String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        LingYunConfig.filePath = filePath;
    }

    
}
