package com.lingyun.common.vo;
import lombok.Data;
import lombok.ToString;
/**
 * 用户登录表单
 * @author 没事别学JAVA
 */
@Data
@ToString
public class LoginFrom {
    /**
     * 账号
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码
     */
    private String verificationCode;
    /**
     * redis 中的验证码的key
     *
     * */
    private String keyCode;
}


