package com.lingyun.common.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 *
 * 用户注册信息
 *
 * @author 没事别学JAVA
 */
@Data
@ToString
@ApiModel(value = "注册接受数据对象",description = "注册虚拟表")
public class UserRegister {

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 验证码
     * */
    @ApiModelProperty(value = "验证码")
    private String verificationCode;



}
