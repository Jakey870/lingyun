package com.lingyun.common.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 *
 * 用户管理搜索表单
 *
 * @author 没事别学JAVA
 * 2021/11/16 1:18
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserQueryCriteria implements Serializable{
    /**
     * 当前页
     */
    private Integer current;
    /**
     * 页大小
     */
    private Integer size;

    /**
     * 账号
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 最后登录ip
     */
    private String loginIp;

    /**
     * 角色id
     */
    private Long roleId;
}
