package com.lingyun.common.vo.es;

import lombok.Data;
import lombok.ToString;

/**
 * @author 没事别学JAVA
 *
 *
 */
@Data
@ToString
public class SearchParam {

    /**
     * 搜索词
     */
    private String keyword;
    /**
     * 当前页
     */
    private Integer current;
    /**
     * 页大小
     */
    private Integer size;

}
