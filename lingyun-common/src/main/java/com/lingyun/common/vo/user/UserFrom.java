package com.lingyun.common.vo.user;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.pojo.LyRole;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 添加  修改 用户表单
 * @author 没事别学JAVA
 * 2021/11/20 3:21
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserFrom implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 账号
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 用户类型（00系统用户 01注册用户）
     */
    private String userType;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 手机号码
     */
    private String phonenumber;
    /**
     * 性别 （0男 1女 2未知）
     */
    private String userSex;
    /**
     * 头像路径
     */
    private String avatar;
    /**
     * 密码
     */
    private String password;
    /**
     * 账号状态（0正常 1停用）
     */
    private String userStatus;
    /**
     * 角色id
     */
    private Long[] roleIds;

    /***
     * 附加属性
     */
    List<LyRole> lyRoles;
}
