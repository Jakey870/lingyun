package com.lingyun.common.vo.dict;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DictDataPage {


    /**
     * 当前页
     */
    private Integer current;
    /**
     * 页大小
     */
    private Integer size;


    /**
     *字典类型
     */
    private String dictType;
    /**
     *字典标签
     */
    private String dictLabel;
    /**
     *状态（0正常 1停用）
     */
    private String status;

    /**
     * 创建时间范围
     */
    private String[] dateTimeRangeCreate;

    /**
     * 更新时间范围
     */
    private String[] dateTimeRangeUpdate;


}
