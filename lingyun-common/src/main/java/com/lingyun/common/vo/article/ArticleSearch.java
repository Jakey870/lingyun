package com.lingyun.common.vo.article;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ArticleSearch implements Serializable{


    /**
     * 当前页
     */
    private Integer current;
    /**
     * 页大小
     */
    private Integer size;

    /**
     * 文章名称
     */
    private String articleName;
    /**
     * 文章状态
     */
    private String articleStatus;

    /**
     * 分类id
     */
    private Long classifyId;



}
