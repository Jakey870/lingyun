package com.lingyun.common.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 没事别学JAVA
 * 登录的用户信息
 * 2021/12/13 18:55
 */

@Data
@ToString
public class LoginUser implements UserDetails{
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户标识
     */
    private String token;
    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 用户所拥有的角色
     */
    private Set<String> roles;

    /**
     * 登录时间
     */
    private Long loginTime;
    /**
     * 过期时间
     */
    private Long expireTime;
    /**
     * 用户信息
     */
    private LyUser lyUser;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities=new ArrayList<>();
        for (String str:permissions){
            authorities.add(new SimpleGrantedAuthority("ROLE_"+str));
        }
        return authorities;
    }

    @JSONField(serialize = false)
    @Override
    public String getPassword() {
        return this.lyUser.getPassword();
    }

    @Override
    public String getUsername() {
        return this.lyUser.getUserName();
    }

    @JSONField(serialize = false)
    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @JSONField(serialize = false)
    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @JSONField(serialize = false)
    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @JSONField(serialize = false)
    @Override
    public boolean isEnabled() {
        return false;
    }


    public LoginUser(Long userId,  LyUser lyUser ,Set<String> permissions) {
        this.userId = userId;
        this.lyUser = lyUser;
        this.permissions = permissions;
    }

    public LoginUser(Long userId,  LyUser lyUser ,Set<String> permissions,Set<String> roles) {
        this.userId = userId;
        this.lyUser = lyUser;
        this.permissions = permissions;
        this.roles = roles;
    }

    public LoginUser() {
    }



}
