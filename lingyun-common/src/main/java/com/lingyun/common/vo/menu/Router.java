package com.lingyun.common.vo.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 左侧导航栏信息
 * @author 没事别学JAVA
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Router implements Serializable {
    /**
     *菜单名称
     */
    private String navName;

    /**
     *路由地址
     */
    private String path;

    /**
     * 是否显示，显示为true 时在左侧导航栏显示
     */
    private Boolean hidden;
    /**
     * 其他元素
     */
    private Meta meta;
    /**
     * 是否有子类
     */
    private Boolean child;
    /**
     *打开方式（tab页签 new新窗口）
     */
    private String target;
    /**
     * 子路由
     */
    private List<Router> children;
}
