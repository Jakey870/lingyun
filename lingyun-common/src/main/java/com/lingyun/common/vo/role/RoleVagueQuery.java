package com.lingyun.common.vo.role;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 角色管理搜索表单
 *
 * @author 没事别学JAVA
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RoleVagueQuery implements Serializable {
    /**
     * 当前页
     */
    private Integer current;
    /**
     * 页大小
     */
    private Integer size;

    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 权限字符
     */
    private String roleKey;

    /**
     * 状态
     */
    private String roleStatus;

}
