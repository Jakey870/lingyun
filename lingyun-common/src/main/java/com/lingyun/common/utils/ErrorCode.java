package com.lingyun.common.utils;


/**
 * @author 没事别学JAVA
 * */
public class ErrorCode {


    /**
     * 认证模块错误码-start
     */
    public final static String AUTH_UNKNOWN = "30000";
    /**用户已存在*/
    public final static String AUTH_USER_ALREADY_EXISTS = "30001";
    /**认证失败*/
    public final static String AUTH_AUTHENTICATION_FAILED = "30002";
    /** 前端注册信息为空 */
    public final static String AUTH_PARAMETER_ERROR = "30003";
    /**邮件注册，激活失败*/
    public final static String AUTH_ACTIVATE_FAILED = "30004";
    /**置换token失败*/
    public final static String AUTH_REPLACEMENT_FAILED = "30005";
    /**token无效*/
    public final static String AUTH_TOKEN_INVALID = "30006";
    /**非法的用户名*/
    public static final String AUTH_ILLEGAL_USERCODE = "30007";
    /**token格式错误*/
    public final static String AUTH_TOKEN_GESHICUOWU = "30008";
    /**token保护期内*/
    public final static String AUTH_TOKEN_BAOHUQI = "30009";
    /** 邮箱验证码过期*/
    public final  static String EMAIL_VERIFICATION_CODE_INVALID="30010";
    /** 邮箱正则验证失败 */
    public final static String EMAIL_REGEX_VERIFICATION_FAILED = "30011";
    /** 用户不存在 */
    public final static String USER_DOES_NOT_EXIST = "30012";
    /** 用户不存在 */
    public final static String USER_PASSWORD_ERROR = "30013";

    /** 用户添加失败 */
    public final static String USER_ADD_ERROR = "30013";















}
