package com.lingyun.common.utils;

import java.util.HashMap;

public class EditorResult extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    public static final String ERRON_TAG = "erron";

    /**
     * 返回内容
     */
    public static final String MESSAGE_TAG = "message";

    /**
     * 数据对象
     */
    public static final String DATA_TAG = "data";

    /**
     * 状态类型
     */
    public enum Type {
        /**
         * 成功
         */
        SUCCESS(0),
        /**
         * 警告
         */
        WARN(401),
        /**
         * 错误
         */
        ERROR(500);

        private final int value;

        Type(int value) {
            this.value = value;
        }

        public int value() {
            return this.value;
        }
    }

    /**
     * 初始化一个新创建的 editorResult 对象，使其表示一个空消息。
     */
    public EditorResult() {

    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     * @param type 状态类型
     * @param msg  返回内容
     */
    public EditorResult(EditorResult.Type type, String msg) {
        super.put(ERRON_TAG, type.value);
        super.put(MESSAGE_TAG, msg);
    }


    /**
     * 初始化一个新创建的 AjaxResult 对象
     * @param type 状态类型
     * @param data 数据对象
     */
    public EditorResult(EditorResult.Type type, Object data) {
        super.put(ERRON_TAG, type.value);
        if (StringUtils.isNotNull(data)) {
            super.put(DATA_TAG, data);
        }
    }



    /**
     * 返回成功消息
     * @param data 数据对象
     * @return 成功消息
     */
    public static EditorResult success(Object data) {
        return new EditorResult(EditorResult.Type.SUCCESS, data);
    }

    /**
     * 返回错误消息
     * @param msg  返回内容

     * @return 警告消息
     */
    public static EditorResult error(String msg) {
        return new EditorResult(EditorResult.Type.ERROR, msg);
    }



}
