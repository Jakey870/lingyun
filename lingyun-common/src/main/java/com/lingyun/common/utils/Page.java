package com.lingyun.common.utils;

import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@ToString
public class Page<T> {

    /**
     * 当前页数
     */
    private Integer curPage;
    /**
     * 总记录数
     */
    private Integer total;
    /**
     * 每页条数
     */
    private Integer size;
    /**
     * 共多少页
     */
    private Integer pages;
    /**
     * 起始数据
     */
    private Integer beginPos;
    /**
     * 数据
     */
    private List<T> rows;

    public Page() {

    }

    public Page(Integer curPage, Integer size) {
        this.curPage = curPage;
        this.size = size;
    }

    public Page(Integer curPage, Integer size, Integer total, List<T> rows) {
        /**
         * 当前页面
         */
        this.curPage = curPage;
        /**
         * 每页多少条
         */
        this.size = size;
        this.beginPos = (curPage - 1) * size;
        /**
         * 总页数=总记录数total/pageSize（+1）
         */
        this.pages = (total + size - 1) / size;
        this.total = total;
        this.rows = rows;
    }


}
