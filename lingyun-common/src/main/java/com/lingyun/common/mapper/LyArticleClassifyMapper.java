package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyArticleClassify;

/**
 * 文章分类关系表(LyArticleClassify)
 *
 * @author 没事别学JAVA
 * @since 2022-06-12 21:45:57
 */
public interface LyArticleClassifyMapper extends BaseMapper<LyArticleClassify> {

    /**
     * 根据文章id删除关系
     *
     * @param articleId
     * @return
     */
    public Integer deleteByArticleId(Long articleId);




}