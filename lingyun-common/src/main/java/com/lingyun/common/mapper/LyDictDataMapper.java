package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyDictData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (LyDictData)
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:23:16
 */
public interface LyDictDataMapper extends BaseMapper<LyDictData> {

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    public List<LyDictData> queryDictDataByType(String dictType);


    /**
     * 更具字典类型 查询 字典数据列表
     * @param page 分页
     * @param queryWrapper 条件
     * @return 字典数据列表
     */
    public IPage<LyDictData> dictDataList(IPage<LyDictData> page, @Param("ew") Wrapper<LyDictData> queryWrapper);


    /**
     *根据字典数据编码查询字典数据
     * @param dictCode 字典编码
     * @return 字典数据信息
     */
    public LyDictData selectDictDataById(Long dictCode);


}
