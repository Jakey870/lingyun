package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyRoleMenu;

import java.util.List;

/**
 * @author 没事别学JAVA
 */
public interface LyRoleMenuMapper extends BaseMapper<LyRoleMenu> {

    public Integer deleteRoleNavByRoleId(Long RoleId);

    public Integer batchRoleNav(List<LyRoleMenu> lyRoleNavList);

}
