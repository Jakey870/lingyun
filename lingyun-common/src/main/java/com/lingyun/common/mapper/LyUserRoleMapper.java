package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyUserRole;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 用户和角色关联表(LyUserRole)表数据库访问层
 * @author 没事别学JAVA
 * @since 2021-03-29 22:20:22
 */

public interface LyUserRoleMapper extends BaseMapper<LyUserRole> {

    public Integer add(LyUserRole lyUserRole);

    public Integer addlist(List<LyUserRole> lyUserRoles);

    public Integer DeleteId(Long userId);

    public Integer selectRoleHaveHowmanyUser(Long roleId);


    /**
     *
     * 取消绑定（将用户与角色解除关联）
     * @param lyUserRole 角色id 和 用户id
     * @return 大于1删除成功
     */
    public Integer cancelBinding(LyUserRole lyUserRole);

    /**
     * 批量取消绑定
     * @param roleId 角色id
     * @param userIds 用户id
     * @return 大于1删除成功
     */
    public Integer batchCancelBinding(@Param("roleId") Long roleId, @Param("userIds") Long[] userIds);


    /**
     * 绑定用户
     * @param userRoles 角色id 与 用户id
     * @return 大于1添加成功
     */
    public Integer bindingUser(List<LyUserRole> userRoles);

    /**
     * 判断 角色 与 用户 的关系是否有重复
     * @param userRole 角色id 与 用户id
     * @return   0为没有 ， 大于0就是有重复的
     */
    public Integer queryBindingIsNoRepeat(LyUserRole userRole);



}
