package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.common.vo.user.UserQueryCriteria;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author 没事别学JAVA
 * @since 2021-03-07 17:06:47
 */
public interface LyUserMapper extends BaseMapper<LyUser> {
    /**
     * Map 查询
     *
     * @return list
     */
    public List<LyUser> getLyUserListByMap(Map<String, Object> params);


    /**
     * 选择性插入
     */
    public Integer addUser(LyUser lyUser);

    /**
     * 选择性查询
     *
     * @return lyUser
     */
    public LyUser getLyUser(Map<String, Object> params);

    /***
     *
     * 用户登录信息查询，表联查
     *
     * */
    public LyUser listLyUser(Map<String, Object> params);

    /**
     * 用户分页查询
     */
    public IPage<UserAll> getUserList(IPage<UserAll> page, @Param("ew") Wrapper<UserAll> queryWrapper);
    /**
     *
     *用户根据id查询
     *
     */
    public UserFrom selectUserById(Long userId);

    /**
     * 更具id修改
     * @param lyUser
     * @return
     */
    public Integer updateUser(LyUser lyUser);


    /**
     * 导出用户Excel信息-也可添加条件
     */
    public List<UserAll> userExportExcel(@Param("ew") Wrapper<UserAll> queryWrapper);

    /**
     *
     * @param page 分页
     * @param queryWrapper 条件
     * @return 授权用户列表
     */
    public IPage<UserAll> authorizationUserList(IPage<UserAll> page, @Param("ew") Wrapper<UserAll> queryWrapper);


}
