package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyDictType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (LyDictType)
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:23:17
 */
public interface LyDictTypeMapper extends BaseMapper<LyDictType> {

    /**
     * 字典类型列表
     * @param page 分页
     * @param queryWrapper 查询条件
     * @return 分页列表数据
     */
    public IPage<LyDictType> dictList(IPage<LyDictType> page, @Param("ew") Wrapper<LyDictType> queryWrapper);



    /**
     * 更具id 查询字典类型
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    public LyDictType selectDictById(Long dictId);


    /**
     * 查询所有字典类型
     * @return  字典类型列表
     */
    public List<LyDictType> selectDictTypeAll();

}
