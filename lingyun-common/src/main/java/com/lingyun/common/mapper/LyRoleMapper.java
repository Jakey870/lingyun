package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色信息表(LyRole)表数据库访问层
 *
 * @author 没事别学JAVA
 * @since 2021-11-24 00:15:47
 */
public interface LyRoleMapper extends BaseMapper<LyRole> {


    /**
     * 根据用户id 获取当前用户的角色列表
     * @param userid 用户id
     * @return
     */
    public List<Long> getUserRole(Long userid);

    /***
     * 获取所有角色
     * @return
     */
    public IPage<LyRole> roleList(IPage<LyRole> page, @Param("ew") Wrapper<LyRole> queryWrapper);

    /**
     * 根据id获取校色信息
     *
     */
    public LyRole selectRoleById(Long roleId);

    /**
     *导出角色Excel信息-也可添加条件
     *
     */
    public List<LyRole> roleExportExcel(@Param("ew") Wrapper<LyRole> queryWrapper);


    /**
     * 根据角色id 查询 用户
     * @param page 分页
     * @param queryWrapper  条件
     * @return 用户集合
     */
    public IPage<LyUser> selectRoleUser(IPage<LyUser> page, @Param("ew") Wrapper<LyUser> queryWrapper);



}
