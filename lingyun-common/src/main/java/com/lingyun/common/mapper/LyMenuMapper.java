package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyMenu;

import java.util.List;

/**
 * (LyLeftNav)表数据库访问层
 *
 * @author 没事别学JAVA
 * @since 2021-11-10 16:22:28
 */
public interface LyMenuMapper extends BaseMapper<LyMenu> {


    /**
     * 获取全部 菜单 目录（左侧导航栏）
     * @return  菜单列表
     */
    public List<LyMenu> menuList();

    /**
     * 更具角色id 查询 角色拥有的菜单
     * @return  当菜单列表
     */
    public List<LyMenu> queryMentuByRoleId(Long userId);

    /**
     * 获取全部 菜单 目录 按钮功能（树状图）
     * @return  全部 菜单 目录 按钮功能（树状图）
     */
    public List<LyMenu> roleMenuTress();


    /**
     * 更具角色id 获取当前角色所拥有的 菜单 目录 按钮功能 （树状图）
     * @return  当前角色所拥有的 菜单 目录 按钮功能 （树状图）
     */
    public List<LyMenu> roleNavTressById(Long userId);



    /**
     * 更具用户ID获取权限
     * @param userId 用户id
     * @return 当前用户 校色的权限
     */
    public List<String> selectUserPermsByUserId(Long userId);

    /**
     * 为 admin 角色获取全部权限
     * @return 返回全部权限
     */
    public List<String> selectAdminUserPermsAll();

    /**
     * 更具校色id 获取角色导航 id 列表
     * @param roleId
     * @return
     */
    public List<Long> selectMenuByRoleId(Long roleId);

    /**
     * 获取全部菜单id
     * @return 菜单id集合
     */
    public List<Long> getMenuIdAll();

    /**
     * 根据父id获取 子菜单 和 子按钮
     *
     * @param parentId  父id
     * @return 集合
     */
    public List<Long> selectParentId(Long parentId);


}
