package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyArticle;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (LyArticle)表数据库访问层
 *
 * @author 没事别学JAVA
 * @since 2021-04-29 12:45:34
 */

public interface LyArticleMapper extends BaseMapper<LyArticle> {

    public IPage<LyArticle> articleList(IPage<LyArticle> page, @Param("ew") Wrapper<LyArticle> queryWrapper);


    public List<LyArticle> resetList();

    public LyArticle getArticleById(Long articleId);

}
