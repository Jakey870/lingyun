package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.JobAndTrigger;
import org.apache.ibatis.annotations.Param;

/**
 * @author 没事别学JAVA
 * 2021/4/27 18:07
 */
public interface JobMapper {


    /**
     * 查询定时作业和触发器列表
     *
     * @return 定时作业和触发器列表
     */
    public IPage<JobAndTrigger> quartzlist(IPage<JobAndTrigger> page, @Param("ew") Wrapper<JobAndTrigger> queryWrapper);


}
